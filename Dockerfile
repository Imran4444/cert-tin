### STAGE 1: Build ###
FROM node:carbon
WORKDIR /usr/src/app
COPY package*.json ./
RUN rm -rf node_module
RUN npm install
COPY . .
RUN npm install typescript@'>=2.1.0 <2.4.0'
RUN npm run build

# Stage 2
FROM nginx:1.17.1-alpine
COPY --from=build-step /app/docs /usr/share/nginx/html