import { Component, ViewChild, ElementRef, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { MainPopupComponent } from '../mainPopup/mainPopup.component';

@Component({
  selector: 'app-home',
  templateUrl: '../home/home.component.html',
  styleUrls: ['../home/home.component.css']
})

export class HomeComponent implements OnInit {
  @ViewChild('planss') public viewPlan: ElementRef;
  @ViewChild('usecases') public viewFeature: ElementRef;
  showMenu: boolean;
  showPopup: any;

  constructor(public router: Router, public dialog: MdDialog) {
    console.log('Added the home componenet');
    // if (localStorage.getItem('popup')) {
    //   this.showPopup = localStorage.getItem('popup');
    // } else {
    //   this.showPopup = 'show';
    // }
  }

  ngOnInit() {
    this.showMenu = false;
    // if (this.showPopup === 'show') {
    //   this.openPopup();
    // }
  }

  openPopup() {
    const dialogRef = this.dialog.open(MainPopupComponent);
    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
      localStorage.setItem('popup', 'hide');
    });
  }

  gotoPlans() {
    //  var element = document.getElementById("planss");
    // element.scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' });
    this.viewPlan.nativeElement.scrollIntoView({behavior: 'smooth', block: 'end', inline: 'start'});
  }

  gotoFeatures() {
    this.viewFeature.nativeElement.scrollIntoView({behavior: 'smooth', block: 'end', inline: 'start'});
  }

  gotoSignIn() {
    this.router.navigate(['/extra/accountLogin']);
  }

  gotoRegister() {
    this.router.navigate(['/extra/sign-up']);
  }

}

