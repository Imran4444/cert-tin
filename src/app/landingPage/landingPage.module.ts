import { NgModule } from '@angular/core';
import { LandingPagesRoutingModule } from './landingPage.routing';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { LandingPageComponent } from './landingPage.component';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from '@angular/material';
import { MainPopupComponent } from './mainPopup/mainPopup.component';
import { PlansComponent } from './plans/plans.component';
import { FaqComponent } from './faq/faq.component';
import { ContactComponent, MailSent } from './contact/contact.component';

@NgModule({
  imports: [LandingPagesRoutingModule, CommonModule, FormsModule, MaterialModule, ReactiveFormsModule],
  declarations: [LandingPageComponent, HomeComponent, MainPopupComponent, PlansComponent, FaqComponent, ContactComponent, MailSent],
  providers: [],
  entryComponents: [MainPopupComponent, MailSent]
})

export class LandingPageModule {
}
