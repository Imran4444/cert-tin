import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
@Component({
  selector: 'mainpopup-component',
  templateUrl : './mainPopup.component.html',
  styleUrls: ['./mainPopup.component.css']
})
export class MainPopupComponent {
  constructor(public dialogRef: MdDialogRef<MainPopupComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) { }
  close() {
    this.dialogRef.close();
  }
}
