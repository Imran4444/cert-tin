import { Routes, RouterModule } from '@angular/router';
import { LandingPageComponent } from './landingPage.component';
import { HomeComponent } from './home/home.component';
import { PlansComponent } from './plans/plans.component';
import {FaqComponent} from "./faq/faq.component";
import { ContactComponent } from './contact/contact.component';

const LandingPagesRoutes: Routes = [
  {
    path: 'landingPage',
    component: LandingPageComponent,
    children: [
      {path: '', redirectTo: '/landingPage/home', pathMatch: 'full'},
      {path: 'home', component: HomeComponent},
      {path: 'plans', component: PlansComponent},
      {path: 'faqs', component: FaqComponent},
      {path: 'contact', component : ContactComponent }
    ]
  }
];

export const LandingPagesRoutingModule = RouterModule.forChild(LandingPagesRoutes);
