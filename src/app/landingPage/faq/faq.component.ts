import { Component } from "@angular/core";
import { Router } from '@angular/router';

@Component({
  selector: 'faqs-component',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent {

  constructor(public router: Router) {
  }

  gotoRegister() {
    this.router.navigate(['/extra/sign-up']);
  }

  gotoSignIn() {
    this.router.navigate(['/extra/accountLogin']);
  }
}
