import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-plans',
  templateUrl: './plans.component.html',
  styleUrls: ['./plans.component.scss']
})
export class PlansComponent implements OnInit {

  constructor(public router: Router) {
  }

  ngOnInit() {
  }

  gotoContact() {
    this.router.navigate(['/landingPage/contact']);
  }

  gotoRegister() {
    this.router.navigate(['/extra/sign-up']);
  }

  gotoSignIn() {
    this.router.navigate(['/extra/accountLogin']);
  }
}
