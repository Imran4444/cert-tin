import { BodypartsModel } from './bodyparts.model';

export class ContactModal {
  constructor( public bodyparts: BodypartsModel, public mailRecieversName: any, public mailSendersName: any, public message: any, public subject: any,  public toAddress: any) {
  }
}
