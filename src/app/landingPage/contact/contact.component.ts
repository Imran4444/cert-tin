
import { Component, Inject, OnInit } from '@angular/core';
import { ContactModal } from './contact.modal';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiCalls } from '../../services/services';
import { BodypartsModel } from './bodyparts.model';
import { ValidationSuccessComponent } from '../../Inbox/verifyprocess/verifyProcess.component';
import { MD_DIALOG_DATA, MdDialog, MdDialogRef } from '@angular/material';
import { ValidationProvider } from '../../providers/validationProvider';
import {KlaviyoModal} from "../../Modals/klaviyoModal";
import {KlaviyoContactFormModel} from "../../Modals/klaviyoContactFormModel";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})

export class ContactComponent implements OnInit {
  public contactModel: any;
  contactForm: FormGroup;
  submitted = false;
  public bodyObj: BodypartsModel;
  showLoader: boolean;
  klaviyodata: any;
  eventProperties: any;
  constructor(public formBuilder: FormBuilder, public router: Router, public api: ApiCalls, public dialog: MdDialog, public  verify: ValidationProvider,
  public apiCall: ApiCalls) {
    console.log('contact page');

    this.bodyObj = new BodypartsModel('', '', '', '', 'contactUs_template.ftl');
    this.contactModel = new ContactModal(this.bodyObj, '', '', '', '', '');
  }

  ngOnInit() {
    this.showLoader = false;
    console.log(this.contactModel);
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      contact: ['', Validators.required],
      organization: [''],
      subject: ['', Validators.required],
      message: ['', Validators.required]
    });
  }

  get f() {
    return this.contactForm.controls;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  gotoRegister() {
    this.router.navigate(['/extra/sign-up']);
  }

  gotoSignIn() {
    this.router.navigate(['/extra/accountLogin']);
  }

  onSubmit() {
    this.submitted = true;
    this.showLoader = true;
    // stop here if form is invalid
    if (this.contactForm.invalid) {
      this.showLoader = false;

      return;
    } else {
      console.log(this.contactForm.controls.name.value);
      this.bodyObj = new BodypartsModel(this.contactForm.controls.email.value, this.contactForm.controls.contact.value, this.contactForm.controls.organization.value, this.contactForm.controls.subject.value, 'contactUs_template.ftl');
      this.contactModel = new ContactModal(this.bodyObj, 'fliptin', this.contactForm.controls.name.value, this.contactForm.controls.message.value, 'Cert Tin Contact Us', 'connect@fliptin.com');
      console.log(JSON.stringify(this.contactModel));
      this.api.doSendMail(JSON.stringify(this.contactModel)).then((res) => {
        this.showLoader = false;
        console.log(res);
        this.eventProperties = new KlaviyoContactFormModel(this.contactForm.controls.email.value, this.contactForm.controls.contact.value, this.contactForm.controls.message.value,
      this.contactForm.controls.name.value, this.contactForm.controls.organization.value, this.contactForm.controls.subject.value);
        const emailID = {
          email: this.contactForm.controls.email.value
        };
        console.log(JSON.stringify(this.eventProperties));
        this.klaviyodata = new KlaviyoModal('Juv2YU', 'Certtin_contactUs', emailID, this.eventProperties)
        this.klaviyodata = (JSON.stringify(this.klaviyodata));
        this.klaviyodata = btoa(this.klaviyodata);
        console.log(this.klaviyodata);
        this.apiCall.klaviyoProfile(this.klaviyodata).then((result) => {
          console.log(result);
          this.createZoho();
        }, (error) => {
          console.log('' + JSON.stringify(error));
        });
        let resultdata = this.verify.setMailSuccess();
        this.openDialog(resultdata);
      }, (err) => {
        console.log(err);
      });
    }

  }
  createZoho() {
    const zohoServer = 'http://fliptin.com';
    const leadOwner = "<Leads> <row no='1'> <FL val='Lead Owner'>" + this.contactForm.controls.name.value + "</FL> </row> </Leads>"
    console.log(leadOwner);
    console.log(zohoServer + '/v1RestApi/api/ZohoCRM/?xmldata=' + leadOwner + '&zohoparam=Leads&filepath=none.png');
    this.apiCall.createZohoLead(leadOwner).then((res) => {
      console.log(res);
    }, (err) => {
      console.log(err);
    });
  }

  openDialog(resultdata: any): void {
    let dialogRef = this.dialog.open(MailSent, {
      width: '450px',
      data: resultdata
    });
  }
}
@Component({
  selector: 'mailSent-success',
  templateUrl: 'mailSentSuccess.html',
})
export class MailSent {
  constructor(public dialogRef: MdDialogRef<ValidationSuccessComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              public router: Router,) {
    console.log(this.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
  }




}
