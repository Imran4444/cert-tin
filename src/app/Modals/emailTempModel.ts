import { EmailBodyPartsModel } from './emailBdyParts';
export class EmailTempModel {
  constructor(public bodyParts: EmailBodyPartsModel,
              public mailRecieversName: any,
              public mailSendersName: any,
              public message: any,
              public subject: any,
              public toAddress: any) { }
}
