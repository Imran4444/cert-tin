export class AuditModal {
    constructor(
        public orgname : any,
        public clientIp: any,
        public lang: any,
        public lat: any,
        public timeZoneId: any,
        public userId: any,
        public userName: any,
        public verb: any,
        public web: any,
        // public appName: any,
        // public browserType: any,
        // public clientIp: any,
        // public location: any,
        // public orgname: any,
        // public requestTime: any,
        // public sourceIp: any,
        
        ) {
    }
}