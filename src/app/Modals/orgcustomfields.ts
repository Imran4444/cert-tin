export class Orgcustomfields {
  constructor(public title: string,
              public firstName: string,
              public lastName: string,
              public role: any,
              public area: any,
              public phoneNumber: any,
              public emails: any,
              public countryCode: any,
              public customerId: any,
              public subscriptionId: any,
              public planName: any) {

  }
}
