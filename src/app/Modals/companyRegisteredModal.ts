export class CompanyRegisteredModal {
  constructor(public country: any,
              public compRegistrationNo: any,
              public name: any,
              public vatNumber: any,
              public registrationDate: any,
              public postCode: any,
              public region: any,
              public line1: any,
              public line2: any,
              public line3: any) { }
}
