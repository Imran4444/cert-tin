/**
 * Created by Soulstice on 7/30/2018.
 */
export class CustomFields {
  constructor(public idNumber: string,
              public idType: string,
              public nationality: string,
              public expiryDate: string) {

  }
}
