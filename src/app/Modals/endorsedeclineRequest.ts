/**
 * Created by Soulstice on 9/5/2018.
 */
export class EndorseDeclineRequest {
  constructor(public auditFields: any,
              public claimId: any,
              public comment: any,
              public level: any,
              public claimStatus: any,
              public issuedPersonId: any) {

  }
}
