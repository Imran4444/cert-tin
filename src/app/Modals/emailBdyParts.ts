export class EmailBodyPartsModel {
  constructor(public emailTemplateName: any,
              public orgUpgrade: any,
              public approved: any,
              public orgname: any,
              public reason: any,
              public firstName: any,
              public lastName: any,
              public certtinRoot: any) { }
}
