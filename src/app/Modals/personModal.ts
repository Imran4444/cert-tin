export class PersonModal {
  constructor(public title: any,
              public firstName: any,
              public lastName: any,
              public dateOfBirth: any,
              public email: any,
              public countryCode: any,
              public phoneNumber: any
  ) {

  }
}
