/**
 * Created by Soulstice on 19/12/2018.
 */
export class JobRequestModal {
    constructor(
      public aboutJob: string,
      public cimOrgId: string,
      public experience: string,
      public jobExpiryDate: string,
      public jobTitle: string,
      public jobType: any,
      public jobUrl: string,
      public orgLocation: string,
      public orgLogo: string,
      public orgName: string,
      public skills: string,
      public jobPostedBy: string) {
  
    }
  }