/**
 * Created by Soulstice on 7/26/2018.
 */
export class Company {
  constructor(public id: string,
              public organisationId: string,
              public registrationNumber: string,
              public taxNumber: string,
              public industry: string,
              public businessStartDate: string) {

  }
}
