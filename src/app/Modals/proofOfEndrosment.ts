export class ProofOfEndrosment {
  constructor(public firstName: any,
              public lastName: any,
              public studentID: any,
              public organisation: any,
              public fullTime: any,
              public courseName: any,
              public specialisation: any,
              public startDate: any,
              public endDate: any,
              public bulkUploadFileUrl: any) {

  }
}
