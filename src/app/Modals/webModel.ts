export class WebModal {
    constructor(
        public browser: any,
        public browserVersion: any,
        public clientIP: any,
        public os: any,
        public osVersion: any,
        public userAgent: any,
      ) {
    }
}