export class ContactpersonModal {
  constructor(public active: any,
              public emails: any,
              public firstName: any,
              public lastName: any,
              public gender: any,
              public address: any,
              public organisations: any,
              public phones: any,
              public title: any) {

  }
}
