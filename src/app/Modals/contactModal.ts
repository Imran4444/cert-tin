export class ContactModal {
  constructor(public title: any,
              public firstName: any,
              public lastName: any,
              public role: any,
              public countryCode: any,
              public area: any,
              public phoneNumber: any,
              public emails: any) {}
}
