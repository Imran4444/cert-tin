export class HDWallet {
  constructor(public numWallets: any,
              public walletsPerDialog: any,
              public wallets: any,
              public id: any,
              public hdk: any,
              public dPath: any,
              public defaultDPath: any,
              public alternativeDPath: any,
              public customDPath: any,
              public ledgerPath: any,
              public ledgerClassicPath: any,
              public  trezorTestnetPath: any,
              public trezorClassicPath: any,
              public trezorPath: any
              ) {

  }
}
