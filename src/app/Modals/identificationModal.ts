export class IdentificationModal {
  constructor(public idNumber: any,
              public idProofType: any,
              public nationality: any,
              public expiryDate: any,
              public issuedDate: any) {
  }
}
