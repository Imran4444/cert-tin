/**
 * Created by Soulstice on 7/30/2018.
 */
export class LoginRequest{
  constructor(public clientId: string,
              public clientSecret: string,
              public grant_type: string,
              public password: string,
              public username: string) {

  }
}
