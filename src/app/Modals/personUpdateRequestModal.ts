/**
 * Created by Soulstice on 7/26/2018.
 */
export class PersonUpdateRequest {
  constructor(public auditFields: any,
              public id: string,
              public title: string,
              public active: boolean,
              public pictureUri: string,
              public firstName: string,
              public lastName: string,
              public dateOfBirth: string,
              public address: any[],
              public idify: any,
              public emails: any[],
              public phones: any[],
              public organisations: any[],
              public upgradeAccount: any,

) {

}
}
