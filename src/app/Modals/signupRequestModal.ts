/**
 * Created by Soulstice on 7/26/2018.
 */
export class SignUpRequest {
  constructor(public  active: boolean,
              public pictureUrl: string,
              public firstName: string,
              public lastName: string,
              public address: any[],
              public emails: any[],
              public phones: any[],
              public appInstanceId: string,
              public orgActive: boolean,
              public orgName: string,
              public orgType: string,
              public orgWebsite: string,
              public parentOrgId: string,
              public passwordPolicyId: string,
              public userRoles: any[],
              public username: string,
              public customFields: any,
              public signUpType: any,
              public linkedInProfileId: any) {

  }
}

