export class KlaviyoContactFormModel {
  constructor (public email: any,
               public mobile: any,
               public message: any,
               public name: any,
               public organization: any,
               public title: any) { }
}
