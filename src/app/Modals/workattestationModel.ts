/**
 * Created by Soulstice on 9/29/2018.
 */
export class CertWorkAttestationModel {
  constructor(public certificateName: any,
              public description: any,
              public periodFromDate: any,
              public periodEndDate: any) {

  }
}
