export class VerifyModel {
  constructor(public recpID: string, public issuerID: string, public blockChainID: string, public hash: string, public from: string, public to: string) {
  }
}
