export class ProofInternModal {
  constructor(public role: any,
              public currentlyEmployed: any,
              public startDate: any,
              public endDate: any,
              public location: any,
              public recommendationLevel: any,
              public firstName: any,
              public lastName: any,
              public organisation: any,
              public issuerFirstname: any,
              public issuerLastname: any,
              public gender: any,
              public bulkUploadFileUrl: any) {

  }
}
