export class TransactionModalRequest {
    constructor(
        public page: any,
        public size: any,
        public type: any,
        public action: any,
        public sortField: any,
        public sort: any,
    ) { }
}