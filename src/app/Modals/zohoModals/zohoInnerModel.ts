export class ZohoInnerModel {
  constructor(public Lead_Owner: string,
              public First_Name: string,
              public Project_Name: string,
              public NDA: string,
              public Company: string,
              public  Last_Name: string,
              public Email: string,
              public City: string,
              public Description: string) {

  }
}
