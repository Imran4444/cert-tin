export class ZohoMainModel {
  constructor(public data: Array<{
                Lead_Owner: string,
                First_Name: string,
                Project_Name: string,
                NDA: string,
                Company: string,
                Last_Name: string,
                Email: string,
                City: string,
                Description: string
              }>,
              public triggger: any) {
  }
}
