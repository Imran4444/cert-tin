export class QualificationCertificateModal {
  constructor(public courseName: any,
              public specialisation: any,
              public startDate: any,
              public endDate: any,
              public tranScript: any,
              public firstName: any,
              public lastName: any,
              public organisation: any,
              public bulkUploadFileUrl: any) {

  }
}

