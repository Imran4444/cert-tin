/**
 * Created by Soulstice on 8/2/2018.
 */
export class SubscriptionAddress {
  constructor(public city: any,
              public country: any,
              public line1: any,
              public line2: any,
              public postal_code: any,
              public state: any) {

  }
}
