/**
 * Created by Soulstice on 9/29/2018.
 */
export class CertQualicationModel {
  constructor(public courseName: any,
              public description: any,
              public specialization: any,
              public periodFromDate: any,
              public periodEndDate: any) {

  }
}
