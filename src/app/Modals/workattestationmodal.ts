export class WorkAttestationModal {
  constructor(public position : any,
              public currentlyEmployed: any,
              public startDate: any,
              public endDate: any,
              public salary: any,
              public employeeNumber: any,
              public gender: any,
              public firstName: any,
              public lastName: any,
              public organisation: any,
              public fulltime: any,
              public issuerCountryCode: any,
              public issuerMobile: any,
              public issuerEmail: any,
              public issuerFirstname: any,
              public issuerLastname: any,
              public bulkUploadFileUrl: any) {

  }
}
