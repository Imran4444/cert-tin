export class GetListModalRequest {
constructor(
    public page: any,
    public paginationRequired: any, 
    public searchFields: any,
    public size: any,
    public sortingField: any,
    public sortingOrder: any, 
    ){}
}