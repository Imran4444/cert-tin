export class OrgKeyListModalRequest {
    constructor(
        public paginationRequired: any,
        public searchFields: any = {},
    ) { }
}