/**
 * Created by Soulstice on 7/30/2018.
 */
export class InstantLoginRequest {
  constructor(public expiryDate: string,
              public loginApplicationInstances: any[],
              public organisation: string,
              public passwordPolicyId: string,
              public personId: string,
              public username: string) {

  }
}
