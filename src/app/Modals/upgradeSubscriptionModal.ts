/**
 * Created by Soulstice on 8/2/2018.
 */
export class UpgradeSubscription {
  constructor(public auditFields: any,
              public address: any,
              public customerId: string,
              public organisationId: string,
              public planId: string,
              public source: any,
              public subscriptionId: string,
              public planName: string) {

  }
}
