export class SignUpModal {
  constructor(public firstName: any,
              public lastName: any,
              public location: any,
              public country: any,
              public industry: any,
              public organisation: any,
              public organisationWebsite: any,
              public email: any,
              public countryCode: any,
              public mobileNumber: any,
              public linkedInProfileId: any
){}
}
