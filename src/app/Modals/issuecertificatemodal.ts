export class IssueCertificateModal {
  constructor(public attachments: any,
              public auditFields: any,
              public certificateName: any,
              public certificateType: any,
              public claimRequestId: any,
              public courseName: any,
              public description: any,
              public endDate: any,
              public individualCimPersonId: any,
              public individualPublickKey: any,
              public issuedCimPersonId: any,
              public issuerCimOrgId: any,
              public specialization: any,
              public startDate: any,
              public yearOfCompleted: any,
              public customTemplateUrl: any,
              public certificateHtml: any,
              public certificateText: any,
              public eventName: any,
              public role: any,
              public certification: any,
              public position: any,
              public knownYears: any,
              public contentTemplateId: any,
              public bulkUploadFileUrl: any) {

  }
}
