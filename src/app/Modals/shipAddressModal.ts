export class ShipAddressModal {
    constructor(
        public addressType:any,
        public companyName: any,
        public phoneNumber: any,
        public selectCountry: any,
        public streetAddress: any,
        public city: any,
        public selectState: any,
        public postCode: any) {

    }
}