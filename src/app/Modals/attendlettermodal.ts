export class AttendLetterModal {
  constructor(public event: any,
              public startDate: any,
              public endDate: any,
              public location: any,
              public firstName: any,
              public lastName: any,
              public organisation: any,
              public issuerFirstname: any,
              public issuerLastname: any,
              public bulkUploadFileUrl: any) {

  }
}
