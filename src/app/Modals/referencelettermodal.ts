export class ReferenceLettermodal {
  constructor(public yearsKnown: any,
              public position: any,
              public recommendationLevel: any,
              public gender: any,
              public firstName: any,
              public lastName: any,
              public organisation: any,
              public issuerCountryCode: any,
              public issuerMobile: any,
              public issuerEmail: any,
              public issuerFirstname: any,
              public issuerLastname: any,
              public bulkUploadFileUrl: any) {

  }
}
