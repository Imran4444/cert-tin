export class CertModal {
  constructor(public auditFields: any,
              public certificateId: any,
              public certificateName: any,
              public certificateStatus: any,
              public courseName: any,
              public description: any,
              public startDate: any,
              public endDate: any,
              public specialization: any,
              public level: any,
              public issuedPersonId: any,
              public customTemplateUrl: any ) {

  }
}
