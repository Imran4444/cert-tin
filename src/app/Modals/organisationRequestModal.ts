/**
 * Created by Soulstice on 7/26/2018.
 */
export class OrganisationRequest {
  constructor(public auditFields: any,
              public id: string,
              public type: string,
              public active: boolean,
              public name: string,
              public logoUrl: string,
              public website: string,
              public company: any[],
              public address: any[],
              public applications: any[],
              public passwordPolicyId: any,
              public appInstanceId: any,
              public upgradeAccount: any,
              public personId: any,
              public customFields: any) {

  }
}
