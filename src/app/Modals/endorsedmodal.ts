export class EndorsedModal {
  constructor(public auditFields: any,
              public claimStatus: any,
              public comment: any,
              public claimRequestId: any,
              public respondedPersonId: any) {

  }
}

