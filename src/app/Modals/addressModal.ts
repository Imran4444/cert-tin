/**
 * Created by Soulstice on 7/26/2018.
 */
export class AddressRequest {
  constructor(public line1: string,
    public line2: string,
    public line3: string,
    public city: string,
    public country: string,
    public postCode: string,
    public preferedAddress: boolean,
    public region: string,
    public type: string) {

  }
}
