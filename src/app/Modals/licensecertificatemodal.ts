export class LicenseCertificateModal {
  constructor(public certification: any,
              public issuedDate: any,
              public level: any,
              public expirationDate: any,
              public certificationNumber: any,
              public firstName: any,
              public lastName: any,
              public bulkUploadFileUrl: any) {

  }
}
