export class UpgradeProfessionalAccount {
    constructor(
        public devRootPath: any ,
        public appointedPersonName: any,
        public email: any,
        public organisationId: any,
        public instanceLoginId: any) {
    }
  }
