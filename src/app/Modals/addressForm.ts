export class AddressModal {
  constructor(public country: any,
              public region: any,
              public postCode: any,
              public line1: any,
              public line2: any,
              public line3: any  ) {
  }
}
