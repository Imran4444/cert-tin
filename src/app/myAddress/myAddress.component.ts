import { Component, OnInit } from '@angular/core';
import { HDWallet } from '../Modals/HDWallet';
import { SeedUnlock } from '../Modals/seedUnlock';
import * as Util from 'ethereumjs-util';
import * as bip39 from 'bip39';
import * as HDKey from 'hdkey';
import { WalletInfo } from '../Modals/walletInfo';
import { ApiCalls } from '../services/services';
import { API_KEY } from "../endpoint";
import { TransactionModalRequest } from '../Modals/transactionModal';
import * as moment from 'moment';
import { LayoutService } from '../layout/layout.service';
@Component({
  selector: 'my-address',
  styleUrls: ['./myAddress.component.scss'],
  templateUrl: './myAddress.component.html'
})
export class AddressDetailsComponent implements OnInit {
  public mm: any;
  hd: any;
  hdWallet: any;
  nprivateKey: any;
  privateKey: any;
  accountData: any;
  unlocked: any;
  walletDetails: any;
  qrCode: any;
  UqrCode: any;
  organizationId: any;
  address: any;
  orgPublicKey: any;
  enCripted: any;
  privateKey12: any;
  copyItem: any;
  copyEvent: any;
  inner: any;
  pageSize: any;
  totalpages: any;
  currentPage: any;
  firstItem: any;
  rowSelect: any;
  lastItem: any;
  pagIndexVal: any;
  checkedIdx: any;
  placeholdervalue: any;
  searchValue: any;
  searchKey: any;
  sortingOrder: any;
  sortingField: any;
  showLoader: any;
  balance: any;
  redirectRopsten: any;
  organisationResponse: any;
  transactionResponse: any;
  paginationRequired: any;
  searchFields: any;
  transactionList: any;
  totalPagesCount: any;
  typeSort: boolean;
  actionSort: boolean;
  sortClickValue: any;
  sortFieldValue: any;
  tHash: any;

  constructor(public service: ApiCalls, public layoutService: LayoutService) {

    this.mm = moment;
    this.organizationId = '12345';
    this.privateKey12 = 'bd57cbf1f73d619ecde572e2b7f7570d8f7619b725851c89295b076ffff54e88';
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.copyItem = `<html>
<head>
  <style>
    #qrcode {
      width:160px;
      height:160px;
      margin-top:15px;
    }
  </style>
  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="//cdn.rawgit.com/davidshimjs/qrcodejs/gh-pages/qrcode.min.js"></script>
  <script>
    var qrcode = new QRCode("qrcode");
    var key = 'dasdas';
    document.getElementById('text12').value = "dasdas";
    function makeCode () {
      var elText = document.getElementById("text");

      if (!elText.value) {
        alert("Input a text");
        elText.focus();
        return;
      }

      qrcode.makeCode(elText.value);
    }

    makeCode();

    $("#text").
    on("blur", function () {
      makeCode();
    }).
    on("keydown", function (e) {
      if (e.keyCode == 13) {
        makeCode();
      }
    });
  </script>
</head>
<body>
<input id="text12" type="text" value="orgPublicKey" style="width:80%" /><br />
<div id="qrcode"></div>
</body>
</html>` ;
  }
  ngOnInit() {
    this.paginationRequired = true;
    this.searchFields = '';
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.typeSort = false;
    this.actionSort = false;
    this.hd = new SeedUnlock(bip39, HDKey);
    console.log(this.hd);
    this.hdWallet = new HDWallet(0, 5, '', '', '', "m/44'/60'/0'/0", "m/44'/60'/0'/0", "m/44'/60'/0'", "m/44'/60'/1'/0", "m/44'/60'/0'", "m/44'/60'/160720'/0'", "m/44'/1'/0'/0", "m/44'/61'/0'/0", "m/44'/60'/0'/0");
    console.log(this.hdWallet);
    // this.usingPrivateKey('PrivateKey');
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log(this.organisationResponse);
    this.orgPublicKey = this.organisationResponse.orgPublicKey;
    localStorage.setItem('orgPublicKey', JSON.stringify(this.orgPublicKey));
    this.redirectRopsten = 'https://kovan.etherscan.io/address/' + this.orgPublicKey;
    if (this.orgPublicKey) {
      this.getBalance();
    }
    this.getTransactionList()
  }

  getBalance() {
    const requestedData = { address: this.orgPublicKey, apiKey: API_KEY };
    this.service.getBalancee(requestedData).then((value) => {
      this.balance = value;
      console.log('balance...'+this.balance)
    });
  }
  unEncripted() {
    this.enCripted = true;
  }




// /* To copy Text from Textbox */
//   copyInputMessage(preElement){
//     preElement.select();
//     document.execCommand('copy');
//     preElement.setSelectionRange(0, 0);
//   }

/* To copy any Text */
// copyText(val: string){
//   let selBox = document.createElement('textarea');
//     selBox.style.position = 'fixed';
//     selBox.style.left = '0';
//     selBox.style.top = '0';
//     selBox.style.opacity = '0';
//     selBox.value = val;
//     document.body.appendChild(selBox);
//     selBox.focus();
//     selBox.select();
//     document.execCommand('copy');
//     document.body.removeChild(selBox);
//   }



  copied(ev) {
    this.copyEvent = ev;
    console.log(ev);
    console.log(ev.content);
    this.inner = ev.content;
  }
  encripted() {
    this.enCripted = false;
  }
  /*Not working*/
  /*  clicktoCopy(element) {
      element.select();
      document.execCommand('copy');
      element.setSelectionRange(0, 0);
      this.copyItem = element;
    }*/




  /* Transaction List Request */
  getTransactionList() {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    let transactionListRequest = new TransactionModalRequest(this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder)
    console.log('Page Request ' + JSON.stringify(transactionListRequest));
    this.service.doGetTransactionList(this.orgPublicKey, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder).then((result) => {
        this.layoutService.updatePreloaderState('hide');
      if (result) {
        this.showLoader = false;
        this.transactionResponse = result;
        this.transactionList = this.transactionResponse.transactions;
        this.totalPagesCount = this.transactionResponse.totalPages;
        this.totalpages = this.transactionResponse.totalElements;
        console.log(result);
      }
    }
      , (error) => {
        this.showLoader = false;
        console.log('' + JSON.stringify(error));
      });
  }

  /* Records per Page Request */
  getChanged() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getTransactionList();
  }

  /* Previous Page Request */
  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getTransactionList();
    }
  }


  /* Index Select Request */
  selectRow(transaction, indexValue, ev) {
    this.tHash = transaction.txHash;
    console.log(this.tHash);
    this.pagIndexVal = indexValue;
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
    window.open('https://kovan.etherscan.io/tx/' + this.tHash);
  }

  /* Next Page Request */
  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getTransactionList();
    }
  }

  /* Search Request */
  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'type') {
      this.placeholdervalue = 'Type';
      this.searchKey = 'type';
    } else if (selectedKey === 'action') {
      this.placeholdervalue = 'Action';
      this.searchKey = 'action';
    } else {

    }
  }


  getSearchResult(searchValue) {
    if (this.searchKey) {
      this.searchValue = searchValue;
      this.getTransactionList();
    }
  }

  sortingList(sortingType) {
    this.sortingField = sortingType;
    // this.sortingOrder = sortingOrder;
    // console.log('sorting order...' + sortingOrder + 'sorting type...' + sortingType);
    if (this.sortingField === 'type') {
      this.typeSort = true;
      this.actionSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'action') {
      this.typeSort = false;
      this.actionSort = true;
      this.SortClicking();
    }
    else {
      this.typeSort = false;
      this.actionSort = false;
      this.SortClicking();
    }

  }
  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getTransactionList();
  }
}
