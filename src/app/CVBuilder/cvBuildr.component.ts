import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiCalls} from '../services/services';
import * as moment from 'moment';
import {CVCONTEXT, DEV_ROOT_PATH} from "../endpoint";
import {auditService} from "../providers/audit.service";

@Component({
  selector: 'app-cv-builder',
  templateUrl: './cvBuilder.component.html',
  styleUrls: ['./cvBuilder.component.css']
})
export class CvBuildrComponent implements OnInit {
  cvBuilderResp: any;
  public mm: any;
  workExperience: any;
  public qrCodeVal: any;
  workCertificates: any;
  workEducation: any;
  public rootPath: any;
  cvClaimsData: any;
  auditReq: any;

  constructor(public router: Router,
              public route: ActivatedRoute,
              public auditHistory: auditService,
              public apiCalls: ApiCalls) {
    this.mm = moment;
    this.rootPath = DEV_ROOT_PATH
    this.route.params.subscribe((params) => this.getCvBuilderDetails(params['cvId']));
    console.log('Hi this is cv builder view');
  }

  ngOnInit() {
    console.log('Hi this is cv builder view');
  }

  getCvBuilderDetails(cvId: any) {
    this.auditReq = this.auditHistory.userAudit('cvBuilder Details');
    console.log('audit details...' + JSON.stringify(this.auditReq))
    this.qrCodeVal = CVCONTEXT + cvId;
    let cvBuilderDetailsReq = {
      auditFields: this.auditReq,
      uuid: cvId
    }
    console.log('cvid...' + JSON.stringify(cvBuilderDetailsReq));
    this.apiCalls.doGetCvDEtails(cvBuilderDetailsReq).then((result) => {
      this.cvBuilderResp = result;
      this.cvClaimsData = this.cvBuilderResp.cv.cvClaims;
      this.workExperience = this.cvBuilderResp.cv.cvClaims.work;
      this.workEducation = this.cvBuilderResp.cv.cvClaims.course;
      this.workCertificates = this.cvBuilderResp.cv.certificates;
      console.log('cv builder claims...' + JSON.stringify(result));
    }, (error) => {
      console.log('cv builder error...' + JSON.stringify(error));
    });
  }

  login() {
    this.router.navigate(['/extra/accountLogin']);
  }

  register() {
    this.router.navigate(['/extra/sign-up']);
  }
}
