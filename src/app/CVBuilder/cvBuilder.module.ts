import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CvBuildrComponent } from './cvBuildr.component';
import { CvPagesRoutingModule } from './cvBuilder.routing';
import { NgxQRCodeModule } from 'ngx-qrcode3';
import { MaterialModule } from '@angular/material';


@NgModule({
  imports: [CvPagesRoutingModule,
    FormsModule,
    CommonModule,
    NgxQRCodeModule,
    MaterialModule
  ],
  declarations: [CvBuildrComponent]
})
export class CvBuilderModule {
  constructor() {
    console.log('Hi this is raviraj');
  }
}
