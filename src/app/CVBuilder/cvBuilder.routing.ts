import { RouterModule, Routes } from '@angular/router';
import { CvBuildrComponent } from './cvBuildr.component';

export const CvBuilderRoutes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: '/extra/accountLogin', pathMatch: 'full'},
      {path: 'view/:cvId', component: CvBuildrComponent}

    ]
  }
]
export const CvPagesRoutingModule = RouterModule.forChild(CvBuilderRoutes);
