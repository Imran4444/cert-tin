import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../services/services';
import { UpdateOrgTemplate } from '../Modals/updateOrgTemplate';
import { ChangePasswordComponent } from '../globalPopup/changePassword/changePassword.component';
import { MdDialog } from '@angular/material';
import { CertificateViewComponent } from '../globalPopup/vewCertificate/certificateView.component';
import * as moment from 'moment';
import { auditService } from '../providers/audit.service';
import { TemplateServices } from '../services/templateServices';

@Component({
  selector: 'app-templates',
  templateUrl: '../templates/templates.component.html',
  styleUrls: ['../templates/templates.component.css']
})
export class TemplatesComponent implements OnInit {
  public tabSelected: any;
  public personalTemplate: string;
  public loginResponse: any;
  public organizationDetails: any;
  public templates: any;
  public workTemplate: any;
  public skillsTemplate: any;
  public courseTemplates: any;
  public workAtteTemplates: any;
  public qualiTemplates: any;
  public updateTemplateObj: UpdateOrgTemplate;
  public mm: any
  auditReq: any;
  public selectedTab: any
  getCertificateDetailsResponse: any
  public licenseTemplate: any;
  public attendanceTemplate: any;
  public certificateHtml: any;

  constructor(public apiCalls: ApiCalls, public dialog: MdDialog,
              public auditHistory: auditService, public tempApi: TemplateServices) {
    this.mm = moment;
    this.tabSelected = 1;
    this.selectedTab = 1;
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.certificateHtml = '<p>To Whom It May Concern:</p><p>This letter is to verify that XYXY yZYZ is employed at XYXYyZYZ in the position of Work attestation posi.</p><p>XYXY yZYZ started working in this capacity as a full-time employee on DD/MM/YYY and is currently working in this position as per DD/MM/YYY. </p><p>XYXYXYX has an annual salary of YZYYZZ and his employee number is YZYYZZ.</p><p>If you require any additional information, please contact me at +XX XXXXXXXXor via email  XYXYXY.n@xvxvxv.com..</p>';
  }

  ngOnInit() {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    console.log(this.loginResponse);
    console.log(this.loginResponse.orgId)
    this.getOrganization(this.loginResponse.orgId);
    this.getTempApi();
  }

  getTempApi() {
    console.log('console.log');
    this.tempApi.getStaticJson().then((result) => {
      console.log(result);
      this.getCertificateDetailsResponse = result;
    }, (error) => {
      console.log(error);
    });
  }

  getOrganization(orgId) {
    console.log(orgId);
    this.apiCalls.doGetOrgtemplates(orgId).then((result) => {
      this.organizationDetails = result;
      console.log(JSON.stringify(this.organizationDetails));
      this.templates = this.organizationDetails.organisation.templates
      this.personalTemplate = this.templates.PERSONAL;
      this.workTemplate = this.templates.WORK;
      this.skillsTemplate = this.templates.SKILLS;
      this.licenseTemplate = this.templates.LICENSE_CERTIFICATE
      this.courseTemplates = this.templates.COURSE;
      this.workAtteTemplates = this.templates.WORK_ATTESTATION;
      this.attendanceTemplate = this.templates.ATTENDANCE_LETTER
      this.qualiTemplates = this.templates.QUALIFICATION_CERTIFICATE;
      console.log(this.organizationDetails.organisation.templates);

    }, (error) => {
      console.log('org details error..' + JSON.stringify(error));
    });
  }

  gototab(tabIndex: any) {
    this.tabSelected = +this.selectedTab;
  }

  tempSelected(selectedTemplate: any, tempType: any) {
    this.auditReq = this.auditHistory.userAudit('Update Org template');
    this.updateTemplateObj = new UpdateOrgTemplate(this.auditReq, this.organizationDetails.organisation.cimOrganisationId, selectedTemplate, tempType);
    console.log(this.updateTemplateObj);
    this.apiCalls.doUpdateTemplate(this.updateTemplateObj).then((res) => {
      console.log(res);
      this.organizationDetails = res
      this.templates = this.organizationDetails.organisation.templates
      localStorage.setItem('orgTemplate', JSON.stringify(this.templates))
      this.personalTemplate = this.templates.PERSONAL;
      this.workTemplate = this.templates.WORK;
      this.skillsTemplate = this.templates.SKILLS;
      this.licenseTemplate = this.templates.LICENSE_CERTIFICATE
      this.courseTemplates = this.templates.COURSE;
      this.attendanceTemplate = this.templates.ATTENDANCE_LETTER
      this.workAtteTemplates = this.templates.WORK_ATTESTATION;
      this.qualiTemplates = this.templates.QUALIFICATION_CERTIFICATE;
    });
  }

  vewCertificate(type: any, name: any, certType: any) {
    const obj = {
      tempType: type,
      orgLogo: this.organizationDetails.organisation.organisationLogoUrl,
      certificate: name,
      cert: certType
    }
    const dialogRef = this.dialog.open(CertificateViewComponent, {
      width: '800px',
      data: obj
    });
  }

}
