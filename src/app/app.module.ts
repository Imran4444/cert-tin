import { NgModule, ApplicationRef } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Layout
import { LayoutComponent } from './layout/layout.component';
import { PreloaderDirective } from './layout/preloader.directive';
// Header
import { AppHeaderComponent } from './layout/header/header.component';
// Sidenav
import { AppSidenavComponent } from './layout/sidenav/sidenav.component';
import { ToggleOffcanvasNavDirective } from './layout/sidenav/toggle-offcanvas-nav.directive';
import { AutoCloseMobileNavDirective } from './layout/sidenav/auto-close-mobile-nav.directive';
import { AppSidenavMenuComponent } from './layout/sidenav/sidenav-menu/sidenav-menu.component';
import { AccordionNavDirective } from './layout/sidenav/sidenav-menu/accordion-nav.directive';

import { AppendSubmenuIconDirective } from './layout/sidenav/sidenav-menu/append-submenu-icon.directive';
import { HighlightActiveItemsDirective } from './layout/sidenav/sidenav-menu/highlight-active-items.directive';
// Customizer
import { AppCustomizerComponent } from './layout/customizer/customizer.component';
import { ToggleQuickviewDirective } from './layout/customizer/toggle-quickview.directive';
// Footer
import { AppFooterComponent } from './layout/footer/footer.component';
// Search Overaly
import { AppSearchOverlayComponent } from './layout/search-overlay/search-overlay.component';
import { SearchOverlayDirective } from './layout/search-overlay/search-overlay.directive';
import { OpenSearchOverlaylDirective } from './layout/search-overlay/open-search-overlay.directive';

// Pages
import { DashboardComponent } from './dashboard/dashboard.component';
// Sub modules
import { LayoutModule } from './layout/layout.module';
// hmr

import { removeNgStyles, createNewHosts } from '@angularclass/hmr';
import { LoaderComponent } from './loader/loader.component';
import { DashboardServices } from './dashboard/dashboardServices';
import { Countriescodes } from './providers/countriescodes';
import { GlobalPopupModule } from './globalPopup/globalPopup.module';
import { ApiCalls } from './services/services';
import { IndustryTypes } from './providers/industrytypes';
import { CommonModule, DatePipe } from '@angular/common';
import { AdminSettingsComponent } from './admin-settings/admin-settings.component';
import { CookieService } from 'ngx-cookie-service';
import { MySharedService } from './services/mySharedService ';
import { AddUserComponent } from './usermanagement/add-user/add-user.component';
import { UserListComponent } from './usermanagement/user-list/user-list.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { AuditLogsComponent } from './audit-logs/audit-logs.component';
import { LoginService } from './extra-pages/login/loginService';
import { IndividualComponent } from './individual/individual.component';
import { ClaimDetailsComponent } from './individual/claimdetails/claimdetails.component';
import { auditService } from './providers/audit.service';
import { AddressDetailsComponent } from './myAddress/myAddress.component';
import { NgxQRCodeModule } from 'ngx-qrcode3';
import { InboxModule } from './Inbox/inbox.module';
import { TemplatesComponent } from './templates/templates.component';
import { DateFormatPipe } from './services/dateTimeFormate';

// import { ClaimsComponent } from './Inbox/claims/claims.component';
import { SharedModule } from './directives/shared.module';
import { SlimScrollDirective } from './layout/sidenav/sidenav-menu/slim-scroll.directive';
import { LandingPageModule } from './landingPage/landingPage.module';
import { CountryCodePopupComponent } from "./extra-pages/countryCodePopup/countryCode.component";
import { enableProdMode } from '@angular/core';
import { Ng2DeviceDetectorModule } from 'ng2-device-detector';
import { TemplateServices } from './services/templateServices';
import { BadgesModule } from './badges/badges.module';
import { JobAdvertisementComponent } from './job-advertisement/job-advertisement.component';
import { PostJobComponent } from './job-advertisement/post-job/post-job.component';
import { ViewJobComponent } from './job-advertisement/view-job/view-job.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';

enableProdMode();


@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    // Sub modules
    LayoutModule,
    GlobalPopupModule,
    NgxQRCodeModule,
    InboxModule,
    BadgesModule,
    SharedModule,
    LandingPageModule,
    ReactiveFormsModule,
    GooglePlaceModule,
    Ng2DeviceDetectorModule.forRoot()
  ],
  declarations: [
    AppComponent,
    // Layout
    LayoutComponent,
    PreloaderDirective,
    // Header
    AppHeaderComponent,
    // Sidenav
    AppSidenavComponent,
    ToggleOffcanvasNavDirective,
    AutoCloseMobileNavDirective,
    SlimScrollDirective,
    AppSidenavMenuComponent,
    AccordionNavDirective,
    AppendSubmenuIconDirective,
    HighlightActiveItemsDirective,
    // Customizer
    AppCustomizerComponent,
    ToggleQuickviewDirective,
    // Footer
    AppFooterComponent,
    // Search overlay
    AppSearchOverlayComponent,
    SearchOverlayDirective,
    OpenSearchOverlaylDirective,
    //
    DashboardComponent,
    LoaderComponent,
    AdminSettingsComponent,
    AddUserComponent,
    UserListComponent,
    TransactionsComponent,
    AuditLogsComponent,
    IndividualComponent,
    ClaimDetailsComponent,
    AddressDetailsComponent,
    TemplatesComponent,
    DateFormatPipe,
    CountryCodePopupComponent,
    JobAdvertisementComponent,
    PostJobComponent,
    ViewJobComponent,

  ],
  exports: [AppHeaderComponent],
  providers: [
    LoginService,
    DashboardServices,
    auditService,
    Countriescodes,
    ApiCalls,
    TemplateServices,
    IndustryTypes,
    DatePipe,
    CookieService,
    MySharedService,
  ],
  bootstrap: [AppComponent],
  entryComponents: [CountryCodePopupComponent]
})
export class AppModule {
  constructor(public appRef: ApplicationRef) {
  }

  hmrOnInit(store) {
    console.log('HMR store', store);
  }

  hmrOnDestroy(store) {
    const cmpLocation = this.appRef.components.map(
      (cmp) => cmp.location.nativeElement
    );
    // recreate elements
    store.disposeOldHosts = createNewHosts(cmpLocation);
    // remove styles
    removeNgStyles();
  }

  hmrAfterDestroy(store) {
    // display new elements
    store.disposeOldHosts();
    delete store.disposeOldHosts;
  }
}
