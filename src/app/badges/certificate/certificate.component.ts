import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiCalls } from '../../services/services';
import * as moment from 'moment';
import { auditService } from "../../providers/audit.service";
import {DEV_ROOT_PATH} from "../../endpoint";

@Component({
  selector: 'badge-certificate',
  templateUrl: '../certificate/certificate.component.html',
  styleUrls: ['../certificate/certificate.component.css']
})

export class CertificateComponent {
  public certificateDetails: any;
  mm: any;
  public footerInfo: string | any;
  public template: any;
  public borderBtm: string;
  public borderTop: string;
  public auditReq: any;
  public isLoaded: boolean;
  public page: number;
  public totalPages: number;
public publicUrlPath: any;
  constructor(public router: Router, public route: ActivatedRoute, public apiCalls: ApiCalls, public auditHistory: auditService,) {
    this.page = 1;
    this.mm = moment;
    this.route.params.subscribe((params) => this.getUserDetails(params['uuid']));
  }

  getUserDetails(uuid: any) {
    this.publicUrlPath = DEV_ROOT_PATH+'/#/badges/certificate/'+uuid;
    console.log('public url...'+this.publicUrlPath)
    this.auditReq = this.auditHistory.userAudit('Certificate Details');
    let certificateDetailsReq = {
      auditFields: this.auditReq,
      uuid: uuid
    }
    console.log('certificate details...' + JSON.stringify(certificateDetailsReq))
    this.apiCalls.getcertuuidDetails(certificateDetailsReq).then((result) => {
      console.log(result);
      this.certificateDetails = result;
      console.log('cert details...'+JSON.stringify(this.certificateDetails))
      if (this.certificateDetails.organisation) {
        this.footerInfo = this.certificateDetails.organisation
      }

      this.template = this.certificateDetails.certificate.template;
      if (this.template === 'WORKATTESTATION_DEFAULT' || this.template === 'QUALIFICATION_DEFAULT' || this.template === 'WORKATTESTATION_ORG' || this.template === 'QUALIFICATION_ORG') {
        this.borderTop = 'assets/images/certBorders/border-top-1.png';
        this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
      } else if (this.template === 'WORKATTESTATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      } else if (this.template === 'QUALIFICATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-3.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-3.png';
      } else if (this.template === 'LICENSE_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      }
      console.log('New Details' + JSON.stringify(result));
    });
  }

  register() {
    this.router.navigate(['/extra/sign-up']);

  }

  login() {
    this.router.navigate(['/extra/accountLogin']);
  }

  checkPdf(url: any) {
    console.log(url);
    if (url.includes('.pdf')) {
      return true
    } else {
      return false
    }

  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.isLoaded = true;
  }

  nextPage() {
    this.page++;
  }

  prevPage() {
    this.page--;
  }
}
