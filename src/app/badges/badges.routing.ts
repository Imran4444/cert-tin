import { RouterModule, Routes } from '@angular/router';
import { CertificateComponent } from './certificate/certificate.component';
import { ClaimComponent } from './claim/claim.component';

export const BadgePagesRoutes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: '/extra/accountLogin', pathMatch: 'full'},
      {path: 'certificate/:uuid', component: CertificateComponent},
      {path: 'claim/:uuid', component: ClaimComponent}

    ]
  }
]
export const BadgePagesRoutingModule = RouterModule.forChild(BadgePagesRoutes);
