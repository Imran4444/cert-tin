import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiCalls } from '../../services/services';
import * as moment from 'moment';

@Component({
  selector: 'badge-claim',
  templateUrl: '../claim/claim.component.html',
  styleUrls: ['../claim/claim.component.css']
})
export class ClaimComponent {
  public claimDetails: any;
  mm: any;
  constructor(public  router: Router, public route: ActivatedRoute, public apiCalls: ApiCalls) {
    this.mm = moment;
    this.route.params.subscribe((params) => this.getUserDetails(params['uuid']));
  }

  getUserDetails(uuid: any) {
    console.log(uuid);
    this.apiCalls.getclaimuuidDetails(uuid).then((result) => {
      console.log(result);
      this.claimDetails = result;
    });
  }

  register() {
    this.router.navigate(['/extra/sign-up']);

  }

  login() {
    this.router.navigate(['/extra/accountLogin']);
  }
}
