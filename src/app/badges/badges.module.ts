import { NgModule } from '@angular/core';
import { BadgePagesRoutingModule } from './badges.routing';
import { ClaimComponent } from './claim/claim.component';
import { CertificateComponent } from './certificate/certificate.component';
import { FormsModule } from '@angular/forms';
import { CvPagesRoutingModule } from '../CVBuilder/cvBuilder.routing';
import { NgxQRCodeModule } from 'ngx-qrcode3';
import { MaterialModule } from '@angular/material';
import { CommonModule } from '@angular/common';
import { InboxModule } from '../Inbox/inbox.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [BadgePagesRoutingModule, CommonModule,  FormsModule, MaterialModule, NgxQRCodeModule, InboxModule, PdfViewerModule],
  declarations: [ClaimComponent, CertificateComponent],
  providers: []

})

export class BadgesModule {

}
