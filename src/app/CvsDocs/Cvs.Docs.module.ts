import { NgModule } from '@angular/core';
import { CvsDocsComponent } from './CvsDocs.component';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  imports: [
    FormsModule,
    BrowserModule
  ],
  declarations: [CvsDocsComponent],
  entryComponents: [],
  providers: []
})

export class CvsDocsModule {
  constructor() {

  }
}
