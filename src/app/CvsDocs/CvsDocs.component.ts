import { Component } from '@angular/core';

@Component({
  selector: 'app-cvs-docs',
  templateUrl: '../CvsDocs/CvsDocs.component.html',
  styleUrls: ['../CvsDocs/CvsDocs.component.css']
})
export class CvsDocsComponent {
  public verified: boolean = false;

  constructor() {
  }

  verifiy() {
    this.verified = true;
  }

  upgrade() {
    console.log('upgrade')
  }
}

