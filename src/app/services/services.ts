///<reference path="../endpoint.ts"/>
import { Http, RequestOptions, Headers } from '@angular/http';
import {
  CHANGE_PASSWORD_URL,
  GETORGANISATION_DETAILS,
  CIM_OAUTH,
  GET_LOGINID_BY_EMAIL_URL,
  GET_PASSWORD_POLICY_URL,
  GET_PERSON_URL,
  ORGANISATION_CREATION_URL,
  PERSON_UPDATE_URL,
  RESEND_PASSWORD_URL,
  USER_LOGIN_URL,
  VERIFIED_LIST,
  USER_SIGNUP_URL,
  GET_STRIPE_URL,
  UPGRAGE_SUBSCRIPTION_URL,
  GET_AUDITLOGS_URL,
  GET_CLAIMS_BY_TYPE,
  GET_CLAIM_DETAILS,
  UPDATE_ENDORSE_DECLINE,
  GET_ORGANISATION_DETAILS,
  GETINDIVIDUALS_URL,
  INDIVIDUAL_TRACKERDETAILS,
  GET_PERSON_LIST,
  GET_CLAIMS_COUNT,
  GETORG_TEMPLATES,
  UPDATEORG_TEMPLATES,
  GET_TRANSACTIONS_LIST,
  GET_CERTIFICATES_BY_TYPE,
  GET_CERTIFICATE_DETAILS,
  CLAIM_UUID,
  CERTIFICATE_UUID,
  GET_UPDATE_CERT_STATUS,
  GET_SHARED_VERIFICATIONS_URL,
  GETCLAIMDETAILS_URL,
  GETCERTIFICATEDETAILS_URL,
  GET_BLOCKCHAIN_DETAILS, GET_DASH_BOARD_URL, GETLINKEDUSER_URL, CVBUILDER_CLAIMS, GET_BLOCKCHAIN_RECEIPT, CVVIEWCLAIMS,
  SEND_EMAIL,
  UPGRADE_PRO_ACCOUNT,
  GET_ORG_PROCESS,
  KYC_UPGRADE, ZOHOLEAD_CREATION, KYC_NEW_SIGNUP_URL, KLAVIYO_DATA, GET_ALL_INBOX_LIST, GET_NEWCLAIM_DETAILS,
  APPLYCERTIFICATE_URL, ISSUECERTIFICATE, GET_ISSUED_CERTIFICATES, GET_TRANSFERRED_CERTIFICATES, GET_ISSUEDCERT_DETAILS,
  GET_AUDITLOGSLIST_URL, SEARCH_INDIVIDUALS, REVOKE_CERT, GET_ORG_LIST, TRANSFER_CERTIFICATE, GET_ORG_KEY_LIST,
  UPLOAD_EXCEL, UPLOAD_SIGNUP_REQ,
  INANDOUTBOXCOUNT, SAVE_KYC, UPGRADE_KYC_PROFESSIONAL, UPGRADE_MOBILE_KYC, ZOHO_SERVICE_LEAD, ZOHO_SERVICE_URL, POST_JOB_SERVICE, GET_JOB_LIST
} from '../endpoint';
import { Injectable } from '@angular/core';
import { PasswordModel } from '../extra-pages/password/model/passwordModel';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import { GetListModalRequest } from '../Modals/getListModal';
import { HttpHeaders } from '@angular/common/http';
import { error } from 'util';
import { APP_INSTANCE_ID } from '../AppConstants';


@Injectable()
export class ApiCalls {
  public countryList: any;
  public inoutboxType: any;
  public lat: any;
  public lang: any;
  typeValue: string;
  actionValue: string;
  private url: string;

  constructor(public http: Http) {
  }

  getPersonsLocation(position: any) {
    this.lat = position.coords.latitude;
    this.lang = position.coords.longitude;
    localStorage.setItem('latitude', this.lat);
    localStorage.setItem('longitude', this.lang);
    console.log(position.coords.latitude, position.coords.longitude);
    return new Promise((resolve, reject) => {
      this.http.get('https://maps.googleapis.com/maps/api/geocode/json?latlng=' + position.coords.latitude + ',' + position.coords.longitude + '&key=AIzaSyARcSpoyBezW3bLCO7o0tsks_M4soHfPqU&country').subscribe((success) => {
        this.countryList = success.json();
        for (let i = 0; this.countryList.results[0].address_components.length > i; i++) {
          if (this.countryList.results[0].address_components[i].types[0] === 'country') {
            resolve(this.countryList.results[0].address_components[i]);
          }
        }
      }, (error) => {
        reject(JSON.parse(error._body));
      });
    });
  }

  doSignUp(signupRequest: any) {
    return new Promise((resolve, reject) => {
      this.http.post(USER_SIGNUP_URL, signupRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  kycDoSignup(signupRequest) {
    return new Promise((resolve, reject) => {
      this.http.post(KYC_NEW_SIGNUP_URL, signupRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetPasswordPolicy(policyId: any) {
    return new Promise((resolve, reject) => {
      this.http.get(GET_PASSWORD_POLICY_URL + policyId + '/GetPasswordPolicy.do').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doInstantLogin(userLoginRequest: any) {
    return new Promise((resolve, reject) => {
      this.http.post(USER_LOGIN_URL, userLoginRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doLogin(email, password) {
    return new Promise((resolve, reject) => {
      let options: RequestOptions;
      const clientId = 'certin';
      const clientSecret = 'secret';
      const params_body = 'grant_type=password&username=' + email.trim() + '&password=' + password.trim();
      const authHeader = 'Basic ' + btoa(clientId + ':' + clientSecret);
      const headers = new Headers();
      headers.append('Content-Type', 'application/x-www-form-urlencoded');
      headers.append('Authorization', authHeader);
      options = new RequestOptions({headers: headers});
      this.http.post(CIM_OAUTH, params_body, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        JSON.parse(JSON.stringify(error._body || null));
        reject(JSON.parse(error._body));
      });
    });
  }

  getLoginId(username) {
    return new Promise((resolve, reject) => {
      const params = 'userName=' + username;
      console.log('forgot req..' + GET_LOGINID_BY_EMAIL_URL + username + '/' + APP_INSTANCE_ID + '/findLoginByUsernameAndAppInstanceId.do');
      this.http.get(GET_LOGINID_BY_EMAIL_URL + username + '/' + APP_INSTANCE_ID + '/findLoginByUsernameAndAppInstanceId.do').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        JSON.parse(JSON.stringify(error._body || null));
        reject(JSON.parse(error._body));
      });
    });
  }

  doResendPassword(loginId: any) {
    return new Promise((resolve, reject) => {
      this.http.get(RESEND_PASSWORD_URL + loginId + '/ResetPassword.do?sendOtp=email\'' + '?sendOtp=email').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doChangePassword(loginId: any, password: any) {
    const changePasswordRequest = new PasswordModel(password);
    return new Promise((resolve, reject) => {
      this.http.post(CHANGE_PASSWORD_URL + loginId + '/ChangePassword.do', changePasswordRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetPerson(personId: any) {
    return new Promise((resolve, reject) => {
      console.log('get person url..' + GET_PERSON_URL + personId + '/GetPerson.do');
      this.http.get(GET_PERSON_URL + personId + '/GetPerson.do').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getPersonByemail(email: any) {
    return new Promise((resolve, reject) => {
      console.log('get person url..' + GET_PERSON_URL + email + '/getPersonByEmail.do');
      this.http.get(GET_PERSON_URL + email + '/getPersonByEmail.do').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }

  doPersonUpdate(personRequest: any) {
    console.log('update person req..' + JSON.stringify(personRequest));
    return new Promise((resolve, reject) => {
      this.http.post(PERSON_UPDATE_URL, personRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doOrganisationCreation(orgRequest: any) {
    console.log('org update req...' + JSON.stringify(orgRequest));
    return new Promise((resolve, reject) => {
      this.http.post(ORGANISATION_CREATION_URL, orgRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetOrganization(orgId) {
    return new Promise((resolve, reject) => {
      this.http.get(GETORGANISATION_DETAILS + orgId + '/GetOrganisationWithContactPerson.do').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetOrgtemplates(orgId) {
    return new Promise((resolve, reject) => {
      this.http.get(GETORG_TEMPLATES + orgId + '/getOrganisationDetails.do').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doUpdateTemplate(obj) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(UPDATEORG_TEMPLATES, JSON.stringify(obj), options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetAllSubscriptions(customerId: any) {
    return new Promise((resolve, reject) => {
      this.http.get(GET_STRIPE_URL + 'stripe/' + customerId + '/getCustomerSubscription').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetSubscriptionById(customerId, subscriptionId) {
    return new Promise((resolve, reject) => {
      this.http.get(GET_STRIPE_URL + 'stripe/' + customerId + '/' + subscriptionId + '/getSubscriptionInvoices').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetAllInvoices(customerId: any) {
    return new Promise((resolve, reject) => {
      this.http.get(GET_STRIPE_URL + 'stripe/' + customerId + '/getCustomerInvoices').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doUpgradeSubscription(subscriptionRequest) {
    console.log('' + JSON.stringify(subscriptionRequest));
    return new Promise((resolve, reject) => {
      this.http.post(UPGRAGE_SUBSCRIPTION_URL, subscriptionRequest).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetOrganisationDetails(orgId) {
    console.log(GET_ORGANISATION_DETAILS + orgId + '/getOrganisationDetails');
    return new Promise((resolve, reject) => {
      this.http.get(GET_ORGANISATION_DETAILS + orgId + '/getOrganisationDetails').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetAuditLog(auditPageRequest) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(GET_AUDITLOGSLIST_URL, JSON.stringify(auditPageRequest), options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetPersonList(personListRequest) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(GET_PERSON_LIST, JSON.stringify(personListRequest), options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetnewClaims(orgId, claimType, pageNumber, pageSize, searchKey, searchValue, sortingField, sortingOrder, status, keyStatus, inboxOrSentType) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    /*Inbox Outbox claims getting*/
    console.log('person req..' + GET_ALL_INBOX_LIST + orgId + '/getAllRequests?' + keyStatus + '=' + status + '&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder + inboxOrSentType + claimType);
    if (status) {
      if (status === 'CLOSE') {
        status = 'CLOSED'
      }
      return new Promise((resolve, reject) => {
        this.http.get(GET_ALL_INBOX_LIST + orgId + '/getAllRequests?' + keyStatus + '=' + status + '&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder + inboxOrSentType + claimType, options).subscribe((result) => {
          resolve(result.json());
        }, (error) => {
          reject(error.json());
        });
      });
    } else {
      console.log(status);
    }
  }


  doGetClaims(orgId, claimType, pageNumber, pageSize, searchKey, searchValue, sortingField, sortingOrder, status) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    if (status) {

      /*Inbox Outbox claims getting*/
      console.log('person req..' + GET_CLAIMS_BY_TYPE + orgId + '/claimType/' + claimType + '/getClaimsByType?claimStatus=' + status + '&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder);
      return new Promise((resolve, reject) => {
        this.http.get(GET_CLAIMS_BY_TYPE + orgId + '/claimType/' + claimType + '/getClaimsByType?claimStatus=' + status + '&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
          resolve(result.json());
        }, (error) => {
          reject(error.json());
        });
      });
    } else {
      /*All claims getting*/
      console.log('person req..' + GET_CLAIMS_BY_TYPE + orgId + '/claimType/' + claimType + '/getClaimsByType?page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder);
      return new Promise((resolve, reject) => {
        this.http.get(GET_CLAIMS_BY_TYPE + orgId + '/claimType/' + claimType + '/getClaimsByType?page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
          resolve(result.json());
        }, (error) => {
          reject(error.json());
        });
      });
    }
  }


  getValidStatus(siteInput) {
    return new Promise((resolve, reject) => {
      this.http.get('http://ip-api.com/json/' + siteInput).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetClaimDetails(claimId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_NEWCLAIM_DETAILS + claimId + '/getClaimRequestDetailsById', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doEndorseDecline(request, issueType: any) {
    console.log(issueType);
    /* if (issueType !== 'CERTIFICATES') {
     this.url = UPDATE_ENDORSE_DECLINE;
     } else {
     this.url = GET_UPDATE_CERT_STATUS;
     }*/
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    console.log('Endorse Request: ' + JSON.stringify(request));
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(UPDATE_ENDORSE_DECLINE, JSON.stringify(request), options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }


  doGetClaimTypeCount(orgId, status) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('' + GET_CLAIMS_COUNT + orgId + '/status/' + status + '/getAllClaimsTypeCountByOrg');
    return new Promise((resolve, reject) => {
      this.http.get(GET_CLAIMS_COUNT + orgId + '/status/' + status + '/getAllClaimsTypeCountByOrg', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doIndividualClaimsData(orgId, currentPage, pageSize, searchKey, searchValue) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('indi ..' + GETINDIVIDUALS_URL + orgId + '/getClaimStatusCountsByOrg?page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue);
    return new Promise((resolve, reject) => {
      this.http.get(GETINDIVIDUALS_URL + orgId + '/getClaimStatusCountsByOrg?page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doIndividualClaimDetails(individualClaimData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('details service..' + INDIVIDUAL_TRACKERDETAILS + individualClaimData + '/getClaimTrackAndPersonDetails');
    return new Promise((resolve, reject) => {
      this.http.get(INDIVIDUAL_TRACKERDETAILS + individualClaimData + '/getClaimTrackAndPersonDetails', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }

  getBalancee(requestedData) {
    return new Promise((resolve, reject) => {
      this.http.get('https://kovan.etherscan.io/api?module=account&action=balance&address=' + requestedData.address + '&tag=latest&apikey=' + requestedData.apiKey).map(res => res.json()).subscribe(result => {
        console.log('Address ' + result.result);
        result = result.result / 1000000000000000000;
        console.log('Address ' + result);
        resolve(result);
      });
    });
  }

  getcertuuidDetails(certificateDetailsReq: any) {
    return new Promise((resolve, reject) => {
      this.http.post(CERTIFICATE_UUID, certificateDetailsReq).subscribe((res) => {
        resolve(res.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getclaimuuidDetails(uuid: any) {
    return new Promise((resolve, reject) => {
      this.http.get(CLAIM_UUID + uuid).subscribe((res) => {
        resolve(res.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetCertificateList(orgId, certificateType, pageNumber, pageSize, searchKey, searchValue, sortingField, sortingOrder, status) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    /*Inbox Outbox certificates getting*/
    if (status) {
      console.log('cert req..' + GET_CERTIFICATES_BY_TYPE + orgId + '/certificateType/' + certificateType + '/getCertificatesByType?status=' + status + '&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder);
      return new Promise((resolve, reject) => {
        this.http.get(GET_CERTIFICATES_BY_TYPE + orgId + '/certificateType/' + certificateType + '/getCertificatesByType?status=' + status + '&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
          resolve(result.json());
        }, (error) => {
          reject(error.json());
        });
      });
    } else {
      /*All claims getting*/
      console.log('cert req..' + GET_CERTIFICATES_BY_TYPE + orgId + '/certificateType/' + certificateType + '/getCertificatesByType?page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder);
      return new Promise((resolve, reject) => {
        this.http.get(GET_CERTIFICATES_BY_TYPE + orgId + '/certificateType/' + certificateType + '/getCertificatesByType?page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
          resolve(result.json());
        }, (error) => {
          reject(error.json());
        });
      });
    }
  }

  doGetTransactionList(orgPublicKey, currentPage, pageSize, searchKey, searchValue, sortingField, sortingOrder) {
    if (searchKey === 'type') {
      this.typeValue = searchValue;
      this.actionValue = '';
    } else if (searchKey === 'action') {
      this.actionValue = searchValue;
      this.typeValue = '';
    }
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_TRANSACTIONS_LIST + orgPublicKey + '/getFromTransactionsByPublicKey?page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder);
    return new Promise((resolve, reject) => {
      this.http.get(GET_TRANSACTIONS_LIST + orgPublicKey + '/getFromTransactionsByPublicKey?page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetAllTransactionList(orgPublicKey, currentPage, pageSize, searchKey, searchValue, sortingField, sortingOrder) {
    if (searchKey === 'type') {
      this.typeValue = searchValue;
      this.actionValue = '';
    } else if (searchKey === 'action') {
      this.actionValue = searchValue;
      this.typeValue = '';
    }
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('Request Details' + GET_TRANSACTIONS_LIST + orgPublicKey + '/getAllTransactionsByPublicKey?page=' + currentPage + '&size=' + pageSize + '&' + '&type=' + this.typeValue + '&' + '&action=' + this.actionValue + '&' + '&sortField=' + sortingField + '&sort=' + sortingOrder);
    return new Promise((resolve, reject) => {
      this.http.get(GET_TRANSACTIONS_LIST + orgPublicKey + '/getAllTransactionsByPublicKey?page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortField=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetCertificateDetails(certificateId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_CERTIFICATE_DETAILS + certificateId + '/getCertificateDetails');
    return new Promise((resolve, reject) => {
      this.http.get(GET_CERTIFICATE_DETAILS + certificateId + '/getCertificateDetails', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetUpdateCertificateStatus(request) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(GET_UPDATE_CERT_STATUS, JSON.stringify(request), options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getVerifiedList(organisationId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(VERIFIED_LIST + organisationId + '/getSharedDetailsByOrg?sharedStatus=VERIFIED', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetRequesterSharedData(orgId, pageNumber, pageSize, searchKey, searchValue, sortingField, sortingOrder) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('verification req url..' + GET_SHARED_VERIFICATIONS_URL + orgId + '/getSharedDetailsByOrg?sharedStatus=SHARED&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortfield=' + sortingField + '&sort=' + sortingOrder);
    return new Promise((resolve, reject) => {
      this.http.get(GET_SHARED_VERIFICATIONS_URL + orgId + '/getSharedDetailsByOrg?sharedStatus=SHARED&page=' + pageNumber + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortfield=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetClaimDetailsData(claimId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('get Claim Req..' + GETCLAIMDETAILS_URL + claimId + '/getClaimDetails');
    return new Promise((resolve, reject) => {
      this.http.get(GETCLAIMDETAILS_URL + claimId + '/getClaimDetails', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }


  doGetCertificateDetailsData(certificateId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('get certificate  Req..' + GETCERTIFICATEDETAILS_URL + certificateId + '/getCertificateDetails');
    return new Promise((resolve, reject) => {
      this.http.get(GETCERTIFICATEDETAILS_URL + certificateId + '/getCertificateDetails', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }

  doGetBlockDetails(txHash) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_BLOCKCHAIN_DETAILS + txHash + '&apikey=VBFBKC9USABU8XV5V8YKN816ACPAGWYJ91', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetBlockChainReceipt(txHash) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_BLOCKCHAIN_RECEIPT + txHash + '&apikey=VBFBKC9USABU8XV5V8YKN816ACPAGWYJ91', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doCheckPerson(id: any) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GETLINKEDUSER_URL + id + '/getUserByLinkedinId', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetOrgByKey(orgListRequest) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(GET_ORG_KEY_LIST, JSON.stringify(orgListRequest), options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetDashboardClaimandCertificateCount(cimOrgId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_DASH_BOARD_URL + cimOrgId + '/getClaimAndCertStatusCountByOrg', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetDashBoardTransactionCount(cimOrgId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_DASH_BOARD_URL + cimOrgId + '/getTransactionsStatusCount');
    return new Promise((resolve, reject) => {
      this.http.get(GET_DASH_BOARD_URL + cimOrgId + '/getTransactionsStatusCount', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetDashBoardUsersandRequestsCount(cimOrgId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_DASH_BOARD_URL + cimOrgId + '/getUsersAndRequestersCount', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetDashBoardChartBarCount(cimOrgId, fromDate, toDate) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_DASH_BOARD_URL + cimOrgId + '/getClaimAndCertBarChartCount?fromDate=' + fromDate + '&toDate=' + toDate, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetCVBuilderClaims(cvId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('do get cv builder req..' + CVBUILDER_CLAIMS + cvId + '/claimsList?page=1&size=5&claimStatus=&sort=DESC&sortField=start_date');
    return new Promise((resolve, reject) => {
      this.http.get(CVBUILDER_CLAIMS + cvId + '/claimsList?page=1&size=5&claimStatus=&sort=DESC&sortField=start_date', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetCvDEtails(cvIdReq) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('claims...' + CVVIEWCLAIMS);
    return new Promise((resolve, reject) => {
      this.http.post(CVVIEWCLAIMS, cvIdReq, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }

  doSendMail(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(SEND_EMAIL, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doUpgradeProAccount(reqObject) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(UPGRADE_PRO_ACCOUNT, reqObject, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetOrgProcess(orgId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_ORG_PROCESS + orgId + '/GetOrganisation.do', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  upgradeKyc(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('reqData reqData' + JSON.stringify(reqData));
    return new Promise((resolve, reject) => {
      this.http.post(KYC_UPGRADE, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  klaviyoProfile(requestData: any) {
    console.log(requestData);
    return new Promise((resolve, reject) => {
      this.http.get(KLAVIYO_DATA + requestData).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  createZohoLead(leadOwner) {
    console.log(leadOwner);
    return new Promise((resolve, reject) => {
      this.http.get(ZOHOLEAD_CREATION + leadOwner + '&zohoparam=Leads&filepath=none.png').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doApplyCertificate(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(APPLYCERTIFICATE_URL, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doIssueCertificate(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(ISSUECERTIFICATE, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }

  getCertsIssuedData(orgId, pageNumber, pageSize, searchKey, searchValue, sortingField, sortingOrder, certType) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_ISSUED_CERTIFICATES + orgId + '/getOrganisationCertificates?' + 'page=' + pageNumber + '&size=' + pageSize + '&copyType=MASTER' + '&' + searchKey + '=' + searchValue + '&sortfield=' + sortingField + '&sort=' + sortingOrder + '&certType=' + certType)
    return new Promise((resolve, reject) => {
      this.http.get(GET_ISSUED_CERTIFICATES + orgId + '/getOrganisationCertificates?' + 'page=' + pageNumber + '&size=' + pageSize + '&copyType=MASTER' + '&' + searchKey + '=' + searchValue + '&sortfield=' + sortingField + '&sort=' + sortingOrder + '&certType=' + certType, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getcertsTransferredData(orgId, pageNumber, pageSize, searchKey, searchValue, sortingField, sortingOrder) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_TRANSFERRED_CERTIFICATES + orgId + '/getOrganisationCertificates?' + 'page=' + pageNumber + '&size=' + pageSize + '&copyType=CERTIFIED' + '&' + searchKey + '=' + searchValue + '&sortfield=' + sortingField + '&sort=' + sortingOrder)
    return new Promise((resolve, reject) => {
      this.http.get(GET_TRANSFERRED_CERTIFICATES + orgId + '/getOrganisationCertificates?' + 'page=' + pageNumber + '&size=' + pageSize + '&copyType=CERTIFIED' + '&' + searchKey + '=' + searchValue + '&sortfield=' + sortingField + '&sort=' + sortingOrder, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getIssuedCertDetails(certId) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_ISSUEDCERT_DETAILS + certId + '/getCertificateDetails');
    return new Promise((resolve, reject) => {
      this.http.get(GET_ISSUEDCERT_DETAILS + certId + '/getCertificateDetails', options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getIndividuals(name, pageNumber, pageSize) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(SEARCH_INDIVIDUALS + '?name=' + name + '&page=' + pageNumber + '&size=' + pageSize, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  revokeCertificate(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(REVOKE_CERT, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getOrgList(name) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(GET_ORG_LIST + '?name=' + name, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  transferCertificate(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(reqData);
    return new Promise((resolve, reject) => {
      this.http.post(TRANSFER_CERTIFICATE, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doInboxTabCount(organisationId, type) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(INANDOUTBOXCOUNT + organisationId + type, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  saveKycAndKyv(reqData) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(reqData);
    return new Promise((resolve, reject) => {
      this.http.post(SAVE_KYC, reqData, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  upgradeKycConfirm(request) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(request);
    return new Promise((resolve, reject) => {
      this.http.post(UPGRADE_KYC_PROFESSIONAL, request, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  upgradeKycConfirmMobile(request) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(request);
    return new Promise((resolve, reject) => {
      this.http.post(UPGRADE_MOBILE_KYC, request, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  createLead(MyXML: any) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Authorization', 'bc821bddc335b6b730c943aa690dac14');
    options = new RequestOptions({headers: headers});
    console.log(ZOHO_SERVICE_LEAD + MyXML)
    const myLeadrequest = {
      url: ZOHO_SERVICE_LEAD + MyXML
    }
    return new Promise((resolve, reject) => {
      this.http.post(ZOHO_SERVICE_URL, JSON.stringify(myLeadrequest), options).subscribe((result) => {
        console.log(result)
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });

  }

  doPostJob(postJobRequest) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(POST_JOB_SERVICE, postJobRequest, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  doGetJobList(orgId, currentPage, pageSize, searchKey, searchValue,  sortingField,sortingOrder) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log(GET_JOB_LIST + orgId + '/findJobAdvertisementByCimOrgId?' + 'page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortingField=' + sortingField + '&sortingOrder=' + sortingOrder)
     return new Promise((resolve, reject) => {
       this.http.get(GET_JOB_LIST +  orgId +'/findJobAdvertisementByCimOrgId?' + 'page=' + currentPage + '&size=' + pageSize + '&' + searchKey + '=' + searchValue + '&sortingField=' + sortingField + '&sortingOrder=' + sortingOrder, options).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
         reject(error.json());
       });
     });
  }

  /*  createLead(MyXML: any) {
      let options: RequestOptions;
      const headers = new Headers();
      headers.append('Authorization', 'bc821bddc335b6b730c943aa690dac14');
      options = new RequestOptions({headers: headers});
      console.log(ZOHO_SERVICE_LEAD + MyXML)
      return new Promise((resolve, reject) => {
        this.http.post(ZOHO_SERVICE_LEAD, JSON.stringify(MyXML), options).subscribe((result) => {
          console.log(result)
          resolve(result.json());
        }, (error) => {
          reject(error.json());
        });
      });

    }*/
  uploadExcel(file) {
    let options: RequestOptions;
    const headers = new Headers();
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.post(UPLOAD_EXCEL, file, options).subscribe((result) => {
        console.log(result);
        resolve(result.json());
      }, (err) => {
        reject(err.json());
      });
    });
  }
  validationFile(file) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    console.log('bulk upload req  : ' +  JSON.stringify(file));
    return new Promise((resolve, reject) => {
      this.http.post(UPLOAD_SIGNUP_REQ, JSON.stringify(file), options).subscribe((result) => {
        console.log(result);
        resolve(result.json());
      }, (err) => {
        reject(err.json());
      });
    });
  }
}
