import { Injectable } from '@angular/core';
import { GET_CERT_TYPES, TEMPLATE_URL } from '../endpoint';
import { Headers, Http, RequestOptions } from '@angular/http';
import { error } from 'util';

@Injectable()

export class TemplateServices {
  constructor(public http: Http) {
    console.log('hi this is template user');
  }

  getCertTypes() {
    return new Promise((resolve, reject) => {
      this.http.get(GET_CERT_TYPES).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }

  getCertContetntType(certType: any) {
    console.log(certType)
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
    options = new RequestOptions({headers: headers});
    return new Promise((resolve, reject) => {
      this.http.get(TEMPLATE_URL + certType + '/getTemplateVersionsByCertType', options).subscribe((result) => {
        console.log(result.json());
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      })
    })
  }

  getStaticJson() {
    return new Promise((resolve, reject) => {
      this.http.get('http://localhost:8080/assets/StaticjsonFiles/workAttestation.json').subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      })
    })
  }


}
