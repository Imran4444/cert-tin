import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';
import {DATEFORMATE} from '../endpoint';

@Pipe({
  name: 'dateTimeFormat'
})

/**
 * Created by Soulstice on 9/27/2018.
 */
export class DateFormatPipe implements PipeTransform {
  momentValue: any;

  transform(value: any, args?: any): any {
    this.momentValue = moment.utc(value).toDate();
    if (this.momentValue) {
      return this.transform( this.momentValue, DATEFORMATE);
    }
  }
}
