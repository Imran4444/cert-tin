import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { observable } from 'rxjs/symbol/observable';

@Injectable()
export class MySharedService {
  public data: any;
  public dataChangeObserver: any;
  public dataChange: Observable<any>;
  public orgData: any;
  public orgDataChangeObserver: any;
  public orgDataChange: Observable<any>;

  public inoutboxData: any;
  public inoutboxDataChangeObserver: any;
  public inoutboxDataChange: Observable<any>;
  public orgInfo: Observable<any>;
  public accountHeader: Observable<any>;
  public accountlevel: any;

  constructor() {
    this.dataChange = new Observable((observer: any) => {
      this.dataChangeObserver = observer;
    });

    this.orgInfo = new Observable((observer: any) => {
      this.orgDataChangeObserver = observer;
    });
    this.accountHeader = new Observable((observer) => {
      this.accountlevel = observer;
    })

    this.inoutboxDataChange = new Observable((observer: any) => {
      this.inoutboxDataChangeObserver = observer;
    });
  }

  setimgUrl(data: any) {
    this.data = data;
    this.dataChangeObserver.next(this.data);
  }

  setOrganisationDetails(data: any) {
    this.orgData = data;
    this.orgDataChangeObserver.next(this.orgData);
    this.accountlevel.next(this.orgData);
  }

  setInboxOutbox(data: any) {
    this.inoutboxData = data;
    this.inoutboxDataChangeObserver.next(this.inoutboxData);
  }
}
