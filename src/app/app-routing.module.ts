import { RouterModule, Routes } from '@angular/router';

import { LayoutComponent } from './layout/layout.component';
import { DashboardComponent } from './dashboard/dashboard.component';

// Page Layouts

const AppRoutes: Routes = [
  {path: '', redirectTo: '/landingPage/home', pathMatch: 'full'},
  {path: 'app', component: LayoutComponent},
  {path: 'extra', loadChildren: './extra-pages/extra-pages.module#ExtraPagesModule'},
  {path: 'landingPage', loadChildren: './landingPage/landingPage.module#LandingPageModule'},
  {path: 'badges', loadChildren: './badges/badges.module#BadgesModule'},
  {path: 'curriculumvitae', loadChildren: './CVBuilder/cvBuilder.module#CvBuilderModule'},
  {path: '**', redirectTo: '/landingPage/home', pathMatch: 'full'},
];

export const AppRoutingModule = RouterModule.forRoot(AppRoutes, {useHash: true});
