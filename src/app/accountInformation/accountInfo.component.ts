import {Component, OnInit} from '@angular/core';
import {PersonModal} from '../Modals/personModal';
import {AddressModal} from '../Modals/addressForm';
import {IdentificationModal} from '../Modals/identificationModal';
import {OrganisationModal} from '../Modals/orgGeneralModal';
import {CompanyRegisteredModal} from '../Modals/companyRegisteredModal';
import {ContactModal} from '../Modals/contactModal';
import {Countriescodes} from "../providers/countriescodes";
import {Http} from "@angular/http";
import {AWS_PHOTO_UPLOAD_URL} from "../endpoint";
import {EMAIL_PATTERN, WEBSITE_PATTERN} from "../providers/constants";
import {IndustryTypes} from "../providers/industrytypes";
import {ApiCalls} from "../services/services";
import {PersonUpdateRequest} from "../Modals/personUpdateRequestModal";
import {Emails} from "../Modals/emailModal";
import {Phones} from "../Modals/phonesModal";
import {DatePipe} from "@angular/common";
import {error} from "util";
import {OrganisationRequest} from "../Modals/organisationRequestModal";
import {AddressRequest} from "../Modals/addressModal";
import {Company} from "../Modals/companyModal";
import {Orgcustomfields} from "../Modals/orgcustomfields";
import {APP_ID, APP_INSTANCE_ID, PASSWORD_POLICY_ID} from "../AppConstants";
import {CompanyDetailsModal} from "../Modals/companyDetailsModal";
import {GlobalPopupComponent} from "../globalPopup/globalPopup.component";
import {ValidationProvider} from "../providers/validationProvider";
import {CustomFields} from "../Modals/customFieldsModal";
import {MdDialog} from "@angular/material";
import {ErrorMessages} from "../providers/errorMessages";
import {MySharedService} from '../services/mySharedService ';
import {LoginService} from '../extra-pages/login/loginService';
import {AuditModal} from '../Modals/auditModal';
import * as moment from 'moment';
import 'moment-timezone';
import {ContactpersonModal} from "../Modals/contactpersonmodal";
import {auditService} from '../providers/audit.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-account-info',
  templateUrl: '../accountInformation/accountInfo.component.html',
  styleUrls: ['../accountInformation/accountInfo.component.css']
})

export class AccountInfoComponent implements OnInit {
  public imgUrl: string
  public accountinfo: boolean
  public tabSelected: number;
  public foo: any = 1
  public minimizeValue: boolean = true
  public OminimizeValue: boolean = false
  personForm: any;
  personModal: any;
  addressForm: any;
  addressModal: any;
  identificationForm: any;
  identificationModal: any;
  orgGeneralModal: any;
  companyRegisteredModal: any;
  companyDetailsModal: any;
  contactModal: any;
  countryDetails: any;
  file: any;
  fileGeneral: any;
  isReadOnly: boolean;
  public personalPerson: boolean;
  public addressPerson: boolean;
  public iddentification: boolean;
  public orgGeneral: boolean;
  public orgCompany: any;
  public orgContactnew: any;
  public logoUrl: any;
  public orgUploadUrl: any;
  public showLoader: any;
  maxDate: any;
  emailPattern: any;
  public industryNames: any;
  public personDetails: any;
  public contactDetails: any;
  public personData: any;
  public emailsArray: any;
  public phonesArray: any;
  public personOrganisation: any;
  public addressArray: any;
  public loginResponseStorage: any;
  public organizationAddressArray: any;
  public organizationCompanyArray: any;
  public orgPhonesArr: any;
  public orgEmailsArr: any;
  public organizationDetails: any;
  public appIdArr: any;
  public organisationName: any;
  public companyId: any;
  public updatePersonResult: any;
  public updateOrgResponse: any;
  public updatePersonLoading: any;
  public updateOrgLoading: any;
  public contactsEmailArr: any;
  public contactsPhoneArr: any;
  public contactpersonModal: any;
  public isContactPersonCb: any;
  customerId: any;
  subscriptionId: any;
  planName: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  personCheckBox: any;
  personIdContactPerson: any;
  public contactOrgPersonArr: any;
  public organisationsContactsArr: any;
  public orgPersonUpgradeAccount: any;
  public emailVerificationStatus: any;
  public personOrgGender: any;
  public organisationResponse: any;
  public userDetails: any;
  public expiryDate: string;
  accountPersonForm: FormGroup;
  processedImages: any = [];
  submitted: any;
  organizationForm: FormGroup;
  submitTry: any;
  webSitePatern: any;
  validityStatus: any;
  siteCheck: any;
  auditReq: any;
  birthdayDate: any;
  issuedDate: any;
  getAdminpersonDetails: any;
  public companyRegistrationDate: any;

  constructor(public countriesCodes: Countriescodes,
              public http: Http,
              public industryTypes: IndustryTypes,
              public apiCalls: ApiCalls,
              public datePipe: DatePipe,
              public validations: ValidationProvider,
              public dialog: MdDialog,
              public errorMessage: ErrorMessages,
              public loginservice: LoginService,
              public shareService: MySharedService,
              public auditHistory: auditService,
              public  formBuilder: FormBuilder,
              public router: Router) {
    this.tabSelected = 1;
    this.personModal = new PersonModal('', '', '', '', '', '', '');
    this.addressModal = new AddressModal('', '', '', '', '', '');
    this.identificationModal = new IdentificationModal('', '', '', '', '');
    this.orgGeneralModal = new OrganisationModal('', '');
    this.companyDetailsModal = new CompanyDetailsModal('', '', '');
    this.companyRegisteredModal = new CompanyRegisteredModal('', '', '', '', '', '', '', '', '', '');
    this.contactModal = new ContactModal('', '', '', '', '', '', '', '');
    this.auditModal = new AuditModal('', '', '', '', '', '', '', '', '');
    this.personModal.title = 'defaultTitle';
    this.personModal.countryCode = 'defaultCode';
    this.addressModal.country = 'defaultCountry';
    this.identificationModal.idProofType = 'defaultIdtype';
    this.identificationModal.nationality = 'defaultNationality';
    this.orgGeneralModal.type = 'defaultIndustrytype';
    this.companyRegisteredModal.country = 'defaultCountry';
    this.contactModal.title = 'defaultTitle';
    this.contactModal.countryCode = 'defaultCountryCode';
    this.emailPattern = EMAIL_PATTERN;
    this.webSitePatern = WEBSITE_PATTERN;
  }

  ngOnInit() {
    this.accountPersonForm = this.formBuilder.group({
      title: ['', Validators.required],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      dob: ['', Validators.required],
      emailId: ['', [Validators.email, Validators.required]],
      countryCode: ['', Validators.required],
      mob: ['', Validators.required],
      line1: ['', Validators.required],
      line2: ['', Validators.required],
      line3: [''],
      country: ['', Validators.required],
      region: ['', Validators.required],
      pinCode: ['', Validators.required],
      idType: [''],
      idNum: [''],
      expiry: [''],
      issued: [''],
      national: [''],
    });
    this.organizationForm = this.formBuilder.group({
      indType: ['', Validators.required],
      orgUrl: ['', Validators.required],
      country: [''],
      regNum: [''],
      orgName: ['', Validators.required],
      vatNum: [''],
      regDate: [''],
      line1: [''],
      line2: [''],
      line3: [''],
      region: [''],
      pin: [''],
      userDetails: [''],
      title: [''],
      firstName: [''],
      lastName: [''],
      role: [''],
      countryCode: [''],
      phoneNum: [''],
      emailId: [''],
    });
    this.orgPersonUpgradeAccount = false;
    this.contactsPhoneArr = [];
    this.contactOrgPersonArr = [];
    this.contactsEmailArr = [];
    this.isReadOnly = false;
    this.updatePersonLoading = false;
    this.updateOrgLoading = false;
    this.loginResponseStorage = JSON.parse(localStorage.getItem('loginResponse'));
    this.personDetails = JSON.parse(localStorage.getItem('personResponse'));
    this.emailsArray = [];
    this.phonesArray = [];
    this.addressArray = [];
    this.orgPhonesArr = [];
    this.orgEmailsArr = [];
    this.appIdArr = [];
    this.organizationAddressArray = [];
    this.organizationCompanyArray = [];
    this.showLoader = false;
    this.maxDate = new Date();
    this.countryDetails = this.countriesCodes.getCountries();
    this.industryNames = this.industryTypes.getIndustries();
    this.personalPerson = false;
    this.addressPerson = false;
    this.iddentification = false;
    this.orgGeneral = false;
    this.orgCompany = false;
    this.orgContactnew = false;
    if (this.personDetails) {
      console.log('person details...' + JSON.stringify(this.personDetails))
      this.personModal = this.personDetails.person;
      if (!this.personModal.title) {
        this.personModal.title = 'defaultTitle';
      }
      this.logoUrl = this.personDetails.person.pictureUri;
      this.personModal.email = this.personDetails.person.emails[0].email;
      this.emailVerificationStatus = this.personDetails.person.emails[0].verefied;
      this.personModal.countryCode = this.personDetails.person.phones[0].countryCode;
      this.personModal.phoneNumber = this.personDetails.person.phones[0].number;
      this.personModal.dateOfBirth = new Date(this.personDetails.person.dateOfBirth);
      this.addressModal = this.personDetails.person.address[0];
      this.personOrganisation = this.personDetails.person.organisations;
      if (this.personDetails.person.idify) {
        this.orgPersonUpgradeAccount = true;
        this.identificationModal = this.personDetails.person.idify;
        if (this.personDetails.person.idify.expiryDate) {
          this.identificationModal.expiryDate = new Date(this.personDetails.person.idify.expiryDate);
        }
        if (this.personDetails.person.idify.issuedDate) {
          this.identificationModal.issuedDate = new Date(this.personDetails.person.idify.issuedDate);
          console.log('get issued date.....' + this.identificationModal.issuedDate)
        }

      } else {
        this.orgPersonUpgradeAccount = false;
      }
    }
    if (this.loginResponseStorage) {
      // this.getPersonDetails(this.loginResponseStorage.personId);
      console.log('login response...' + JSON.stringify(this.loginResponseStorage))
      this.getOrganization(this.loginResponseStorage.orgId);
    }
  }

  get f() {
    return this.accountPersonForm.controls;
  }

  get f1() {
    return this.organizationForm.controls;
  }

  minimizePersonal() {
    if (this.personalPerson) {
      this.personalPerson = false;
    } else if (!this.personalPerson) {
      this.personalPerson = true;
    }
  }

  addressMinimize() {
    if (this.addressPerson) {
      this.addressPerson = false;
    } else if (!this.addressPerson) {
      this.addressPerson = true;
    }
  }

  iddentFicationMinimize() {
    if (this.iddentification) {
      this.iddentification = false;
    } else if (!this.iddentification) {
      this.iddentification = true;
    }
  }

  cancel() {
    this.router.navigate(['/app/dashboard']);
  }

  orgGen() {
    if (this.orgGeneral) {
      this.orgGeneral = false;
    } else if (!this.orgGeneral) {
      this.orgGeneral = true;
    }
  }

  checkValidSite() {
    if (this.orgGeneralModal.website) {
      this.apiCalls
        .getValidStatus(this.orgGeneralModal.website)
        .then((result) => {
          this.siteCheck = result;
          this.validityStatus = this.siteCheck.status;
        });
    }
  }

  orgComp() {
    if (this.orgCompany) {
      this.orgCompany = false;
    } else if (!this.orgCompany) {
      this.orgCompany = true;
    }
  }

  orgContact() {
    if (this.orgContactnew) {
      this.orgContactnew = false;
    } else if (!this.orgContactnew) {
      this.orgContactnew = true;
    }

  }

  minimize(data: number) {
    this.foo = data;
    if (data === 2) {
      this.OminimizeValue = false;
    } else {
      this.minimizeValue = false;
      this.OminimizeValue = false;
    }

  }

  expand(data: number) {
    this.foo = data;
    if (data === 2) {
      this.OminimizeValue = true;
      this.minimizeValue = false;
    } else {
      this.minimizeValue = true;
      this.OminimizeValue = false;
    }
  }

  numberOnly(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  uploadPicture(event) {
    this.showLoader = true;
    let formData: FormData = new FormData();
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];
    this.http.post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData).map(res => res.json())
      .subscribe((data) => {
        // this.logoUrl = data.imageURL;
        this.logoUrl = data.resizedImageUrl;
        this.showLoader = false;
        // alert('Image Uploaded Successfully');
      }, (err) => {
        this.showLoader = false;
        alert('Oops !! Image Uploading Failed, Please Try Again');
      });
  }

  uploadPicturegeneral(event) {
    this.showLoader = true;
    let formData: FormData = new FormData();
    let eventObj: MSInputMethodContext = <MSInputMethodContext> event;
    let target: HTMLInputElement = <HTMLInputElement> eventObj.target;
    let files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];
    this.http.post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData).map((res) => res.json())
      .subscribe((data) => {
        // this.orgUploadUrl = data.imageURL;
        this.orgUploadUrl = data.resizedImageUrl;
        this.showLoader = false;
        // alert('Image Uploaded Successfully');
      }, (err) => {
        this.showLoader = false;
        alert('Oops !! Image Uploading Failed, Please Try Again');
      });
  }

  gototab(tabIndex: any) {
    // this.companyRegisteredModal = new CompanyRegisteredModal('', '', '', '', '', '', '', '', '', '');
    this.tabSelected = tabIndex;
  }

  getPersonDetails(personId) {
    this.apiCalls.doGetPerson(personId).then((result) => {
      this.personDetails = result;
      localStorage.setItem('personResponse', JSON.stringify(this.personDetails));
      this.personModal = this.personDetails.person;
      if (!this.personModal.title) {
        this.personModal.title = 'defaultTitle';
      }
      this.logoUrl = this.personDetails.person.pictureUri;
      this.personModal.email = this.personDetails.person.emails[0].email;
      this.personModal.countryCode = this.personDetails.person.phones[0].countryCode;
      this.personModal.phoneNumber = this.personDetails.person.phones[0].number;
      this.personModal.dateOfBirth = new Date(this.personDetails.person.dateOfBirth);
      this.addressModal = this.personDetails.person.address[0];
      this.personOrganisation = this.personDetails.person.organisations;
      if (this.personDetails.person.idify) {
        this.identificationModal = this.personDetails.person.idify;
        if (this.personDetails.person.idify.expiryDate) {
          this.identificationModal.expiryDate = new Date(this.personDetails.person.idify.expiryDate);
        }
        if (this.personDetails.person.idify.issuedDate) {
          this.identificationModal.issuedDate = new Date(this.personDetails.person.idify.issuedDate);
        }


      }
    }, (error) => {
    });
  }

  addUserDetails(userDetails: any) {
    this.personCheckBox = userDetails;
    if (userDetails === true) {
      this.isReadOnly = true;
      this.contactDetails = JSON.parse(localStorage.getItem('personResponse'));
      this.contactModal = this.contactDetails;
      this.contactModal.title = this.contactDetails.person.title;
      this.contactModal.firstName = this.contactDetails.person.firstName;
      this.contactModal.lastName = this.contactDetails.person.lastName;
      this.contactModal.countryCode = this.contactDetails.person.phones[0].countryCode;
      this.contactModal.phoneNumber = this.contactDetails.person.phones[0].number;
      this.contactModal.emails = this.contactDetails.person.emails[0].email;
    } else if (userDetails === false) {
      this.isReadOnly = false;
      this.contactModal = new ContactModal('', '', '', '', '', '', '', '');
    } else {
      this.isReadOnly = false;
    }
  }

  createAuditLog() {
    this.auditHistory.userAudit('Update Account');
  }

  updatePerson() {
    this.submitted = true;
    if (this.accountPersonForm.valid) {
      //this.createAuditLog();
      this.imgUrl = 'http://d2btkkuyusotz5.cloudfront.net/soulstice.co.za/Open-Platform-01b1.png';
      // this.shareService.setimgUrl(this.imgUrl);
      this.updatePersonLoading = true;
      const emailsArr = new Emails(true, this.personModal.email, 'private', this.emailVerificationStatus);
      this.emailsArray.push(emailsArr);
      const phoneArr = new Phones(true, this.personModal.countryCode, this.personModal.phoneNumber, 'private', false);
      this.phonesArray.push(phoneArr);
      this.addressArray.push(this.addressModal);
      if (this.personDetails.person.idify) {
        if (this.personDetails.person.idify.expiryDate) {
          const expDate = this.datePipe.transform(this.identificationModal.expiryDate, 'yyyy-MM-dd');
          if (this.identificationModal.idProofType === 'passport') {
            const hourData = this.datePipe.transform(new Date(), 'HH:mm');
            this.expiryDate = expDate + ' ' + hourData + ':00.00Z';
          } else {
            this.expiryDate = '2018-10-26 16:03:00.00Z';
          }
        }
        if (this.personDetails.person.idify.issuedDate) {
          const expDate = this.datePipe.transform(this.identificationModal.issuedDate, 'yyyy-MM-dd');
          const hourData = this.datePipe.transform(new Date(), 'HH:mm');
          this.issuedDate = expDate + ' ' + hourData + ':00.00Z';

        }

      }
      if (this.personModal.dateOfBirth) {
        const birthDate = this.datePipe.transform(this.personModal.dateOfBirth, 'yyyy-MM-dd');
        const hourData = this.datePipe.transform(new Date(), 'HH:mm');
        this.birthdayDate = birthDate + ' ' + hourData + ':00.00Z';
      }

      this.auditReq = this.auditHistory.userAudit('Update Person');
      const personcustomFields = new IdentificationModal(this.identificationModal.idNumber, this.identificationModal.idProofType, this.identificationModal.nationality, this.expiryDate, this.issuedDate);
      const personUpdateRequest = new PersonUpdateRequest(this.auditReq, this.loginResponseStorage.personId, this.personModal.title, true, this.logoUrl, this.personModal.firstName, this.personModal.lastName, this.birthdayDate, this.addressArray, personcustomFields,
        this.emailsArray, this.phonesArray, this.personOrganisation, true);
      this.apiCalls.doPersonUpdate(personUpdateRequest).then((result) => {
        this.updatePersonResult = result;
        this.updatePersonLoading = false;
        console.log('update person resp...' + JSON.stringify(this.updatePersonResult))
        this.shareService.setimgUrl(this.updatePersonResult);
        localStorage.setItem('personResponse', JSON.stringify(this.updatePersonResult));
        const resultData = this.validations.errorPopup(this.errorMessage.personSuccess().title, this.errorMessage.personSuccess().message, '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
      }, (error) => {
        this.updatePersonLoading = false;
        const resultData = this.validations.errorPopup(this.errorMessage.personFail().title, error.responseMessage, '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
      });
    } else {
      console.log('invalid form')
    }
  }

  getOrganization(orgId) {
    this.apiCalls.doGetOrganization(orgId).then((result) => {
      this.organizationDetails = result;
      console.log('org details...' + JSON.stringify(this.organizationDetails))
      localStorage.setItem('orgResponse', JSON.stringify(this.organizationDetails))
      this.orgGeneralModal = this.organizationDetails.organisation;
      if (this.orgGeneralModal) {
        if (this.orgGeneralModal.customFields) {
          if (this.orgGeneralModal.customFields.adminPersonId) {
            this.getAdminPersonDetails(this.orgGeneralModal.customFields.adminPersonId);
          }
        }
      }

      this.organisationName = this.organizationDetails.organisation.name;
      this.orgUploadUrl = this.organizationDetails.organisation.logoUrl;
      if (this.organizationDetails.organisation.address) {
        this.companyRegisteredModal = this.organizationDetails.organisation.address[0];
        this.companyRegisteredModal.line1 = this.companyRegisteredModal.line1;
        this.companyRegisteredModal.line2 = this.companyRegisteredModal.line2;
        this.companyRegisteredModal.line3 = this.companyRegisteredModal.line3;
      }
      if (this.organizationDetails.listOfCompany) {
        if (this.organizationDetails.listOfCompany.length) {
          this.companyDetailsModal = this.organizationDetails.listOfCompany[0];
          if(this.organizationDetails.listOfCompany[0].businessStartDate){
            this.companyDetailsModal.businessStartDate = new Date(this.organizationDetails.listOfCompany[0].businessStartDate);
          }else {
            this.companyDetailsModal.businessStartDate = ''
          }

        }
      }
      if (this.organizationDetails.organisation.customFields) {
        this.userDetails = this.organizationDetails.organisation.customFields.isContactPerson;
        this.contactModal = this.organizationDetails.organisation.customFields;
        this.customerId = this.organizationDetails.organisation.customFields.customerId;
        this.subscriptionId = this.organizationDetails.organisation.customFields.subscriptionId;
        this.planName = this.organizationDetails.organisation.customFields.planName;
        if (this.userDetails === false) {
          this.contactModal.title = this.organizationDetails.person.title;
          this.contactModal.firstName = this.organizationDetails.person.firstName;
          this.contactModal.lastName = this.organizationDetails.person.lastName;
          this.contactModal.countryCode = this.organizationDetails.person.phones[0].countryCode;
          this.contactModal.phoneNumber = this.organizationDetails.person.phones[0].number;
          this.contactModal.emails = this.organizationDetails.person.emails[0].email;
        } else if (this.userDetails) {
          this.addUserDetails(true);
        }
      }


    }, (error) => {
      console.log('org details error..' + JSON.stringify(error));
    });
  }

  updateOrganization() {
    this.submitTry = true;
    if (this.organizationForm.valid && this.validityStatus != 'fail') {
      this.createAuditLog();
      this.updateOrgLoading = true;
      this.appIdArr.push(APP_ID);
      const addressRequest = new AddressRequest(this.companyRegisteredModal.line1, this.companyRegisteredModal.line2, this.companyRegisteredModal.line3, '', this.companyRegisteredModal.country, this.companyRegisteredModal.postCode, true, this.companyRegisteredModal.region, '')
      if (this.companyDetailsModal) {
        this.companyId = this.companyDetailsModal.id;
      } else {
        this.companyId = '';
      }
      if (this.personCheckBox === true) {
        this.isContactPersonCb = true;
        this.personIdContactPerson = this.loginResponseStorage.personId;
        this.contactpersonModal = {};
      } else {
        this.personIdContactPerson = null;
        const contactEmails = new Emails(true, this.contactModal.emails, 'private', true);
        this.contactsEmailArr.push(contactEmails)
        const contactPhone = new Phones(true, this.contactModal.countryCode, this.contactModal.phoneNumber, 'private', false);
        this.contactsPhoneArr.push(contactPhone)
        this.contactOrgPersonArr.push(addressRequest);
        this.organisationsContactsArr = [];
        this.contactpersonModal = new ContactpersonModal(false, this.contactsEmailArr, this.contactModal.firstName, this.contactModal.lastName, 'Male', this.contactOrgPersonArr, this.organisationsContactsArr, this.contactsPhoneArr, this.contactModal.title);
        this.isContactPersonCb = false;
      }
      if (this.companyDetailsModal.businessStartDate) {
        const companyRegDate = this.datePipe.transform(this.companyDetailsModal.businessStartDate, 'yyyy-MM-dd');
        const hourData = this.datePipe.transform(new Date(), 'HH:mm');
        this.companyRegistrationDate = companyRegDate + ' ' + hourData + ':00.00Z'
      }else {
        this.companyRegistrationDate = '';
      }

      const company = new Company(this.companyId, this.loginResponseStorage.orgId, this.companyDetailsModal.registrationNumber, this.companyDetailsModal.taxNumber, this.orgGeneralModal.type, this.companyRegistrationDate);
      this.organizationCompanyArray.push(company)
      this.organizationAddressArray.push(addressRequest);
      this.auditReq = this.auditHistory.userAudit('Update Organisation');
      // const orgcustomfields = new Orgcustomfields(this.contactModal.title, this.contactModal.firstName, this.contactModal.lastName, this.contactModal.role, this.contactModal.area, this.contactModal.phoneNumber, this.contactModal.emails, this.contactModal.countryCode, this.customerId, this.subscriptionId, this.planName);
      const organisationRequest = new OrganisationRequest(this.auditReq, this.loginResponseStorage.orgId, this.orgGeneralModal.type, true, this.organisationName, this.orgUploadUrl, this.orgGeneralModal.website, this.organizationCompanyArray, this.organizationAddressArray, this.appIdArr, PASSWORD_POLICY_ID,
        APP_INSTANCE_ID, this.orgPersonUpgradeAccount, this.personIdContactPerson, null)
      console.log('update org...' + JSON.stringify(organisationRequest))
      this.apiCalls.doOrganisationCreation(organisationRequest).then((result) => {
        console.log('update org response..' + JSON.stringify(result));
        this.updateOrgResponse = result;
        this.updateOrgLoading = false;
        localStorage.setItem('orgResponse', JSON.stringify(this.updateOrgResponse));
        this.getOrganisationDetails(this.loginResponseStorage.orgId);
        const resultData = this.validations.errorPopup(this.errorMessage.organizationSuccess().title, this.errorMessage.organizationSuccess().message, '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
        this.gototab(2);
      }, (error) => {
        this.updateOrgLoading = false;
        console.log('update org error..' + JSON.stringify(error));
        const resultData = this.validations.errorPopup(this.errorMessage.organizationFail().title, error.responseMessage, '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
      });
    } else {
      console.log('faillll')
    }
  }

  getOrganisationDetails(orgId) {
    this.apiCalls.doGetOrganisationDetails(orgId).then((result) => {
      this.organisationResponse = result;
      localStorage.setItem('STRIPE_ORGDETAILS', JSON.stringify(this.organisationResponse));
      this.shareService.setOrganisationDetails(this.organisationResponse);
    }, (error) => {
    });
  }

  cancelBtn() {
    this.router.navigate(['/app/dashboard']);

  }

  getAdminPersonDetails(personId) {
    console.log('admin person id...' + personId)
    this.apiCalls.doGetPerson(personId).then((result) => {
      this.getAdminpersonDetails = result;
      this.contactModal.firstName = this.getAdminpersonDetails.person.firstName;
      this.contactModal.lastName = this.getAdminpersonDetails.person.lastName;
      this.contactModal.countryCode = this.getAdminpersonDetails.person.phones[0].countryCode;
      this.contactModal.phoneNumber = this.getAdminpersonDetails.person.phones[0].number;
      this.contactModal.emails = this.getAdminpersonDetails.person.emails[0].email;
      console.log('get admin person details...' + JSON.stringify(this.getAdminpersonDetails))
    }, (error) => {
      console.log('get person error..' + JSON.stringify(error));
    });
  }


}
