import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import {AccountInfoComponent} from './accountInfo.component';
import {MaterialModule, MdNativeDateModule} from '@angular/material';
import {ValidationProvider} from '../providers/validationProvider';
import {ErrorMessages} from '../providers/errorMessages';

@NgModule({
  imports: [FormsModule,
    BrowserModule,
    MaterialModule,
    FormsModule,
    MdNativeDateModule,
  ReactiveFormsModule],
  declarations: [AccountInfoComponent],
  providers: [ValidationProvider, ErrorMessages],
})
export class AccountInfoModule {
  constructor() {
  }
}
