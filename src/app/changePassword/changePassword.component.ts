import { Component, OnInit } from '@angular/core';
import { PasswordModel } from '../extra-pages/password/model/passwordModel';
import { AuditModal } from '../Modals/auditModal';
import { Router } from '@angular/router';
import { ApiCalls } from '../services/services';
import { ValidationProvider } from '../providers/validationProvider';
import { ErrorMessages } from '../providers/errorMessages';
import { MdDialog } from '@angular/material';
import { DatePipe } from '@angular/common';
import { LoginService } from '../extra-pages/login/loginService';
import { auditService } from '../providers/audit.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-change-password',
  templateUrl: '../changePassword/changePassword.component.html',
  styleUrls: ['../changePassword/changePassword.component.css']
})
export class ChangePasswordComponent implements OnInit {
  public passwordObj: PasswordModel;
  public confpassword: any;
  loginResponse: any;
  showLoader: any;
  passwordStrength: any;
  passwordError: any;
  changePasswordError: any;
  cpassword: any;
  changePasswordResult: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  changePasswordForm: FormGroup;
  public barLabel = 'Password strength:';
  submitted: any;
  auditReq: any;
  constructor(
    public router: Router,
    public apiCall: ApiCalls,
    public validations: ValidationProvider,
    public errorMessage: ErrorMessages,
    public dialog: MdDialog,
    public datePipe: DatePipe,
    public loginservice: LoginService,
    public auditHistory: auditService,
    public formBuilder: FormBuilder
  ) {
    this.auditModal = new AuditModal(
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      ''
    );
  }

  ngOnInit() {
    this.changePasswordForm = this.formBuilder.group({
      pwd : ['', Validators.required],
      cnfrmPwd: ['', Validators.required]
    })
    this.passwordObj = new PasswordModel('');
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
  }
  get f() {
   return this.changePasswordForm.controls;
  }
  createAuditLog() {
    this.auditHistory.userAudit('Password Changed');
  }

  goToDashboard() {
    this.submitted = true;
    if (!this.changePasswordForm.valid || (this.passwordObj.password !== this.confpassword) || this.passwordStrength == 'Weak') { } else {
      this.createAuditLog();
      this.showLoader = true;
      const loginId = this.loginResponse.loginId;
      this.auditReq = this.auditHistory.userAudit('Password Changed');
      this.apiCall.doChangePassword(loginId, this.passwordObj.password).then(
        (result) => {
          console.log('' + JSON.stringify(result));
          this.changePasswordResult = result;
          if (this.changePasswordResult.responseCode === '404') {
            this.changePasswordError = this.changePasswordResult.developerMessage;
            // this.passwordObj.password = '';
            // this.confpassword = '';
          } else {
            this.router.navigate(['/app/account']);
          }
          this.showLoader = false;
        },
        (error) => {
          console.log('Error ' + JSON.stringify(error));
          this.passwordError = error;
          this.changePasswordError = this.passwordError.developerMessage;
          // this.passwordObj.password = '';
          // this.confpassword = '';
          // const resultData = this.validations.errorPopup(this.errorMessage.invalidPasswordFormat().title, error.responseMessage, '');
          // const dialogRef = this.dialog.open(GlobalPopupComponent, {
          //   width: '450px',
          //   data: resultData,
          //   disableClose: true
          // });
          this.showLoader = false;
        }
      );
    }
  }
  passwordStatuss(ev) {
    this.passwordStrength = ev;
    console.log(this.passwordStrength);
  }
  goBack() {
    this.router.navigate(['/app/adminSettings']);
  }
}
