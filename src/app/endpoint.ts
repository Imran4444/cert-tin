/**
 * CIM End-Points
 */

// const CONTEXT_PATH = 'http://localhost:8093/CIMServer/api';
export const CONTEXT_PATH = 'http://103.60.212.198:8030/CIMServer/api';
//export const CERTTIN_CONTEXT_PATH = 'http://192.168.1.37:5656/CertTinV1/api/';

export const CERTTIN_CONTEXT_PATH = 'http://dev1.fliptin.com/CertTinV1/api/';
export const CIM_OAUTH = 'http://dev1.fliptin.com/CIMOAuth/oauth/token';
export const CVCONTEXT = 'http://dev1.fliptin.com/certtinweb/#//curriculumvitae/view/';
export const SOCIAL_ROOT = 'http://dev1.fliptin.com';
export const DEV_ROOT_PATH = 'http://dev1.fliptin.com/certtinweb';
export const CIM_CONTEXT_PATH = 'http://dev1.fliptin.com/CIMServer2/api';
// export const CIM_CONTEXT_PATH = 'http://192.168.1.42:8080/CIMServer2/api';
export const UPGRADE_PRO_ACC_PATH = 'http://dev1.fliptin.com/certtinweb/#/extra/updateaccount/';

/* export const CERTTIN_CONTEXT_PATH = 'http://certtin.com/CertTinV1/api/';
export const CIM_OAUTH = 'http://certtin.com/CIMOAuth/oauth/token';
export const CVCONTEXT = 'http://certtin.com/#/curriculumvitae/view/';
export const SOCIAL_ROOT = 'http://certtin.com';
export const DEV_ROOT_PATH = 'http://certtin.com';
export const CIM_CONTEXT_PATH = 'http://certtin.com/CIMServer2/api';
export const UPGRADE_PRO_ACC_PATH = 'http://certtin.com/#/extra/updateaccount/'; */

export const SEND_EMAIL = 'http://dev.fliptin.com/AWS/api/sendEmail';
/*export const USER_SIGNUP_URL = CIM_CONTEXT_PATH + '/login/signup';*/
export const USER_SIGNUP_URL = CERTTIN_CONTEXT_PATH + 'certtin/signUp';
/*export const STRIPE_CONTEXT_PATH = 'http://dev.fliptin.com/Stripe/api/';*/
export const USER_LOGIN_URL = CIM_CONTEXT_PATH + '/login/CreateLogin.do';
export const GET_PASSWORD_POLICY_URL = CIM_CONTEXT_PATH + '/policy/';

export const RESEND_PASSWORD_URL = CIM_CONTEXT_PATH + '/login/';
export const CHANGE_PASSWORD_URL = CIM_CONTEXT_PATH + '/login/';
export const GET_LOGINID_BY_EMAIL_URL = CIM_CONTEXT_PATH + '/login/';
export const GET_PERSON_URL = CIM_CONTEXT_PATH + '/person/';
// export const GET_PERSON_URL = CERTTIN_CONTEXT_PATH + 'person/';
// export const PERSON_UPDATE_URL = CIM_CONTEXT_PATH + '/person/UpdatePerson.do';
export const PERSON_UPDATE_URL = CERTTIN_CONTEXT_PATH + 'person/updatePersonDetails';
// export const ORGANISATION_CREATION_URL = CIM_CONTEXT_PATH + '/organisation/UpdateOrganisation.do';
// export const ORGANISATION_CREATION_URL = CERTTIN_CONTEXT_PATH + 'organisation/updateOrganisation';
export const ORGANISATION_CREATION_URL = CERTTIN_CONTEXT_PATH + 'organisation/updateOrganisation';
// export const GETORGANISATION_DETAILS = CIM_CONTEXT_PATH + '/organisation/';
export const GETORGANISATION_DETAILS = CIM_CONTEXT_PATH + '/organisation/';
export const GETORG_TEMPLATES = CERTTIN_CONTEXT_PATH + '/organisation/';
export const UPDATEORG_TEMPLATES = CERTTIN_CONTEXT_PATH + 'organisation/updateOrganisationTemplate';
/*Subscriptions*/
export const GET_ORGANISATION_DETAILS = CERTTIN_CONTEXT_PATH + 'organisation/';
/*
 export const GET_STRIPE_URL = STRIPE_CONTEXT_PATH;
 export const UPGRAGE_SUBSCRIPTION_URL = STRIPE_CONTEXT_PATH + 'subscription/upgradeSubscription';
 */
// export const UPGRAGE_SUBSCRIPTION_URL = 'http://192.168.1.45:8080/CIMServer2/api/login/upgradeSubscription';
export const GET_STRIPE_URL = CERTTIN_CONTEXT_PATH;
export const UPGRAGE_SUBSCRIPTION_URL = CERTTIN_CONTEXT_PATH + '/stripe/upgradeSubscription';

/*DashBoard*/
export const GET_DASH_BOARD_URL = CERTTIN_CONTEXT_PATH + 'dashboard/';
/*Claims*/
export const GET_CLAIMS_BY_TYPE = CERTTIN_CONTEXT_PATH + 'claim/organisation/';
export const GET_CLAIM_DETAILS = CERTTIN_CONTEXT_PATH + 'claim/';
export const UPDATE_ENDORSE_DECLINE = CERTTIN_CONTEXT_PATH + 'claim/updateClaimStatus';
export const GET_CLAIMS_COUNT = CERTTIN_CONTEXT_PATH + 'claim/';

/*Certificates*/
export const GET_CERTIFICATES_BY_TYPE = CERTTIN_CONTEXT_PATH + 'certificate/organisation/';
export const GET_CERTIFICATE_DETAILS = CERTTIN_CONTEXT_PATH + 'certificate/';

/*Individual*/

export const GET_ORG_LIST_URL = CONTEXT_PATH + '/organisation/GetOrganisationList.do';
export const GET_ORG_DETAIL_URL = CONTEXT_PATH + '/organisation/';
export const GET_DASHBOARD_LIST = CONTEXT_PATH + '/home/GetSummaryStats.do';
export const GET_ORG_APPS_URL = CONTEXT_PATH + '/organisation/orgId/GetOrganisationApplications.do';
export const GET_APP_LIST_URL = CONTEXT_PATH + '/application/GetApplicationList.do';
export const GET_PER_LIST_URL = CONTEXT_PATH + '/person/GetPersonList.do';
export const GET_PER_DETAILS_URL = CONTEXT_PATH + '/person/personId/GetPerson.do';
export const CREATE_ORGANIZATION = CONTEXT_PATH + '/organisation/CreateOrganisation.do';
export const DELETE_APP = CONTEXT_PATH + '/application/';
export const DELETE_PERS = CONTEXT_PATH + '/person/';
export const UPDATE_PERS_DETAILS = CONTEXT_PATH + '/person/UpdatePerson.do';
export const GET_PERS_DETAILS = CONTEXT_PATH + '/person/';
export const CREATE_PERSON_DETAILS = CONTEXT_PATH + '/person/CreatePerson.do';

export const orgTypeUrl = CONTEXT_PATH + '/organisation/GetOrganisationType.do';
export const UPDATE_ORGANIZATION = CONTEXT_PATH + '/organisation/UpdateOrganisation.do';
export const DELETE_ORGANISATION = CONTEXT_PATH + '/organisation/';

export const GET_ALLAPPLICATIONS = CONTEXT_PATH + '/organisation/';
export const GET_APP_DETAILS = CONTEXT_PATH + '/application/';
export const CREATE_APP_DETAILS = CONTEXT_PATH + '/application/CreateApplication.do';
export const UPDATE_APP_DETAILS = CONTEXT_PATH + '/application/UpdateApplication.do';
// export const AWS_PHOTO_UPLOAD_URL = CONTEXT_PATH +'/application/uploadImage?imageName='

export const AWS_PHOTO_UPLOAD_URL = 'http://dev.fliptin.com/AWS/api/ImageProxyWithMultipart?bucketName=flipaccess&folderName=NA&imageName=';

// LOGIN RELATED URLS ============================================>>>
export const CREATE_LOGIN_URL = CONTEXT_PATH + '/login/CreateLogin.do';
export const GET_LOGIN_URL = CONTEXT_PATH + '/login/679223702361/GetLogin.do';
export const GET_LOGIN_LIST_URL = CONTEXT_PATH + '/login/GetLoginList.do';
export const RESET_PASSWORD_URL = CONTEXT_PATH + '/login/679223702361/ResetPassword.do';
export const LOGIN_URL = 'http://oig.fliptin.com:8082/auth';
export const CREATE_AUDIT_URL = CIM_CONTEXT_PATH + '/eventLog/CreateEventLog.do';
export const GET_AUDITLOGS_URL = CIM_CONTEXT_PATH + '/eventLog/GetEventLogList.do';
export const GET_AUDITLOGSLIST_URL = CERTTIN_CONTEXT_PATH + 'audit/getApiAuditList.do';
export const GET_SOURCE_IP = 'dev.fliptin.com';
export const GET_APP_NAME = 'CERTTIN-WEB';
export const GETINDIVIDUALS_URL = CERTTIN_CONTEXT_PATH + 'claim/';
export const INDIVIDUAL_TRACKERDETAILS = CERTTIN_CONTEXT_PATH + 'claim/';
export const CERTIFICATE_UUID = CERTTIN_CONTEXT_PATH + 'certificate/getCertificateByUid';
export const CLAIM_UUID = CERTTIN_CONTEXT_PATH + 'claim/';
export const GET_PERSON_LIST = CIM_CONTEXT_PATH + '/person/GetPersonList.do';
export const GET_TRANSACTIONS_LIST = CERTTIN_CONTEXT_PATH + 'transaction/';
export const GET_UPDATE_CERT_STATUS = CERTTIN_CONTEXT_PATH + 'certificate/updateCertificateStatus/';
export const VERIFIED_LIST = CERTTIN_CONTEXT_PATH + 'share/';

export const GET_SHARED_VERIFICATIONS_URL = CERTTIN_CONTEXT_PATH + 'share/';
export const GETCERTIFICATEDETAILS_URL = CERTTIN_CONTEXT_PATH + 'certificate/';
export const GETCLAIMDETAILS_URL = CERTTIN_CONTEXT_PATH + 'claim/';
export const GETLINKEDUSER_URL = CERTTIN_CONTEXT_PATH + 'certtin/';
// http://dev.fliptin.com/CertTin/api/certificate/905832491774/getCertificateDetails

// http://dev.fliptin.com/CertTin/api/claim/671240929532/getClaimDetails

// My Address Api Key
export const API_KEY = 'VBFBKC9USABU8XV5V8YKN816ACPAGWYJ91';
export const DATEFORMATE = 'short';
/*export const GET_BLOCKCHAIN_DETAILS = 'https://api-ropsten.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=';
 export const GET_BLOCKCHAIN_RECEIPT = 'https://api-ropsten.etherscan.io/api?module=proxy&action=eth_getTransactionReceipt&txhash=';*/

export const GET_BLOCKCHAIN_DETAILS = 'https://api-kovan.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=';
export const GET_BLOCKCHAIN_RECEIPT = 'https://api-kovan.etherscan.io/api?module=proxy&action=eth_getTransactionReceipt&txhash=';

export const GET_ORG_KEY_LIST = CIM_CONTEXT_PATH + '/organisation/GetOrganisationList.do';

export const CVBUILDER_CLAIMS = CERTTIN_CONTEXT_PATH + 'claim/';

export const CVVIEWCLAIMS = CERTTIN_CONTEXT_PATH + 'cv/getCVByUid';
export const UPGRADE_PRO_ACCOUNT = CIM_CONTEXT_PATH + '/organisation/upgradeToProfessionalAccount.do';
export const GET_ORG_PROCESS = CIM_CONTEXT_PATH + '/organisation/';
export const KYC_UPGRADE = CIM_CONTEXT_PATH + '/person/updatePersonKycAndKyv.do';
export const KLAVIYO_DATA = 'https://a.klaviyo.com/api/track?data=';
const ZOHOSERVER = 'http://fliptin.com';
export const ZOHOLEAD_CREATION = ZOHOSERVER + '/v1RestApi/api/ZohoCRM/?xmldata=';

/*template services*/
export const TEMP_CONTEXT_PATH = 'http://dev1.fliptin.com/CertTinV1/api/';
//export const TEMP_CONTEXT_PATH = 'http://certtin.com/CertTinV1/api/';
//export const TEMP_CONTEXT_PATH = 'http://192.168.1.47:5656/CertTinV1/api/';

export const KYC_NEW_SIGNUP_URL = TEMP_CONTEXT_PATH + 'certtin/signUp';
export const GET_NEWCLAIM_DETAILS = TEMP_CONTEXT_PATH + 'claim/';
export const TEMPLATE_URL = TEMP_CONTEXT_PATH + 'certificate/';
export const GET_CERT_TYPES = TEMP_CONTEXT_PATH + 'certificate/getCertificateTypes';
export const APPLYCERTIFICATE_URL = CERTTIN_CONTEXT_PATH + 'certificate/getTemplateContent';
export const GET_ALL_INBOX_LIST = TEMP_CONTEXT_PATH + 'claim/organisation/';
export const ISSUECERTIFICATE = CERTTIN_CONTEXT_PATH + 'certificate/issueCertificate';
export const GET_ISSUED_CERTIFICATES = CERTTIN_CONTEXT_PATH + 'certificate/organisation/';
export const GET_TRANSFERRED_CERTIFICATES = CERTTIN_CONTEXT_PATH + 'certificate/organisation/';
export const GET_ISSUEDCERT_DETAILS = CERTTIN_CONTEXT_PATH + 'certificate/';
export const SEARCH_INDIVIDUALS = CERTTIN_CONTEXT_PATH + 'certtin/searchIndividuals';
export const REVOKE_CERT = CERTTIN_CONTEXT_PATH + 'certificate/revokeCertificate';
export const GET_ORG_LIST = CERTTIN_CONTEXT_PATH + 'claim/searchOrganisation';
export const TRANSFER_CERTIFICATE = CERTTIN_CONTEXT_PATH + 'certificate/shareCertificate';
export const INANDOUTBOXCOUNT = CERTTIN_CONTEXT_PATH + 'dashboard/';
export const SAVE_KYC = CERTTIN_CONTEXT_PATH + 'saveKyvAndKyc';
export const UPGRADE_KYC_PROFESSIONAL = CERTTIN_CONTEXT_PATH + 'organisation/upgradeToProfessional';
export const UPGRADE_MOBILE_KYC = CERTTIN_CONTEXT_PATH + 'certtin/updateAccountStatus';
export const ZOHO_SERVICE_LEAD = 'https://crm.zoho.com/crm/private/xml/Leads/insertRecords?newFormat=1&authtoken=bc821bddc335b6b730c943aa690dac14&scope=crmapi&wfTrigger=true&xmlData='
export const ZOHO_SERVICE_URL = CERTTIN_CONTEXT_PATH + 'createLeadInZoho'
/*export const ZOHO_SERVICE_LEAD = 'https://www.zohoapis.com/crm/v2/Leads'*/
export const UPLOAD_EXCEL = CERTTIN_CONTEXT_PATH + 'certtin/validateSignupFile' ;
export const UPLOAD_SIGNUP_REQ =  CERTTIN_CONTEXT_PATH + 'certtin/uploadSignupRequestData';
export const POST_JOB_SERVICE = CERTTIN_CONTEXT_PATH + 'jobAdvertisement/createJob';
export const GET_JOB_LIST = CERTTIN_CONTEXT_PATH + 'jobAdvertisement/';
