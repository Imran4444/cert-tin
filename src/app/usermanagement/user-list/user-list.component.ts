import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GetListModalRequest } from '../../Modals/getListModal';
import { ApiCalls } from '../../services/services';
import { AddUserComponent } from '../add-user/add-user.component';
import { MdDialog } from '@angular/material';
import { GlobalPopupComponent } from '../../globalPopup/globalPopup.component';
import { ValidationProvider } from '../../providers/validationProvider';
import { KycUpgradePopupComponent } from '../../globalPopup/kyc-upgrade-popup/kyc-upgrade-popup.component';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  firstItem: number;
  lastItem: number;
  placeholdervalue: any;
  rowSelect: boolean;
  pageSize: any;
  currentPage: any;
  paginationRequired: boolean;
  firstNameSort: boolean;
  lastNameSort: boolean;
  emailSort: boolean;
  searchFields: any = {};
  sortingField: any;
  sortingOrder: any;
  totalCount: number;
  searchResult: any = {};
  personList: any;
  personResponse: any;
  pagIndexVal: any;
  checkedIdx: any;
  personDetails: any;
  showLoader: any;
  searchKey: any;
  searchValue: any;
  loginResponse: any;
  organisationId: any;
  sortClickValue: any;
  sortFieldValue: any;

  constructor(public router: Router,
    public apiCall: ApiCalls,
    public dialog: MdDialog,
    public validations: ValidationProvider) {

    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.searchResult = {};
    this.searchFields = {};
    this.currentPage = 1;
    this.firstItem = 1;
    this.totalCount = 0;
    this.lastItem = 10;
    this.sortingField = '';
    this.sortingOrder = '';
    this.pageSize = 10;
  }

  ngOnInit() {
    this.firstNameSort = false;
    this.lastNameSort = false;
    this.emailSort = false;
    this.placeholdervalue = 'Search';
    this.searchValue = '';
    this.pageSize = '10';
    this.rowSelect = false;
    this.showLoader = false;
    this.paginationRequired = true;
    this.searchFields['organisations'] = this.organisationId;
    this.getList();
  }

  selectRow(person, indexValue, ev) {
    console.log(person);
    this.personDetails = person;
    this.pagIndexVal = indexValue;
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getList() {
    this.showLoader = true;
    let personListRequest = new GetListModalRequest(this.currentPage, this.paginationRequired, this.searchFields, this.pageSize, this.sortingField, this.sortingOrder)
    console.log('req...' + JSON.stringify(personListRequest));
    this.apiCall.doGetPersonList(personListRequest).then((result) => {
      if (result) {
        this.showLoader = false;
        this.personResponse = result;
        console.log(this.personResponse);
        this.personList = this.personResponse.listOfPerson;
        this.totalCount = this.personResponse.totalElements;
      }
    }
      , (error) => {
        this.showLoader = false;
        console.log('' + JSON.stringify(error));
      });
  }

  addUser() {
    const dataObj = { status: 'add' };
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '60%',
      data: dataObj
    });
    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
      if (res !== 'noData') {

      }
    });
  }

  viewUser(person) {
    this.personDetails = person;
    const dataObj = { status: 'view', person: this.personDetails };
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '60%',
      data: dataObj
    });
    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
      if (res !== 'noData') {
        this.getList();
      }
    });
  }

  editUser(person, indexValue, ev) {
    ev.stopPropagation();
    this.personDetails = person;
    const dataObj = { status: 'edit', person: this.personDetails };
    const dialogRef = this.dialog.open(AddUserComponent, {
      width: '60%',
      data: dataObj,
      disableClose: true
    });
    /* const dialogRef = this.dialog.open(KycUpgradePopupComponent, {
      width: '100%', 
      height: '100%',
    }); */
    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
      if (res !== 'noData') {
        this.getList();
        this.rowSelect = false;
        this.checkedIdx = null;
        const resultData = this.validations.errorPopup('Success', 'Updated Successfully', '');
        const dialogRef1 = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
       
      }
    });
  }

  getSearchResult() {
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    if (this.searchValue && this.searchKey) {
      this.searchFields[this.searchKey] = this.searchValue;
      this.getList();
    } else {
      this.searchFields = {};
      this.searchFields['organisations'] = this.organisationId;
      this.getList();
    }
  }

  selectedSearch(selectedKey) {
    this.checkedIdx = null;
    this.placeholdervalue = selectedKey;
    if (selectedKey === 'First Name') {
      this.searchKey = 'first_name';
    } else if (selectedKey === 'Last Name') {
      this.searchKey = 'last_name';
    } else if (selectedKey === 'Email') {
      this.searchKey = 'emails.email';
    }
    console.log(this.searchKey);
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    console.log(this.pageSize);
    this.getList();
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'first_name') {
      this.firstNameSort = true;
      this.lastNameSort = false;
      this.emailSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'last_name') {
      this.firstNameSort = false;
      this.lastNameSort = true;
      this.emailSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'emails.email') {
      this.emailSort = true;
      this.firstNameSort = false;
      this.lastNameSort = false;
      this.SortClicking();
    }
    else {
      this.firstNameSort = false;
      this.lastNameSort = false;
      this.emailSort = false;
    }

  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = '';
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      if (this.firstItem < 0) {
        this.firstItem = 1;
      }
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = '';
      console.log('page index..' + this.pagIndexVal)
      console.log('checked value' + this.checkedIdx)
    }
    console.log(this.currentPage);
    console.log(this.personResponse.totalPages);
    if (this.personResponse.totalPages > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getList();
    }
  }

}
