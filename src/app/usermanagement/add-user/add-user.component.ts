import { Component, Inject, OnInit } from '@angular/core';
import { PersonModal } from '../../Modals/personModal';
import { EMAIL_PATTERN } from '../../providers/constants';
import { Countriescodes } from "../../providers/countriescodes";
import { MD_DIALOG_DATA, MdDialogRef } from "@angular/material";
import { Router } from "@angular/router";
import { DatePipe } from "@angular/common";
import { AWS_PHOTO_UPLOAD_URL } from "../../endpoint";
import { Http } from "@angular/http";
import { auditService } from "../../providers/audit.service";
import { GlobalPopupComponent } from "../../globalPopup/globalPopup.component";
import { PersonUpdateRequest } from "../../Modals/personUpdateRequestModal";
import { ApiCalls } from "../../services/services";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss']
})
export class AddUserComponent implements OnInit {
  userModal: any;
  public updateUserLoading: any;
  public showLoader: any;
  public emailPattern: any;
  countryDetails: any;
  disable: any;
  personDetails: any;
  logoUrl: any;
  file: any;
  loginResponseStorage: any;
  nopicker: any;
  picker: any;
  addUserForm: FormGroup;
  submitted: any;
  private formDissabled: boolean;
  private formEmailPhone: boolean;
  btnshowLoader: any;
  auditReq: any;

  constructor(public countriesCodes: Countriescodes,
              public dialogRef: MdDialogRef<AddUserComponent>,
              public router: Router,
              @Inject(MD_DIALOG_DATA) public data: any,
              public datePipe: DatePipe,
              public http: Http,
              public auditHistory: auditService,
              public apiCalls: ApiCalls,
              public formBuilder: FormBuilder,
              ) {
    this.userModal = new PersonModal('', '', '', '', '', '', '');
    this.emailPattern = EMAIL_PATTERN;

  }

  ngOnInit() {
    this.btnshowLoader = false;
    console.log(this.data);
    if (this.data.status) {
      if (this.data.status === 'view') {
        this.formDissabled = true;
      }else if (this.data.status === 'edit'){
        this.formEmailPhone = true;
      }
      this.addUserForm = this.formBuilder.group({
        title: this.formBuilder.control({value: '', disabled: this.formDissabled}),
        firstName: this.formBuilder.control({value: '', disabled: this.formDissabled}, [Validators.required]),
        lastName: this.formBuilder.control({value: '', disabled: this.formDissabled}, [Validators.required]),
        dob: this.formBuilder.control({value: '', disabled: this.formDissabled}, [Validators.required]),
        emailId: this.formBuilder.control({value: '', disabled: this.formDissabled || this.formEmailPhone}, [Validators.required, Validators.email]),
        countryCode: this.formBuilder.control({value: '', disabled: this.formDissabled}, [Validators.required]),
        mobile: this.formBuilder.control({value: '', disabled: this.formDissabled ||  this.formEmailPhone}, [Validators.required]),
      });
    }
    this.loginResponseStorage = JSON.parse(localStorage.getItem('loginResponse'));
    this.updateUserLoading = false;
    this.showLoader = false;
    this.userModal.title = 'defaultTitle';
    this.userModal.countryCode = 'defaultCode';
    this.countryDetails = this.countriesCodes.getCountries();
    if (this.data.status == 'view' || this.data.status == 'edit') {
      this.personDetails = this.data.person;
      this.userModal.title = this.personDetails.title;
      console.log(this.personDetails.title)
      this.userModal.firstName = this.personDetails.firstName;
      this.userModal.lastName = this.personDetails.lastName;
      this.userModal.dateOfBirth = this.datePipe.transform(this.personDetails.dateOfBirth, 'd/M/yy');
      this.userModal.email = this.personDetails.emails[0].email;
      this.userModal.countryCode = this.personDetails.phones[0].countryCode;
      this.userModal.phoneNumber = this.personDetails.phones[0].number;
      if (this.personDetails.pictureUri) {
        this.logoUrl = this.personDetails.pictureUri;
      }
      console.log(this.personDetails);
    } else {
    }

    if (this.data.status == 'view') {
      this.disable = true;
    } else {
      this.disable = false;
    }
  }

  get f() {
    return this.addUserForm.controls;
  }

  saveDetails() {
    this.btnshowLoader = true;
    this.submitted = true;
    if (this.addUserForm.valid) {
      if (this.data.status == 'edit') {
        // this.createAuditLog('Edit User');
        this.updateDetails();
      } else {
        this.dialogRef.close();
       // this.createAuditLog('Add User');
      }
    }
  }

  updateDetails() {
    this.btnshowLoader = true;
    const personcustomFields = {};
    if (this.personDetails.idify) {
      const personcustomFields = this.personDetails.idify;
    } else {
      const personcustomFields = {};
    }
    const dob = this.datePipe.transform(this.userModal.dateOfBirth, 'y-dd-MM 00:00:00');
    console.log(dob);
    this.auditReq = this.auditHistory.userAudit('Update Person');
    const personUpdateRequest = new PersonUpdateRequest(this.auditReq, this.personDetails.id, this.userModal.title, true, this.logoUrl, this.userModal.firstName,
      this.userModal.lastName, dob, this.personDetails.address, personcustomFields,
      this.personDetails.emails, this.personDetails.phones, this.personDetails.organisations, false);
    this.apiCalls.doPersonUpdate(personUpdateRequest).then((result) => {
      this.btnshowLoader = false;
      console.log(result);
      this.dialogRef.close('Updated Sucessfully');
    }, (error) => {
      this.btnshowLoader = false;
      console.log(error);
      this.dialogRef.close();
    });
  }

  uploadPicture(event) {
    this.showLoader = true;
    let formData: FormData = new FormData();
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];
    this.http.post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData).map((res) => res.json())
      .subscribe((data) => {
        this.logoUrl = data.imageURL;
        this.personDetails.pictureUri = this.logoUrl;
        this.showLoader = false;
        // alert('Image Uploaded Successfully');
      }, (err) => {
        this.showLoader = false;
        alert('Oops !! Image Uploading Failed, Please Try Again');
      });
  }

  cancel() {
    this.dialogRef.close('noData');
  }

  numberOnly(event) {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  createAuditLog(verb) {
    this.auditHistory.userAudit(verb);
  }
}
