import {Injectable} from "@angular/core";

@Injectable()

export class LeftNavigationItemList{
  listOfItems:any[];
  constructor(){
    this.listOfItems=[];
  }
  getListOfnavigationItems(authorities){
    this.listOfItems=[];
   for( var i=0; authorities.length>i; i++){
     if(authorities[i].permission=='IssueCertificate'){
       let linkObj={
         "link": "/app/organization/requestlist",
         'linktext': 'Requested',
         'icon': 'assignment_late'
       }
       this.listOfItems.push(linkObj);
     }else if(authorities[i].permission=='VerifyCertificate'){
     let linkObj={
       "link": "/app/user/certlist",
       'linktext': 'Inbox',
       'icon': 'verified_user'
     }
       this.listOfItems.push(linkObj);
     }
   }
     return this.listOfItems;
  }
}
