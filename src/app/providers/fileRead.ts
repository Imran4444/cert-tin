import {  Injectable } from '@angular/core';
@Injectable()
export class FileReadJson{
  constructor(){}
  readeFile(inputValue: any){
   return new Promise((resolve, reject)=>{
     var file: File = inputValue.files[0];
     var myReader: FileReader = new FileReader();
     myReader.readAsText(file);
     myReader.onload = function (e) {
       console.log(e)
      resolve (JSON.parse(myReader.result))
     }
   })
  }
}
