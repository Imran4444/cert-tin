import { Injectable } from "@angular/core";
import { DatePipe } from "@angular/common";
import { AuditModal } from "../Modals/auditModal";
import { LoginService } from "../extra-pages/login/loginService";
import { WebModal } from "../Modals/webModel";
import { Ng2DeviceService } from 'ng2-device-detector';

@Injectable()

export class auditService {
  deviceInfo = null;
  loginResult: any;
  localIP: string;
  browser: string;
  timeZone: string;
  appName: string;
  sourseIP: string;
  location: string;
  requestTime: string;
  auditLog: any;
  web: any;
  browserVersion: any;
  orgname: any;
  public lat: any;
  public lang: any;
  public os: any;
  public osVersion: any;
  public userAgent: any;

  constructor(public datePipe: DatePipe,
              public loginservice: LoginService,
              private deviceService: Ng2DeviceService) {
  }

  userAudit(verb: any) {
    console.log(verb);
    this.loginResult = JSON.parse(localStorage.getItem('loginResponse'));
    if (this.loginResult) {

      this.orgname = this.loginResult.orgName;
      this.localIP = localStorage.getItem('localeIP');
      // this.browser = localStorage.getItem('browserDetail');
      // this.browserVersion = localStorage.getItem('browserVersion');
      this.timeZone = localStorage.getItem('timeZoneID');
      this.lat = localStorage.getItem('latitude');
      this.lang = localStorage.getItem('longitude');
      this.appName = 'CERTTIN-WEB';
      this.sourseIP = 'dev.fliptin.com';
      this.location = localStorage.getItem('locationName');
      this.deviceInfo = this.deviceService.getDeviceInfo();
      console.log('Device Info' + JSON.stringify(this.deviceInfo));
      this.os = this.deviceInfo.os;
      this.osVersion = this.deviceInfo.os_version;
      this.userAgent = this.deviceInfo.userAgent;
      this.browser = this.deviceInfo.browser;
      this.browserVersion = this.deviceInfo.browser_version;
      // this.requestTime = moment().format();
      const newDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd');
      const newhour = this.datePipe.transform(new Date(), 'HH:mm');
      this.requestTime = newDate + ' ' + newhour + ':00.521Z';
      console.log(this.requestTime);
      this.web = new WebModal(this.browser, this.browserVersion, this.sourseIP, this.os, this.osVersion, this.userAgent);

      this.auditLog = new AuditModal(this.orgname, this.localIP, this.lat, this.lang, this.timeZone, this.loginResult.personId, this.loginResult.username, verb, this.web);
      // this.auditLog = new AuditModal(this.appName, this.browser, this.localIP, this.location, this.loginResult.orgName, this.requestTime, this.sourseIP, this.timeZone, this.loginResult.personId, this.loginResult.username, verb);
      console.log(this.auditLog);
      // this.loginservice.createAudit(auditLog).then((result) => {
      //   console.log('Audit Response' + JSON.stringify(result));
      // }, (error) => {
      //   console.log('Audit Response ERR' + JSON.stringify(error));
      // });
    }
    return this.auditLog;
  }
}
