import { Injectable } from '@angular/core';

@Injectable()
export class ValidationProvider {
  constructor() {
  }

  registarion(email, phoneNumber, routePage) {
    const result = {
      title: 'Registration Confirmation',
      //  message: `We have sent a one time passcode (OTP) to   <b>` + phoneNumber + `</b>  and an email confirmation link to.<b>` + email + `</b> <br> Please click on the verification link in your email to activate your account using the OTP code sent by SMS.`,
      //message: `We have sent a one time passcode (OTP) and an email confirmation link to <b>` + email + `</b>. Please click on the verification link in your email to activate your account and using the OTP code sent to your email.`,
      message: `You’ve almost completed registration! Check your email for  OTP to activate your account`,
      route: routePage
    }
    return result;
  }

  errorPopup(titleData, errorMessage, routePage) {
    const result = {
      title: titleData,
      message: errorMessage,
      route: routePage
    }
    return result;
  }

  hashSuccess() {
    let verificationResult = {'message': 'Certificate Issued successfully ', 'key': 'Success', 'success': true}
    return verificationResult
  }

  endrosedSuccess() {
    let verificationResult = {'message': 'Endorsed successfully ', 'key': 'Claim Endorse', 'success': true}
    return verificationResult
  }

  certificateSuccess() {
    let verificationResult = {
      'message': 'Certificate issued successfully. ',
      'key': 'Issue Certificate',
      'success': true
    }
    return verificationResult
  }

  hashFailure() {
    let verificationResult = {'message': 'Hash  verification failed', 'key': 'Failed', 'success': false}
    return verificationResult
  }

  transactionSuccess() {
    let verificationResult = {'message': 'Transaction verified successfully', 'key': 'Success', 'success': true}
    return verificationResult
  }

  transactionfailur() {
    let verificationResult = {
      'message': 'Transaction detail from network not matched',
      'key': 'Failed',
      'success': false
    }
    return verificationResult
  }

  networkFailure() {
    let verificationResult = {'message': 'No response from network', 'key': 'Failed', 'success': false}
    return verificationResult
  }

  successClass() {
    let className = ['custom-circle-box-true']
    return className
  }

  errorClass() {
    let className = ['custom-circle-box-false']
    return className
  }

  issueSuccess() {
    let verificationResult = {'message': 'Certificate Issued Successfully', 'key': 'Success', 'success': true}
    return verificationResult
  }

  issueFailure() {
    let verificationResult = {'message': 'Please Try Again', 'key': 'Failed', 'success': false}
    return verificationResult
  }
  setMailSuccess() {
    let verificationResult = {'message': 'Details have been submitted successfully. We will get in touch with you soon. ', 'key': 'Cert Tin Contact Us', 'success': true}
    return verificationResult
  }
}


