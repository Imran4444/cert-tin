/**
 * Created by Hello on 11/2/2017.
 */
export class GetOrginizationComponent{
  adminOrgsmangResp: any;

  constructor() {
    this.adminOrgsmangResp = null;
  }

  public getOrgMangResData() {
    return this.adminOrgsmangResp;
  }

  public setOrgMangResData(value: any) {
    return this.adminOrgsmangResp = value;

  }
}
