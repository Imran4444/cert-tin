export const FLIPCERTS_PORT = 'http://certs.fliptin.com/FlipCerts/api';
// export const FLIPCERTS_PORT = 'http://103.60.212.198:8080/FlipCerts/api';
export const EMAIL_PATTERN =  "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
  + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
  + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
  + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
  + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
  + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
export const WEBSITE_PATTERN = "^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+";
export const  GET_CERTIFICATESREQUEST_BY_UNIID = FLIPCERTS_PORT + '/getCertificateRequestbyUniIdAndStatus';
export const ISSUE_CERTIFICATE_BY_UNIVERSITY= FLIPCERTS_PORT+ '/issueCertificate'
export const SEND_RAW_TRANSACTION= FLIPCERTS_PORT+'/sendRawTransaction'
export const  GET_ORG_CERTIFICATES = FLIPCERTS_PORT+"/getCertificateRequestsByOrgId?OrganizationId="
export const GET_ORG_VIEWCERTIFICATE = FLIPCERTS_PORT+"/getDigitalCertificate?certificateRequestId="
export const GET_ORG_VERIFIEDSTATUS = FLIPCERTS_PORT+"/verifyCertificate?certificateRequestedId="
export const UNIVERSITY_ID = 'UCT';
export const ORGREQUEST_LIST= FLIPCERTS_PORT+"/getCertificateRequestsByOrgId"
export const ORGANIZATION_ID= '12345';
// GIF
export const LOADING_GIF = 'http://d2btkkuyusotz5.cloudfront.net/flipcerts/loading.gif';
// Images
export const LEFT_ARROW_IMAGE = 'http://d2btkkuyusotz5.cloudfront.net/flipcerts/arrow_left.png';
export const  GETREQESTPROFILE= FLIPCERTS_PORT+ '/getUserDetailsByUseruuid?useruuid='
export const RIGHT_ARROW_IMAGE = 'http://d2btkkuyusotz5.cloudfront.net/flipcerts/arrow.png';
export const GETBALANCE_API= 'VBFBKC9USABU8XV5V8YKN816ACPAGWYJ91';



