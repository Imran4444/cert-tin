export class IndustryTypes {
  public industryData;

  constructor() {

  }

  getIndustries() {
    this.industryData = [
      {
        group: 'corp',
        industryName: 'Accounting'
      }, {
        group: 'tech',
        industryName: 'Airlines/Aviation'
      }, {
        group: 'org',
        industryName: 'Alternative Dispute Resolution'
      }, {
        group: 'hlth',
        industryName: 'Alternative Medicine'
      }, {
        group: 'art',
        industryName: 'Animation'
      }, {
        group: 'good',
        industryName: 'Apparel & Fashion'
      }, {
        group: 'cons',
        industryName: 'Architecture & Planning'
      }, {
        group: 'art',
        industryName: 'Arts and Crafts'
      }, {
        group: 'man',
        industryName: 'Automotive'
      }, {
        group: 'gov',
        industryName: 'Aviation & Aerospace'
      }, {
        group: 'fin',
        industryName: 'Banking'
      }, {
        group: 'gov',
        industryName: 'Biotechnology'
      }, {
        group: 'med',
        industryName: 'Broadcast Media'
      }, {
        group: 'cons',
        industryName: 'Building Materials'
      }, {
        group: 'corp',
        industryName: 'Business Supplies and Equipment'
      }, {
        group: 'fin',
        industryName: 'Capital Markets'
      }, {
        group: 'man',
        industryName: 'Chemicals'
      }, {
        group: 'org',
        industryName: 'Civic & Social Organization'
      }, {
        group: 'cons',
        industryName: 'Civil Engineering'
      }, {
        group: 'corp',
        industryName: 'Commercial Real Estate'
      }, {
        group: 'tech',
        industryName: 'Computer & Network Security'
      }, {
        group: 'med',
        industryName: 'Computer Games'
      }, {
        group: 'tech',
        industryName: 'Computer Hardware'
      }, {
        group: 'tech',
        industryName: 'Computer Networking'
      }, {
        group: 'tech',
        industryName: 'Computer Software'
      }, {
        group: 'cons',
        industryName: 'Construction'
      }, {
        group: 'good',
        industryName: 'Consumer Electronics'
      }, {
        group: 'good',
        industryName: 'Consumer Goods'
      }, {
        group: 'org',
        industryName: 'Consumer Services'
      }, {
        group: 'good',
        industryName: 'Cosmetics'
      }, {
        group: 'agr',
        industryName: 'Dairy'
      }, {
        group: 'gov',
        industryName: 'Defense & Space'
      }, {
        group: 'art',
        industryName: 'Design'
      }, {
        group: 'edu',
        industryName: 'Education Management'
      }, {
        group: 'edu',
        industryName: 'E-Learning'
      }, {
        group: 'good',
        industryName: 'Electrical/Electronic Manufacturing'
      }, {
        group: 'med',
        industryName: 'Entertainment'
      }, {
        group: 'org',
        industryName: 'Environmental Services'
      }, {
        group: 'corp',
        industryName: 'Events Services'
      }, {
        group: 'gov',
        industryName: 'Executive Office'
      }, {
        group: 'corp',
        industryName: 'Facilities Services'
      }, {
        group: 'agr',
        industryName: 'Farming'
      }, {
        group: 'fin',
        industryName: 'Financial Services'
      }, {
        group: 'art',
        industryName: 'Fine Art'
      }, {
        group: 'agr',
        industryName: 'Fishery'
      }, {
        group: 'rec',
        industryName: 'Food & Beverages'
      }, {
        group: 'good',
        industryName: 'Food Production'
      }, {
        group: 'org',
        industryName: 'Fund-Raising'
      }, {
        group: 'good',
        industryName: 'Furniture'
      }, {
        group: 'rec',
        industryName: 'Gambling & Casinos'
      }, {
        group: 'cons',
        industryName: 'Glass, Ceramics & Concrete'
      }, {
        group: 'gov',
        industryName: 'Government Administration'
      }, {
        group: 'gov',
        industryName: 'Government Relations'
      }, {
        group: 'art, med',
        industryName: 'Graphic Design'
      }, {
        group: 'hlth, rec',
        industryName: 'Health, Wellness and Fitness'
      }, {
        group: 'edu',
        industryName: 'Higher Education'
      }, {
        group: 'hlth',
        industryName: 'Hospital & Health Care'
      }, {
        group: 'rec, serv, tran',
        industryName: 'Hospitality'
      }, {
        group: 'corp',
        industryName: 'HospitalityHuman Resources'
      }, {
        group: 'corp, good, tran',
        industryName: 'Import and Export'
      }, {
        group: 'org, serv',
        industryName: 'Individual & Family Services'
      }, {
        group: 'cons, man',
        industryName: 'Industrial Automation'
      }, {
        group: 'med, serv',
        industryName: 'Information Services'
      }, {
        group: 'tech',
        industryName: 'Information Technology and Services'
      }, {
        group: 'fin',
        industryName: 'Insurance'
      }, {
        group: 'gov',
        industryName: 'International Affairs'
      }, {
        group: 'gov, org, tran',
        industryName: 'International Trade and Development'
      }, {
        group: 'tech',
        industryName: 'Internet'
      }, {
        group: 'fin',
        industryName: 'Investment Banking'
      }, {
        group: 'fin',
        industryName: 'Investment Management'
      }, {
        group: 'gov, leg',
        industryName: 'Judiciary'
      }, {
        group: 'gov, leg',
        industryName: 'Law Enforcement'
      }, {
        group: 'leg',
        industryName: 'Law Practice'
      }, {
        group: 'leg',
        industryName: 'Legal Services'
      }, {
        group: 'gov, leg',
        industryName: 'Legislative Office'
      }, {
        group: 'rec, serv, tran',
        industryName: 'Leisure, Travel & Tourism'
      }, {
        group: 'med, rec, serv',
        industryName: 'Libraries'
      }, {
        group: 'corp, tran',
        industryName: 'Logistics and Supply Chain'
      }, {
        group: 'good',
        industryName: 'Luxury Goods & Jewelry'
      }, {
        group: 'man',
        industryName: 'Machinery'
      }, {
        group: 'corp',
        industryName: 'Management Consulting'
      }, {
        group: 'tran',
        industryName: 'Maritime'
      }, {
        group: 'corp',
        industryName: 'Market Research'
      }, {
        group: 'corp, med',
        industryName: 'Marketing and Advertising'
      }, {
        group: 'cons, gov, man',
        industryName: 'Mechanical or Industrial Engineering'
      }, {
        group: 'med, rec',
        industryName: 'Media Production'
      }, {
        group: 'hlth',
        industryName: 'Medical Devices'
      }, {
        group: 'hlth',
        industryName: 'Medical Practice'
      }, {
        group: 'hlth',
        industryName: 'Mental Health Care'
      }, {
        group: 'gov',
        industryName: 'Military'
      }, {
        group: 'man',
        industryName: 'Mining & Metals'
      }, {
        group: 'art, med, rec',
        industryName: 'Motion Pictures and Film'
      }, {
        group: 'art, med, rec',
        industryName: 'Museums and Institutions'
      }, {
        group: 'art, rec',
        industryName: 'Music'
      }, {
        group: 'gov, man, tech',
        industryName: 'Nanotechnology'
      }, {
        group: 'med, rec',
        industryName: 'Newspapers'
      }, {
        group: 'org',
        industryName: 'Non-Profit Organization Management'
      }, {
        group: 'man',
        industryName: 'Oil & Energy'
      }, {
        group: 'med',
        industryName: 'Online Media'
      }, {
        group: 'corp',
        industryName: 'Outsourcing/Offshoring'
      }, {
        group: 'serv, tran',
        industryName: 'Package/Freight Delivery'
      }, {
        group: 'good, man',
        industryName: 'Packaging and Containers'
      }, {
        group: 'man',
        industryName: 'Paper & Forest Products'
      }, {
        group: 'art, med, rec',
        industryName: 'Performing Arts'
      }, {
        group: 'hlth, tech',
        industryName: 'Pharmaceuticals'
      }, {
        group: 'org',
        industryName: 'Philanthropy'
      }, {
        group: 'art, med, rec',
        industryName: 'Photography'
      }, {
        group: 'man',
        industryName: 'Plastics'
      }, {
        group: 'gov, org',
        industryName: 'Political Organization'
      }, {
        group: 'edu',
        industryName: 'Primary/Secondary Education'
      }, {
        group: 'med, rec',
        industryName: 'Printing'
      }, {
        group: 'corp',
        industryName: 'Professional Training & Coaching'
      }, {
        group: 'corp, org',
        industryName: 'Program Development'
      }, {
        group: 'gov',
        industryName: 'Public Policy'
      }, {
        group: 'corp',
        industryName: 'Public Relations and Communications'
      }, {
        group: 'gov',
        industryName: 'Public Safety'
      }, {
        group: 'med, rec',
        industryName: 'Publishing'
      }, {
        group: 'man',
        industryName: 'Railroad Manufacture'
      }, {
        group: 'agr',
        industryName: 'Ranching'
      }, {
        group: 'cons, fin, good',
        industryName: 'Real Estate'
      }, {
        group: 'rec, serv',
        industryName: 'Recreational Facilities and Services'
      }, {
        group: 'org, serv',
        industryName: 'Religious Institutions'
      }, {
        group: 'gov, man, org',
        industryName: 'Renewables & Environment'
      }, {
        group: 'edu, gov',
        industryName: 'Research'
      }, {
        group: 'rec, serv',
        industryName: 'Restaurants'
      }, {
        group: 'good, man',
        industryName: 'Retail'
      }, {
        group: 'corp, org, serv',
        industryName: 'Security and Investigations'
      }, {
        group: 'tech',
        industryName: 'Semiconductors'
      }, {
        group: 'tech',
        industryName: 'Semiconductors'
      }, {
        group: 'man',
        industryName: 'Shipbuilding'
      }, {
        group: 'good, rec',
        industryName: 'Sporting Goods'
      }, {
        group: 'rec',
        industryName: 'Sports'
      }, {
        group: 'corp',
        industryName: 'Staffing and Recruiting'
      }, {
        group: 'good',
        industryName: 'Supermarkets'
      }, {
        group: 'gov, tech',
        industryName: 'Telecommunications'
      }, {
        group: 'man',
        industryName: 'Textiles'
      }, {
        group: 'gov, org',
        industryName: 'Think Tanks'
      }, {
        group: 'good',
        industryName: 'Tobacco'
      }, {
        group: 'corp, gov, serv',
        industryName: 'Translation and Localization'
      }, {
        group: 'tran',
        industryName: 'Transportation/Trucking/Railroad'
      }, {
        group: 'man',
        industryName: 'Utilities'
      }, {
        group: 'fin, tech',
        industryName: 'Venture Capital & Private Equity'
      }, {
        group: 'hlth',
        industryName: 'Veterinary'
      }, {
        group: 'tran',
        industryName: 'Warehousing'
      }, {
        group: 'good',
        industryName: 'Wholesale'
      }, {
        group: 'good, man, rec',
        industryName: 'Wine and Spirits'
      }, {
        group: 'tech',
        industryName: 'Wireless'
      }, {
        group: '	art, med, rec',
        industryName: 'Writing and Editing'
      },

    ];
    return this.industryData;
  }
}
