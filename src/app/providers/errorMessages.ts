import {Injectable} from '@angular/core';
/**
 * Created by Soulstice on 8/1/2018.
 */
@Injectable()
export class ErrorMessages {
  constructor() {
  }

  signupError() {
    const signupError = {'title': 'Registration Fail', 'message': 'message'}
    return signupError;
  }

  accountLoginError() {
    const accountloginError = {'title': 'Invalid email or password', 'message': 'custom message'}
    return accountloginError;
  }

  otpLoginError() {
    const otploginError = {'title': 'Invalid email or OTP', 'message': 'OTP  or email  you have entered is incorrect'}
    return otploginError;
  }

  userNotFoundError() {
    const usernotfoundError = {'title': 'User not found', 'message': 'The entered email is not exists. Please enter registered email'}
    return usernotfoundError;
  }
  forgotUserNotFound(){
    const usernotfoundError = {'title': 'User not found', 'message': 'User not exists with the entered email id. Please try to create an account.'}
    return usernotfoundError;
  }

  sendOtpError() {
    const sendotpError = {'title': 'Send OTP Fail', 'message': 'custom message'}
    return sendotpError;
  }

  sendOtpSuccess() {
    const sendotpSuccess = {'title': 'OTP Resent', 'message': 'We have resent your OTP. Please check your email.'};
    return sendotpSuccess;
  }

  invalidPasswordFormat() {
    const invalidpasswordformat = {'title': 'Invalid password format', 'message': 'custom message'}
    return invalidpasswordformat;
  }
  organizationSuccess(){
    const invalidpasswordformat = {'title': 'Profile update', 'message': 'Organization information has been updated successfully'}
    return invalidpasswordformat;
  }
  organizationFail(){
    const invalidpasswordformat = {'title': 'Profile update', 'message': 'Organization information update has been Failed'}
    return invalidpasswordformat;
  }
  personSuccess(){
    const invalidpasswordformat = {'title': 'Profile update', 'message': 'Profile personal information has been updated successfully'}
    return invalidpasswordformat;
  }
  personFail(){
    const invalidpasswordformat = {'title': 'Profile update', 'message': 'Profile personal information update has been Failed'}
    return invalidpasswordformat;
  }
  accountLoginBadCredentials(){
    const accountloginError = {'title': 'Invalid user details', 'message': 'The username or password you have entered is incorrect'}
    return accountloginError;
  }
  accountLoginunAuth(){
    const accountloginError = {'title': 'Login failed ', 'message': 'User not exists with the entered email id. Please try to create an account'}
    return accountloginError;
  }
  signUpOrgExists(){
    const accountloginError = {'title': 'Registration Fail', 'message': 'Organization already exists.'}
    return accountloginError;
  }
  signupServerError(){
    const accountloginError = {'title': 'Registration Fail', 'message': 'Server error.'}
    return accountloginError;
  }
  signupEmailExists(){
    const accountloginError = {'title': 'Registration Fail', 'message': 'It looks like you have already registered. Please try to login.'}
    return accountloginError;
  }
  emailFailure(){
    const accountloginError = {'title': 'Login Fail', 'message': 'Please verify your email to login'}
    return accountloginError;
  }

  endorseSuccessful(){
    const accountloginError = {'title': 'Endorse', 'message': 'Claim has been Endorsed Sucessfully'}
    return accountloginError;
  }
  claimDecline(){
    const accountloginError = {'title': 'Decline', 'message': 'Claim has been Declined'}
    return accountloginError;
  }
}
