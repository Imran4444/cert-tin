import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkAttestationsComponent } from './work-attestations.component';

describe('WorkAttestationsComponent', () => {
  let component: WorkAttestationsComponent;
  let fixture: ComponentFixture<WorkAttestationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkAttestationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkAttestationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
