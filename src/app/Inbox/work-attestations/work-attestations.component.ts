import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../services/services';
import {MdDialog} from '@angular/material';
import {Router} from "@angular/router";
import * as moment from 'moment';

@Component({
  selector: 'app-work-attestations',
  templateUrl: './work-attestations.component.html',
  styleUrls: ['./work-attestations.component.scss']
})
export class WorkAttestationsComponent implements OnInit {
  searchFields: any = {};
  sortingField: any;
  sortingOrder: any;
  totalCount: number;
  totalPagesCount: any;
  searchResult: any = {};
  firstItem: number;
  lastItem: number;
  placeholdervalue: any;
  pageSize: any;
  currentPage: any;
  pagIndexVal: any;
  checkedIdx: any;
  searchKey: any;
  searchValue: any;
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  inoutboxType: any;
  status: any;
  workAttestationResponse: any;
  workAttestationListResponse: any;
  certificateStatus: any;
  sortClickValue: any;
  sortFieldValue: any;
  personNameSort: boolean;
  roleSort: boolean;
  mm: any;

  constructor(public dialog: MdDialog,
              public apiCalls: ApiCalls,
              public router: Router) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.searchResult = {};
    this.searchFields = {};
    this.currentPage = 1;
    this.firstItem = 1;
    this.totalCount = 0;
    this.lastItem = 10;
    this.sortingField = '';
    this.sortingOrder = '';
    this.pageSize = 10;
    this.mm = moment;
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.pageSize = '10';
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.inoutboxType) {
      if (this.organisationId) {
        this.getWorkAttestationList();
      }
    }
  }

  getWorkAttestationList() {
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSED';
    }
    this.showLoader = true;
    this.apiCalls.doGetCertificateList(this.organisationId, 'WORKATTESTATION', this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      this.showLoader = false;
      console.log('WorkA ' + JSON.stringify(result));
      this.workAttestationResponse = result;
      this.workAttestationListResponse = this.workAttestationResponse.certificates;
      this.totalCount = this.workAttestationResponse.totalElements;
      this.totalPagesCount = this.workAttestationResponse.totalPages;
    }, (error) => {
      this.showLoader = false;
      console.log('' + JSON.stringify(error));
    });
  }

  selectRow(workAttestation, indexValue, ev) {
    this.pagIndexVal = indexValue;
    this.certificateStatus = workAttestation.status;
    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(workAttestation));
    localStorage.setItem('CertificateType', 'WorkAttestation');
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  goToView(workAttestation) {
    this.certificateStatus = workAttestation.status;
    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(workAttestation));
    localStorage.setItem('CertificateType', 'WorkAttestation');
    if (this.certificateStatus === 'OPEN') {
      this.router.navigate(['app/cert-inbox-view']);
    } else if (this.certificateStatus === 'CLOSED') {
      this.router.navigate(['app/certificate']);
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getWorkAttestationList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Role') {
      this.placeholdervalue = 'Role';
      this.searchKey = 'role';
    } else {
      this.placeholdervalue = 'Certificate status';
      this.searchKey = 'certificateStatus';
    }
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getWorkAttestationList();
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    this.sortingField = sortingType;
    if (this.sortingField === 'personName') {
      this.personNameSort = true;
      this.roleSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'role') {
      this.personNameSort = false;
      this.roleSort = true;
      this.SortClicking();
      // this.sortingOrder = sortingOrder;
      // console.log('sorting order...' + sortingOrder + 'sorting type...' + sortingType);

    }
  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getWorkAttestationList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getWorkAttestationList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getWorkAttestationList();
    }
  }

}
