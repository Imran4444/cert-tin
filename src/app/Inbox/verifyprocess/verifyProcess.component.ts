import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http } from '@angular/http';
import { MD_DIALOG_DATA, MdDialog, MdDialogRef } from '@angular/material';
import { ValidationProvider } from '../../providers/validationProvider';
import { DatePipe } from '@angular/common';
import { VerifyModel } from '../../Modals/verifyModel';

@Component({
  selector: 'app-verify-Process',
  templateUrl: '../verifyprocess/verifyProcess.component.html',
  styleUrls: ['../verifyprocess/verifyProcess.component.css']
})
export class VerifyProcessComponent implements OnInit {
  extractKey: boolean;
  extractTranID: boolean;
  extractDC: boolean;
  tranDetails: boolean
  typeProgress: string
  step1start: boolean;
  step2start: boolean
  keyProgress: number
  titile: any;
  jsonText: any;
  recpID: string;
  issuerID: string;
  blockChainID: string;
  hash: string;
  from: string;
  to: string;
  blockChainResponse: any
  public verifyDetails: any;
  public step3start: boolean;
  public step4start: boolean;
  public conectedTser: boolean;
  public step5start: boolean;
  public step6start: boolean;
  public validationDone: boolean;
  public classNames: string[];
  public fileName: any;
  public digitalcertificateDetails: any;
  successcode: any
  certificateReqId: any;
  viewCertificateresponse: any;
  viewCertificateDetails: any;
  public verifyObj: any;

  constructor(public router: Router,
              public http: Http,
              public dialog: MdDialog,
              public  verify: ValidationProvider,
              public datePipe: DatePipe) {
    this.verifyObj = JSON.parse(localStorage.getItem('verifyObj'));
    console.log(JSON.stringify(this.verifyObj));
    this.hash = '0xf33fac78fab2005f62d8641dea6cbbe004898e09a553e190facf9b7266b1ac22'
    this.classNames = ['custom-circle-box']
    this.conectedTser = false;
    this.typeProgress = 'indeterminate'
    this.titile = 'Certificate Verification ';
    this.step1start = false;
    this.step3start = false
    this.step4start = null
    this.verifyDetails = new VerifyModel('', '', '', '', '', '');

  }

  ngOnInit() {
    window.scrollTo(0,0);
    this.step1();
  }

  step1() {
    this.keyProgress = 100;
    this.verifyDetails = new VerifyModel(this.verifyObj.certificate.individualPublicKey, this.verifyObj.certificate.issuerPublicKey, '', '', '', '');
    this.step1start = false;
    this.extractKey = true;
    let that = this
    this.step2start = true
    setTimeout(function () {
      that.step2()
    }, 3000);
  }

  step2() {
    this.keyProgress = 100;
    this.verifyDetails = new VerifyModel(this.verifyObj.certificate.individualPublicKey, this.verifyObj.certificate.issuerPublicKey, this.verifyObj.transactions[0].id, '', '', '');
    let that = this
    this.step2start = false
    this.step3start = true
    this.extractTranID = true
    setTimeout(function () {
      that.step3()
    }, 3000);
  }

  step3() {
    this.step3start = false
    this.verifyDetails = new VerifyModel(this.verifyObj.certificate.individualPublicKey, this.verifyObj.certificate.issuerPublicKey, this.verifyObj.transactions[0].id, this.verifyObj.transactions[0].txHash, '', '');
    this.step4start = true;
    this.extractDC = true;
    let that = this
    setTimeout(function () {
      that.step4()
    }, 3000);
  }

  step4() {
    this.step4start = false;
    this.http.get('https://kovan.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=' + this.verifyObj.transactions[0].txHash + '&apikey=VBFBKC9USABU8XV5V8YKN816ACPAGWYJ91').map((res) => res.json()).subscribe((result) => {
      console.log("=====" + result);
      if (result.result) {
        this.step5start = true;
        let blockChainResponse = result
        console.log(blockChainResponse);
        let that = this
        setTimeout(function () {
          that.step5(blockChainResponse)
        }, 3000);
        this.conectedTser = true
      } else {
        let resultdata = this.verify.networkFailure()
        this.openDialog(resultdata)
      }
    });

  }

  step5(blockChainResponse: any) {
    let to = blockChainResponse.result.to;
    let from = blockChainResponse.result.from;
    console.log(to, from)
    //  let jto = this.jsonText.digitalCertificate.recipient.recipientProfile.publicKey.split(',');
    // console.log("jto1" + this.jsonText.digitalCertificate.recipient.recipientProfile.publicKey)
    if ('0xcf9de8d91fbe209872cf314382856501d6fb8f87' === '0xcf9de8d91fbe209872cf314382856501d6fb8f87') {
      this.classNames = this.verify.successClass()
      this.step5start = false;
      this.tranDetails = true;
      this.step6start = true
      this.verifyDetails = new VerifyModel(this.verifyObj.certificate.individualPublicKey, this.verifyObj.certificate.issuerPublicKey, this.verifyObj.transactions[0].id, this.verifyObj.transactions[0].txHash, this.verifyObj.certificate.individualPublicKey, this.verifyObj.certificate.issuerPublicKey);
      let that = this
      setTimeout(function() {
        that.step6(blockChainResponse);
      }, 2000);
    } else {
      this.classNames = this.verify.errorClass()
      this.step5start = false;
      this.tranDetails = false;
      let resultdata = this.verify.transactionfailur()
      this.openDialog(resultdata)
    }
  }

  step6(blockChainResponse: any) {
    if ('0xcf9de8d91fbe209872cf314382856501d6fb8f87' === '0xcf9de8d91fbe209872cf314382856501d6fb8f87') {
      let resultdata = this.verify.hashSuccess()
      this.statusUpdate(resultdata);
      this.step6start = false;
      this.validationDone = true

    } else {
      this.step6start = false;
      this.validationDone = false
      let resultdata = this.verify.hashFailure()
      this.openDialog(resultdata)

    }
  }

  statusUpdate(resultdata) {
    this.openDialog(resultdata);
    /*   let currentDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    console.log("current date:" + currentDate+ "")
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    console.log("statusupdate req:" + GET_ORG_VERIFIEDSTATUS + this.certificateReqId + '&verifiedDate=' + currentDate+ "")
    this.http.get(GET_ORG_VERIFIEDSTATUS + this.certificateReqId + '&verifiedDate=' + currentDate+ "", {headers: headers}).map((res) => res.json()).subscribe((result) => {
      console.log("status response:" + JSON.stringify(result))
      this.openDialog(resultdata)
    }, (error) => {
      console.log("status response error:" + JSON.stringify(error))
    }, () => {

    })*/
  }

  openDialog(resultdata: any): void {
    let dialogRef = this.dialog.open(ValidationSuccessComponent, {
      width: '450px',
      data: resultdata,
      disableClose: true
    });
  }

}

@Component({
  selector: 'validation-success',
  templateUrl: 'validationSuccess.html',
})
export class ValidationSuccessComponent {
  constructor(public dialogRef: MdDialogRef<ValidationSuccessComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              public router: Router,) {
    console.log(this.data)
  }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigate(['/app/certs-issued']);
  }

}
