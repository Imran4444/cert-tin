import {Component, OnInit} from '@angular/core';
import {MdDialog} from '@angular/material';
import {ApiCalls} from '../../services/services';
import {Router} from "@angular/router";
import * as moment from 'moment';

@Component({
  selector: 'app-qualification',
  templateUrl: '../qualification/qualification.component.html',
  styleUrls: ['../qualification/qualification.css']

})
export class QualificationComponent implements OnInit {
  searchFields: any = {};
  sortingField: any;
  sortingOrder: any;
  totalCount: number;
  totalPagesCount: any;
  searchResult: any = {};
  firstItem: number;
  lastItem: number;
  placeholdervalue: any;
  pageSize: any;
  currentPage: any;
  pagIndexVal: any;
  checkedIdx: any;
  searchKey: any;
  searchValue: any;
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  inoutboxType: any;
  status: any;
  qualificationResponse: any;
  qualificationListResponse: any;
  certificateStatus: any;
  personNameSort: boolean;
  courseNameSort: boolean;
  specializationSort: boolean;
  sortClickValue: any;
  sortFieldValue: any;
  public mm: any;

  constructor(public dialog: MdDialog,
              public apiCalls: ApiCalls,
              public router: Router) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.searchResult = {};
    this.searchFields = {};
    this.currentPage = 1;
    this.firstItem = 1;
    this.totalCount = 0;
    this.lastItem = 10;
    this.sortingField = '';
    this.sortingOrder = '';
    this.pageSize = 10;
    this.mm = moment;
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.pageSize = '10';
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.inoutboxType) {
      if (this.organisationId) {
        this.getQualificationList();
      }
    }
  }

  getQualificationList() {
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSED';
    } else if (this.inoutboxType === 'ALL') {
      this.status = '';
    }
    this.showLoader = true;
    this.apiCalls.doGetCertificateList(this.organisationId, 'QUALIFICATION', this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      this.showLoader = false;
      console.log('QUA ' + JSON.stringify(result));
      this.qualificationResponse = result;
      this.qualificationListResponse = this.qualificationResponse.certificates;
      this.totalCount = this.qualificationResponse.totalElements;
      this.totalPagesCount = this.qualificationResponse.totalPages;
    }, (error) => {
      this.showLoader = false;
      console.log('' + JSON.stringify(error));
    });
  }

  selectRow(qualification, indexValue, ev) {
    this.pagIndexVal = indexValue;
    this.certificateStatus = qualification.status;
    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(qualification));
    localStorage.setItem('CertificateType', 'Qualification');
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  goToView(qualification) {
    this.certificateStatus = qualification.status;
    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(qualification));
    localStorage.setItem('CertificateType', 'Qualification');
    if (this.certificateStatus === 'OPEN') {
      this.router.navigate(['app/cert-inbox-view']);
    } else if (this.certificateStatus === 'CLOSED') {
      this.router.navigate(['app/certificate']);
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getQualificationList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Degree') {
      this.placeholdervalue = 'Degree';
      this.searchKey = 'courseName';
    } else if (selectedKey === 'Specialization') {
      this.placeholdervalue = 'Specialization';
      this.searchKey = 'specialization';
    } else {
      this.placeholdervalue = 'Certificate status';
      this.searchKey = 'certificateStatus';
    }
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getQualificationList();
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'personName') {
      this.personNameSort = true;
      this.courseNameSort = false;
      this.specializationSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'courseName') {
      this.personNameSort = false;
      this.courseNameSort = true;
      this.specializationSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'specialization') {
      this.personNameSort = false;
      this.courseNameSort = false;
      this.specializationSort = true;
      this.SortClicking();
    }

  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getQualificationList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getQualificationList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getQualificationList();
    }
  }

}
