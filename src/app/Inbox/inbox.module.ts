import { NgModule } from '@angular/core';
import { InboxComponent } from './inbox.component';
import { WorkclaimComponent } from './work/workclaim.component';
import { WorkEndroldComponent } from './popups/workEnroled/workEndrold.component';
import { QualificationComponent } from './qualification/qualification.component';
import { EnroleDeclineComponent } from './popups/declined/enroleDecline.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';
import { ClaimsComponent } from './claims/claims.component';
import { PersonalComponent } from './claims/personal/personal.component';
import { WorkClaimComponent } from './claims/work-claim/work-claim.component';
import { SkillClaimComponent } from './claims/skill-claim/skill-claim.component';
import { CourseClaimComponent } from './claims/course-claim/course-claim.component';
import { ViewComponent } from './claims/view/view.component';
import { AllClaimComponent } from './claims/all-claim/allclaim.component';
import { CertviewComponent } from './claims/certView/certview.component';
import { WorkAttestationsComponent } from './work-attestations/work-attestations.component';
import { AllCertificatesComponent } from './claims/all-certificates/allcertificates.component';
import { CertificateOutboxViewComponent } from './claims/certificate-outbox-view/certificate-outbox-view.component';
import { CertInboxViewComponent } from './cert-inbox-view/cert-inbox-view.component';
import { VerificationsComponent } from './verifications/verifications.component';
import { ValidationSuccessComponent, VerifyProcessComponent } from './verifyprocess/verifyProcess.component';
import { IssueEndrosedComponent, IssueSuccessComponent } from './IssueEndrosedProcess/issueEndrosed.component';
import { RouterModule } from '@angular/router';
import { VerifiedComponent } from './verified/verified.component';
import { VerifyCertificateComponent } from './verify-certificate-view/verifycert.component';
import { ChangePasswordComponent } from '../changePassword/changePassword.component';
import { ReasonPopupComponent } from './popups/reason-popup/reason-popup.component';
import { NgxQRCodeModule } from 'ngx-qrcode3';
import { WorkAttestationComponent } from '../TemplateDirective/WorkAttestation/workAttestation.component';
import { QualificationDirectiveComponent } from '../TemplateDirective/Qualification/qualificationDirective.component';
import { ProfessionalAccountComponent } from '../professional-account/professional-account.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { ExtraPagesModule } from '../extra-pages/extra-pages.module';
import { AttendenceComponent } from '../TemplateDirective/Attendence/attendence.component';
import { LicenseComponent } from '../TemplateDirective/license/license.component';
import { ProofOfInternComponent } from '../TemplateDirective/proofOfInternship/proofOfIntern.component';
import { ReferenceCertificateComponent } from '../TemplateDirective/reference/referenceCertificate.component';
import { CertsIssuedComponent } from '../certs/issued/issued.component';
import { ViewIssuedCertsComponent } from '../certs/issued/viewIssuedCerts/view-issued.component';
import { TransferCertsComponent } from '../certs/transfer/transfer.component';
import {IssueNewCertComponent} from "../certs/issued/issue-new-certificate/issue-new.component";
import {SearchRecipientComponent} from "../certs/issued/search-recipient/searchRecipient.component";
import { CommonModule } from '@angular/common';
import { EventComponent } from '../TemplateDirective/Eevent/event.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  imports: [FormsModule, CommonModule, MaterialModule, RouterModule, ReactiveFormsModule, NgxQRCodeModule, GooglePlaceModule, ExtraPagesModule, PdfViewerModule],
  declarations: [InboxComponent, CertificateOutboxViewComponent, CertviewComponent, WorkclaimComponent, WorkEndroldComponent,
    QualificationComponent, EnroleDeclineComponent, ClaimsComponent, PersonalComponent, WorkClaimComponent, SkillClaimComponent,
    CourseClaimComponent, AllClaimComponent, ViewComponent, WorkAttestationsComponent, AllCertificatesComponent, CertInboxViewComponent,
    VerifyProcessComponent, ValidationSuccessComponent, IssueEndrosedComponent, IssueSuccessComponent, VerificationsComponent,

    VerifiedComponent, VerifyCertificateComponent, ChangePasswordComponent, ReasonPopupComponent, WorkAttestationComponent, QualificationDirectiveComponent,
    ProfessionalAccountComponent, AttendenceComponent, LicenseComponent, ProofOfInternComponent, ReferenceCertificateComponent, CertsIssuedComponent,
    ViewIssuedCertsComponent, TransferCertsComponent, IssueNewCertComponent, SearchRecipientComponent, EventComponent],
  exports: [WorkAttestationComponent, QualificationDirectiveComponent, AttendenceComponent, EventComponent, LicenseComponent, ProofOfInternComponent, ReferenceCertificateComponent, ],
  providers: [],
  entryComponents: [WorkEndroldComponent, ReasonPopupComponent, EnroleDeclineComponent, ValidationSuccessComponent, IssueSuccessComponent]
})

export class InboxModule {
  constructor() {
  }

}
