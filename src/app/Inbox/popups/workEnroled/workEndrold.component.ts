import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-work-endroled',
  templateUrl: '../workEnroled/workEnroledSuccess.component.html',
  styleUrls: ['../workEnroled/workEndroled.component.css']
})

export class WorkEndroldComponent {
  constructor(public dialogRef: MdDialogRef<WorkEndroldComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {

  }

  close(): void {
    this.dialogRef.close();
  }

}
