import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-decline-endrole',
  templateUrl: '../declined/enroleDecline.component.html',
  styleUrls: ['../declined/enroleDecline.component.css']
})
export class EnroleDeclineComponent {
  constructor(public dialogRef: MdDialogRef<EnroleDeclineComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
  }

  close(): void {
    this.dialogRef.close();
  }
}
