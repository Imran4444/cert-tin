import {Component, OnInit, Inject} from '@angular/core';
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from '@angular/material';
import {ApiCalls} from '../../../services/services';
import {Router} from '@angular/router';
import {GlobalPopupComponent} from "../../../globalPopup/globalPopup.component";
import {ValidationProvider} from "../../../providers/validationProvider";

@Component({
  selector: 'app-reason-popup',
  templateUrl: './reason-popup.component.html',
  styleUrls: ['./reason-popup.component.scss']
})
export class ReasonPopupComponent implements OnInit {
  public commnets: any;
  public inoutboxType: any;
  private claimType: any;
  public certdetails: any;
  public certData: any;
  public showLoader: boolean;
  submitted: any;

  constructor(public dialogRef: MdDialogRef<ReasonPopupComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              public apiCall: ApiCalls,
              public router: Router,
              public dialog: MdDialog,
              public verify: ValidationProvider) {
    console.log(this.data);
  }

  decline() {
    this.showLoader = true;
    this.submitted = true;
    if (this.commnets) {
      this.showLoader = true;
      this.data.comment = this.commnets;
      console.log(this.data);
      localStorage.setItem('issueObj', JSON.stringify(this.data));
      // this.dialogRef.close();
      console.log('decline req..' + JSON.stringify(this.data))
      this.apiCall.doEndorseDecline(this.data, this.inoutboxType).then((result) => {
        this.certdetails = result;
        console.log(JSON.stringify(result));
        this.showLoader = false;
        if(this.certdetails){
          this.dialogRef.close();
          this.router.navigate(['app/all-claims']);
        }

        /*if (this.inoutboxType === 'INBOX') {
         if (this.claimType === 'Personal') {
         this.router.navigate(['app/personal-claims']);
         } else if (this.claimType === 'Work') {
         this.router.navigate(['app/work-claims']);
         } else if (this.claimType === 'Skill') {
         this.router.navigate(['app/skill-claims']);
         } else {
         this.router.navigate(['app/course-claims']);
         }
         } else if (this.inoutboxType === 'ALL') {
         this.router.navigate(['app/all-claims']);
         } else if (this.inoutboxType === 'CERTIFICATES') {
         localStorage.setItem('INOUTBOX', 'INBOX');
         if (this.certData.certificateType === 'WORKATTESTATION') {
         this.router.navigate(['app/work-attestations']);
         } else if (this.certData.certificateType === 'QUALIFICATION') {
         this.router.navigate(['app/qualification']);
         }
         }*/


        console.log('Endorse Decline Result' + JSON.stringify(result));
      }, (error) => {
        console.log('' + JSON.stringify(error));
        this.showLoader = false;
        const resultData = this.verify.errorPopup(error.developerMessage, error.responseMessage, '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
        this.dialogRef.close();
      });
    }
  }

  ngOnInit() {
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    this.claimType = localStorage.getItem('claimType');
    this.certData = JSON.parse(localStorage.getItem('certDetails'));
    console.log(this.inoutboxType);
    console.log(this.claimType);
    console.log(this.data);
  }

  goBack() {
    this.dialogRef.close();
  }
}
