import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../services/services';
import { Router } from '@angular/router';
import { auditService } from '../../../providers/audit.service';
import { ErrorMessages } from '../../../providers/errorMessages';
import { ValidationProvider } from '../../../providers/validationProvider';
import { MdDialog } from '@angular/material';
import * as moment from 'moment';
import { EXDEV } from "constants";
import { DEV_ROOT_PATH } from '../../../endpoint';
import { LayoutService } from '../../../layout/layout.service';
import { GlobalPopupComponent } from "../../../globalPopup/globalPopup.component";

@Component({
  selector: 'app-certview',
  templateUrl: '../certificate-outbox-view/certificate-outbox-view.component.html',
  styleUrls: ['../certificate-outbox-view/certificate-outbox-view.component.css']
})

export class CertificateOutboxViewComponent implements OnInit {
  certificateDetails: any;
  certificateType: any;
  getCertificateDetailsResponse: any;
  getCertificateDetail: any;
  commnets: any;
  endorseLevel: any;
  showLoader: any;
  endorseDeclineResult: string;
  inoutboxType: any;
  private loginResponse: any;
  public organizationDetails: any;
  public mm: any;
  template: any;
  borderTop: any;
  borderBtm: any;
  transactionDetail: any;
  txHash: any;
  blockChainResponse: any;
  blockChaindetails: any;
  public blockGasPrice: any;
  public blockGasPriceFix: any;
  public blockGasPriceGw: any;
  public blockGasLimit: any;
  public blockNonce: any;
  public blockHeight: any;
  public blockPosition: any;
  public blockValue: any;
  public blockChainReceiptResponse: any;
  public gasUsedByTransaction: any;
  public gasUsedByTransactionEther: any;
  public totalTransactionCost: any;
  public rootPath: string;
  public getPersonDetail: any;
  public personResponse: any;
  public footerInfo: string | any;
  public myName: any;
  transferLoader: any;
  revoked: any;
  resultData: any;
  revokeLoader: any;
  public isLoaded: boolean;
  public page: number;
  public totalPages: number;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public auditHistory: auditService,
              public errorMessage: ErrorMessages,
              public validations: ValidationProvider,
              public dialog: MdDialog, public apiCalls: ApiCalls, public layoutService: LayoutService) {
    this.page = 1;
    this.rootPath = DEV_ROOT_PATH
    this.showLoader = false;
    this.getCertificateDetail = {};
    this.mm = moment;
    /*  this.blockGasPrice = '-';
     this.blockGasLimit = '-';
     this.blockNonce = '-';
     this.blockHeight = '-';
     this.blockPosition = '-';
     this.blockValue = '-';
     this.blockGasPriceGw = '-';*/
    this.blockPosition = '-';
  }

  checkPdf(url: any) {
    console.log(url);
    if (url.includes('.pdf')) {
      return true
    } else {
      return false
    }

  }

  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.isLoaded = true;
  }

  nextPage() {
    this.page++;
  }

  prevPage() {
    this.page--;
  }

  ngOnInit() {
    this.revokeLoader = false;
    this.transferLoader = false;
    // this.myName = 'raviraj'
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'))
    this.getOrganization(this.loginResponse.orgId);
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    this.certificateType = localStorage.getItem('CertificateType');
    this.certificateDetails = JSON.parse(localStorage.getItem('CERTIFICATEDETAILS'));
    console.log('' + this.certificateDetails.requesterEmail);
    this.getCertificateDetails();
  }

  getOrganization(orgId) {
    console.log(orgId);
    this.apiCalls.doGetOrgtemplates(orgId).then((result) => {
      this.organizationDetails = result;
      console.log(JSON.stringify(this.organizationDetails));
      /*  this.templates = this.organizationDetails.organisation.templates
       this.personalTemplate = this.templates.PERSONAL;
       this.workTemplate = this.templates.WORK;
       this.skillsTemplate = this.templates.SKILLS;
       this.courseTemplates = this.templates.COURSE;
       this.workAtteTemplates = this.templates.WORKATTESTATION;
       this.qualiTemplates = this.templates.QUALIFICATION;
       console.log(this.organizationDetails.organisation.templates);*/

    }, (error) => {
      console.log('org details error..' + JSON.stringify(error));
    });
  }

  getCertificateDetails() {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCall.doGetCertificateDetails(this.certificateDetails.id).then((result) => {
      this.showLoader = false;
      this.layoutService.updatePreloaderState('hide');
      console.log('Tran' + JSON.stringify(result));
      this.getCertificateDetailsResponse = result;
      this.getCertificateDetail = this.getCertificateDetailsResponse.certificate;
      this.footerInfo = this.getCertificateDetailsResponse.organisation
      this.getPersonDetail = this.getCertificateDetailsResponse.person
      console.log('Cert Detail' + this.getCertificateDetailsResponse.certificate);
      console.log('Transaction Detail' + this.getCertificateDetailsResponse.transactions);
      this.transactionDetail = this.getCertificateDetailsResponse.transactions;
      if (this.transactionDetail.length) {
        this.txHash = this.transactionDetail[0].txHash;
        if (this.txHash) {
          this.getBlockchainDetails();
        }
      }

      console.log('Tran' + JSON.stringify(this.transactionDetail));
      this.template = this.getCertificateDetail.template;
      this.endorseLevel = this.getCertificateDetail.level;
      this.commnets = this.getCertificateDetail.comment;
      if (this.template === 'WORKATTESTATION_DEFAULT' || this.template === 'QUALIFICATION_DEFAULT' || this.template === 'WORKATTESTATION_ORG' || this.template === 'QUALIFICATION_ORG') {
        this.borderTop = 'assets/images/certBorders/border-top-1.png';
        this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
      } else if (this.template === 'WORKATTESTATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      } else if (this.template === 'QUALIFICATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-3.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-3.png';
      } else if (this.template === 'LICENSE_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      }
      if (this.getCertificateDetail.accountlevel === 1) {
        this.endorseLevel = 'Personal';
      }
    }, (error) => {
      console.log(error);
      this.showLoader = false;
    });
  }

  getBlockchainDetails() {
    console.log('TX-HASH' + this.txHash);
    this.apiCall.doGetBlockDetails(this.txHash).then((res) => {
      this.blockChainResponse = res;
      this.blockChaindetails = this.blockChainResponse.result;
      if (this.blockChaindetails) {
        this.blockGasPrice = Number.parseInt(this.blockChaindetails.gasPrice) / 1000000000000000000;
        this.blockGasPriceFix = (this.blockGasPrice).toFixed(9);
        // this.blockGasPrice = (this.blockGasPrice).toFixed(9);
        /*this.blockGasPrice = Number.parseInt(this.blockChaindetails.gasPrice);
         this.blockGasPrice = '0.00000000' + (this.blockGasPrice / 1000000000) + ' Ether';*/
        this.blockGasPriceGw = Number.parseInt(this.blockChaindetails.gasPrice) / 1000000000 + ' Gwei';
        // this.blockGasPrice = this.blockGasPriceGw * 0.000000001;
        this.blockGasLimit = Number.parseInt(this.blockChaindetails.gas);
        this.blockNonce = Number.parseInt(this.blockChaindetails.nonce);
        this.blockHeight = Number.parseInt(this.blockChaindetails.blockNumber);
        this.blockPosition = Number.parseInt(this.blockChaindetails.transactionIndex);
        this.blockValue = Number.parseInt(this.blockChaindetails.value) / 1000000000000000000 + ' Ether';
        this.getBlockChainReceipt();
      }
      console.log('Block Chain Details' + JSON.stringify(this.blockChaindetails));
    }, (error) => {
      console.log(error);
      this.showLoader = false;
    });
  }

  getBlockChainReceipt() {
    this.apiCall.doGetBlockChainReceipt(this.txHash).then((result) => {
      this.blockChainReceiptResponse = result;
      this.gasUsedByTransaction = Number.parseInt(this.blockChainReceiptResponse.result.gasUsed);
      this.gasUsedByTransactionEther = (this.gasUsedByTransaction / 1000000000000000000);
      this.totalTransactionCost = (this.gasUsedByTransactionEther * this.blockGasPrice) * 1000000000000000000;
      console.log('' + JSON.stringify(this.blockChainReceiptResponse));
    }, (error) => {
      console.log('' + JSON.stringify(error));
    });
  }

  transfer() {
    localStorage.setItem('chooseRecipientName', 'transfer');
    console.log(localStorage.getItem('chooseRecipientName'));
    localStorage.setItem('transferCertDetails', JSON.stringify(this.getCertificateDetailsResponse));
    this.router.navigate(['app/search-recipient']);
  }

  revokeCert() {
    this.revokeLoader = true;
    const req = {
      certificateId: this.getCertificateDetailsResponse.certificate.id,
      cimOrgId: this.getCertificateDetailsResponse.organisation.organisation.id
    }
    this.apiCall.revokeCertificate(JSON.stringify(req)).then((rev) => {
      this.revokeLoader = false;
      console.log(rev);
      this.revoked = rev;
      this.resultData = this.validations.errorPopup('Revoked', 'Certificate is Revoked', ['/app/certs-issued']);
      if (this.revoked.certificate && !this.revoked.certificate.isValid) {
        const modal = this.dialog.open(GlobalPopupComponent, {
          data: this.resultData,
          width: '450px',
          disableClose: true
        });
        modal.afterClosed().subscribe((res) => {
          console.log(res);
        });
      }
    }, (err) => {
      this.revokeLoader = false;
      console.log(err);
    });
  }
}
