import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../../services/services';
import {Router} from '@angular/router';
import {auditService} from '../../../providers/audit.service';
import {ErrorMessages} from '../../../providers/errorMessages';
import {ValidationProvider} from '../../../providers/validationProvider';
import {MdDialog} from '@angular/material';
import * as moment from 'moment';
import {DEV_ROOT_PATH} from '../../../endpoint';
import {LayoutService} from '../../../layout/layout.service';

@Component({
  selector: 'app-certview',
  templateUrl: '../certView/certview.component.html',
  styleUrls: ['../certView/certview.component.css']
})

export class CertviewComponent implements OnInit {
  claimDetails: any;
  claimType: any;
  getClaimDetailsResponse: any;
  getClaimDetail: any;
  commnets: any;
  endorseLevel: any;
  showLoader: any;
  endorseDeclineResult: string;
  inoutboxType: any;
  transactionDetail: any;
  blockChaindetails: any;
  txHash: any;
  private loginResponse: any;
  public organizationDetails: any;
  public mm: any;
  blockChainResponse: any;
  public blockGasPrice: any;
  public blockGasPriceFix: any;
  public blockGasLimit: any;
  public blockGasPriceGw: any;
  public blockNonce: any;
  public blockHeight: any;
  public blockPosition: any;
  public blockValue: any;
  public blockChainReceiptResponse: any;
  public gasUsedByTransaction: any;
  public gasUsedByTransactionEther: any;
  public totalTransactionCost: any;
  public rootPath: string;
  public claimPersonDetails: any;
  public claimDates: any;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public auditHistory: auditService,
              public errorMessage: ErrorMessages,
              public validations: ValidationProvider,
              public dialog: MdDialog, public apiCalls: ApiCalls, public layoutService: LayoutService) {
    this.mm = moment;
    this.rootPath = DEV_ROOT_PATH
    this.showLoader = false;
    this.getClaimDetail = {};
    this.claimDates = {};

    /*this.blockGasPrice = '0';
     this.blockGasLimit = '-';
     this.blockNonce = '-';
     this.blockHeight = '-';
     this.blockPosition = '-';
     this.blockValue = '-';
     this.blockGasPriceGw = '-';*/
  }

  ngOnInit() {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.getOrganization(this.loginResponse.orgId);
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    this.claimType = localStorage.getItem('claimType');
    this.claimDetails = JSON.parse(localStorage.getItem('CLAIMDETAILS'));
    if (this.claimDetails.id) {
      this.getClaimDetails();
    }
    // if (this.getClaimDetailsResponse.transactions[0].txHash) {
    //   this.getBlockchainDetails();
    // }

  }

  getOrganization(orgId) {
    this.layoutService.updatePreloaderState('active');
    this.apiCalls.doGetOrgtemplates(orgId).then((result) => {
      this.organizationDetails = result;
      this.layoutService.updatePreloaderState('hide');
      /*  this.templates = this.organizationDetails.organisation.templates
       this.personalTemplate = this.templates.PERSONAL;
       this.workTemplate = this.templates.WORK;
       this.skillsTemplate = this.templates.SKILLS;
       this.courseTemplates = this.templates.COURSE;
       this.workAtteTemplates = this.templates.WORKATTESTATION;
       this.qualiTemplates = this.templates.QUALIFICATION;
       console.log(this.organizationDetails.organisation.templates);*/

    }, (error) => {
    });
  }

  getClaimDetails() {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCall.doGetClaimDetails(this.claimDetails.id).then((result) => {
      console.log('get claim details response...' + JSON.stringify(result))
      this.showLoader = false;
      this.getClaimDetailsResponse = result;
      this.getClaimDetail = this.getClaimDetailsResponse.calimRequest.claim;
      this.claimPersonDetails = this.getClaimDetailsResponse.person;
      this.claimDates = this.getClaimDetail.claim;
      // this.transactionDetail = this.getClaimDetailsResponse.transactions;
      this.endorseLevel = this.getClaimDetail.level;
      // this.commnets = this.getClaimDetail.comment;
      // this.txHash = this.transactionDetail[0].txHash;
      /*
       if (this.txHash) {
       this.getBlockchainDetails();
       }
       */
      /*
       if (this.getClaimDetail.accountlevel === 1) {
       this.endorseLevel = 'Personal';
       }
       */
      this.layoutService.updatePreloaderState('hide');
    }, (error) => {
      this.showLoader = false;
      this.layoutService.updatePreloaderState('hide');
    });
  }

  getBlockchainDetails() {
    this.layoutService.updatePreloaderState('active');
    this.apiCall.doGetBlockDetails(this.txHash).then((res) => {
      this.blockChainResponse = res;
      this.blockChaindetails = this.blockChainResponse.result;
      this.layoutService.updatePreloaderState('hide');
      if (this.blockChaindetails) {
        this.blockGasPrice = Number.parseInt(this.blockChaindetails.gasPrice) / 1000000000000000000;
        this.blockGasPriceFix = (this.blockGasPrice).toFixed(9);
        /* this.blockGasPrice = Number.parseInt(this.blockChaindetails.gasPrice);
         this.blockGasPrice = '0.00000000' + (this.blockGasPrice / 1000000000) + ' Ether';*/
        this.blockGasPriceGw = Number.parseInt(this.blockChaindetails.gasPrice) / 1000000000 + ' Gwei';
        this.blockGasLimit = Number.parseInt(this.blockChaindetails.gas);
        this.blockNonce = Number.parseInt(this.blockChaindetails.nonce);
        this.blockHeight = Number.parseInt(this.blockChaindetails.blockNumber);
        this.blockPosition = Number.parseInt(this.blockChaindetails.transactionIndex);
        this.blockValue = Number.parseInt(this.blockChaindetails.value) / 1000000000000000000 + ' Ether';
        this.getBlockChainReceipt();
      }
    }, (error) => {
      this.showLoader = false;
      this.layoutService.updatePreloaderState('hide');
    });

  }

  getBlockChainReceipt() {
    this.layoutService.updatePreloaderState('active');
    this.apiCall.doGetBlockChainReceipt(this.txHash).then((result) => {
      this.blockChainReceiptResponse = result;
      this.gasUsedByTransaction = Number.parseInt(this.blockChainReceiptResponse.result.gasUsed);
      this.gasUsedByTransactionEther = (this.gasUsedByTransaction / 1000000000000000000);
      this.totalTransactionCost = (this.gasUsedByTransactionEther * this.blockGasPrice) * 1000000000000000000;
      this.layoutService.updatePreloaderState('hide');
    }, (error) => {
      this.layoutService.updatePreloaderState('hide');
    });
  }
  openEvidence(evidences) {
    window.open(evidences, '_blank');
  }
  openCertificate(evidences) {
    let certificateUrl = this.rootPath + '/#/badges/certificate/' + evidences
    console.log('aaaa..' + certificateUrl)
    window.open(certificateUrl, '_blank');
  }
}
