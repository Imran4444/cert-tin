import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../../services/services';
import {Router} from '@angular/router';
import {MySharedService} from '../../../services/mySharedService ';
import { LayoutService } from '../../../layout/layout.service';

@Component({
  selector: 'app-allcertificates',
  templateUrl: './allcertificates.component.html',
  styleUrls: ['./allcertificates.component.scss']
})
export class AllCertificatesComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  certificateResponse: any;
  certificateResponseList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  claimStatus: any;
  sortClickValue: any;
  sortFieldValue: any;
  personNameSort: boolean;
  cTypeSort: boolean;
  roleSort: boolean;
  courseNameSort: boolean;
  specializationSort: boolean;
  statusSort: boolean;
  certificateStatusSort: boolean;
  public selectedCertificateType: any;
  public filterData: any;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService,private layoutService: LayoutService) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.selectedCertificateType = 'All';
    this.filterData = 'All';
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    /* this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getAllCertificatesList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.inoutboxType) {
      if (this.organisationId) {
        this.getAllCertificatesList(this.selectedCertificateType);
      }
    }
  }

  selectRow(personalClaims, indexValue, ev) {
    this.pagIndexVal = indexValue;
    /* if (this.checkedIdx === false) {
     this.checkedIdx = true;
     } else {
     this.checkedIdx = false;
     }*/
    this.claimStatus = personalClaims.status;
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('claimType', personalClaims.claimType);
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getFilter(certificateType) {
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.getAllCertificatesList(certificateType);
  }

  getAllCertificatesList(certificateType) {
    this.layoutService.updatePreloaderState('active');
    this.selectedCertificateType = certificateType;
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSE';
    } else {
      this.status = '';
    }

    this.showLoader = true;
    this.apiCall.doGetCertificateList(this.organisationId, this.selectedCertificateType, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      console.log('' + JSON.stringify(result));
      this.layoutService.updatePreloaderState('hide');
      this.showLoader = false;
      this.certificateResponse = result;
      this.certificateResponseList = this.certificateResponse.certificates;
      this.totalPagesCount = this.certificateResponse.totalPages;
      this.totalpages = this.certificateResponse.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.certificateResponseList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getAllCertificatesList(this.selectedCertificateType);
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getAllCertificatesList(this.selectedCertificateType);
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getAllCertificatesList(this.selectedCertificateType);
    }
  }

  selectedSearch(selectedKey) {
    this.searchKey = '';
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Type of certificate') {
      this.placeholdervalue = 'Type of certificate';
      this.searchKey = 'cType';
    } else if (selectedKey === 'Role') {
      this.placeholdervalue = 'Role';
      this.searchKey = 'role';
    } else if (selectedKey === 'Degree') {
      this.placeholdervalue = 'Degree';
      this.searchKey = 'courseName';
    } else if (selectedKey === 'Specialization') {
      this.placeholdervalue = 'Specialization';
      this.searchKey = 'specialization';
    } else if (selectedKey === 'Status') {
      this.placeholdervalue = 'Status';
      this.searchKey = 'status';
    } else {
      this.placeholdervalue = 'Certificate status';
      this.searchKey = 'certificateStatus';
    }
    this.getAllCertificatesList(this.selectedCertificateType);
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getAllCertificatesList(this.selectedCertificateType);
    }
  }

  goToView(certificateDetails) {
    this.claimStatus = certificateDetails.status;
    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(certificateDetails));
    localStorage.setItem('CertificateType', certificateDetails.certificateType);
    if (this.claimStatus === 'OPEN') {
      this.router.navigate(['app/cert-inbox-view']);
    } else if (this.claimStatus === 'CLOSED') {
      this.router.navigate(['app/certificate']);
    }
  }

  sortingNameClaim(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'personName') {
      this.personNameSort = true;
      this.cTypeSort = false;
      this.roleSort = false;
      this.courseNameSort = false;
      this.specializationSort = false;
      this.statusSort = false;
      this.certificateStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'cType') {
      this.personNameSort = false;
      this.cTypeSort = true;
      this.roleSort = false;
      this.courseNameSort = false;
      this.specializationSort = false;
      this.statusSort = false;
      this.certificateStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'role') {
      this.personNameSort = false;
      this.cTypeSort = false;
      this.roleSort = true;
      this.courseNameSort = false;
      this.specializationSort = false;
      this.statusSort = false;
      this.certificateStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'courseName') {
      this.personNameSort = false;
      this.cTypeSort = false;
      this.roleSort = false;
      this.courseNameSort = true;
      this.specializationSort = false;
      this.statusSort = false;
      this.certificateStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'specialization') {
      this.personNameSort = false;
      this.cTypeSort = false;
      this.roleSort = false;
      this.courseNameSort = false;
      this.specializationSort = true;
      this.statusSort = false;
      this.certificateStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'status') {
      this.personNameSort = false;
      this.cTypeSort = false;
      this.roleSort = false;
      this.courseNameSort = false;
      this.specializationSort = false;
      this.statusSort = true;
      this.certificateStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'certificateStatus') {
      this.personNameSort = false;
      this.cTypeSort = false;
      this.roleSort = false;
      this.courseNameSort = false;
      this.specializationSort = false;
      this.statusSort = false;
      this.certificateStatusSort = true;
      this.SortClicking();
    }
  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getAllCertificatesList(this.selectedCertificateType);
  }
}
