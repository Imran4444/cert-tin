import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../../services/services';
import {Router} from '@angular/router';
import {MySharedService} from "../../../services/mySharedService ";
import * as moment from 'moment';

@Component({
  selector: 'app-skill-claim',
  templateUrl: './skill-claim.component.html',
  styleUrls: ['./skill-claim.component.scss']
})
export class SkillClaimComponent implements OnInit {
  rowSelect: any;
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  skillClaimResponse: any;
  skillClaimsList: any[];
  searchValue: any;
  searchKey: any;
  placeholdervalue: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  public mm: any;
  sortClickValue: any;
  sortFieldValue: any;
  nameSort: boolean;
  claimNameSort: boolean;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.mm = moment;
  }

  ngOnInit() {
    this.placeholdervalue = 'Search';
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    /*this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getSkillsList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.organisationId) {
      this.getSkillsList();
    }
  }

  selectRow(skillClaims, indexValue, ev) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(skillClaims));
    localStorage.setItem('claimType', 'Skill');
    this.pagIndexVal = indexValue;
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getSkillsList() {
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSED';
    } else if (this.inoutboxType === 'ALL') {
      this.status = '';
    }
    this.showLoader = true;
    this.apiCall.doGetClaims(this.organisationId, 'SKILLS', this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      console.log('' + JSON.stringify(result));
      this.showLoader = false;
      this.skillClaimResponse = result;
      this.skillClaimsList = this.skillClaimResponse.claims;
      this.totalPagesCount = this.skillClaimResponse.totalPages;
      this.totalpages = this.skillClaimResponse.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.skillClaimsList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getSkillsList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getSkillsList();
    }
  }

  goToView(skillClaims) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(skillClaims));
    localStorage.setItem('claimType', 'Skill');
    if (this.inoutboxType === 'INBOX') {
      this.router.navigate(['app/view']);
    } else {
      this.router.navigate(['app/claim-certificate']);
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getSkillsList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Name of claim') {
      this.placeholdervalue = 'Name of claim';
      this.searchKey = 'claimName';
    } else if (selectedKey === 'Status') {
      this.placeholdervalue = 'Status';
      this.searchKey = 'status';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getSkillsList();
    }
  }

  sortingNameClaim(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'requester_name') {
      this.nameSort = true;
      this.claimNameSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'heading') {
      this.nameSort = false;
      this.claimNameSort = true;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.claimNameSort = false;
      this.SortClicking();
    }

  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getSkillsList();
  }
}

