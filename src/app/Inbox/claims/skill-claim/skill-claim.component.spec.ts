import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SkillClaimComponent } from './skill-claim.component';

describe('SkillClaimComponent', () => {
  let component: SkillClaimComponent;
  let fixture: ComponentFixture<SkillClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SkillClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SkillClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
