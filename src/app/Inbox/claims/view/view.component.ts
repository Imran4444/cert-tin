import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../../services/services';
import {EndorseDeclineRequest} from '../../../Modals/endorsedeclineRequest';
import {Router} from '@angular/router';
import {auditService} from '../../../providers/audit.service';
import {ErrorMessages} from '../../../providers/errorMessages';
import {ValidationProvider} from '../../../providers/validationProvider';
import {MdDialog} from '@angular/material';
import {GlobalPopupComponent} from '../../../globalPopup/globalPopup.component';
import * as moment from 'moment';
import {ReasonPopupComponent} from '../../popups/reason-popup/reason-popup.component';
import {LayoutService} from '../../../layout/layout.service';
import {EndorsedModal} from "../../../Modals/endorsedmodal";
import {DEV_ROOT_PATH} from "../../../endpoint";

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  claimDetails: any;
  claimType: any;
  getClaimDetailsResponse: any;
  getClaimDetail: any;
  getPersonDetail: any;
  commnets: any;
  endorseLevel: any;
  showLoader: any;
  endorseDeclineResult: string;
  inoutboxType: any;
  mm: any;
  loginResponse: any;
  personId: any;
  auditReq: any;
  certdetails: any;
  claimDates: any;
  public rootPath: string;
  endorseLoader: any;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public auditHistory: auditService,
              public errorMessage: ErrorMessages,
              public validations: ValidationProvider,
              public dialog: MdDialog, public layoutService: LayoutService) {
    this.rootPath = DEV_ROOT_PATH
    this.endorseLevel = 'Organisation';
    this.commnets = '';
    this.showLoader = false;
    this.mm = moment;
    this.getClaimDetail = {};
    this.claimDates = {};

  }

  ngOnInit() {
    this.endorseLoader = false;
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    this.claimType = localStorage.getItem('claimType');
    this.claimDetails = JSON.parse(localStorage.getItem('CLAIMDETAILS'));
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.personId = this.loginResponse.personId;
    console.log('' + JSON.stringify(this.claimType));
    this.getClaimDetails();
  }

  getClaimDetails() {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCall.doGetClaimDetails(this.claimDetails.id).then((result) => {
      this.showLoader = false;
      this.getClaimDetailsResponse = result;
      this.getClaimDetail = this.getClaimDetailsResponse.calimRequest;
      this.claimDates = this.getClaimDetail.claim;
      this.getPersonDetail = this.getClaimDetailsResponse.person;
      console.log('ClaimDetailsResponse' + JSON.stringify(this.getClaimDetailsResponse));
      // this.endorseLevel = this.getClaimDetail.level;
      this.commnets = this.getClaimDetail.comment;
      this.layoutService.updatePreloaderState('hide');
      // if (this.getClaimDetail.accountlevel === 1) {
      //   this.endorseLevel = 'Personal';
      // }
    }, (error) => {
      console.log(error);
      this.showLoader = false;
    });
  }

  getEndroseDeclined(status) {
   
    /* if (this.endorseLevel === 'Organization') {
     this.endorseLevel = 'AWAITING_APPROVAL';
     }*/
    localStorage.setItem('INOUTBOX', 'INBOX');
    /* if (status === 'DECLINED') {
     localStorage.setItem('certDetails', JSON.stringify(this.getClaimDetail));
     this.auditReq = this.auditHistory.userAudit('CLAIM ' + status);
     const endorseRequest = new EndorseDeclineRequest(this.auditReq,this.claimDetails.id, '', 1, status, this.personId);
     console.log('Endorse Request' + (endorseRequest));
     const dialogRef = this.dialog.open(ReasonPopupComponent, {
     width: '450px',
     data: endorseRequest
     });
     } else {
     this.auditReq = this.auditHistory.userAudit('CLAIM ' + status);
     const endorseRequest = new EndorseDeclineRequest(this.auditReq,this.claimDetails.id, this.commnets, 1, status, this.personId);
     localStorage.setItem('issueObj', JSON.stringify(endorseRequest));
     localStorage.setItem('certDetails', JSON.stringify(this.getClaimDetail));
     this.router.navigate(['/app/issue-endrose']);
     console.log(endorseRequest);
     }*/


    if (status === 'DECLINED') {
      localStorage.setItem('certDetails', JSON.stringify(this.getClaimDetail));
      this.auditReq = this.auditHistory.userAudit('CLAIM ' + status);
      const endorseRequest = new EndorsedModal(this.auditReq, status, this.commnets, this.getClaimDetail.id, this.personId);

      console.log('Endorse Request' + JSON.stringify(endorseRequest));
      const dialogRef = this.dialog.open(ReasonPopupComponent, {
        width: '450px',
        data: endorseRequest
      });
    } else {
      this.endorseLoader = true;
      this.auditReq = this.auditHistory.userAudit('CLAIM ' + status);
      const endorseRequest = new EndorsedModal(this.auditReq, status, '', this.getClaimDetail.id, this.personId);
      localStorage.setItem('issueObj', JSON.stringify(endorseRequest));
      localStorage.setItem('certDetails', JSON.stringify(this.getClaimDetail));
      // this.router.navigate(['/app/issue-endrose']);
      this.apiCall.doEndorseDecline(endorseRequest, this.inoutboxType).then((result) => {
        this.endorseLoader = false;
        this.certdetails = result;
        console.log(JSON.stringify(result));
        this.showLoader = false;
        if (this.certdetails.responseCode) {
          const popupData = this.validations.errorPopup('Success', 'Claim Endorsed Successfully', ['app/all-claims']);
          const dialogRef = this.dialog.open(GlobalPopupComponent, {
            width: '300px',
            data: popupData,
            disableClose: true
          });
        }
        console.log('Endorse Decline Result' + JSON.stringify(result));
      }, (error) => {
        console.log('' + JSON.stringify(error));
        this.showLoader = false;
      });


      console.log('endorse req' + JSON.stringify(endorseRequest));
    }

  }

  

  openEvidence(evidences) {
    window.open(evidences, '_blank');
  }

  openCertificate(evidences) {
    let certificateUrl = this.rootPath + '/#/badges/certificate/' + evidences
    console.log('aaaa..' + certificateUrl)
    window.open(certificateUrl, '_blank');
  }
}
