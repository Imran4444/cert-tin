import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkClaimComponent } from './work-claim.component';

describe('WorkClaimComponent', () => {
  let component: WorkClaimComponent;
  let fixture: ComponentFixture<WorkClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
