import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../../services/services';
import {Router} from '@angular/router';
import * as moment from 'moment';
import 'moment-timezone';
import {MySharedService} from "../../../services/mySharedService ";
import { ReasonPopupComponent } from '../../popups/reason-popup/reason-popup.component';
import { MdDialog } from '@angular/material';

@Component({
  selector: 'app-work-claim',
  templateUrl: './work-claim.component.html',
  styleUrls: ['./work-claim.component.scss']
})
export class WorkClaimComponent implements OnInit {
  rowSelect: any;
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  workClaimResponse: any;
  workClaimsList: any[];
  searchValue: any;
  searchKey: any;
  placeholdervalue: any;
  sortingField: any;
  sortingOrder: any;
  status: any;
  inoutboxType: any;
  timeZone: any;
  workClaimDate: any;
  public mm: any;
  sortClickValue: any;
  sortFieldValue: any;
  nameSort: boolean;
  claimNameSort: boolean;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService,
              public dialog: MdDialog) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.mm = moment;
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.timeZone = localStorage.getItem('timeZoneID');
    console.log('Time Zone ID:' + this.timeZone);

    /*  this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getWorkList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.organisationId) {
      this.getWorkList();
    }
  }

  selectRow(workClaims, indexValue, ev) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(workClaims));
    localStorage.setItem('claimType', 'Work');
    this.pagIndexVal = indexValue;
    /* if (this.checkedIdx === false) {
     this.checkedIdx = true;
     } else {
     this.checkedIdx = false;
     }*/

    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  

  getWorkList() {
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSED';
    } else if (this.inoutboxType === 'ALL') {
      this.status = '';
    }
    this.showLoader = true;
    this.apiCall.doGetClaims(this.organisationId, 'WORK',this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      console.log('' + JSON.stringify(result));
      this.showLoader = false;
      this.workClaimResponse = result;
      this.workClaimsList = this.workClaimResponse.claims;
      /* for (const workClaim of this.workClaimsList){
       this.workClaimsList.claimDate = moment.utc(workClaim.claimDate).toDate();
       }*/
      /*  for (let i = 0; i < this.workClaimsList.length; i++) {
       this.workClaimDate = this.workClaimsList[i];
       this.workClaimsList[i].claimDate = moment.utc(this.workClaimDate.claimDate).toDate();
       }*/
      this.totalPagesCount = this.workClaimResponse.totalPages;
      this.totalpages = this.workClaimResponse.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.workClaimsList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getWorkList();
  }

  goToView(workClaims) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(workClaims));
    localStorage.setItem('claimType', 'Work');
    if (this.inoutboxType === 'INBOX') {
      this.router.navigate(['app/view']);
    } else {
      this.router.navigate(['app/claim-certificate']);
    }
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getWorkList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getWorkList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Name of claim') {
      this.placeholdervalue = 'Name of claim';
      this.searchKey = 'claimName';
    } else if (selectedKey === 'Status') {
      this.placeholdervalue = 'Status';
      this.searchKey = 'status';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getWorkList();
    }
  }

  sortingNameClaim(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'requester_name') {
      this.nameSort = true;
      this.claimNameSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'heading') {
      this.nameSort = false;
      this.claimNameSort = true;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.claimNameSort = false;
      this.SortClicking();
    }

  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getWorkList();
  }
}
