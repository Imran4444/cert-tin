import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../../services/services';
import {Router} from '@angular/router';
import {MySharedService} from '../../../services/mySharedService ';
import * as moment from 'moment';
import {LayoutService} from '../../../layout/layout.service';
import {GlobalPopupComponent} from "../../../globalPopup/globalPopup.component";
import {ValidationProvider} from "../../../providers/validationProvider";
import {MdDialog} from "@angular/material";

@Component({
  selector: 'app-allclaim',
  templateUrl: './allclaim.component.html',
  styleUrls: ['./allclaim.component.scss']
})
export class AllClaimComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  personalClaimResponse: any;
  personalClaimsList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  claimStatus: any;
  nameSort: boolean;
  claimNameSort: boolean;
  statusSort: boolean;
  claimStatusSort: boolean;
  claimTypeSort: boolean;
  sortClickValue: any;
  sortFieldValue: any;
  public mm: any;
  public selectedClaimType: any;
  public filterData: any;
  public claimType: any;
  public keyStatus: any;
public inboxOrSentType: any;
  orgDetails: any;
  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService, private layoutService: LayoutService,
              public validations: ValidationProvider, public dialog: MdDialog) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.sortClickValue = 0;
    this.mm = moment;
    this.selectedClaimType = 'ALL';
    this.filterData = 'ALL';
    this.claimType = 'ALL';
    this.orgDetails = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log(this.orgDetails);
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    /* this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getAllClaimsList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.inoutboxType) {
      if (this.organisationId) {
        this.getAllClaimsList(this.selectedClaimType);
      }
    }
  }

  selectRow(personalClaims, indexValue, ev) {
    this.pagIndexVal = indexValue;
    /* if (this.checkedIdx === false) {
     this.checkedIdx = true;
     } else {
     this.checkedIdx = false;
     }*/
    this.claimStatus = personalClaims.status;
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('claimType', personalClaims.claimType);
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getFilter(claimType) {
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.sortClickValue = 0;
    this.getAllClaimsList(claimType);
  }

  getAllClaimsList(claimType) {
    this.layoutService.updatePreloaderState('active');
    if (claimType === 'ALL') {
      claimType = '';
    }
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
      this.keyStatus = 'status'
      this.inboxOrSentType = '&requestType='
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'DECLINED'; //CLOSE
      this.keyStatus = 'claimStatus'
      this.inboxOrSentType = '&claimType='
    } else {
      this.status = '';
      this.keyStatus = 'status'
    }

    this.showLoader = true;
    this.apiCall.doGetnewClaims(this.organisationId, claimType, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status, this.keyStatus,this.inboxOrSentType).then((result) => {
      console.log('all claims' + JSON.stringify(result));
      this.showLoader = false;
      this.personalClaimResponse = result;
      this.personalClaimsList = this.personalClaimResponse.requests;
      this.totalPagesCount = this.personalClaimResponse.totalPages;
      this.totalpages = this.personalClaimResponse.totalElements;
      this.layoutService.updatePreloaderState('hide');
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.personalClaimsList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getAllClaimsList(this.selectedClaimType);
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getAllClaimsList(this.selectedClaimType);
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getAllClaimsList(this.selectedClaimType);
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Type of claim') {
      this.placeholdervalue = 'Type of claim';
      this.searchKey = 'cType';
    } else if (selectedKey === 'Name of claim') {
      this.placeholdervalue = 'Name of claim';
      this.searchKey = 'claimType';
    } else if (selectedKey === 'Status') {
      this.placeholdervalue = 'Status';
      this.searchKey = 'status';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getAllClaimsList(this.selectedClaimType);
    }
  }

  goToView(personalClaims) {
    this.claimStatus = personalClaims.status;
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('claimType', personalClaims.requestType);
    if (personalClaims.requestType === 'ENDORSEMENT') {
      if (this.claimStatus === 'OPEN') {
        this.router.navigate(['app/view']);
      } else if (this.claimStatus === 'CLOSED') {
        localStorage.setItem('claimType', personalClaims.claim.claimType);
        this.router.navigate(['app/claim-certificate']);
      }
    } else {
      if (this.orgDetails.organisation.stripePlanName !== 'Trial') {
        if (this.claimStatus === 'OPEN') {
          localStorage.setItem('CertificateType', personalClaims.requestType);
          this.router.navigate(['app/cert-inbox-view']);
        } else if (this.claimStatus === 'CLOSED') {
          this.router.navigate(['app/certificate']);
        }
      } else {
        const accountVerify = this.validations.errorPopup('Account Level', 'Your account has to be professional  to unlock this feature. To have a Professional account please complete your KYC by updating your personal and organisation details in My account.', '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: accountVerify,
          disableClose: true
        });
      }
    }
  }

// sortingNameClaim(sortingType, sortingOrder) {
// this.sortingField = sortingType;
// this.sortingOrder = sortingOrder;
// console.log('sorting order...' + sortingOrder + 'sorting type...' + sortingType);
// this.getAllClaimsList();
// }

  sortingNameClaim(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'requester_name') {
      this.nameSort = true;
      this.claimTypeSort = false;
      this.claimNameSort = false;
      this.statusSort = false;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'claim_type') {
      this.nameSort = false;
      this.claimTypeSort = true;
      this.claimNameSort = false;
      this.statusSort = false;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'heading') {
      this.nameSort = false;
      this.claimTypeSort = false;
      this.claimNameSort = true;
      this.statusSort = false;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'status') {
      this.nameSort = false;
      this.claimTypeSort = false;
      this.claimNameSort = false;
      this.statusSort = true;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'claim_status') {
      this.nameSort = false;
      this.claimTypeSort = false;
      this.claimNameSort = false;
      this.statusSort = false;
      this.claimStatusSort = true;
      this.SortClicking();
    }
  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getAllClaimsList(this.selectedClaimType);
  }
}
