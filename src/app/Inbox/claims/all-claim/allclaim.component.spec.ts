import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {AllClaimComponent} from './allclaim.component';

describe('PersonalComponent', () => {
  let component: AllClaimComponent;
  let fixture: ComponentFixture<AllClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AllClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AllClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
