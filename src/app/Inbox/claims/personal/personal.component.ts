import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../services/services';
import { Router } from '@angular/router';

import * as moment from 'moment';
import { MySharedService } from '../../../services/mySharedService ';

@Component({
  selector: 'app-personal',
  templateUrl: './personal.component.html',
  styleUrls: ['./personal.component.scss']
})
export class PersonalComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  personalClaimResponse: any;
  personalClaimsList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  personalClaimDate: any;
  public mm: any;
  nameSort: boolean;
  claimNameSort: boolean;
  statusSort: boolean;
  claimStatusSort: boolean;
  sortClickValue: any;
  sortFieldValue: any;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.mm = moment;
    this.sortClickValue = 0;
  }

  ngOnInit() {
    this.nameSort = false;
    this.claimNameSort = false;
    this.statusSort = false;
    this.claimStatusSort = false;
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    /* this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getPersonalClaimsList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.inoutboxType) {
      if (this.organisationId) {
        this.getPersonalClaimsList();
      }
    }
  }

  selectRow(personalClaims, indexValue, ev) {
    this.pagIndexVal = indexValue;
    /* if (this.checkedIdx === false) {
     this.checkedIdx = true;
     } else {
     this.checkedIdx = false;
     }*/
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('claimType', 'Personal');
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getPersonalClaimsList() {
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSED';
    } else if (this.inoutboxType === 'ALL') {
      this.status = '';
    }

    this.showLoader = true;
    this.apiCall.doGetClaims(this.organisationId, 'PERSONAL', this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      console.log('' + JSON.stringify(result));
      this.showLoader = false;
      this.personalClaimResponse = result;
      this.personalClaimsList = this.personalClaimResponse.claims;
      this.totalPagesCount = this.personalClaimResponse.totalPages;
      this.totalpages = this.personalClaimResponse.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.personalClaimsList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getPersonalClaimsList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getPersonalClaimsList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getPersonalClaimsList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Name of claim') {
      this.placeholdervalue = 'Name of claim';
      this.searchKey = 'claimName';
    } else if (selectedKey === 'Status') {
      this.placeholdervalue = 'Status';
      this.searchKey = 'status';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getPersonalClaimsList();
    }
  }

  goToView(personalClaims) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('claimType', 'Personal');
    if (this.inoutboxType === 'INBOX') {
      this.router.navigate(['app/view']);
    } else {
      this.router.navigate(['app/claim-certificate']);
    }
  }

  sortingNameClaim(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'requester_name') {
      this.nameSort = true;
      this.claimNameSort = false;
      this.statusSort = false;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'heading') {
      this.nameSort = false;
      this.claimNameSort = true;
      this.statusSort = false;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'status') {
      this.nameSort = false;
      this.claimNameSort = false;
      this.statusSort = true;
      this.claimStatusSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'claim_status') {
      this.nameSort = false;
      this.claimNameSort = false;
      this.statusSort = false;
      this.claimStatusSort = true;
      this.SortClicking();
    }
  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getPersonalClaimsList();
  }
}
