import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CourseClaimComponent } from './course-claim.component';

describe('CourseClaimComponent', () => {
  let component: CourseClaimComponent;
  let fixture: ComponentFixture<CourseClaimComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CourseClaimComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CourseClaimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
