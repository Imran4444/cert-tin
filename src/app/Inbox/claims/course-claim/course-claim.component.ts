import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../services/services';
import { Router } from '@angular/router';
import { MySharedService } from "../../../services/mySharedService ";
import * as moment from 'moment';

@Component({
  selector: 'app-course-claim',
  templateUrl: './course-claim.component.html',
  styleUrls: ['./course-claim.component.scss']
})
export class CourseClaimComponent implements OnInit {

  rowSelect: any;
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  courseClaimResponse: any;
  courseClaimsList: any[];
  searchValue: any;
  searchKey: any;
  placeholdervalue: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  public mm: any;
  public nameSort: boolean;
  public claimNameSort: boolean;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService) {
    this.nameSort = false
    this.claimNameSort= false
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.mm = moment;
  }

  ngOnInit() {

    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    /* this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getCourseList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.organisationId) {
      this.getCourseList();
    }
  }

  selectRow(courseClaims, indexValue, ev) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(courseClaims));
    localStorage.setItem('claimType', 'Course');
    this.pagIndexVal = indexValue;
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getCourseList() {
    if (this.inoutboxType === 'INBOX') {
      this.status = 'OPEN';
    } else if (this.inoutboxType === 'OUTBOX') {
      this.status = 'CLOSED';
    } else if (this.inoutboxType === 'ALL') {
      this.status = '';
    }
    this.showLoader = true;
    this.apiCall.doGetClaims(this.organisationId, 'COURSE', this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.status).then((result) => {
      console.log('' + JSON.stringify(result));
      this.showLoader = false;
      this.courseClaimResponse = result;
      this.courseClaimsList = this.courseClaimResponse.claims;
      this.totalPagesCount = this.courseClaimResponse.totalPages;
      this.totalpages = this.courseClaimResponse.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.courseClaimsList = [];
        this.totalpages = 0;
      }
    });
  }

  getCahnaged() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getCourseList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getCourseList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getCourseList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'Name') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'personName';
    } else if (selectedKey === 'Name of claim') {
      this.placeholdervalue = 'Name of claim';
      this.searchKey = 'claimName';
    } else if (selectedKey === 'Status') {
      this.placeholdervalue = 'Status';
      this.searchKey = 'status';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getCourseList();
    }
  }

  goToView(courseCalims) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(courseCalims));
    localStorage.setItem('claimType', 'Course');
    if (this.inoutboxType === 'INBOX') {
      this.router.navigate(['app/view']);
    } else {
      this.router.navigate(['app/claim-certificate']);
    }
  }

  sortingNameClaim(sortingType, sortingOrder) {
    this.sortingField = sortingType;
    this.sortingOrder = sortingOrder;
    console.log('sorting order...' + sortingOrder + 'sorting type...' + sortingType);
    this.getCourseList();
  }
}
