import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CertInboxViewComponent } from './cert-inbox-view.component';

describe('CertInboxViewComponent', () => {
  let component: CertInboxViewComponent;
  let fixture: ComponentFixture<CertInboxViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CertInboxViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CertInboxViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
