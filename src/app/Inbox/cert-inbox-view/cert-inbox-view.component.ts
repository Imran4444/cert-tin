import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../services/services';
import { CertModal } from '../../Modals/certModal';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';
import { FormControl, FormGroup } from '@angular/forms';
import { auditService } from '../../providers/audit.service';
import { LayoutService } from '../../layout/layout.service';
import { TemplateServices } from '../../services/templateServices';
import { CertificateApply } from "../../Modals/certificateapply";
import { AttendLetterModal } from "../../Modals/attendlettermodal";
import { ProofInternModal } from "../../Modals/proofinternmodal";
import { WorkAttestationModal } from "../../Modals/workattestationmodal";
import { QualificationCertificateModal } from "../../Modals/qualificationCertificatemodal";
import { LicenseCertificateModal } from "../../Modals/licensecertificatemodal";
import { ReferenceLettermodal } from "../../Modals/referencelettermodal";
import { IssueCertificateModal } from "../../Modals/issuecertificatemodal";
import { GlobalPopupComponent } from "../../globalPopup/globalPopup.component";
import { ValidationProvider } from "../../providers/validationProvider";
import { MdDialog } from "@angular/material";
import { AWS_PHOTO_UPLOAD_URL, DEV_ROOT_PATH } from '../../endpoint';
import { Http } from '@angular/http';
import { ProofOfEndrosment } from '../../Modals/proofOfEndrosment';

@Component({
  selector: 'app-cert-inbox-view',
  templateUrl: './cert-inbox-view.component.html',
  styleUrls: ['./cert-inbox-view.component.scss']
})
export class CertInboxViewComponent implements OnInit {
  claimDetails: any;
  certType: any;
  getCertDetailsResponse: any;
  getCertDetail: any;
  getPersonDetail: any;
  commnets: any;
  endorseLevel: any;
  showLoader: any;
  endorseDeclineResult: string;
  inoutboxType: any;
  certificateID: any;
  certificateDetails: any;
  public mm: any;
  public template: any;
  public borderTop: string;
  public borderBtm: string;
  public currentHour: any;
  startDate: any;
  endDate: any;
  certificateIssueModel: any;
  qualicationModel: any;
  workattesstationModel: any;
  submitTry: any;
  issueForm: any;
  public personResponse: any;
  public footerInfo: any;
  public loginResponse: any;
  public personId: any;
  auditReq: any;
  public viewType: any;
  public certTypeList: any;
  public selected: any
  public selectCertTypeResponse: any;
  public certTypeTemplate: any;
  public certificateTypes: any;
  public submitted: any;
  certificateapply: any;
  auditApplyReq: any;
  public fromDateName: any;
  public endDateName: any;
  public attendLetterModal: any;
  public proofinternModal: any;
  workAttestationModal: any;
  qualificationModal: any;
  licenseCertificateModal: any;
  referencelettermodalData: any;
  reqFormsData: any;
  applyCertificateResp: any;
  auditIssueCert: any;
  issuedCertResponse: any;
  public organizationName: any;
  public startDateInbox: any;
  public endDateInbox: any;
  public textViewInfo: any;
  public resultData: any;
  public file: any;
  public certImg: any;
  public showloader: boolean;
  public btnText: any;
  public attachmentRequired: boolean;
  public rootPath: string;
  maxDate: any;
  endDateCheck: any;
  issueLoader: any;
  public proofOfEndros: ProofOfEndrosment;
  public transcriptFuture: any;
  public transcriptForm: any;
  public pdfSrc: any;
  public page: number;
  public totalPages: number;
  public isLoaded: boolean;

  constructor(public apiCall: ApiCalls,
              public router: Router,
              public datePipe: DatePipe,
              public auditHistory: auditService,
              public layoutService: LayoutService,
              public tempApi: TemplateServices,
              public validations: ValidationProvider,
              public dialog: MdDialog,
              public http: Http) {
    this.page =1;
    this.pdfSrc = 'assets/pdfcSample/sample.pdf'
    this.rootPath = DEV_ROOT_PATH;
    this.attachmentRequired = false;

    this.viewType = 'text';
    this.mm = moment;
    this.reqFormsData = {};
    this.maxDate = new Date();
    this.certificateIssueModel = new CertModal('', '', '', '', '', '', '', '', '', '', '', '');
    this.certificateapply = new CertificateApply('', '', '', '')
    this.attendLetterModal = new AttendLetterModal('', '', '', '', '', '', '', '', '', '');
    this.proofinternModal = new ProofInternModal('', '', '', '', '', '', '', '', '', '', '', '', '');
    this.workAttestationModal = new WorkAttestationModal('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    this.qualificationModal = new QualificationCertificateModal('', '', '', '', '', '', '', '', '')
    this.licenseCertificateModal = new LicenseCertificateModal('', '', '', '', '', '', '', '');
    this.referencelettermodalData = new ReferenceLettermodal('', '', '', '', '', '', '', '', '', '', '', '', '')
    this.proofOfEndros = new ProofOfEndrosment('', '', '', '', '', '', '', '', '', '');
    this.issueForm = new FormGroup({
      certName: new FormControl(''),
      courseName: new FormControl(''),
      splczation: new FormControl(''),
      strtDate: new FormControl(''),
      endate: new FormControl({value: '', disabled: this.proofinternModal.currentlyEmployed}),
      descrptn: new FormControl(''),
      certificateTypeform: new FormControl(''),
      templateForm: new FormControl(''),
      eventName: new FormControl(''),
      roleName: new FormControl(''),
      currentlyWorkingForm: new FormControl(''),
      recomondForm: new FormControl(''),
      courseNameForm: new FormControl(''),
      knownYearsForm: new FormControl(''),
      certificationForm: new FormControl(''),
      locationForm: new FormControl(''),
      salaryeForm: new FormControl(''),
      empNumberForm: new FormControl(''),
      specializationForm: new FormControl(''),
      transcriptForm: new FormControl(''),
      levelForm: new FormControl(''),
      certificateNumberForm: new FormControl(''),
      positionForm: new FormControl(''),
      fullTimeForm: new FormControl(''),
      studentIdform: new FormControl(''),
      courseOfstudyForm: new FormControl('')
    });
  }

  ngOnInit() {
    this.issueLoader = false;
    this.submitted = false;
    this.endDateCheck = false;
    this.transcriptForm = true;
    this.transcriptFuture = new Date();
    this.getcertTypes()
    this.workAttestationModal.fulltime = false;
    this.proofinternModal.currentlyEmployed = false;
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.personId = this.loginResponse.personId;
    this.currentHour = this.datePipe.transform(new Date(), 'HH:mm');
    this.certificateDetails = JSON.parse(localStorage.getItem('CLAIMDETAILS'));
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    this.certificateID = this.certificateDetails.id;
    console.log('Person Response' + JSON.stringify(this.personResponse));
    console.log('Claim Response' + JSON.stringify(this.certificateDetails));
    /* console.log( JSON.stringify(this.certificateID));*/
    this.getCertDetails();
    this.certType = localStorage.getItem('CertificateType');
  }

  getcertTypes() {
    this.tempApi.getCertTypes().then((res) => {
      console.log('certTypeList' + JSON.stringify(res));
      this.certTypeList = res;
    });
  }

  selectedCertType(typeObj: any) {
    console.log(this.issueForm.value);
    this.certificateapply.contentTemplateId = '';
    console.log(typeObj);
    this.selected = typeObj;
    if (this.selected === 'LICENSE_CERTIFICATE') {
      this.fromDateName = 'Issued Date';
      this.endDateName = 'Expiry Date';
    } else {
      this.fromDateName = 'Start Date';
      this.endDateName = 'End Date';
    }
    this.tempApi.getCertContetntType(typeObj).then((result) => {
      this.selectCertTypeResponse = result;
      this.certTypeTemplate = this.selectCertTypeResponse.templates;
      console.log('select cert type' + JSON.stringify(result));
    });

  }

  setView(viewType: any) {
    this.viewType = viewType;
  }

  uploadPicture(event) {
    this.showloader = true;
    let formData: FormData = new FormData();
    let eventObj: MSInputMethodContext = <MSInputMethodContext>event;
    let target: HTMLInputElement = <HTMLInputElement>eventObj.target;
    let files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];

    this.http.post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData).map(res => res.json())
      .subscribe((data) => {
        this.btnText = files[0].name;
        // this.logoUrl = data.imageURL;
        this.certImg = data.imageURL;
        this.showloader = false;
        this.attachmentRequired = false;
        // alert('Image Uploaded Successfully');
      }, (err) => {
        this.showloader = false;
        alert('Oops !! Image Uploading Failed, Please Try Again');
      });
  }

  getCertDetails() {
    // doGetClaimDetails
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.certificateID = this.certificateDetails.id;
    this.apiCall.doGetClaimDetails(this.certificateID).then((result) => {
      this.showLoader = false;
      this.getCertDetailsResponse = result;
      this.getCertDetail = this.getCertDetailsResponse.calimRequest;
      this.footerInfo = this.getCertDetailsResponse.organisation;
      this.organizationName = this.getCertDetailsResponse.organisation.organisation.name;
      console.log('cert details resp:' + JSON.stringify(this.getCertDetailsResponse));
      this.certificateapply.certType = this.getCertDetail.requestType;
      console.log('cert type...' + this.certificateapply.certType);
      this.qualificationModal.courseName = this.getCertDetail.claim.heading;
      this.licenseCertificateModal.certification = this.getCertDetail.claim.heading
      this.attendLetterModal.event = this.getCertDetail.claim.heading
      this.proofinternModal.role = this.getCertDetail.claim.heading
      if (this.getCertDetail.requestType === 'TRANSCRIPT') {
        this.btnText = 'Upload Transcript';
      } else {
        this.btnText = 'Upload Template';
      }
      this.attendLetterModal.location = this.getCertDetail.claim.location
      if (this.getCertDetail.claim.startDate) {
        this.certificateIssueModel.startDate = new Date(this.getCertDetail.claim.startDate)
      }
      if (this.getCertDetail.claim.endDate) {
        this.certificateIssueModel.endDate = new Date(this.getCertDetail.claim.endDate)
      }
      this.selectedCertType(this.getCertDetail.requestType);


      /* if (this.getCertDetail.requestType === 'QUALIFICATION_CERTIFICATE') {
       this.certificateapply.certType = 'QUALIFICATION_CERTIFICATE';
       this.selectedCertType('QUALIFICATION_CERTIFICATE');
       this.certificateIssueModel.courseName = this.getCertDetail.courseName;
       } else if (this.getCertDetail.certificateType === 'WORK_ATTESTATION') {
       this.selectedCertType('WORK_ATTESTATION');
       this.certificateIssueModel.certificateName = this.getCertDetail.certificateName;
       }*/
      /*this.certificateIssueModel.specialization = this.getCertDetail.specialization;
       this.certificateIssueModel.startDate = new Date(this.getCertDetail.periodFromDate);
       this.certificateIssueModel.endDate = new Date(this.getCertDetail.periodEndDate);
       this.certificateIssueModel.description = this.getCertDetail.description;*/
      this.template = this.getCertDetailsResponse.template;
      this.getPersonDetail = this.getCertDetailsResponse.person;
      if (this.template === 'WORKATTESTATION_DEFAULT' || this.template === 'QUALIFICATION_DEFAULT' || this.template === 'WORKATTESTATION_ORG' || this.template === 'QUALIFICATION_ORG') {
        this.borderTop = 'assets/images/certBorders/border-top-1.png';
        this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
      } else if (this.template === 'WORKATTESTATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      } else if (this.template === 'QUALIFICATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-3.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-3.png';
      } else if (this.template === 'LICENSE_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      } else {
        this.borderTop = 'assets/images/certBorders/border-top-1.png';
        this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
      }
      this.layoutService.updatePreloaderState('hide');
      // this.endorseLevel = this.getClaimDetail.level;
      // this.commnets = this.getClaimDetail.comment;
      // if (this.getClaimDetail.accountlevel === 1) {
      //   this.endorseLevel = 'Personal';
      // }
    }, (error) => {
      console.log(error);
      this.showLoader = false;
    });
  }

  createAuditLog() {
    this.auditHistory.userAudit('Certificate');
  }

  getEndroseDeclined(Status) {
    console.log(this.issueForm);
    this.submitTry = true;
    if (this.issueForm.valid) {
      console.log('' + Status);

      this.startDate = this.datePipe.transform(this.certificateIssueModel.startDate, 'yyyy-MM-dd') + ' ' + this.currentHour + ':00.527Z';
      this.endDate = this.datePipe.transform(this.certificateIssueModel.endDate, 'yyyy-MM-dd') + ' ' + this.currentHour + ':00.527Z';

      this.showLoader = true;
      if (Status === 'ISSUED') {
        // const endorseRequest = new CertModal(this.certificateID, Status, '', '1');
        this.auditReq = this.auditHistory.userAudit('CERTIFICATE ' + Status);
        const endorseRequest = new CertModal(this.auditReq, this.certificateID, this.certificateIssueModel.certificateName, Status, this.certificateIssueModel.courseName, this.certificateIssueModel.description, this.startDate, this.endDate, this.certificateIssueModel.specialization, 'Organisation', this.personId, this.certificateIssueModel.customTemplateUrl);
        console.log('endorseReq' + JSON.stringify(endorseRequest));
        console.log('auditReq' + JSON.stringify(this.auditReq));
        localStorage.setItem('issueObj', JSON.stringify(endorseRequest));
        localStorage.setItem('certDetails', JSON.stringify(this.getCertDetail));
        localStorage.setItem('INOUTBOX', 'CERTIFICATES');
        this.router.navigate(['/app/issue-endrose']);

        /*   this.apiCall.doGetUpdateCertificateStatus(endorseRequest).then((result) => {
         this.showLoader = false;
         console.log(result)
         if (this.getCertDetail.certificateType === 'WORKATTESTATION') {
         this.router.navigate(['app/work-attestations'])
         } else if (this.getCertDetail.certificateType === 'QUALIFICATION') {
         this.router.navigate(['app/qualification'])
         }
         }, (error) => {
         console.log(error);
         this.showLoader = false;
         });*/
      } else {
        /* if (this.getCertDetail.certificateType === 'WORKATTESTATION') {
         this.router.navigate(['app/work-attestations']);
         } else if (this.getCertDetail.certificateType === 'QUALIFICATION') {
         this.router.navigate(['app/qualification']);
         }*/
        localStorage.setItem('INOUTBOX', 'INBOX');
        this.router.navigate(['app/all-certificates']);
      }
    } else {
      console.log(this.issueForm.value);
    }
  }

  get f() {
    return this.issueForm.controls;
  }
  afterLoadComplete(pdfData: any) {
    this.totalPages = pdfData.numPages;
    this.isLoaded = true;
  }
  nextPage() {
    this.page++;
  }

  prevPage() {
    this.page--;
  }

  applyChanges() {
    this.submitted = true;
    this.reqFormsData = new ReferenceLettermodal(this.referencelettermodalData.yearsKnown, this.referencelettermodalData.position, +this.proofinternModal.recommendationLevel, 'M', this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.phones[0].countryCode, this.personResponse.person.phones[0].number, this.personResponse.person.emails[0].email, this.personResponse.person.firstName, this.personResponse.person.lastName, '');
    console.log('form validddd' + this.issueForm.invalid)
    if (this.issueForm.invalid || this.certificateapply.contentTemplateId === 'custom_template') {
      if (this.certImg) {
        this.checkPdf(this.certImg);
        console.log('transcript 123.....' + this.certificateapply.certType)
        if (this.certificateapply.certType == 'TRANSCRIPT') {
          this.certificateapply.contentTemplateId = 'custom_template'
          console.log('transcript.....')
        }
        this.certificateIssueModel.customTemplateUrl = this.certImg;
        console.log('custom template url.....' + this.certificateIssueModel.customTemplateUrl + 'iddddd.....' + this.certificateapply.contentTemplateId)
      } else {
        this.attachmentRequired = true;
      }
    } else {

      this.certificateIssueModel.customTemplateUrl = ''
      console.log(this.certificateIssueModel.startDate)
      if (this.certificateIssueModel.startDate) {
        this.startDateInbox = this.datePipe.transform(this.certificateIssueModel.startDate, 'yyyy-MM-dd')
      } else {
        this.startDateInbox = ''
      }
      if (this.certificateIssueModel.endDate) {
        this.endDateInbox = this.datePipe.transform(this.certificateIssueModel.endDate, 'yyyy-MM-dd')
      } else {
        this.endDateInbox = '';
      }
      if (this.certificateapply.certType === 'ATTENDANCE_LETTER') {
        this.reqFormsData = new AttendLetterModal(this.attendLetterModal.event, this.startDateInbox, this.endDateInbox, this.attendLetterModal.location, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.firstName, this.personResponse.person.lastName, '');
      } else if (this.certificateapply.certType === 'PROOF_OF_INTERNSHIP') {
        this.reqFormsData = new ProofInternModal(this.proofinternModal.role, this.proofinternModal.currentlyEmployed, this.startDateInbox, this.endDateInbox, this.attendLetterModal.location, +this.proofinternModal.recommendationLevel, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.firstName, this.personResponse.person.lastName, 'M', '');
      } else if (this.certificateapply.certType === 'WORK_ATTESTATION') {
        this.reqFormsData = new WorkAttestationModal(this.proofinternModal.role, this.proofinternModal.currentlyEmployed, this.startDateInbox, this.endDateInbox, this.workAttestationModal.salary, this.workAttestationModal.employeeNumber, 'M', this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.workAttestationModal.fulltime, this.personResponse.person.phones[0].countryCode, this.personResponse.person.phones[0].number, this.personResponse.person.emails[0].email, this.personResponse.person.firstName, this.personResponse.person.lastName, '');
      } else if (this.certificateapply.certType === 'QUALIFICATION_CERTIFICATE') {
        this.reqFormsData = new QualificationCertificateModal(this.qualificationModal.courseName, this.qualificationModal.specialisation, this.startDateInbox, this.endDateInbox, this.qualificationModal.tranScript, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, '');
      } else if (this.certificateapply.certType === 'LICENSE_CERTIFICATE') {
        this.reqFormsData = new LicenseCertificateModal(this.licenseCertificateModal.certification, this.startDateInbox, this.licenseCertificateModal.level, this.endDateInbox, this.licenseCertificateModal.certificationNumber, this.getPersonDetail.firstName, this.getPersonDetail.lastName, '');
      } else if (this.certificateapply.certType === 'REFERENCE_LETTER') {
        this.reqFormsData = new ReferenceLettermodal(this.referencelettermodalData.yearsKnown, this.referencelettermodalData.position, +this.proofinternModal.recommendationLevel, 'M', this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.phones[0].countryCode, this.personResponse.person.phones[0].number, this.personResponse.person.emails[0].email, this.personResponse.person.firstName, this.personResponse.person.lastName, '');
      } else if (this.certificateapply.certType === 'PROOF_OF_ENROLLMENT') {
        this.reqFormsData = new ProofOfEndrosment(this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.proofOfEndros.studentID, this.organizationName, this.workAttestationModal.fulltime, this.qualificationModal.courseName, this.qualificationModal.specialisation, this.startDateInbox, this.endDateInbox, '');
      }

      this.auditApplyReq = this.auditHistory.userAudit('Certificate Applynow');
      this.certificateapply = new CertificateApply(this.auditApplyReq, this.certificateapply.certType, this.certificateapply.contentTemplateId, this.reqFormsData);
      console.log('apply changes req...' + JSON.stringify(this.certificateapply))
      this.apiCall.doApplyCertificate(this.certificateapply).then((result) => {
        this.applyCertificateResp = result;
        this.textViewInfo = this.applyCertificateResp.content
        if (this.certificateapply.certType == 'TRANSCRIPT') {
          this.transcriptForm = false;
        }
        console.log('applyCertificateResp...' + JSON.stringify(this.applyCertificateResp));
      }, (error) => {
        console.log('applyCertificateResp error...' + JSON.stringify(error));
      });
    }
  }

  issueCertificate() {
    this.issueLoader = true;
    if (this.certificateapply.contentTemplateId !== 'custom_template') {
      this.certificateIssueModel.customTemplateUrl = '';
      this.issueCall();
    } else {
      this.applyCertificateResp = {}
      this.applyCertificateResp.htmlContent = '';
      this.applyCertificateResp.content = '';
      this.issueCall();
    }
  }

  issueCall() {

    this.issueLoader = true;
    if (this.certificateIssueModel.startDate) {
      const startDateInbox = this.datePipe.transform(this.certificateIssueModel.startDate, 'yyyy-MM-dd');
      this.startDateInbox = startDateInbox + ' ' + this.currentHour + ':00.527Z';
    } else {
      this.startDateInbox = '';
    }
    if (this.certificateIssueModel.endDate) {
      const endDateInbox = this.datePipe.transform(this.certificateIssueModel.endDate, 'yyyy-MM-dd');
      this.endDateInbox = endDateInbox + ' ' + this.currentHour + ':00.527Z';
    } else {
      this.endDateInbox = '';
    }
    this.auditIssueCert = this.auditHistory.userAudit('Issue certificate');
    const issuecertificateModal = new IssueCertificateModal([], this.auditIssueCert, this.certificateapply.certType, this.certificateapply.certType, this.getCertDetail.id, this.qualificationModal.courseName, this.getCertDetail.claim.description, this.endDateInbox, this.getPersonDetail.cimPersonId, this.getCertDetail.claim.requesterPublicKey, this.personId, this.footerInfo.organisation.id, this.qualificationModal.specialisation, this.startDateInbox, this.endDateInbox, this.certificateIssueModel.customTemplateUrl, this.applyCertificateResp.htmlContent, this.applyCertificateResp.content, this.attendLetterModal.event, this.proofinternModal.role, this.licenseCertificateModal.certification, this.workAttestationModal.position, this.referencelettermodalData.yearsKnown, '', '');
    console.log('issued request....' + JSON.stringify(issuecertificateModal));
    this.apiCall.doIssueCertificate(issuecertificateModal).then((result) => {
      this.issueLoader = false;
      this.issuedCertResponse = result;
      /*  this.resultData = this.validations.errorPopup('Success', this.issuedCertResponse.responseMessage, ['app/all-claims']);
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: this.resultData,
          disableClose: true
        });*/
      console.log('issuedCertResponse...' + JSON.stringify(this.issuedCertResponse));
      localStorage.setItem('verifyObj', JSON.stringify(this.issuedCertResponse));
      this.router.navigate(['app/verify']);
    }, (error) => {
      this.issueLoader = false;
      this.resultData = this.validations.errorPopup('Fail', 'Issued failed, Please try again.', ['app/all-claims']);
      const dialogRef = this.dialog.open(GlobalPopupComponent, {
        width: '450px',
        data: this.resultData,
        disableClose: true
      });
      console.log('issuedCertResponse error...' + JSON.stringify(error));
    });
  }

  getHtmlView() {
    let txt = this.textViewInfo;
    let txttostore = '<p>' + txt.replace(/\n/g, "</p>\n<p>") + '</p>';
    console.log(txttostore);
    this.applyCertificateResp.content = this.textViewInfo
    this.applyCertificateResp.htmlContent = txttostore;
  }

  cancelCertificate() {
    this.router.navigate(['app/all-claims']);
  }

  openEvidence(evidences) {
    window.open(evidences, '_blank');
  }

  openCertificate(evidences) {
    let certificateUrl = this.rootPath + '/#/badges/certificate/' + evidences
    console.log('aaaa..' + certificateUrl)
    console.log('open certificate..')
    window.open(certificateUrl, '_blank');
  }

  currentlyWorkChange(checkValue) {
    console.log('check value...' + checkValue)
    if (checkValue == true) {
      this.endDateCheck = true;
      this.certificateIssueModel.endDate = '';
    } else {
      this.endDateCheck = false;
      if (this.getCertDetail.claim.endDate) {
        this.certificateIssueModel.endDate = new Date(this.getCertDetail.claim.endDate)
      }

    }
  }

  checkPdf(url: any) {
    console.log(url);
    if (url.includes('.pdf')) {
       return true
    }else{
      return false
    }

  }

  /*isCompletedCheck(isCompletedValue) {
   console.log('is comple t..' + isCompletedValue);
   if (isCompletedValue == true) {
   this.transcriptFuture = new Date();
   }else {
   this.certificateIssueModel.startDate = new Date()
   }
   }*/
  contentTemplateChange(contentTemplate) {
    console.log('selected template:' + contentTemplate)
    if (contentTemplate === 'custom_template') {

    }
  }
}
