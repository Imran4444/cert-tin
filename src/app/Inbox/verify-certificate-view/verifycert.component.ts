import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { ApiCalls } from "../../services/services";
import * as moment from 'moment';
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'verification-certificate',
  templateUrl: './verifycert.component.html',
  styleUrls: ['./verifycert.component.css']
})
export class VerifyCertificateComponent implements OnInit {
  storedValue: any;
  status: any;
  shareVerifyData: any;
  shareVerifyDataType: any;
  calimDetailsResp: any;
  certificateDetailsResp: any;
  certificateStatustype: any;
  claimStatustype: any;
  template: any;
  borderBtm: any;
  borderTop: any;
  showLoader: any;
  pictureUrl: any;
  public mm: any;
  public personResponse: any;
  public footerInfo: string | any;

  constructor(public router: Router,
              public apiCall: ApiCalls, public layoutService: LayoutService) {
    this.mm = moment;
    this.storedValue = localStorage.getItem('verifiedStatus');
    if (this.storedValue === 'notVerified') {
      this.status = false;
    } else if (this.storedValue === 'verified') {
      this.status = true;
    }
  }

  ngOnInit() {
    this.shareVerifyData = JSON.parse(localStorage.getItem('SHAREVERIFYDETAILS'))
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
    console.log('share verify data..' + JSON.stringify(this.shareVerifyData));
    this.shareVerifyDataType = this.shareVerifyData.type;
    if (this.shareVerifyDataType === 'CLAIM') {
      this.getClaimDetails(this.shareVerifyData.ccId);
    } else if (this.shareVerifyDataType === 'CERTIFICATE') {
      this.getCertificateDetails(this.shareVerifyData.ccId);
    }
  }

  getClaimDetails(claimId) {
    this.layoutService.updatePreloaderState('active');
    this.apiCall.doGetClaimDetailsData(claimId).then((result) => {
      this.calimDetailsResp = result;
      this.template = this.calimDetailsResp.claim;
      this.pictureUrl = this.calimDetailsResp.person.pictureUrl;

      console.log(this.calimDetailsResp);
      this.certificateStatustype = ''
      this.claimStatustype = this.calimDetailsResp.claim.claimType;
      this.layoutService.updatePreloaderState('hide');
    }, (error) => {
      console.log('error view claim...' + JSON.stringify(error));
    });
  }

  getCertificateDetails(certificateId) {
    this.apiCall.doGetCertificateDetailsData(certificateId).then((result) => {
      this.certificateDetailsResp = result;
      console.log(this.certificateDetailsResp);
      this.footerInfo = this.certificateDetailsResp.organisation
      this.template = this.certificateDetailsResp.certificate.template;
      this.pictureUrl = this.certificateDetailsResp.person.pictureUrl;
      this.claimStatustype = ''
      this.certificateStatustype = this.certificateDetailsResp.certificate.certificateType;
      if (this.template === 'WORKATTESTATION_DEFAULT' || this.template === 'QUALIFICATION_DEFAULT' || this.template === 'WORKATTESTATION_ORG' || this.template === 'QUALIFICATION_ORG') {
        this.borderTop = 'assets/images/certBorders/border-top-1.png';
        this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
      } else if (this.template === 'WORKATTESTATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-2.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
      } else if (this.template === 'QUALIFICATION_TEMP') {
        this.borderTop = 'assets/images/certBorders/top-border-3.png';
        this.borderBtm = 'assets/images/certBorders/bottom-border-3.png';
      }
      // console.log('cert details resp..' + JSON.stringify(this.certificateDetailsResp));
      console.log('cert details resp..' + JSON.stringify(this.certificateDetailsResp));
    }, (error) => {
      console.log('error view cert...' + JSON.stringify(error));
    });

  }

  verifyPage() {
    const verifyObj = {
      requesterKey: this.certificateDetailsResp.certificate.individualPublicKey,
      issuerPubkey: this.certificateDetailsResp.certificate.individualPublicKey,
      txHash: this.certificateDetailsResp.transactions[0].txHash,
      txId: this.certificateDetailsResp.transactions[0].id
    }
    localStorage.setItem('verifyObj', JSON.stringify(verifyObj));
    this.router.navigate(['/app/verify']);

  }

  cancel() {
    if (this.status) {
      this.router.navigate(['/app/verified']);
    } else {
      this.router.navigate(['/app/verifications']);
    }
  }
}
