import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCalls } from '../../services/services';
import {MySharedService} from "../../services/mySharedService ";
@Component({
  selector : 'verfied-page',
  templateUrl : './verified.component.html',
  styleUrls : ['./verified.component.css']
})
export class VerifiedComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  personalClaimResponse: any;
  personalClaimsList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  verifiedList: any;
  data: any;
  sortClickValue: any;
  sortFieldValue: any;
  requestSort: boolean;
  nameSort: boolean;
  
  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.getVerifiedList();
  }
  getVerifiedList() {
    this.showLoader = true;
    this.apiCall.getVerifiedList(this.organisationId).then((result) => {
      this.showLoader = false;
      this.data = result;
      this.verifiedList = this.data.sharedDetails;
      console.log(this.verifiedList);
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.verifiedList =  [];
      }
    });
  }

  selectRow(personalClaims, indexValue, ev) {
    this.pagIndexVal = indexValue;
    /* if (this.checkedIdx === false) {
     this.checkedIdx = true;
     } else {
     this.checkedIdx = false;
     }*/
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('claimType', 'Personal');
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getPersonalClaimsList() {

  }

  getCahnaged() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getVerifiedList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getVerifiedList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getVerifiedList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'requestType') {
      this.placeholdervalue = 'Request Type';
      this.searchKey = 'requestType';
    } else if (selectedKey === 'requesterName') {
      this.placeholdervalue = 'Request Name';
      this.searchKey = 'requesterName';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      this.getVerifiedList();
    }
  }

  goToView() {
    if (this.inoutboxType === 'INBOX') {
      this.router.navigate(['app/view']);
    } else {
      this.router.navigate(['app/claim-certificate']);
    }
  }

  sorting(sortingType, sortingOrder) {
    this.sortingField = sortingType;
    this.sortingOrder = sortingOrder;
    console.log('sorting order...' + sortingOrder + 'sorting type...' + sortingType);
    this.getVerifiedList();
  }
  verify(personalClaims) {
    localStorage.setItem('CLAIMDETAILS', JSON.stringify(personalClaims));
    localStorage.setItem('verifiedStatus', 'verified');
    this.router.navigate(['/app/verifyCertificate']);
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'requesterName') {
      this.nameSort = true;
      this.requestSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'request_type') {
      this.nameSort = false;
      this.requestSort = true;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.requestSort = false;
      this.SortClicking();
    }
  }


  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getVerifiedList();
  }


}
