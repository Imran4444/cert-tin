import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {ApiCalls} from "../../services/services";
import {MySharedService} from "../../services/mySharedService ";
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'verifications-page',
  templateUrl: './verifications.component.html',
  styleUrls: ['./verifications.component.css']
})
export class VerificationsComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  verificationsResp: any;
  verificationClaimsList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  sortClickValue: any;
  sortFieldValue: any;
  requestSort: boolean;
  nameSort: boolean;


  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService, private layoutService: LayoutService ) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
  }

  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    /* this.shareService.inoutboxDataChange.subscribe((res) => {
     this.inoutboxType = res;
     if (this.organisationId) {
     this.getVerificationsList();
     }
     });*/
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    if (this.inoutboxType) {
      if (this.organisationId) {
        this.getVerificationsList();
      }
    }
  }

  selectRow(shareClaims, indexValue, ev) {
    this.pagIndexVal = indexValue;
    console.log(shareClaims)
    localStorage.setItem('SHAREVERIFYDETAILS', JSON.stringify(shareClaims));
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getVerificationsList() {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCall.doGetRequesterSharedData(this.organisationId, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField,this.sortingOrder).then((result) => {
      console.log(result);
      this.layoutService.updatePreloaderState('hide');
      this.showLoader = false;
      this.verificationsResp = result;
      console.log(this.verificationsResp);
      this.verificationClaimsList = this.verificationsResp.sharedDetails;
      this.totalPagesCount = this.verificationsResp.totalPages;
      this.totalpages = this.verificationsResp.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
      this.showLoader = false;
      if (error.responseCode === '506') {
        this.verificationClaimsList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getVerificationsList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getVerificationsList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getVerificationsList();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'requesterName') {
      this.placeholdervalue = 'Requester Name';
      this.searchKey = 'requesterName';
    } else if (selectedKey === 'requestType') {
      this.placeholdervalue = 'Requster Type';
      this.searchKey = 'type';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    console.log('searchValue...' + this.searchValue)
    if (this.searchKey) {
      this.getVerificationsList();
    }
  }

  verify(shareClaims) {
    localStorage.setItem('SHAREVERIFYDETAILS', JSON.stringify(shareClaims));
    localStorage.setItem('verifiedStatus', 'notVerified');
    this.router.navigate(['/app/verifyCertificate']);
  }



  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'request_name') {
      this.nameSort = true;
      this.SortClicking();
    } else if (this.sortingField === 'request_type') {
      this.nameSort = false;
      this.requestSort = true;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.requestSort = false;
      this.SortClicking();
    }
  }


  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getVerificationsList();
  }


}
