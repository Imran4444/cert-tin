import { Component } from '@angular/core';
import { MdDialog } from '@angular/material';
import { WorkEndroldComponent } from '../popups/workEnroled/workEndrold.component';
import { EnroleDeclineComponent } from '../popups/declined/enroleDecline.component';

@Component({
  selector: 'app-work-claim',
  templateUrl: '../work/workclaim.component.html',
  styleUrls: ['../work/workcliam.component.css']

})
export class WorkclaimComponent {
  public commnets: any = 'Knowledgeable and hard working. Well liked by colleagues and showing good initiative and have good ideas.'

  constructor(public dialog: MdDialog) {
  }

  getEndrose() {
    const dialogRef = this.dialog.open(WorkEndroldComponent, {
      width: '450px',
      data: 'Work'
    });
  }

  getDeclined() {
    const dialogRef = this.dialog.open(EnroleDeclineComponent, {
      width: '450px',
      data: 'Work'
    });
  }

}
