import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialog, MdDialogRef} from '@angular/material';
import {Router} from '@angular/router';
import {Http} from '@angular/http';
import {ValidationProvider} from '../../providers/validationProvider';
import {DatePipe} from '@angular/common';
import {VerifyModel} from '../../Modals/verifyModel';
import {ApiCalls} from '../../services/services';
import {GlobalPopupComponent} from "../../globalPopup/globalPopup.component";

@Component({
  selector: 'app-issue-endrosed',
  templateUrl: '../IssueEndrosedProcess/issueEndrosed.component.html',
  styleUrls: ['../IssueEndrosedProcess/issueEndrosed.component.css']
})

export class IssueEndrosedComponent implements OnInit {
  extractKey: boolean;
  extractTranID: boolean;
  extractDC: boolean;
  tranDetails: boolean;
  typeProgress: string;
  step1start: boolean;
  step2start: boolean;
  keyProgress: number;
  titile: any;
  jsonText: any;
  recpID: string;
  issuerID: string;
  blockChainID: string;
  hash: string;
  from: string;
  to: string;
  blockChainResponse: any;
  public verifyDetails: any;
  public step3start: boolean;
  public step4start: boolean;
  public conectedTser: boolean;
  public step5start: boolean;
  public step6start: boolean;
  public validationDone: boolean;
  public classNames: string[];
  public fileName: any;
  public digitalcertificateDetails: any;
  successcode: any;
  certificateReqId: any;
  viewCertificateresponse: any;
  viewCertificateDetails: any;
  public issueObj: any;
  public certData: any;
  public inoutboxType: any;
  public certdetails: any;
  public txHashData: any;
  public hashIfo: any;

  constructor(public router: Router,
              public http: Http,
              public dialog: MdDialog,
              public  verify: ValidationProvider,
              public datePipe: DatePipe, public apiCall: ApiCalls) {
    this.issueObj = JSON.parse(localStorage.getItem('issueObj'))
    this.certData = JSON.parse(localStorage.getItem('certDetails'));
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    console.log(this.issueObj);
    console.log(this.certData);
    this.hash = '';
    this.classNames = ['custom-circle-box'];
    this.conectedTser = false;
    this.typeProgress = 'indeterminate';
    this.titile = 'Certificate Verification ';
    this.step1start = true;
    this.step3start = false;
    this.step4start = false;
    this.verifyDetails = new VerifyModel('', '', '', '', '', '');

  }

  ngOnInit() {
    this.getstatusUpdate();
  }

  getstatusUpdate() {
    this.step1start = true;
    this.apiCall.doEndorseDecline(this.issueObj, this.inoutboxType).then((result) => {
      this.certdetails = result;
      this.hashIfo = this.certdetails.transactions;
      this.hash = this.hashIfo[0].txHash;
      console.log(JSON.stringify(result));

      if (result) {
        this.step1();
      }
      console.log('Endorse Decline Result' + JSON.stringify(result));
    }, (error) => {
      this.step1start = false;
      console.log('' + JSON.stringify(error));
      const resultData = this.verify.errorPopup(error.developerMessage, error.responseMessage, '');
      const dialogRef = this.dialog.open(GlobalPopupComponent, {
        width: '450px',
        data: resultData,
        disableClose: true
      });
    });
  }

  step1() {
    this.keyProgress = 100;
    this.verifyDetails = new VerifyModel(this.certData.individualPublicKey, this.certData.issuerPublicKey, '', '', '', '');

    this.step1start = false;
    this.extractKey = true;
    this.extractTranID = false;
    let that = this;
    this.step2start = true;
    setTimeout(function () {
      that.step4();
    }, 3000);
  }

  /* step2() {
   this.keyProgress = 100;
   this.verifyDetails = new VerifyModel('0xcf9de8d91fbe209872cf314382856501d6fb8f87', '0xcf9de8d91fbe209872cf314382856501d6fb8f87', '556469146477', '', '', '');
   let that = this
   this.step2start = false
   this.step3start = true
   this.extractTranID = true
   setTimeout(function () {
   that.step3()
   }, 3000);
   }

   step3() {
   this.step3start = false
   this.verifyDetails = new VerifyModel('0xcf9de8d91fbe209872cf314382856501d6fb8f87', '0xcf9de8d91fbe209872cf314382856501d6fb8f87', '556469146477', this.hash, '', '');
   this.step4start = true;
   this.extractDC = true;
   let that = this
   setTimeout(function () {
   that.step4()
   }, 3000);
   }*/

  step4() {
    this.step2start = false;
    this.conectedTser = false
    this.http.get('https://ropsten.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=0xf33fac78fab2005f62d8641dea6cbbe004898e09a553e190facf9b7266b1ac22&apikey=VBFBKC9USABU8XV5V8YKN816ACPAGWYJ91').map((res) => res.json()).subscribe((result) => {
      console.log('' + result);
      if (result.result) {
        this.step4start = true;
        let blockChainResponse = result;
        console.log(blockChainResponse);
        let that = this;
        setTimeout(function () {
          that.step5(blockChainResponse);
        }, 3000);
        this.extractTranID = true;
        this.verifyDetails = new VerifyModel(this.certData.individualPublicKey, this.certData.issuerPublicKey, '', this.hash, '', '');
      } else {
        let resultdata = this.verify.networkFailure();
        this.openDialog(resultdata);
      }
    });

  }

  step5(blockChainResponse: any) {

    let to = blockChainResponse.result.to;
    let from = blockChainResponse.result.from;
    console.log(to, from);
    //  let jto = this.jsonText.digitalCertificate.recipient.recipientProfile.publicKey.split(',');
    // console.log("jto1" + this.jsonText.digitalCertificate.recipient.recipientProfile.publicKey)
    if ('0xcf9de8d91fbe209872cf314382856501d6fb8f87' === '0xcf9de8d91fbe209872cf314382856501d6fb8f87') {
      this.classNames = this.verify.successClass();
      this.step4start = false;
      this.conectedTser = true;
      this.step6start = true;

      this.verifyDetails = new VerifyModel(this.certData.individualPublicKey, this.certData.issuerPublicKey, '556469146477', this.hash, this.certData.issuerPublicKey, this.certData.individualPublicKey);
      let that = this;
      setTimeout(function () {
        that.step6(blockChainResponse);
      }, 2000);
    } else {
      this.classNames = this.verify.errorClass();
      this.step5start = false;
      this.tranDetails = false;
      let resultdata = this.verify.transactionfailur();
      this.openDialog(resultdata);
    }
  }

  step6(blockChainResponse: any) {
    let resultdata;
    if (this.inoutboxType === 'CERTIFICATES') {
      resultdata = this.verify.certificateSuccess();
    } else {
      resultdata = this.verify.endrosedSuccess();
    }
    this.statusUpdate(resultdata);
  }

  statusUpdate(resultdata) {
    console.log(resultdata);
    this.openDialog(resultdata);
    this.step6start = false;
    this.validationDone = true;
  }

  openDialog(resultdata: any): void {
    let dialogRef = this.dialog.open(IssueSuccessComponent, {
      width: '450px',
      data: resultdata,
      disableClose: true
    });
  }

}

@Component({
  selector: 'issue-success',
  templateUrl: 'issue-success.html',
})
export class IssueSuccessComponent {
  public inoutboxType: any;
  public claimType: any;
  public getCertDetail: any;
  public certData: any;

  constructor(public dialogRef: MdDialogRef<IssueSuccessComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              public router: Router) {
    console.log(this.data);
    this.inoutboxType = localStorage.getItem('INOUTBOX');
    this.claimType = localStorage.getItem('claimType');
    this.certData = JSON.parse(localStorage.getItem('certDetails'));

  }

  onNoClick(): void {
    this.dialogRef.close();

    if (this.inoutboxType === 'CERTIFICATES') {
      localStorage.setItem('INOUTBOX', 'INBOX');
      this.router.navigate(['app/all-certificates']);
    } else {
      this.router.navigate(['app/all-claims']);
    }
    /*  if (this.inoutboxType === 'INBOX') {
     if (this.claimType === 'Personal') {
     this.router.navigate(['app/personal-claims']);
     } else if (this.claimType === 'Work') {
     this.router.navigate(['app/work-claims']);
     } else if (this.claimType === 'Skill') {
     this.router.navigate(['app/skill-claims']);
     } else {
     this.router.navigate(['app/course-claims']);
     }
     } else if (this.inoutboxType === 'ALL') {
     this.router.navigate(['app/all-claims']);
     } else if (this.inoutboxType === 'CERTIFICATES') {
     localStorage.setItem('INOUTBOX', 'INBOX');
     this.router.navigate(['app/all-certificates']);
     /!* if (this.certData.certificateType === 'WORKATTESTATION') {
     this.router.navigate(['app/work-attestations'])
     } else if (this.certData.certificateType === 'QUALIFICATION') {
     this.router.navigate(['app/qualification']);
     }*!/

     }*/
    //  this.router.navigate(['/app/user/certlist']);
  }

}
