import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ApiCalls } from "../../services/services";

@Component(
  {
  selector: 'app-individual',
  templateUrl: './claimdetails.component.html',
  styleUrls: ['./claimdetails.component.scss']
})
export class ClaimDetailsComponent implements OnInit {
  notifystatus: any;
  public individualClaimData: any;
  public individualResponse: any;
  public individulaPersonDetails: any;
  public individualNotifications: any;
  public viewAllList: boolean;
  public showLoader: boolean;

  constructor(public apiCalls: ApiCalls) {
  }

  ngOnInit() {
    this.notifystatus = 'request';
    this.viewAllList = false;
    this.showLoader = false;
    this.individualClaimData = JSON.parse(localStorage.getItem('INDIVIDUALCLAIMDETAILS'));
    if (this.individualClaimData) {
      this.getIndividualDetails(this.individualClaimData);
    }
  }

  viewAll() {
    this.viewAllList = true;
  }

  getIndividualDetails(individualClaimData) {
    this.showLoader = true;
    console.log('view individual claim..' + JSON.stringify(individualClaimData));
    this.apiCalls.doIndividualClaimDetails(individualClaimData).then((result) => {
      this.showLoader = false;
      this.individualResponse = result;
      this.individulaPersonDetails = this.individualResponse.person;
      this.individualNotifications = this.individualResponse.messages;
      console.log(this.individualNotifications[0]);
      console.log('deatls resp..' + JSON.stringify(this.individualResponse));
    }, (error) => {
      this.showLoader = false;
      console.log('deatls resp err..' + JSON.stringify(error));
    });
  }
}
