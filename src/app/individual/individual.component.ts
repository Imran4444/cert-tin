import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiCalls } from '../services/services';
import { LayoutService } from '../layout/layout.service';

@Component({
  selector: 'app-individual',
  templateUrl: './individual.component.html',
  styleUrls: ['./individual.component.scss']
})
export class IndividualComponent implements OnInit {
  rowSelect: any;
  claimsListResp: any;
  claimsListRespIndividual: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  currentPage: any;
  individualCalimsList: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  nameSort: boolean;
  sortClickValue: any;
  sortFieldValue: any;

  constructor(public  router: Router,
              public formsModule: FormsModule,
              public apiCalls: ApiCalls, private layoutService: LayoutService) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
  }

  ngOnInit() {
    this.placeholdervalue= 'Search'
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    if (this.organisationId) {
      this.getIndividualClaims(this.organisationId);
    }
  }

  selectRow(individualClaims, indexValue, ev) {
    this.pagIndexVal = indexValue;
    const individualEmail = individualClaims.requesterId;
    console.log('individual data row ..' + JSON.stringify(individualClaims));
    localStorage.setItem('INDIVIDUALCLAIMDETAILS', JSON.stringify(individualEmail));
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  detailsPage(individualClaims) {
    console.log('individual data details..' + JSON.stringify(individualClaims));
    const individualEmail = individualClaims.requesterId;
    localStorage.setItem('INDIVIDUALCLAIMDETAILS', JSON.stringify(individualEmail));
    this.router.navigate(['/app/individualClaimDetails']);
  }

  getIndividualClaims(organisationId) {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCalls.doIndividualClaimsData(organisationId, this.currentPage, this.pageSize, this.searchKey, this.searchValue).then((result) => {
      this.layoutService.updatePreloaderState('hide');
      this.claimsListResp = result;
      this.showLoader = false;
      this.individualCalimsList = this.claimsListResp.counts;
      this.totalPagesCount = this.claimsListResp.totalPages;
      this.totalpages = this.claimsListResp.totalElements;
      console.log('indiviadual resp..' + JSON.stringify(this.claimsListResp));
    }, (error) => {
      this.showLoader = false;
      console.log('indiviadual resp err..' + JSON.stringify(error));
    });
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'personName') {
      this.placeholdervalue = 'First Name';
      this.searchKey = 'personName';
    }
  }

  getSearchResult() {
    if (this.searchKey) {
      if (this.organisationId) {
        this.getIndividualClaims(this.organisationId);
      }
    }
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      if (this.organisationId) {
        this.getIndividualClaims(this.organisationId);
      }
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      if (this.organisationId) {
        this.getIndividualClaims(this.organisationId);
      }
    }
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    if (this.organisationId) {
      this.getIndividualClaims(this.organisationId);
    }
  }
  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'personName') {
      this.nameSort = true;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.SortClicking();
    }
  }

  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    if (this.organisationId) {
      this.getIndividualClaims(this.organisationId);
    }
  }
}
