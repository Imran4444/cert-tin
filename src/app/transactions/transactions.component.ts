import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../services/services';
import { GetListModalRequest } from '../Modals/getListModal';
import { TransactionModalRequest } from '../Modals/transactionModal';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { LayoutService } from '../layout/layout.service';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit {
  public mm: any
  pageSize: any;
  showLoader: any;
  transactionResponse: any;
  firstItem: number;
  lastItem: number;
  placeholdervalue: any;
  rowSelect: boolean;
  currentPage: any;
  paginationRequired: boolean;
  typeSort: boolean;
  actionSort: boolean;
  searchFields: any = {};
  sortingField: any;
  sortingOrder: any;
  totalCount: number;
  searchResult: any = {};
  orgPublicKey: any;
  searchValue: any;
  searchKey: any;
  totalpages: any;
  transactionList: any;
  totalPagesCount: any;
  pagIndexVal: any;
  checkedIdx: any;
  sortClickValue: any;
  sortFieldValue: any;
  tHash: any;


  constructor(public service: ApiCalls,
  public router: Router, public layoutService: LayoutService) {
    this.mm = moment;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
  }

  ngOnInit() {
    this.pageSize = '10';
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.typeSort = false;
    this.actionSort = false;
    this.placeholdervalue = 'Search';
    this.getAllTransactionList()
  }


  /* Transaction List Request */
  getAllTransactionList() {
    this.layoutService.updatePreloaderState('active');
    this.orgPublicKey = JSON.parse(localStorage.getItem('orgPublicKey'));
    console.log(this.orgPublicKey)
    this.showLoader = true;
    let transactionListRequest = new TransactionModalRequest(this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder)
    console.log('Page Request ' + JSON.stringify(transactionListRequest));
    this.service.doGetAllTransactionList(this.orgPublicKey, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder).then((result) => {
        this.layoutService.updatePreloaderState('hide');
        if (result) {
          this.showLoader = false;
          this.transactionResponse = result;
          this.transactionList = this.transactionResponse.transactions;
          this.totalPagesCount = this.transactionResponse.totalPages;
          this.totalpages = this.transactionResponse.totalElements;
          console.log('Transactions List'+ JSON.stringify(this.transactionResponse));

        }
      }
      , (error) => {
        this.showLoader = false;
        console.log('' + JSON.stringify(error));
      });
  }

  /* Index Select Request */
  selectRow(transaction, indexValue, ev) {
    this.tHash = transaction.txHash;
    console.log(this.tHash);
    this.pagIndexVal = indexValue;
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
    window.open('https://kovan.etherscan.io/tx/'+this.tHash);
  }

  /* Records per Page Request */
  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getAllTransactionList();
  }

  /* Previous Page Request */
  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getAllTransactionList();
    }
  }

  /* Next Page Request */
  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getAllTransactionList();
    }
  }

  /* Search Request */
  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'type') {
      this.placeholdervalue = 'Type';
      this.searchKey = 'type';
    } else if (selectedKey === 'action') {
      this.placeholdervalue = 'Action';
      this.searchKey = 'action';
    } else {

    }
  }


  getSearchResult(searchValue) {
    if (this.searchKey) {
      this.searchValue = searchValue;
      this.getAllTransactionList();
    }
  }

  sortingList(sortingType) {
    this.sortingField = sortingType;
    // this.sortingOrder = sortingOrder;
    // console.log('sorting order...' + sortingOrder + 'sorting type...' + sortingType);
    if (this.sortingField === 'type') {
      this.typeSort = true;
      this.actionSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'action') {
      this.typeSort = false;
      this.actionSort = true;
      this.SortClicking();
    }
    else {
      this.typeSort = false;
      this.actionSort = false;
    }

  }


  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getAllTransactionList();
  }
}
