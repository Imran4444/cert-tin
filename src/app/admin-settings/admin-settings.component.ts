import { Component, OnInit } from '@angular/core';
import { ChangePasswordComponent } from '../globalPopup/changePassword/changePassword.component';
import { MdDialog } from '@angular/material';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.scss']
})
export class AdminSettingsComponent implements OnInit {
  loginResponse: any;
  roleType: any;

  constructor(public dialog: MdDialog) {
  }

  ngOnInit() {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.roleType = this.loginResponse.roles[0];
  }
  changePassword() {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '400px',
      data: 'Work'
    });
  }

}
