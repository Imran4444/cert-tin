import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiCalls } from '../../services/services';
import * as _ from 'underscore';
import { Location } from '@angular/common';
@Component({
  selector: 'app-update-account',
  templateUrl: './updateAccount.component.html',
  styleUrls: ['./updateAccount.component.css']
})
export class UpdateAccountComponent implements OnInit {
  /*  public userProfile: any;
  public organisationResponse: any;*/
  public personDetails: any;
  public orgDetails: any;
  private org: any;
  private person: any;
  showForms: any;
  registration: any;
  emailIdInUrl: any;
  constructor(public router: Router, public route: ActivatedRoute, public apiCalls: ApiCalls, public location: Location) {
    this.showForms = false;
    router.events.subscribe((val) => {
     console.log(val);
    });
  }
  ngOnInit() {
    this.route.params.subscribe((params) => this.getUserDetails(params['emailId']));
    localStorage.setItem('redirectingUrl', 'email');
  }
  getUserDetails(userEmail: any) {
    console.log(userEmail);
    this.emailIdInUrl = userEmail;
    this.apiCalls.getPersonByemail(userEmail).then((result) => {
      console.log(result);
      this.person = result;
      if (this.person.person) {
        this.personDetails = this.person.person;
        this.registration = true;
        localStorage.setItem('registeredStatus', this.registration);
        this.route.params.subscribe((params) => this.getOrgDetails( params['orgId']));
      } else {
        /*this.showForms = true;*/
        this.registration = false;
        localStorage.setItem('registeredStatus', this.registration);
        this.route.params.subscribe((params) => this.getOrgDetails( params['orgId']));
      }
    });
  }

  getOrgDetails(orgId: any) {
    if (orgId) {
      console.log('organisation found');
      this.apiCalls.doGetOrganisationDetails(orgId).then(
        (result) => {
          this.showForms = true;
          this.org = result;
          this.orgDetails = this.org.organisation;
          console.log('ORG: ' + JSON.stringify(this.orgDetails));
          localStorage.setItem(
            'STRIPE_ORGDETAILS',
            JSON.stringify(this.orgDetails)
          );
        },
        (error) => {
          console.log('' + JSON.stringify(error));
        }
      );
    } else {
      console.log('orgNot found');
    }
  }
}
