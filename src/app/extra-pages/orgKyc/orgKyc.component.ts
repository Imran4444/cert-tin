import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiCalls } from '../../services/services';

@Component({
  selector: 'app-orgKyc-component',
  templateUrl: './orgKyc.component.html',
  styleUrls: ['./orgKyc.component.css']
})
export class OrgKycComponent implements OnInit {
  orgId: any;
  orgDetails: any;
  status: any;
  personId: any;
  personDetails: any;
  showDeclined: any;
  declineReason: any;
  declinedSuccess: any;
  kycUpgrade: any;
  submitted: any;

  constructor(public http: Http, public router: Router, public apiCalls: ApiCalls, public activeRoute: ActivatedRoute) {
    this.activeRoute.params.subscribe((params) => this.getUserDetails(params['personId']));
    this.activeRoute.params.subscribe((params) => this.getUserstatus(params['status']));
    this.activeRoute.params.subscribe((params) => this.getorgDetails(params['orgId']));
  }

  getUserDetails(personId: any) {
    this.personId = personId;
  }

  getUserstatus(status: any) {
    this.status = status
  }

  getorgDetails(orgid: any) {
    this.orgId  = orgid;
  }

  ngOnInit() {
    if (this.status) {
      this.getOrgById();
    }

  }

  getOrgById() {
    this.apiCalls.doGetOrganization(this.orgId).then((orgDetails) => {
      console.log(orgDetails);
      this.orgDetails = orgDetails;
      this.getPersonDetails(this.personId);
    });
  }

  getPersonDetails(personId) {
    this.apiCalls.doGetPerson(personId).then((personDetails) => {
      console.log(personDetails);
      this.personDetails = personDetails;
      // If approved service call must be done on load & if rejected then give comment and call the service
      if (this.status === 'approved') {
        this.declineReason = '';
        this.declinedSuccess = false;
        this.showDeclined = false;
        this.confirmStatus(this.status);
      } else {
        this.showDeclined = true;
      }
    });
  }

  confirmStatus(statusR) {
    this.submitted = true;
    // check whether the comment is given or not & show red border if its not given
    if (statusR === 'rejected') {
      if (!this.declineReason) {
      } else {
        this.upgradeService(statusR);
      }
    } else {
      this.upgradeService(statusR);
    }
  }

  upgradeService(statusR) {
    const req = {
      organisationId: this.orgId,
      personId: this.personDetails.person.id,
      userType: 'web',
      status: statusR,
      reason: this.declineReason
    };
    this.apiCalls.upgradeKycConfirm(JSON.stringify(req)).then((kycUpgrade) => {
      console.log(kycUpgrade);
      this.kycUpgrade = kycUpgrade;
      // If rejected success then show the confirmation page
      if (statusR === 'rejected' && this.kycUpgrade.responseCode === '200') {
        this.declinedSuccess = true;
        this.showDeclined = false;
      }
    }, (err) => {
      console.log(err);
    });
  }

  redirectToHome() {
    this.router.navigate(['/landingPage/home']);
  }
}
