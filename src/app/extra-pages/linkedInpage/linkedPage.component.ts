import { Component, OnInit } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ApiCalls } from '../../services/services';
import { Router } from '@angular/router';
import { MySharedService } from '../../services/mySharedService ';

@Component({
  selector: 'app-linked-page',
  templateUrl: './linkedPage.component.html',
  styleUrls: ['./linkedPage.component.css']
})
export class LinkedPageComponent implements OnInit {
  public userInfo: any;
  public userDetails: any;
  public loginResponse: any;
  public personDetails: any;
  public organisationResponse: any;

  constructor(public cookie: CookieService, public apicall: ApiCalls, public router: Router, public shareService: MySharedService, public apiCall: ApiCalls,) {
    console.log('hi this is linked in loading page');
  }

  ngOnInit() {
    this.userInfo = JSON.parse(this.cookie.get('user'));
    console.log(this.userInfo._json.id);
    this.apicall.doCheckPerson(this.userInfo._json.id).then((result) => {
      console.log(result);
      this.userDetails = result;
      if (this.userDetails.isUserExist) {
        this.login()
        console.log('userExist already exist');
      } else {
        localStorage.setItem('linkedInUser', JSON.stringify(true));
        this.router.navigate(['/extra/sign-up']);
      }
      /*alert('User found')*/
    })
  }


  login() {
    this.apiCall.doLogin(this.userInfo._json.id, this.userInfo._json.id).then(
      (result) => {
        this.loginResponse = result;
        localStorage.setItem(
          'loginResponse',
          JSON.stringify(this.loginResponse)
        );
        if (this.loginResponse) {
          this.getPersonDetails(this.loginResponse.personId);
          /*this.getOrganisationDetails(this.loginResponse.orgId);*/
        }
      }, (error) => {
        console.log('' + JSON.stringify(error));
        /*  this.resultData = this.validations.errorPopup(
            this.errorMessage.accountLoginunAuth().title,
            error.error_description,
            ''
          );*/
        /* const dialogRef = this.dialog.open(GlobalPopupComponent, {
           width: '450px',
           data: this.resultData,
           disableClose: true
         });*/
      }
    );

  }

  getPersonDetails(personId) {
    this.apiCall.doGetPerson(personId).then(
      (result) => {
        this.personDetails = result;
        console.log('person details...' + JSON.stringify(this.personDetails));
        localStorage.setItem(
          'personResponse',
          JSON.stringify(this.personDetails)
        );
        if (this.personDetails) {
          this.getOrganisationDetails(this.loginResponse.orgId);
        }
        /*        if (this.personDetails) {

                  if (this.loginResponse.emailVerification === true) {
                    if (this.loginResponse.oneTimePassword === true) {
                      // this.router.navigate(['/extra/password']);
                      const resultData = this.validations.errorPopup(
                        'Success',
                        'You used a temporary password to login. Please change your password to personal one.',
                        ['/extra/password']
                      );
                      const dialogRef = this.dialog.open(GlobalPopupComponent, {
                        width: '450px',
                        data: resultData,
                        disableClose: true
                      });
                    } else {
                      this.router.navigate(['/app/account']);
                    }
                  } else {
                    const emailVerify = this.validations.errorPopup(
                      this.errorMessage.emailFailure().title,
                      this.errorMessage.emailFailure().message,
                      ''
                    );
                    const dialogRef = this.dialog.open(GlobalPopupComponent, {
                      width: '450px',
                      data: emailVerify,
                      disableClose: true
                    });
                  }
                }*/
      },
      (error) => {
        console.log('get person error..' + JSON.stringify(error));
      }
    );
  }

  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then(
      (result) => {
        this.organisationResponse = result;
        console.log('ORG: ' + JSON.stringify(this.organisationResponse));
        localStorage.setItem(
          'STRIPE_ORGDETAILS',
          JSON.stringify(this.organisationResponse)
        );
        /*this.shareService.setOrganisationDetails(this.organisationResponse);*/
       /* this.cookie.delete('user');
        localStorage.removeItem('linkedInUser')*/
        this.router.navigate(['/app/account']);
      },
      (error) => {
        console.log('' + JSON.stringify(error));
      }
    );
  }

}
