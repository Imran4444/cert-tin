import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import * as queryString from 'query-string';
import { CREATE_AUDIT_URL } from '../../endpoint';

@Injectable()
export class LoginService {
  private requestUser: any;
  loginForm: any;
  constructor(public http: Http,
    ) {
  }

  getLoginDetails(username: any, password: any) {
/*    return new Promise((resolve, reject) => {
      let body = new FormData();
      body.append('grant_type', 'password');
      body.append('appType', 'Mobile');
      body.append('applicationId', '633649394166');
      body.append('merchantName', 'SOULSTICE');
      body.append('username', username);
      body.append('password', password);
      this.http.post('http://oig.fliptin.com:8082/auth', body).map(res => res.json()).subscribe(res => {
        resolve(res);
      })
    });*/
  }


  createAudit(auditCreate: any) {
    let options: RequestOptions;
    const headers = new Headers();
    headers.append('Content-Type', 'application/json');
      options = new RequestOptions({ headers: headers });
    return new Promise((resolve, reject) => {
      this.http.post(CREATE_AUDIT_URL, JSON.stringify(auditCreate),options ).subscribe((result) => {
        resolve(result.json());
      }, (error) => {
        reject(error.json());
      });
    });
  }
  
  getIpAddress() {
    return this.http
      .get('http://api.ipstack.com/check?access_key=8068002a290644ff4dde7a736e3b350c')
      .map(response => response.json())
      .catch(this.handleError);
  }

  private handleError(error: HttpErrorResponse):
    Observable<any> {
    //Log error in the browser console
    console.error('observable error: ', error);
    return Observable.throw(error);
  }

}
