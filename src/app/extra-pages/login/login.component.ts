import {Component, OnInit} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {ActivatedRoute, Router} from '@angular/router';
import {LoginService} from './loginService';
import {EMAIL_PATTERN} from '../../providers/constants';
import {ApiCalls} from '../../services/services';
import {LoginModal} from '../../Modals/loginModal';
import {GlobalPopupComponent} from '../../globalPopup/globalPopup.component';
import {ValidationProvider} from '../../providers/validationProvider';
import {MdDialog} from '@angular/material';
import {ErrorMessages} from '../../providers/errorMessages';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/operator/map';
import {AuditModal} from '../../Modals/auditModal';
import 'moment-timezone';
import {DatePipe} from '@angular/common';
import {auditService} from '../../providers/audit.service';
import {MySharedService} from '../../services/mySharedService ';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
@Component({
  selector: 'my-page-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class PageLoginComponent implements OnInit {
  signinMail: any;
  signInPassword: any;
  public aotherPattern: any;
  loginModal: any;
  showLoader: any;
  resendLoader: any;
  loginResponse: any;
  getUserResponse: any;
  resendOtpResponse: any;
  resultDataLogin: any;
  resendresultData: any;
  personDetails: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  organisationResponse: any;
  loginForm: FormGroup;
  submitted: any;

  constructor(public router: Router,
              public http: Http,
              public apiCall: ApiCalls,
              public validations: ValidationProvider,
              public errorMessage: ErrorMessages,
              public dialog: MdDialog,
              public datePipe: DatePipe,
              public loginservice: LoginService,
              public auditHistory: auditService,
              public shareService: MySharedService,
              public formBuilder: FormBuilder, public route: ActivatedRoute) {
    this.auditModal = new AuditModal('','', '', '', '', '', '', '', '');
    this.loginModal = new LoginModal('', '');
    this.aotherPattern = EMAIL_PATTERN;
    this.route.params.subscribe((params) => this.getEmailRoute(params['emailID']));
  }

  ngOnInit() {
    this.showLoader = false;
    this.resendLoader = false;
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.required],
      pwd: ['', Validators.required]
    });
  }

  getEmailRoute(email: any) {
    // alert(email);
    console.log(email);
    this.loginModal.email = email;
  }

  get f() {
    return this.loginForm.controls;
  }

  createAuditLog() {
    this.auditHistory.userAudit('Login');
  }

  login() {
    this.submitted = true;
    if (this.loginForm.invalid || this.resendLoader || this.showLoader) {

    } else {
      this.showLoader = true;
      this.apiCall.doLogin(this.loginModal.email, this.loginModal.password).then((result) => {
        console.log('loginnnnn' + JSON.stringify(result));
        this.loginResponse = result;
        this.showLoader = false;
        if (this.loginResponse) {
          this.getPersonDetails(this.loginResponse.personId);
          this.getOrganisationDetails(this.loginResponse.orgId);
        }
        localStorage.setItem('loginResponse', JSON.stringify(this.loginResponse));
        // if (this.loginResponse.emailVerification === true) {
        const resultData = this.validations.errorPopup('Success', 'You used a temporary password to login. Please change your password to personal one.', ['/extra/password']);
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
        // this.router.navigate(['/extra/password']);
        // } else {
        //   const emailVerify = this.validations.errorPopup(this.errorMessage.emailFailure().title, this.errorMessage.emailFailure().message, '');
        //   const dialogRef = this.dialog.open(GlobalPopupComponent, {
        //     width: '450px',
        //     data: emailVerify,
        //     disableClose: true
        //   });
        // }
        this.createAuditLog();
      }, (error) => {
        this.showLoader = false;
        console.log('' + JSON.stringify(error));
        /* if (error.error === 'unauthorized') {
         this.resultDataLogin = this.validations.errorPopup(this.errorMessage.otpLoginError().title, this.errorMessage.otpLoginError().message, '');
         } else {
         this.resultDataLogin = this.validations.errorPopup(this.errorMessage.otpLoginError().title, this.errorMessage.otpLoginError().message, '');
         }*/
        this.resultDataLogin = this.validations.errorPopup(
          this.errorMessage.accountLoginunAuth().title,
          error.error_description,
          ''
        );
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: this.resultDataLogin,
          disableClose: true
        });
      });
    }
  }

  resendOTP() {
    this.submitted = true;
    if (!this.loginModal.email || this.resendLoader || this.showLoader) {
    } else {
      this.auditHistory.userAudit('Resend OTP');
      this.resendLoader = true;
      this.apiCall.getLoginId(this.loginModal.email).then((result) => {
        console.log('' + JSON.stringify(result));
        this.getUserResponse = result;
        if (this.getUserResponse.responseCode === '400') {
          const resultData = this.validations.errorPopup(this.errorMessage.userNotFoundError().title, this.errorMessage.userNotFoundError().message, '');
          const dialogRef = this.dialog.open(GlobalPopupComponent, {
            width: '450px',
            data: resultData,
            disableClose: true
          });
          this.resendLoader = false;
        } else {
          const loginId = this.getUserResponse.login.id;

          this.apiCall.doResendPassword(loginId).then((result) => {
            console.log('' + JSON.stringify(result));
            this.resendOtpResponse = result;
            if (this.resendOtpResponse.responseCode === '200') {
              this.resendresultData = this.validations.errorPopup(this.errorMessage.sendOtpSuccess().title, this.errorMessage.sendOtpSuccess().message, '');
            } else {
              this.resendresultData = this.validations.errorPopup(this.errorMessage.sendOtpSuccess().title, this.resendOtpResponse.responseMessage, '');

            }
            const dialogRef = this.dialog.open(GlobalPopupComponent, {
              width: '450px',
              data: this.resendresultData,
              disableClose: true
            });
            this.resendLoader = false;
          }, (error) => {
            console.log('' + JSON.stringify(error));
            const resultData = this.validations.errorPopup(this.errorMessage.sendOtpError().title, error.responseMessage, '');
            const dialogRef = this.dialog.open(GlobalPopupComponent, {
              width: '450px',
              data: resultData,
              disableClose: true
            });
            this.resendLoader = false;
          });
          /*  this.personData = this.getUserResponse.person;
           this.personEmailsArray = this.personData.emails;
           this.personPhonesArray = this.personData.phones;
           this.policyData = this.getUserResponse.pwdPolicy;
           console.log('' + JSON.stringify(this.personPhonesArray) + '' + JSON.stringify(this.policyData));
           for (var i = 0; i < this.personEmailsArray.length; i++) {
           if (this.personEmailsArray[i]._default == true && this.personEmailsArray[i].verefied) {
           this.personEmail = this.personEmailsArray[i].email;
           }
           }

           for (var j = 0; j < this.personPhonesArray.length; j++) {
           if (this.personPhonesArray[j]._default == true) {
           this.personMobile = this.personPhonesArray[j].countryCode + this.personPhonesArray[j].number;
           }
           }
           this.policyEmail = this.policyData.otp.email;
           this.policyMobile = this.policyData.otp.mobile;

           if (this.personEmail && this.personMobile) {
           this.findUser = true;
           this.userInfo = new UserModal(this.personEmail, this.personMobile)
           console.log(this.userInfo)
           this.emailLable = this.censorEmail(this.userInfo.email);
           this.mobileLable = this.censorMobile(this.userInfo.mobile);
           } else {
           this.popup.showDialogBox('Your email or mobile number not verified');
           }*/
        }
      }, (error) => {
        const resultData = this.validations.errorPopup(this.errorMessage.userNotFoundError().title, error.responseMessage, '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
        this.resendLoader = false;
      });
    }
  }

  getPersonDetails(personId) {
    this.apiCall.doGetPerson(personId).then((result) => {
      this.personDetails = result;
      localStorage.setItem('personResponse', JSON.stringify(this.personDetails));
      console.log('person details..' + JSON.stringify(this.personDetails));
    }, (error) => {
      console.log('get person error..' + JSON.stringify(error));
    });
  }

  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then((result) => {
      console.log('ORG: stripe ' + JSON.stringify(result));
      this.organisationResponse = result;
      localStorage.setItem('STRIPE_ORGDETAILS', JSON.stringify(this.organisationResponse));
      // if (this.organisationResponse) {
      //   this.shareService.setOrganisationDetails(this.organisationResponse);
      // }
    }, (error) => {
      console.log('' + JSON.stringify(error));
    });
  }
}
