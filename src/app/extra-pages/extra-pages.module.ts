import { NgModule } from '@angular/core';
import { MaterialModule } from '@angular/material';

import { ExtraPagesRoutingModule } from './extra-pages-routing.module';
import { PageLoginComponent } from './login/login.component';
import { PageSignUpComponent } from './sign-up/sign-up.component';
import { PageForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginService } from './login/loginService';
import { CommonModule } from '@angular/common';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { ValidationProvider } from '../providers/validationProvider';
import { AccountloginComponent } from './accountLogin/accountlogin.component';
import { PasswordComponent } from './password/password.component';
import { ErrorMessages } from '../providers/errorMessages';
import { LinkedPageComponent } from './linkedInpage/linkedPage.component';
import { UpdateAccountComponent } from './upgradeAccount/updateAccount.component';
import { ProffAccDirectiveComponent } from '../globalDirective/proffAccountDirective/proff-account-directive';
import {PersonKycComponent} from "./personKyc/personKyc.component";
import {OrgKycComponent} from "./orgKyc/orgKyc.component";


@NgModule({
  imports: [
    ExtraPagesRoutingModule,
    FormsModule,
    MaterialModule,
    CommonModule,
    GooglePlaceModule,
    ReactiveFormsModule],
  declarations: [
    PageLoginComponent,
    PageSignUpComponent,
    PageForgotPasswordComponent,
    AccountloginComponent,
    PasswordComponent,
    UpdateAccountComponent,
    LinkedPageComponent,
    ProffAccDirectiveComponent,
    PersonKycComponent,
    OrgKycComponent
  ],
  exports: [ProffAccDirectiveComponent],
  providers: [LoginService, ValidationProvider, ErrorMessages]
})

export class ExtraPagesModule {
}
