import { Component, OnInit } from '@angular/core';
import { Countriescodes } from '../../providers/countriescodes';

@Component({
  selector: 'country-popup',
  templateUrl: './countrycode.component.html',
  styleUrls: ['./countryCode.component.css']
})
export class CountryCodePopupComponent implements OnInit {
  countryDetails: any;
  codeOrCountry: any;

  constructor(public countriesCodes: Countriescodes) {
    this.countryDetails = this.countriesCodes.getCountries();
  }

  ngOnInit() {
  }
}
