import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http } from '@angular/http';
import { ApiCalls } from '../../services/services';
import { EmailTempModel } from '../../Modals/emailTempModel';
import { EmailBodyPartsModel } from '../../Modals/emailBdyParts';

@Component({
  selector: 'app-personkyc-component',
  templateUrl: './personKyc.component.html',
  styleUrls: ['./personKyc.component.css']
})
export class PersonKycComponent implements OnInit {
  personDetails: any;
  personId: any;
  status: any;
  declineReason: any;
  showDeclined: any;
  kycUpgrade: any;
  declinedSuccess: any;
  submitted: any;
  statusOfApplication: any;
  constructor(public router: Router, public http: Http, public apiCalls: ApiCalls, public activeRoute: ActivatedRoute) {
    this.activeRoute.params.subscribe((params) => this.getUserDetails(params['personId']));
    this.activeRoute.params.subscribe((params) => this.getUserstatus(params['status']));
    /*this.personId = '639879132085';
    this.status = 'approved';*/ // change to 'rejected' if its in rejected state
  }

  getUserDetails(personId: any) {
    this.personId = personId;
  }

  getUserstatus(status: any) {
    this.status = status;
  }

  ngOnInit() {
    if (this.personId && this.status) {
      this.getPersonDetails(this.personId);
    }
  }

  getPersonDetails(personId) {
    this.apiCalls.doGetPerson(personId).then((personDetails) => {
      console.log(personDetails);
      this.personDetails = personDetails;
      // If approved service call must be done on load & if rejected then give comment and call the service
      if (this.status === 'approved') {
        this.declineReason = '';
        this.declinedSuccess = false;
        this.showDeclined = false;
        this.confirmStatus(this.status);
      } else {
        this.showDeclined = true;
      }
    });
  }

  confirmStatus(statusR) {
    this.submitted = true;
    // check whether the comment is given or not & show red border if its not given
    if (statusR === 'rejected') {
      if (!this.declineReason) {
      } else {
        this.upgradeService(statusR);
      }
    } else {
      this.upgradeService(statusR);
    }
  }

  upgradeService(statusR) {
    const req = {
      organisationId: '',
      personId: this.personId,
      userType: 'mobile',
      status: statusR,
      reason: this.declineReason
    };
    this.apiCalls.upgradeKycConfirmMobile(JSON.stringify(req)).then((kycUpgrade) => {
      console.log(kycUpgrade);
      this.kycUpgrade = kycUpgrade;
      // If rejected success then show the confirmation page
      if (statusR === 'rejected' && this.kycUpgrade.responseCode === '200') {
        this.declinedSuccess = true;
        this.showDeclined = false;
      }
    }, (err) => {
      console.log(err);
    });
  }

  redirectToHome() {
    this.router.navigate(['/landingPage/home']);
  }
}
