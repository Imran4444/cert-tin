import { Component, OnInit, ViewChild } from '@angular/core';
import { ApiCalls } from '../../services/services';
import { LoginModal } from '../../Modals/loginModal';
import { EMAIL_PATTERN } from '../../providers/constants';
import { GlobalPopupComponent } from '../../globalPopup/globalPopup.component';
import { MdDialog } from '@angular/material';
import { ValidationProvider } from '../../providers/validationProvider';
import { Router } from '@angular/router';
import { ErrorMessages } from '../../providers/errorMessages';
import { LoginService } from '../login/loginService';
import * as moment from 'moment';
import 'moment-timezone';
import { AuditModal } from '../../Modals/auditModal';
import { DatePipe } from '@angular/common';
import { DEV_ROOT_PATH, GET_APP_NAME, GET_SOURCE_IP, SOCIAL_ROOT } from '../../endpoint';
import { auditService } from '../../providers/audit.service';
import { MySharedService } from '../../services/mySharedService ';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
/*import {NgProgress} from 'ngx-progressbar';
import {ProgressComponent} from 'ngx-progressbar/src/components/progress.component';*/

@Component({
  selector: 'app-acccount-login',
  templateUrl: '../accountLogin/accountLogin.component.html',
  styleUrls: ['../accountLogin/accountLogin.compoenent.css']
})
export class AccountloginComponent implements OnInit {
  /*@ViewChild(ProgressComponent) progressBar: ProgressComponent;*/
  public loginModal: LoginModal;
  public auditModal: AuditModal;
  public aotherPattern: any;
  public showLoader: any;
  public loginResponse: any;
  public rememberEmail: any;
  public rememberPassword: any;
  rememberChecked: boolean;
  resultData: any;
  personDetails: any;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  countryResponse: any;
  organisationResponse: any;
  public rootPath: string;
  loginForm: FormGroup;
  submitTry: any;
  browserVersion: any;
  auditReq: any;
  linkedInLoader: any;
  xo: any;
  constructor(public router: Router,
              public apiCall: ApiCalls,
              public validations: ValidationProvider,
              public errorMessage: ErrorMessages,
              public dialog: MdDialog,
              public datePipe: DatePipe,
              public loginservice: LoginService,
              public auditHistory: auditService,
              public shareService: MySharedService,
              public formBuilder: FormBuilder
              /*public progress: NgProgress,
              public progresBar: ProgressComponent*/) {
    this.rootPath = SOCIAL_ROOT
    this.loginModal = new LoginModal('', '');
    this.auditModal = new AuditModal(
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
    );
    this.aotherPattern = EMAIL_PATTERN;
    this.rememberChecked = false;
  }

  ngOnInit() {
    const a = ['on1', 'on2', 'on3'];
    const b = ['on4', ...a];
    console.log(b);
    const add = (a1, b2, ...c3) => { this.xo = a1 + b2 + c3;
      console.log(a1);
      console.log(b2);
      console.log(c3);
    }
    console.log(this.router);
    this.linkedInLoader = false;
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', [Validators.required]]
    });
    this.rememberEmail = localStorage.getItem('rememberEmail');
    this.rememberPassword = localStorage.getItem('rememberPassword');
    if (this.rememberEmail && this.rememberPassword) {
      this.loginModal.email = this.rememberEmail;
      this.loginModal.password = this.rememberPassword;
      this.rememberChecked = true;
    }
    this.getIpAdd();
    this.getBrowserandTimeZone();
    this.findMe();
    add(1, 5, 4, 6, 7);
    console.log(this.xo);
  }
  get f() {
    return this.loginForm.controls;
  }
  getBrowserandTimeZone() {
    const tz = moment.tz.guess();
    localStorage.setItem('timeZoneID', tz);
    const ua = navigator.userAgent;
    const browser = ua.match(
      /(opera|chrome|safari|firefox|msie|trident)\/?\s*(\.?\d+(\.\d+)*)/i
    );
    this.getBrowser = [browser[1]];
    this.browserVersion = [browser[2]];
    console.log('Browser' + this.getBrowser + this.browserVersion)
    localStorage.setItem('browserDetail', this.getBrowser);
    localStorage.setItem('browserVersion', this.browserVersion);
    return browser;
  }

  getIpAdd() {
    this.loginservice.getIpAddress().subscribe((data) => {
      this.localeIP = data.ip;
      localStorage.setItem('localeIP', this.localeIP);
    });
  }
  linkedIn(){
    this.linkedInLoader = true;
  }

  createAuditLog() {
    this.auditHistory.userAudit('Login');
    // this.loginResult = JSON.parse(localStorage.getItem('loginResponse'));
    // this.localIP = localStorage.getItem('localeIP');
    // this.browser = localStorage.getItem('browserDetail');
    // this.timeZone = localStorage.getItem('timeZoneID');
    // this.location = localStorage.getItem('locationName');
    // this.verb = 'Login';
    // const newDate = this.datePipe.transform(new Date(), 'yyyy-MM-dd')
    // const newhour = this.datePipe.transform(new Date(), 'HH:mm')
    // this.requestTime = newDate + ' ' + newhour + ':00.521Z'
    // const auditLog = new AuditModal(GET_APP_NAME, this.browser, this.localIP, this.location, this.loginResult.orgName, this.requestTime, GET_SOURCE_IP, this.timeZone, this.loginResult.personId, this.loginResult.username, this.verb);
    // console.log('Login Audit Log' + JSON.stringify(auditLog));
    // this.loginservice.createAudit(auditLog).then((result) => {
    //     console.log('Login Audit Response' + JSON.stringify(result));
    //   },
    //   (error) => {
    //     console.log('Login Audit Response ERR' + JSON.stringify(error));
    //   });
  }

  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.apiCall.getPersonsLocation(position).then((res) => {
          this.countryResponse = res;
          console.log(res)
          this.location = this.countryResponse.long_name;
          localStorage.setItem('locationName', this.location);
        });
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }

  // getIpAdd() {
  //   console.log("ip");
  //   this.loginservice.getIpAddress().subscribe(data => {
  //     console.log('Data'+ data);
  //   });
  // }

  login() {
    this.submitTry = true;
    if (this.loginForm.valid) {
      if (this.rememberEmail && this.rememberPassword) {
        localStorage.setItem('rememberEmail', this.loginModal.email);
        localStorage.setItem('rememberPassword', this.loginModal.password);
      }
      this.showLoader = true;
      this.auditReq = this.auditHistory.userAudit('Login');
      this.apiCall.doLogin(this.loginModal.email, this.loginModal.password).then(
        (result) => {
          console.log(result);
          this.loginResponse = result;
          this.showLoader = false;
          localStorage.setItem(
            'loginResponse',
            JSON.stringify(this.loginResponse)
          );
          if (this.loginResponse) {
            this.getPersonDetails(this.loginResponse.personId);

            // this.auditHistory.userAudit('Login');
          }
          /*
           if (this.loginResponse.emailVerification === true) {
           if (this.loginResponse.oneTimePassword === true) {
           //this.router.navigate(['/extra/password']);
           const resultData = this.validations.errorPopup('Success', 'You used a temporary password to login. Please change your password to personal one.', ['/extra/password']);
           const dialogRef = this.dialog.open(GlobalPopupComponent, {
           width: '450px',
           data: resultData,
           disableClose: true
           });
           } else {
           this.router.navigate(['/app/account']);
           }
           } else {
           const emailVerify = this.validations.errorPopup(this.errorMessage.emailFailure().title, this.errorMessage.emailFailure().message, '');
           const dialogRef = this.dialog.open(GlobalPopupComponent, {
           width: '450px',
           data: emailVerify,
           disableClose: true
           });
           }
           */
        }, (error) => {
          console.log('' + JSON.stringify(error));
          if (error.error_description === 'Bad credentials') {
            this.resultData = this.validations.errorPopup(
              this.errorMessage.accountLoginunAuth().title,
              this.errorMessage.accountLoginBadCredentials().message,
              ''
            );
          } else {
            this.resultData = this.validations.errorPopup(
              this.errorMessage.accountLoginunAuth().title,
              error.error_description,
              ''
            );
          }

          /* this.resultData = this.validations.errorPopup(
            this.errorMessage.accountLoginunAuth().title,
            error.error_description,
            ''
          ); */

          const dialogRef = this.dialog.open(GlobalPopupComponent, {
            width: '450px',
            data: this.resultData,
            disableClose: true
          });
          this.showLoader = false;
        }
      );
      // if (this.loginResponse) {
      //   this.createAuditLog();
      // }
    }
  }
  changeRemember(event) {
    if (event.target.checked === true) {
      localStorage.setItem('rememberEmail', this.loginModal.email);
      localStorage.setItem('rememberPassword', this.loginModal.password);
    } else {
      localStorage.setItem('rememberEmail', '');
      localStorage.setItem('rememberPassword', '');
    }
  }

  getPersonDetails(personId) {
    this.apiCall.doGetPerson(personId).then(
      (result) => {
        this.personDetails = result;
        console.log('person details...' + JSON.stringify(this.personDetails));
        localStorage.setItem(
          'personResponse',
          JSON.stringify(this.personDetails)
        );
        if (this.personDetails) {
          //  if (this.loginResponse.emailVerification === true) {
          if (this.loginResponse.oneTimePassword === true) {
            // this.router.navigate(['/extra/password']);
            const resultData = this.validations.errorPopup(
              'Success',
              'You used a temporary password to login. Please change your password to a personal one.',
              ['/extra/password']
            );
            const dialogRef = this.dialog.open(GlobalPopupComponent, {
              width: '450px',
              data: resultData,
              disableClose: true
            });
          } else {
            this.getOrganisationDetails(this.loginResponse.orgId);

          }
          // } else {
          //   const emailVerify = this.validations.errorPopup(
          //     this.errorMessage.emailFailure().title,
          //     this.errorMessage.emailFailure().message,
          //     ''
          //   );
          //   const dialogRef = this.dialog.open(GlobalPopupComponent, {
          //     width: '450px',
          //     data: emailVerify,
          //     disableClose: true
          //   });
          // }
        }
      },
      (error) => {
        console.log('get person error..' + JSON.stringify(error));
      }
    );
  }

  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then(
      (result) => {
        this.organisationResponse = result;
        console.log('ORG: stripe acc Login page' + JSON.stringify(this.organisationResponse));
        localStorage.setItem(
          'STRIPE_ORGDETAILS',
          JSON.stringify(this.organisationResponse)
        );
        // this.shareService.setOrganisationDetails(this.organisationResponse);
        this.router.navigate(['/app/dashboard']);

      },
      (error) => {
        console.log('' + JSON.stringify(error));
      }
    );
  }
}
