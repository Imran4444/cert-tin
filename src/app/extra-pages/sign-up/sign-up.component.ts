import {Component, OnInit, ViewChild} from '@angular/core';
import {SignUpModal} from '../../Modals/signupModal';
import {Router} from '@angular/router';
import {GooglePlaceDirective} from 'ngx-google-places-autocomplete';
import {Address} from 'ngx-google-places-autocomplete/objects/address';
import {ComponentRestrictions} from 'ngx-google-places-autocomplete/objects/options/componentRestrictions';
import {Countriescodes} from '../../providers/countriescodes';
import {AWS_PHOTO_UPLOAD_URL} from '../../endpoint';
import {Http} from '@angular/http';
import {EMAIL_PATTERN, WEBSITE_PATTERN} from '../../providers/constants';
import {MdDialog} from '@angular/material';
import {WorkEndroldComponent} from '../../Inbox/popups/workEnroled/workEndrold.component';
import {ValidationProvider} from '../../providers/validationProvider';
import {GlobalPopupComponent} from '../../globalPopup/globalPopup.component';
import {ApiCalls} from '../../services/services';
import {SignUpRequest} from '../../Modals/signupRequestModal';
import {AddressRequest} from '../../Modals/addressModal';
import {CustomFields} from '../../Modals/customFieldsModal';
import {Emails} from '../../Modals/emailModal';
import {Phones} from '../../Modals/phonesModal';
import {IndustryTypes} from '../../providers/industrytypes';
import {
  APP_INSTANCE_ID,
  ORGANISATION_ID,
  PASSWORD_POLICY_ID
} from '../../AppConstants';
import * as moment from 'moment';
import {DatePipe} from '@angular/common';
import {InstantLoginRequest} from '../../Modals/InstantLoginModal';
import {LoginAppInstance} from '../../Modals/loginAppInstanceMoal';
import {ErrorMessages} from '../../providers/errorMessages';
import {CookieService} from 'ngx-cookie-service';
import {LoginService} from '../login/loginService';
import {AuditModal} from '../../Modals/auditModal';
import 'moment-timezone';
import {auditService} from '../../providers/audit.service';

import {OrgKeyListModalRequest} from '../../Modals/orgKeyListModel';
import {MySharedService} from '../../services/mySharedService ';
import {CountryCodePopupComponent} from "../countryCodePopup/countryCode.component";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

declare var google: any;

@Component({
  selector: 'my-page-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class PageSignUpComponent implements OnInit {
  file: any;
  signupModel: any;
  public aotherPattern: any;
  public sitePattern: any;
  @ViewChild('placesRef')
  placesRef: GooglePlaceDirective;
  public options: { componentRestrictions: { country: string } };
  countryDetails: any;
  showLoader: any;
  signupLoader: any;
  profileUrl: any;
  public googlePlacesOptions: any;
  public industryNames: any;
  public selectedCountry: any;
  passwordPolicy: any;
  loginExpDate: any;
  currentHour: any;
  emailsArray: any[];
  phonesArray: any[];
  addressArray: any[];
  organisations: any[];
  appInstanceArray: any[];
  userRolesArray: any[];
  personResponse: any;
  public countryResponse: any;
  public selectedObject: string;
  public resultData: any;
  private linkedInUser: any;
  public auditModal: AuditModal;
  public orgSerachFields: any;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  validityStatus: any;
  siteCheck: any;
  public ln_user: any;
  public userName: any;
  public responseInfo: any;
  public loginResponse: any;
  public personDetails: any;
  public organisationResponse: any;
  paginationRequired: boolean;
  showOrgList: boolean;
  orgResponse: any;
  orgresult: any;
  submitted: any;
  RegisterForm: FormGroup;
  constructor(public router: Router,
              public countriesCodes: Countriescodes,
              public http: Http,
              public dialog: MdDialog,
              public loginservice: LoginService,
              public validations: ValidationProvider,
              public apiCall: ApiCalls,
              public industryTypes: IndustryTypes,
              public datePipe: DatePipe,
              public errorMessage: ErrorMessages,
              public cookieService: CookieService,
              public auditHistory: auditService, public shareService: MySharedService,
              public formBuilder: FormBuilder) {
    this.signupModel = new SignUpModal('', '', '', '', '', '', '', '', '', '', '');
    this.signupModel.industry = 'defaultInd';
    this.signupModel.countryCode = 'defaultCode';
    this.signupModel.country = 'defaultCountry';
    this.selectedObject = 'selectYourCountry';
    this.orgSerachFields = {};
    // this.sitePattern = WEBSITE_PATTERN;
    this.aotherPattern = EMAIL_PATTERN;
    this.emailsArray = [];
    this.addressArray = [];
    this.phonesArray = [];
    this.organisations = [];
    this.userRolesArray = [];
    this.appInstanceArray = [];
    this.currentHour = this.datePipe.transform(new Date(), 'HH:mm');
    // this.profileUrl = 'http://d2btkkuyusotz5.cloudfront.net/certTin/profile_pic.jpg';
    this.profileUrl = 'https://flipaccess-resized.s3.amazonaws.com/resized-profile_pic.jpg';
  }

  ngOnInit() {
    this.sitePattern = /^((https?|ftp|smtp):\/\/)?(www.)?[a-z0-9]+(\.[a-z]{2,}){1,3}(#?\/?[a-zA-Z0-9#]+)*\/?(\?[a-zA-Z0-9-_]+=[a-zA-Z0-9-%]+&?)?$/;

    /* this.sitePattern = "^(?:http(s)?:\/\/)?[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+";*/
    this.RegisterForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      email: ['', [Validators.email, Validators.required]],
      countryName: ['', Validators.required],
       countryCode: ['', Validators.required],
       mobileNumber: ['', Validators.required],
       organisation: ['', Validators.required],
       organisationWeb: ['', Validators.required],
       industry: ['', Validators.required],
       location: ['', Validators.required]
    });
    this.ln_user = JSON.parse(localStorage.getItem('linkedInUser'));
    if (this.cookieService.get('user') && this.ln_user) {
      this.linkedInUser = JSON.parse(this.cookieService.get('user'));
      console.log(this.linkedInUser._json);
      this.signupModel.firstName = this.linkedInUser._json.firstName;
      this.signupModel.lastName = this.linkedInUser._json.lastName;
      this.signupModel.email = this.linkedInUser._json.emailAddress;
      this.signupModel.linkedInProfileId = this.linkedInUser._json.id;
      if (this.linkedInUser._json.pictureUrl) {
        this.profileUrl = this.linkedInUser._json.pictureUrl;
      } else {
        this.profileUrl = 'https://flipaccess-resized.s3.amazonaws.com/resized-profile_pic.jpg';
      }
    }
    this.findMe();
    /*   this.googlePlacesOptions = {
     types: [],
     componentRestrictions: {country: 'IN'}
     };*/
    /*     this.placesRef.options.componentRestrictions = new ComponentRestrictions({
     country: 'IN'
     });
     this.placesRef.reset();*/
    this.showLoader = false;
    this.showOrgList = false;
    this.signupLoader = false;
    this.countryDetails = this.countriesCodes.getCountries();
    this.industryNames = this.industryTypes.getIndustries();
    // this.getPasswordPolicy();
  }
  get f() {
    return this.RegisterForm.controls;
  }

  uploadPicture(event) {
    this.showLoader = true;
    console.log(event);
    const formData: FormData = new FormData();
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];
    this.http
      .post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData)
      .map((res) => res.json())
      .subscribe(
        (data) => {
          this.profileUrl = data.resizedImageUrl;
          this.showLoader = false;
          // alert('Image Uploaded Successfully');
          console.log('img upload...' + JSON.stringify(this.profileUrl));
        },
        (err) => {
          this.showLoader = false;
          alert('Oops !! Image Uploading Failed, Please Try Again');
          console.log(err);
        }
      );
  }

  createAuditLog() {
    this.auditHistory.userAudit('Register');
  }

  register() {
    this.showOrgList = false; 
    this.submitted = true;
    if (this.RegisterForm.invalid || this.signupLoader || this.validityStatus == 'fail' ) { } else {
      this.signupLoader = true;
      /*Address model*/
      // this.signupModel.country = this.selectedCountry;
      const address = new AddressRequest(
        this.signupModel.location,
        '',
        '',
        '',
        this.signupModel.country,
        '',
        true,
        '',
        'Physical Address'
      );
      this.addressArray.push(address);
      /*Organisation Model*/
      this.organisations.push(ORGANISATION_ID);
      /*User Roles*/
      const userRole = 'CERTTIN_USER';
      this.userRolesArray.push(userRole);
      /*Custom Fields Model*/
      // const customFields = new CustomFields(this.signupModel.industry, this.signupModel.organisation, this.signupModel.organisationWebsite, '', '', '', '');
      const customFields = {};
      /*Emails Modal*/
      if (this.ln_user) {
        const emails = new Emails(true, this.signupModel.email, 'private', true);
        this.emailsArray.push(emails);
        this.userName = this.signupModel.linkedInProfileId;
      } else {
        const emails = new Emails(true, this.signupModel.email, 'private', false);
        this.emailsArray.push(emails);
        this.userName = this.signupModel.email
      }

      /*Phones Modal*/
      const phones = new Phones(
        true,
        this.signupModel.countryCode,
        this.signupModel.mobileNumber,
        'private',
        false
      );
      this.phonesArray.push(phones);
      /*Signup Request*/
      const signupRequest = new SignUpRequest(
        true,
        this.profileUrl,
        this.signupModel.firstName,
        this.signupModel.lastName,
        this.addressArray,
        this.emailsArray,
        this.phonesArray,
        APP_INSTANCE_ID,
        true,
        this.signupModel.organisation,
        this.signupModel.industry,
        this.signupModel.organisationWebsite,
        ORGANISATION_ID,
        PASSWORD_POLICY_ID,
        this.userRolesArray,
        this.userName,
        customFields,
        'web',
        this.signupModel.linkedInProfileId
      );
      console.log('' + JSON.stringify(signupRequest));
      this.apiCall.doSignUp(signupRequest).then(
        (result) => {
          this.responseInfo = result;

          if (this.responseInfo.isLinkedInUser) {
            this.login();

          } else {
            const resultData = this.validations.registarion(
              this.signupModel.email,
              this.signupModel.mobileNumber,
              ['/extra/login']
            );
            const dialogRef = this.dialog.open(GlobalPopupComponent, {
              width: '450px',
              data: resultData,
              disableClose: true
            });
            this.signupLoader = false;
          }
          console.log('' + JSON.stringify(result));
          /*   this.personResponse = result;
           const personId = this.personResponse.person.id;
           if (personId) {
           this.createLogin(personId);
           }
           */

        }, (error) => {
          console.log('' + JSON.stringify(error));
          this.signupLoader = false;
          if (error.responseMessage === 'Organisation already exist with name and code') {
            this.resultData = this.validations.errorPopup(
              this.errorMessage.signUpOrgExists().title,
              this.errorMessage.signUpOrgExists().message,
              ''
            );
          } else if (error.responseMessage === 'Login already exists with same userName' || error.responseMessage === 'InstanceLogin already exist with userName and instanceId ') {
            this.resultData = this.validations.errorPopup(
              this.errorMessage.signupEmailExists().title,
              this.errorMessage.signupEmailExists().message,
              ''
            );
          } else {
            this.resultData = this.validations.errorPopup(this.errorMessage.signupServerError().title, this.errorMessage.signupServerError().message, '');
          }

          const dialogRef = this.dialog.open(GlobalPopupComponent, {
            width: '450px',
            data: this.resultData,
            disableClose: true
          });
        }
      );
      /*this.createAuditLog();*/
    }
  }

  numberOnly(event): boolean {
    const charCode = event.which ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  cancelSignUp() {
    this.cookieService.delete('user');
    this.router.navigate(['/extra/accountLogin']);
  }

  selectCountry(event: any) {
    console.log(this.selectedObject);
    this.countryDetails.forEach((countryObj) => {
      if (countryObj.code === this.selectedObject) {
        this.placesRef.options.componentRestrictions = new ComponentRestrictions(
          {
            country: this.selectedObject
          }
        );
        this.placesRef.reset();
        this.signupModel.country = countryObj.name;
        this.signupModel.countryCode = countryObj.dial_code;
        console.log('asasas.12..' + this.signupModel.country);
      }
    });
    this.signupModel.location = '';
  }

  handleAddressChange(address: Address) {
    this.signupModel.location = address.formatted_address;
    console.log('change address' + JSON.stringify(this.signupModel.location));
  }

  findMe() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log(position);
        this.apiCall.getPersonsLocation(position).then((res) => {
          this.countryResponse = res;
          console.log(this.countryResponse);
          this.signupModel.country = this.countryResponse.long_name;
          for (let i = 0; i < this.countryDetails.length; i++) {
            if (this.countryDetails[i].name === this.signupModel.country) {
              this.signupModel.countryCode = this.countryDetails[i].dial_code;
              this.signupModel.country = this.countryDetails[i].name;
            }
          }
          this.selectedObject = this.countryResponse.short_name;
          console.log(this.selectedObject);
          console.log(this.countryResponse.short_name);
          this.placesRef.options.componentRestrictions = new ComponentRestrictions(
            {
              country: this.countryResponse.short_name
            }
          );
          this.placesRef.reset();
        });
        console.log('asasas..' + this.signupModel.country);
      });
    } else {
      alert('Geolocation is not supported by this browser.');
    }
  }

  getPasswordPolicy() {
    this.apiCall.doGetPasswordPolicy(PASSWORD_POLICY_ID).then(
      (result) => {
        console.log('' + JSON.stringify(result));
        this.passwordPolicy = result;
        const passwordPeriod = this.passwordPolicy.passwordPolicy.passwordExpiry
          .period;
        const passwordDuration = this.passwordPolicy.passwordPolicy
          .passwordExpiry.duration;
        let passwordexpiry = '';
        if (passwordPeriod === 'Month') {
          const changeMonth = moment()
            .add(passwordDuration, 'month')
            .toString();
          passwordexpiry = this.datePipe.transform(changeMonth, 'yyyy-MM-dd');
          this.loginExpDate =
            passwordexpiry + ' ' + this.currentHour + ':00.00Z';
          // this.loginExpDate = moment(this.loginExpDate).utc().format();
        } else if (passwordPeriod === 'Week') {
          const daysDuration = passwordDuration * 7;
          const changeWeek = moment()
            .add(daysDuration, 'day')
            .toString();
          passwordexpiry = this.datePipe.transform(changeWeek, 'yyyy-MM-dd');
          this.loginExpDate =
            passwordexpiry + ' ' + this.currentHour + ':00.00Z';
          // this.loginExpDate = moment(this.loginExpDate).utc().format();
        } else if (passwordPeriod === 'Day') {
          const changeDay = moment()
            .add(passwordDuration, 'day')
            .toString();
          passwordexpiry = this.datePipe.transform(changeDay, 'yyyy-MM-dd');
          this.loginExpDate =
            passwordexpiry + ' ' + this.currentHour + ':00.00Z';
          // this.loginExpDate = moment(this.loginExpDate).utc().format();
        } else if (passwordPeriod === 'Year' || passwordPeriod === 'Years') {
          const changeYear = moment()
            .add(passwordDuration, 'year')
            .toString();
          passwordexpiry = this.datePipe.transform(changeYear, 'yyyy-MM-dd');
          this.loginExpDate =
            passwordexpiry + ' ' + this.currentHour + ':00.00Z';
          // this.loginExpDate = moment(this.loginExpDate).utc().format();
        } else if (
          passwordPeriod === undefined ||
          passwordPeriod === null ||
          passwordPeriod === ''
        ) {
          passwordexpiry = 'unlimited';
          this.loginExpDate = 0;
        }
        console.log('date format..' + this.loginExpDate);
      },
      (error) => {
        console.log('' + JSON.stringify(error));
      }
    );
  }

  createLogin(personId: any) {
    const userRole = 'CERTTIN_USER';
    this.userRolesArray.push(userRole);
    const appInstances = new LoginAppInstance(
      APP_INSTANCE_ID,
      this.userRolesArray
    );
    this.appInstanceArray.push(appInstances);
    const loginRequest = new InstantLoginRequest(
      this.loginExpDate,
      this.appInstanceArray,
      ORGANISATION_ID,
      PASSWORD_POLICY_ID,
      personId,
      this.signupModel.email
    );
    console.log('' + JSON.stringify(loginRequest));
    this.apiCall.doInstantLogin(loginRequest).then(
      (result) => {
        console.log('' + JSON.stringify(result));
        const resultData = this.validations.registarion(
          this.signupModel.email,
          this.signupModel.mobileNumber,
          ['/extra/login']
        );
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: resultData,
          disableClose: true
        });
        this.signupLoader = false;
        this.router.navigate(['app/dashboard']);
      },
      (error) => {
        console.log('' + JSON.stringify(error));
      }
    );
  }

  checkValidSite() {
    if (this.signupModel.organisationWebsite) {
      this.apiCall
        .getValidStatus(this.signupModel.organisationWebsite)
        .then((result) => {
          console.log(result);
          this.siteCheck = result;
          this.validityStatus = this.siteCheck.status;
        });
    }
  }

  login() {
    this.showLoader = true;
    this.apiCall.doLogin(this.userName, this.userName).then(
      (result) => {
        this.loginResponse = result;
        this.showLoader = false;
        localStorage.setItem(
          'loginResponse',
          JSON.stringify(this.loginResponse)
        );
        if (this.loginResponse) {
          this.getPersonDetails(this.loginResponse.personId);
          /*this.getOrganisationDetails(this.loginResponse.orgId);*/
        }
      }, (error) => {
        console.log('' + JSON.stringify(error));
        /*  this.resultData = this.validations.errorPopup(
         this.errorMessage.accountLoginunAuth().title,
         error.error_description,
         ''
         );*/
        /* const dialogRef = this.dialog.open(GlobalPopupComponent, {
         width: '450px',
         data: this.resultData,
         disableClose: true
         });*/
        this.showLoader = false;
      }
    );
    if (this.loginResponse) {
      this.createAuditLog();
    }
  }

  getPersonDetails(personId) {
    this.apiCall.doGetPerson(personId).then(
      (result) => {
        this.personDetails = result;
        console.log('person details...' + JSON.stringify(this.personDetails));
        localStorage.setItem(
          'personResponse',
          JSON.stringify(this.personDetails)
        );
        if (this.personDetails) {
          this.getOrganisationDetails(this.loginResponse.orgId);
        }
        /*        if (this.personDetails) {

         if (this.loginResponse.emailVerification === true) {
         if (this.loginResponse.oneTimePassword === true) {
         // this.router.navigate(['/extra/password']);
         const resultData = this.validations.errorPopup(
         'Success',
         'You used a temporary password to login. Please change your password to personal one.',
         ['/extra/password']
         );
         const dialogRef = this.dialog.open(GlobalPopupComponent, {
         width: '450px',
         data: resultData,
         disableClose: true
         });
         } else {
         this.router.navigate(['/app/account']);
         }
         } else {
         const emailVerify = this.validations.errorPopup(
         this.errorMessage.emailFailure().title,
         this.errorMessage.emailFailure().message,
         ''
         );
         const dialogRef = this.dialog.open(GlobalPopupComponent, {
         width: '450px',
         data: emailVerify,
         disableClose: true
         });
         }
         }*/
      },
      (error) => {
        console.log('get person error..' + JSON.stringify(error));
      }
    );
  }

  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then(
      (result) => {
        this.organisationResponse = result;
        console.log('ORG: ' + JSON.stringify(this.organisationResponse));
        localStorage.setItem(
          'STRIPE_ORGDETAILS',
          JSON.stringify(this.organisationResponse)
        );
        /*this.shareService.setOrganisationDetails(this.organisationResponse);*/
        /*this.cookieService.delete('user');
         localStorage.removeItem('linkedInUser')*/
        this.router.navigate(['/app/account']);
      },
      (error) => {
        console.log('' + JSON.stringify(error));
      }
    );
  }

  gerOrgKey(searchFields) {
    if (searchFields === '') {
      this.showOrgList = false;
      this.signupModel.organisationWebsite = '';
      this.signupModel.industry = '';
    } else {
      this.showOrgList = true;
      this.paginationRequired = false;
      this.orgSerachFields.name = searchFields;
      let orgListRequest = new OrgKeyListModalRequest(this.paginationRequired, this.orgSerachFields)
      console.log('Req' + JSON.stringify(orgListRequest));
      this.apiCall.doGetOrgByKey(orgListRequest).then((result) => {
          console.log(result);
          this.orgResponse = result;
          this.orgresult = this.orgResponse.listOfOrganisation
          console.log('Resp' + JSON.stringify(this.orgResponse));
        }
        , (error) => {
          this.showLoader = false;
          console.log('' + JSON.stringify(error));
        });
    }
  }

  selectedOrg(org) {
    this.showOrgList = false;
    this.signupModel.organisation = org.name;
    this.signupModel.organisationWebsite = org.website;
    if (this.signupModel.industry != '') {
      this.signupModel.industry = org.type;
    }
  }
  hideDiv(){
    if(this.showOrgList === true){
      this.selectedOrg('')
      this.showOrgList = false;
    }else{
      this.showOrgList = false; 
    }
    
  }

  listHide(){
    this.showOrgList = false; 
  }

  openCodePopup() {
    const dlgRefer = this.dialog.open(CountryCodePopupComponent, {height: '350px'});
    dlgRefer.afterClosed().subscribe((code) => {
      console.log(code);
    });
  }

  // hideList(){
  //   this.orgresult = '';
  // }
}
