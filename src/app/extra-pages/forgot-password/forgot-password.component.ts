import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ForgotModel } from './model/forgot.model';
import { EMAIL_PATTERN } from '../../providers/constants';
import { ApiCalls } from '../../services/services';
import { GlobalPopupComponent } from '../../globalPopup/globalPopup.component';
import { ValidationProvider } from '../../providers/validationProvider';
import { MdDialog } from '@angular/material';
import { ErrorMessages } from '../../providers/errorMessages';
import { AuditModal } from '../../Modals/auditModal';
import { LoginService } from '../../../app/extra-pages/login/loginService';
import 'moment-timezone';
import { DatePipe } from '@angular/common';
import { auditService } from '../../providers/audit.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'my-page-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class PageForgotPasswordComponent implements OnInit {
  public forgot: any;
  public aotherPattern: any;
  showLoader: any;
  getUserResponse: any;
  resendOtpResponse: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  forgotForm: FormGroup;
  submitted: any;
  auditReq: any;
  constructor(
    public router: Router,
    public apiCall: ApiCalls,
    public validations: ValidationProvider,
    public errorMessage: ErrorMessages,
    public dialog: MdDialog,
    public datePipe: DatePipe,
    public loginservice: LoginService,
    public auditHistory: auditService,
    public formBuilder: FormBuilder
  ) {
    this.aotherPattern = EMAIL_PATTERN;
    this.auditModal = new AuditModal(
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      '',
      ''
    );
  }

  ngOnInit() {
    this.forgot = new ForgotModel('');
    this.forgotForm = this.formBuilder.group({
      email: ['', [Validators.email, Validators.required]]
    });
  }
  get f() {
    return this.forgotForm.controls;
  }
  login() {
    this.router.navigate(['/extra/accountLogin']);
  }

  createAuditLog() {
    this.auditHistory.userAudit('OTP Sent');
  }

  resendOTP() {
    this.submitted = true;
    if (this.forgotForm.valid) {
      this.createAuditLog();
      this.showLoader = true;
      this.auditReq =  this.auditHistory.userAudit('OTP Sent');
      this.apiCall.getLoginId(this.forgot.email).then(
        (result) => {
          console.log('' + JSON.stringify(result));
          this.getUserResponse = result;
          if (this.getUserResponse.responseCode === '400') {
            const resultData = this.validations.errorPopup(
              this.errorMessage.forgotUserNotFound().title,
              this.errorMessage.forgotUserNotFound().message,
              ''
            );
            const dialogRef = this.dialog.open(GlobalPopupComponent, {
              width: '450px',
              data: resultData,
              disableClose: true
            });
            this.showLoader = false;
          } else {
            const loginId = this.getUserResponse.login.id;

            if (loginId) {
              this.apiCall.doResendPassword(loginId).then(
                (result) => {
                  console.log('' + JSON.stringify(result));
                  this.resendOtpResponse = result;
                  const resultData = this.validations.errorPopup(
                    this.errorMessage.sendOtpSuccess().title,
                    this.resendOtpResponse.responseMessage,
                    ['/extra/login']
                  );
                  const dialogRef = this.dialog.open(GlobalPopupComponent, {
                    width: '450px',
                    data: resultData,
                    disableClose: true
                  });
                  this.showLoader = false;
                },
                (error) => {
                  console.log('' + JSON.stringify(error));
                  const resultData = this.validations.errorPopup(
                    this.errorMessage.sendOtpError().title,
                    error.responseMessage,
                    ''
                  );
                  const dialogRef = this.dialog.open(GlobalPopupComponent, {
                    width: '450px',
                    data: resultData,
                    disableClose: true
                  });
                  this.showLoader = false;
                }
              );
            }
          }
        },
        (error) => {
          const resultData = this.validations.errorPopup(
            this.errorMessage.userNotFoundError().title,
            error.responseMessage,
            ''
          );
          const dialogRef = this.dialog.open(GlobalPopupComponent, {
            width: '450px',
            data: resultData,
            disableClose: true
          });
          this.showLoader = false;
        }
      );
    }
  }
}
