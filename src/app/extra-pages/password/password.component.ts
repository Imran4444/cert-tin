import { Component, OnInit } from '@angular/core';
import { PasswordModel } from './model/passwordModel';
import { Router } from '@angular/router';
import { ApiCalls } from '../../services/services';
import { GlobalPopupComponent } from '../../globalPopup/globalPopup.component';
import { ValidationProvider } from '../../providers/validationProvider';
import { MdDialog } from '@angular/material';
import { ErrorMessages } from "../../providers/errorMessages";
import { AuditModal } from '../../Modals/auditModal';
import { LoginService } from '../../../app/extra-pages/login/loginService';
import 'moment-timezone';
import { DatePipe } from "@angular/common";
import { auditService } from '../../providers/audit.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-password',
  templateUrl: '../password/password.component.html',
  styleUrls: ['../password/password.component.css']
})
export class PasswordComponent implements OnInit {
  public passwordObj: PasswordModel;
  public confpassword: any;
  loginResponse: any;
  showLoader: any;
  passwordStrength: any;
  passwordError: any;
  changePasswordError: any;
  cpassword: any;
  changePasswordResult: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  setpassword: FormGroup;
  submitted: any;
  organisationResponse: any;
  public barLabel = 'Password strength:';
  auditReq: any;
  constructor(public router: Router,
    public apiCall: ApiCalls,
    public validations: ValidationProvider,
    public errorMessage: ErrorMessages,
    public dialog: MdDialog,
    public datePipe: DatePipe,
    public loginservice: LoginService,
    public auditHistory: auditService,
    public formBuilder : FormBuilder) {
    this.auditModal = new AuditModal('','', '', '', '', '', '', '', '');
  }

  ngOnInit() {
    this.passwordObj = new PasswordModel('');
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.setpassword = this.formBuilder.group({
      passwrd : ['', Validators.required],
      cnfrmPasswrd : ['', Validators.required]
    });
    this.getOrganisationDetails(this.loginResponse.orgId);
  }
  get f() {
    return this.setpassword.controls;
  }
  createAuditLog() {
    this.auditHistory.userAudit('Password Changed');
  }
  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then(
      (result) => {
        this.organisationResponse = result;
        console.log('ORG: stripe acc Login page' + JSON.stringify(this.organisationResponse));
        localStorage.setItem(
          'STRIPE_ORGDETAILS',
          JSON.stringify(this.organisationResponse)
        );
      },
      (error) => {
        console.log('' + JSON.stringify(error));
      }
    );
  }
  goToDashboard() {
    this.submitted = true;
    if (this.setpassword.invalid || (this.passwordObj.password !== this.confpassword) || this.passwordStrength == 'Weak') {
    } else {
      this.createAuditLog()
      this.showLoader = true;
      const loginId = this.loginResponse.loginId;
      this.auditReq = this.auditHistory.userAudit('Password Changed');
      this.apiCall.doChangePassword(loginId, this.passwordObj.password).then((result) => {
        console.log('' + JSON.stringify(result));
        this.changePasswordResult = result;
        if (this.changePasswordResult.responseCode === '404') {
          this.changePasswordError = this.changePasswordResult.developerMessage;
          // this.passwordObj.password = '';
          // this.confpassword = '';
        } else {
          this.router.navigate(['/app/dashboard']);
        }
        this.showLoader = false;
      }, (error) => {
        console.log('Error ' + JSON.stringify(error));
        this.passwordError = error;
        this.changePasswordError = this.passwordError.developerMessage;
        // this.passwordObj.password = '';
        // this.confpassword = '';
        // const resultData = this.validations.errorPopup(this.errorMessage.invalidPasswordFormat().title, error.responseMessage, '');
        // const dialogRef = this.dialog.open(GlobalPopupComponent, {
        //   width: '450px',
        //   data: resultData,
        //   disableClose: true
        // });
        this.showLoader = false;
      });
    }
  }
  passwordStatuss(ev) {
    this.passwordStrength = ev;
    console.log(this.passwordStrength)
  }
}
