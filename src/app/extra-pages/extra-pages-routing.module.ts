import {Routes, RouterModule} from '@angular/router';
import {PageLoginComponent} from './login/login.component';
import {PageSignUpComponent} from './sign-up/sign-up.component';
import {PageForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {AccountloginComponent} from './accountLogin/accountlogin.component';
import {PasswordComponent} from './password/password.component';
import {LinkedPageComponent} from './linkedInpage/linkedPage.component';
import {UpdateAccountComponent} from './upgradeAccount/updateAccount.component';
import {PersonKycComponent} from "./personKyc/personKyc.component";
import {OrgKycComponent} from "./orgKyc/orgKyc.component";

export const ExtraPagesRoutes: Routes = [
  {
    path: '',
    children: [
      {path: '', redirectTo: '/landingPage/home', pathMatch: 'full'},
      // {path: '', redirectTo: '/extra/accountLogin', pathMatch: 'full'},
      {path: 'login', component: PageLoginComponent},
      {path: 'sign-up', component: PageSignUpComponent},
      {path: 'forgot-password', component: PageForgotPasswordComponent},
      {path: 'accountLogin', component: AccountloginComponent},
      {path: 'user/:emailID/login', component: PageLoginComponent},
      {path: 'password', component: PasswordComponent},
      {path: 'linkedin', component: LinkedPageComponent},
      {path: 'updateaccount/:emailId/user/:orgId', component: UpdateAccountComponent},
      {path: 'kycMobile/person/:personId/:status', component: PersonKycComponent},
      {path: 'kycWeb/organisation/:orgId/person/:personId/:status', component: OrgKycComponent}
    ]
  }
];

export const ExtraPagesRoutingModule = RouterModule.forChild(ExtraPagesRoutes);
