import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';

import { DashboardServices } from './dashboardServices';
import * as moment from 'moment';

import { CHARTCONFIG } from '../directives/charts.config';
import { ApiCalls } from "../services/services";
import { LayoutService } from '../layout/layout.service';
import { ActivatedRoute } from "@angular/router";
import { ZohoMainModel } from '../Modals/zohoModals/zoho.main.model';
import { ZohoInnerModel } from '../Modals/zohoModals/zohoInnerModel';

@Component({
  selector: 'my-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './main.component.scss',
    './tableStyles.css',
    './dashboard.component.css'
  ]
})
export class DashboardComponent implements OnInit {
  organisationResponse: any;
  orgPublicKey: any;
  public loginResponse: any;
  public organisationId: any;
  claimandcertResponse: any;
  public claimCountResponse: any;
  public certCountResponse: any;
  public transactionResponse: any;
  public usersResponse: any;
  claimRecievedCount: any = 5;
  claimIssuedCount: any;
  claimVerifiedCount: any;
  public claimChart: any;
  chartCount: any;
  chartCountresult: any;
  endorsedChartCount: any = [];
  issuedChartCount: any = [];
  chartCountDates: any = [];
  trafficChart: any;
  pie1: any;
  pie2: any;
  pie3: any;
  pie4: any;
  noBarGraph: any;
  public zohoModel: ZohoMainModel

  constructor(public http: Http,
              public dashboardServices: DashboardServices,
              public apiCall: ApiCalls, private layoutService: LayoutService, public activeRoute: ActivatedRoute) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.claimRecievedCount = 0;
    this.claimIssuedCount = 0;
    this.claimVerifiedCount = 0;
    // this.claimChart = this.getClaimChart();
    console.log(this.activeRoute.params);
  }

  /* startLoader() {
     this.layoutService.updatePreloaderState('active');
   }
   stopLoader(){
     this.layoutService.updatePreloaderState('hide');
   }*/

  ngOnInit() {
   /* this.getLeadInZoho();*/
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    // this.orgPublicKey = this.organisationResponse.orgPublicKey;
    if (this.organisationId) {
      this.apiCall.doGetDashboardClaimandCertificateCount(this.organisationId).then((result) => {
        this.claimandcertResponse = result;
        this.claimCountResponse = this.claimandcertResponse.claimCount;
        this.certCountResponse = this.claimandcertResponse.certCount;
        /*Claim Count objects*/
        this.claimRecievedCount = this.claimCountResponse.requested;
        this.claimIssuedCount = this.claimCountResponse.issued;
        this.claimVerifiedCount = this.claimCountResponse.verified;
        this.getClaimChart(this.claimCountResponse, this.certCountResponse);
      }, (error) => {
      });
      this.apiCall.doGetDashBoardTransactionCount(this.organisationId).then((result) => {
        this.transactionResponse = result;
        this.getTransactionsChart(this.transactionResponse);
        console.log('transaction chart...' + JSON.stringify(this.transactionResponse))
      }, (error) => {
      });
      this.apiCall.doGetDashBoardUsersandRequestsCount(this.organisationId).then((result) => {
        this.usersResponse = result;
        this.getUserResponseChart(this.usersResponse);
      }, (error) => {
      });
      this.apiCall.doGetDashBoardChartBarCount(this.organisationId, '', '').then((result) => {
        this.chartCountresult = result;
        this.chartCount = this.chartCountresult.counts;
        var i;
        for (i = 0; i < this.chartCount.length; i++) {
          this.endorsedChartCount.push(this.chartCount[i].count.endorsed);
          this.issuedChartCount.push(this.chartCount[i].count.issued);
          this.chartCountDates.push(this.chartCount[i].date);
        }
        if (!this.endorsedChartCount.length && !this.issuedChartCount.length) {
          this.noBarGraph = true;
        } else {
          this.charts(this.endorsedChartCount, this.issuedChartCount, this.chartCountDates);
        }
      }, (error) => {
      });
    }
  }
  getLeadInZoho() {

    const leadXml = "<Leads><row no='1'><FL val='Lead Owner'>Raviraj</FL><FL val='First Name'>Raviraj</FL><FL val='Project Name'>Certtin</FL><FL val='NDA'>No</FL><FL val='Company'>OrgName </FL><FL val='Last Name'> Ganapuram</FL><FL val='Email'>rrganapuram@soulstice.biz</FL><FL val='City'>Hyd</FL><FL val='Description'>details</FL></row></Leads>"
    /*const inerObj = new ZohoInnerModel('raviraj', 'ramesh', 'Certin', 'No', 'Flitpin', 'rajesh', 'rrganapuram@soulstice.biz', 'Hyderabad', 'testing in v2')
    this.zohoModel = new ZohoMainModel([inerObj], ['workflow', 'approval', 'blueprint']);
    console.log(this.zohoModel)
    console.log(this.zohoModel);*/
    this.apiCall.createLead(leadXml).then((res) => {
      console.log(res);
    })
  }

  config = CHARTCONFIG;
  getMonData = () => {
    const data = [];
    for (let i = 0; i < 11; i++) {
      data.push('Mon. ' + i);
    }
    return data;
  }

  monData = this.getMonData();

  //
  charts(endorsed, issued, dates) {
    this.trafficChart = {
      legend: {
        show: true,
        x: 'right',
        y: 'top',
        textStyle: {
          color: this.config.textColor
        },
        data: ['Endorsed', 'Issued']
      },
      grid: {
        x: 40,
        y: 60,
        x2: 40,
        y2: 30,
        borderWidth: 0
      },
      tooltip: {
        show: true,
        trigger: 'axis',
        axisPointer: {
          lineStyle: {
            color: this.config.gray
          }
        }
      },
      xAxis: [
        {
          type: 'category',
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            textStyle: {
              color: this.config.textColor
            }
          },
          splitLine: {
            show: false,
            lineStyle: {
              color: this.config.splitLineColor
            }
          },
          data: dates
        }
      ],
      yAxis: [
        {
          type: 'value',
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            textStyle: {
              color: this.config.textColor
            }
          },
          splitLine: {
            show: true,
            lineStyle: {
              color: this.config.splitLineColor
            }
          }
        }
      ],
      series: [
        {
          name: 'Endorsed',
          type: 'bar',
          clickable: false,
          lineStyle: {
            normal: {
              color: this.config.gray,
            }
          },
          areaStyle: {
            normal: {
              color: this.config.gray,
            },
            emphasis: {}
          },
          data: endorsed,
          legendHoverLink: false,
          z: 1
        },
        {
          name: 'Issued',
          type: 'bar',
          stack: 'traffic',
          smooth: true,
          itemStyle: {
            normal: {
              color: this.config.primary, // '#03A9F4', // Light Blue 500
              barBorderRadius: 0
            },
            emphasis: {
              // color: this.config.primary
            }
          },
          barCategoryGap: '60%',
          data: issued,
          symbol: 'none',
          legendHoverLink: false,
          z: 2
        }
      ]
    };
  }

  getClaimChart(claims, certficates) {
    this.pie1 = {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        orient: 'horizontal',
        position: 'top',
        x: 'center',
        y: 'bottom',
        textStyle: {
          color: this.config.textColor
        }
      },
      toolbox: {
        show: false,
        feature: {
          saveAsImage: {show: true, title: 'save'}
        }
      },
      calculable: true,
      series: [
        {
          name: 'Claims',
          type: 'pie',
          radius: ['50%', '70%'],
          itemStyle: {
            normal: {
              label: {
                show: false
              },
              labelLine: {
                show: false
              }
            },
            emphasis: {
              label: {
                show: false,
                position: 'center',
                textStyle: {
                  fontSize: '10',
                  fontWeight: 'normal',
                }
              }
            }
          },
          data: [
            {value: claims.requested, name: 'Received', color: 'red'},
            {value: claims.endorsed, name: 'Endrosed'},
            {value: claims.declined, name: 'Declined'}
          ]
        }
      ]
    };
    this.pie2 = {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        orient: 'horizontal',
        position: 'top',
        x: 'center',
        y: 'bottom',
        textStyle: {
          color: this.config.textColor
        }
      },
      toolbox: {
        show: false,
        feature: {
          saveAsImage: {show: true, title: 'save'}
        }
      },
      calculable: true,
      series: [
        {
          name: 'Certification',
          type: 'pie',
          radius: ['50%', '70%'],
          itemStyle: {
            normal: {
              label: {
                show: false
              },
              labelLine: {
                show: false
              }
            },
            emphasis: {
              label: {
                show: false,
                position: 'center',
                textStyle: {
                  fontSize: '10',
                  fontWeight: 'normal',
                }
              }
            }
          },
          data: [
            {value: certficates.requested, name: 'Received'},
            {value: certficates.issued, name: 'Issued'},
            {value: certficates.shared, name: 'Shared'}
          ]
        }
      ]
    };
  }

  getUserResponseChart(userResponse) {
    this.pie3 = {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        orient: 'horizontal',
        position: 'top',
        x: 'center',
        y: 'bottom',
        textStyle: {
          color: this.config.textColor
        }
      },
      toolbox: {
        show: false,
        feature: {
          saveAsImage: {show: true, title: 'save'}
        }
      },
      calculable: true,
      series: [
        {
          name: 'User Summary',
          type: 'pie',
          radius: ['50%', '70%'],
          itemStyle: {
            normal: {
              label: {
                show: false
              },
              labelLine: {
                show: false
              }
            },
            emphasis: {
              label: {
                show: false,
                position: 'center',
                textStyle: {
                  fontSize: '10',
                  fontWeight: 'normal',
                }
              }
            }
          },
          data: [
            {value: userResponse.requesters, name: 'Individual Users'},
            {value: userResponse.users, name: 'Organisation Users'}
          ]
        }
      ]
    };
  }

  getTransactionsChart(transactionsResponse) {
    this.pie4 = {
      tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
      },
      legend: {
        orient: 'horizontal',
        position: 'top',
        x: 'center',
        y: 'bottom',
        // data: ['Issued', 'Verified'],
        textStyle: {
          color: this.config.textColor
        }
      },
      toolbox: {
        show: false,
        feature: {
          saveAsImage: {show: true, title: 'save'}
        }
      },
      calculable: true,
      series: [
        {
          name: 'Transaction',
          type: 'pie',
          radius: ['50%', '70%'],
          itemStyle: {
            normal: {
              label: {
                show: false
              },
              labelLine: {
                show: false
              }
            },
            emphasis: {
              label: {
                show: false,
                position: 'center',
                textStyle: {
                  fontSize: '10',
                  fontWeight: 'normal',
                }
              }
            }
          },
          data: [
            {value: transactionsResponse.transactionsCount.endorsed, name: 'Endorsed'},
            {value: transactionsResponse.transactionsCount.issued, name: 'Issued'}
          ]
        }
      ]
    };
  }
}
