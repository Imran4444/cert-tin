"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
exports.__esModule = true;
var core_1 = require("@angular/core");
var charts_config_1 = require("../charts/charts.config");
var DashboardComponent = (function () {
    function DashboardComponent(http) {
        var _this = this;
        this.http = http;
        this.data = {};
        this.today = Date.now();
        this.messages = [{
                from: 'Ali Connors',
                message: 'I will be in your neighborhood',
                photo: 'assets/images/face3.jpg',
                subject: 'Brunch this weekend?'
            }, {
                from: 'Trevor Hansen',
                message: 'Wish I could but we have plans',
                photo: 'assets/images/face6.jpg',
                subject: 'Brunch this weekend?'
            }, {
                from: 'Sandra Adams',
                message: 'Do you have Paris recommendations instead?',
                photo: 'assets/images/face4.jpg',
                subject: 'Brunch this weekend?'
            }];
        this.config = charts_config_1.CHARTCONFIG;
        this.getMonData = function () {
            var data = [];
            for (var i = 0; i < 13; i++) {
                data.push('Mon. ' + i);
            }
            return data;
        };
        this.monData = this.getMonData();
        //
        this.trafficChart = {
            legend: {
                show: true,
                x: 'right',
                y: 'top',
                textStyle: {
                    color: this.config.textColor
                },
                data: ['Trend', 'Search', 'Paid Ads', 'Virality']
            },
            grid: {
                x: 40,
                y: 60,
                x2: 40,
                y2: 30,
                borderWidth: 0
            },
            tooltip: {
                show: true,
                trigger: 'axis',
                axisPointer: {
                    lineStyle: {
                        color: this.config.gray
                    }
                }
            },
            xAxis: [
                {
                    type: 'category',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            color: this.config.textColor
                        }
                    },
                    splitLine: {
                        show: false,
                        lineStyle: {
                            color: this.config.splitLineColor
                        }
                    },
                    data: this.monData
                }
            ],
            yAxis: [
                {
                    type: 'value',
                    axisLine: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    axisLabel: {
                        textStyle: {
                            color: this.config.textColor
                        }
                    },
                    splitLine: {
                        show: true,
                        lineStyle: {
                            color: this.config.splitLineColor
                        }
                    }
                }
            ],
            series: [
                {
                    name: 'Trend',
                    type: 'line',
                    clickable: false,
                    lineStyle: {
                        normal: {
                            color: this.config.gray
                        }
                    },
                    areaStyle: {
                        normal: {
                            color: this.config.gray
                        },
                        emphasis: {}
                    },
                    data: [41, 85, 27, 70, 50, 57, 41, 56, 69, 52, 48, 44, 35],
                    legendHoverLink: false,
                    z: 1
                },
                {
                    name: 'Search',
                    type: 'bar',
                    stack: 'traffic',
                    clickable: false,
                    itemStyle: {
                        normal: {
                            color: this.config.success,
                            barBorderRadius: 0
                        },
                        emphasis: {}
                    },
                    barCategoryGap: '60%',
                    data: [25, 45, 15, 39, 20, 26, 23, 26, 35, 27, 26, 25, 22],
                    legendHoverLink: false,
                    z: 2
                },
                {
                    name: 'Paid Ads',
                    type: 'bar',
                    stack: 'traffic',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: this.config.primary,
                            barBorderRadius: 0
                        },
                        emphasis: {}
                    },
                    barCategoryGap: '60%',
                    data: [10, 25, 6, 19, 24, 25, 12, 15, 26, 13, 12, 8, 7],
                    symbol: 'none',
                    legendHoverLink: false,
                    z: 2
                },
                {
                    name: 'Virality',
                    type: 'bar',
                    stack: 'traffic',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: this.config.info,
                            barBorderRadius: 0
                        },
                        emphasis: {}
                    },
                    barCategoryGap: '60%',
                    data: [6, 15, 6, 12, 6, 6, 6, 15, 8, 13, 10, 11, 6],
                    symbol: 'none',
                    legendHoverLink: false,
                    z: 2
                }
            ]
        };
        //
        this.donutChart = {
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            legend: {
                show: false,
                orient: 'vertical',
                x: 'right',
                data: ['Direct', 'Email', 'Affiliate', 'Video Ads', 'Search']
            },
            toolbox: {
                show: false,
                feature: {
                    restore: { show: true, title: 'restore' },
                    saveAsImage: { show: true, title: 'save as image' }
                }
            },
            calculable: true,
            series: [
                {
                    name: 'Traffic source',
                    type: 'pie',
                    radius: ['51%', '69%'],
                    itemStyle: {
                        normal: {
                            color: this.config.info
                        },
                        emphasis: {
                            label: {
                                show: true,
                                position: 'center',
                                textStyle: {
                                    fontSize: '20',
                                    fontWeight: 'normal'
                                }
                            }
                        }
                    },
                    data: [
                        {
                            value: 40,
                            name: 'United States',
                            itemStyle: {
                                normal: {
                                    color: this.config.success,
                                    label: {
                                        textStyle: {
                                            color: this.config.success
                                        }
                                    },
                                    labelLine: {
                                        lineStyle: {
                                            color: this.config.success
                                        }
                                    }
                                }
                            }
                        },
                        {
                            value: 10,
                            name: 'United Kingdom',
                            itemStyle: {
                                normal: {
                                    color: this.config.primary,
                                    label: {
                                        textStyle: {
                                            color: this.config.primary
                                        }
                                    },
                                    labelLine: {
                                        lineStyle: {
                                            color: this.config.primary
                                        }
                                    }
                                }
                            }
                        },
                        {
                            value: 20,
                            name: 'Canada',
                            itemStyle: {
                                normal: {
                                    color: this.config.infoAlt,
                                    label: {
                                        textStyle: {
                                            color: this.config.infoAlt
                                        }
                                    },
                                    labelLine: {
                                        lineStyle: {
                                            color: this.config.infoAlt
                                        }
                                    }
                                }
                            }
                        },
                        {
                            value: 12,
                            name: 'Germany',
                            itemStyle: {
                                normal: {
                                    color: this.config.info,
                                    label: {
                                        textStyle: {
                                            color: this.config.info
                                        }
                                    },
                                    labelLine: {
                                        lineStyle: {
                                            color: this.config.info
                                        }
                                    }
                                }
                            }
                        },
                        {
                            value: 18,
                            name: 'Japan',
                            itemStyle: {
                                normal: {
                                    color: this.config.warning,
                                    label: {
                                        textStyle: {
                                            color: this.config.warning
                                        }
                                    },
                                    labelLine: {
                                        lineStyle: {
                                            color: this.config.warning
                                        }
                                    }
                                }
                            }
                        }
                    ]
                }
            ]
        };
        //
        this.radarChart = {
            tooltip: {},
            legend: {
                show: false,
                orient: 'vertical',
                x: 'right',
                y: 'bottom',
                data: ['Budget', 'Spending']
            },
            toolbox: {
                show: false
            },
            radar: [
                {
                    axisLine: {
                        show: true,
                        lineStyle: {
                            // for both indicator and axisLine, bad, better seperate them
                            color: '#b1b1b1'
                        }
                    },
                    splitLine: {
                        lineStyle: {
                            color: 'rgba(0,0,0,.1)'
                        }
                    },
                    splitArea: {
                        show: true,
                        areaStyle: {
                            color: this.config.splitAreaColor
                        }
                    },
                    indicator: [
                        { name: 'sales', max: 6000 },
                        { name: 'dministration', max: 16000 },
                        { name: 'Information Techology', max: 30000 },
                        { name: 'Customer Support', max: 38000 },
                        { name: 'Development', max: 52000 },
                        { name: 'Marketing', max: 25000 }
                    ]
                }
            ],
            calculable: true,
            series: [
                {
                    type: 'radar',
                    data: [
                        {
                            name: 'Budget',
                            value: [4300, 10000, 28000, 35000, 50000, 19000],
                            itemStyle: {
                                normal: {
                                    color: this.config.primary
                                }
                            }
                        },
                        {
                            name: 'Spending',
                            value: [5000, 14000, 28000, 31000, 42000, 21000],
                            itemStyle: {
                                normal: {
                                    color: this.config.success
                                }
                            }
                        }
                    ]
                }
            ]
        };
        this.http.get('http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b1b15e88fa797225412429c1c50c122a1').map(function (response) { return response.json(); }).subscribe(function (response) {
            console.log(response);
            _this.data = response.data;
        });
    }
    DashboardComponent = __decorate([
        core_1.Component({
            selector: 'my-dashboard',
            templateUrl: './dashboard.component.html',
            styleUrls: ['./main.component.scss', './tableStyles.css']
        })
    ], DashboardComponent);
    return DashboardComponent;
}());
exports.DashboardComponent = DashboardComponent;
