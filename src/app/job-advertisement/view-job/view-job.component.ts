import { Component, OnInit, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import * as moment from 'moment';

@Component({
  selector: 'app-view-job',
  templateUrl: './view-job.component.html',
  styleUrls: ['./view-job.component.scss']
})
export class ViewJobComponent implements OnInit {
  jobData:any;
  public mm: any;
  constructor(
    @Inject(MD_DIALOG_DATA) public data: any,
    public dialogRef: MdDialogRef<ViewJobComponent>,
  ) {
    this.mm = moment;
  }

  ngOnInit() {
    window.scrollTo(0,0);
    this.jobData = this.data;
    console.log(JSON.stringify(this.jobData));
  }

  cancel(){
    this.dialogRef.close();
  }

}
