import { Component, OnInit } from '@angular/core';
import { JobRequestModal } from '../../Modals/jobRequestModal';
import { DatePipe } from '@angular/common';
import { ApiCalls } from '../../services/services';
import { Router } from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MdDialog } from '@angular/material';
import { GlobalPopupComponent } from '../../globalPopup/globalPopup.component';

@Component({
  selector: 'app-post-job',
  templateUrl: './post-job.component.html',
  styleUrls: ['./post-job.component.scss']
})
export class PostJobComponent implements OnInit {
  showLoader: any;
  btnshowLoader: any;
  orgDetails: any;
  postJobRequest: any;
  minDate: Date;
  jobExpiryDate: any;
  orgName: any;
  orgLogo: any;
  aboutJob: any;
  cimOrgId: any;
  experience: any;
  jobTitle: any;
  jobType: any;
  jobUrl: any;
  orgLocation: any;
  skills: any;
  jobResponse: any;
  loginResponse: any;
  jobPostedBy: any;
  addJobForm: FormGroup;
  submitted: any;
  googlePlacesOptions:any;
  private formDissabled: boolean;
  jobModal: any;
  constructor(
    public datePipe: DatePipe,
    public apiCalls: ApiCalls,
    public router: Router,
    public formBuilder: FormBuilder,
    public dialog: MdDialog,
  ) {
    this.jobModal = new JobRequestModal('', '', '', '', '', '', '', '', '', '', '', '');
  }

  ngOnInit() {
    this.showLoader = false;
    this.btnshowLoader = false;
    this.orgDetails = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log('Org Response' + JSON.stringify(this.orgDetails));
    this.orgLogo = this.orgDetails.organisation.organisationLogoUrl;
    this.jobModal.orgName = this.orgDetails.organisation.organisationName;
    //this.cimOrgId = this.orgDetails.organisation.cimOrganisationId;
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.cimOrgId = this.loginResponse.orgId;
    this.jobPostedBy = this.loginResponse.personId;
    this.minDate = new Date();
    this.addJobForm = this.formBuilder.group({
      orgName: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      jobTitle: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      orgLocation: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      jobType: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      experience: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      jobExpiryDate: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      aboutJob: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      skills: this.formBuilder.control({ value: '', disabled: this.formDissabled }, [Validators.required]),
      jobUrl: this.formBuilder.control({ value: ''}),
    });
  }

  get fData() {
    return this.addJobForm.controls;
  }


  handleAddressChange(address: Address) {
    this.jobModal.orgLocation = address.formatted_address;
    console.log('Location' + JSON.stringify(this.orgLocation));
  }

  postJob() {
    this.submitted = true;
    if (this.addJobForm.valid) {
      const postDate = this.datePipe.transform(new Date(this.jobModal.jobExpiryDate), 'yyyy-MM-dd');
      const hourData = this.datePipe.transform(new Date(), 'HH:mm');
      let jobExpiryDate = postDate + ' ' + hourData + ':00.00Z';
      console.log(jobExpiryDate)
      this.btnshowLoader = true;
      const postJobRequest = new JobRequestModal(this.jobModal.aboutJob, this.cimOrgId, this.jobModal.experience, jobExpiryDate , this.jobModal.jobTitle, this.jobModal.jobType, this.jobModal.jobUrl, this.jobModal.orgLocation, this.orgLogo, this.jobModal.orgName, this.jobModal.skills, this.jobPostedBy);
      console.log('Job Request' + JSON.stringify(postJobRequest));
      this.apiCalls.doPostJob(postJobRequest).then((result) => {
        this.btnshowLoader = false;
        this.jobResponse = result;
        console.log('Job Response' + JSON.stringify(this.jobResponse));
        if (this.jobResponse) {
          const dataObj = { title: 'Job Advertisement', message: 'Job Posted Sucessfully' };
          const dialogRef = this.dialog.open(GlobalPopupComponent, {
            width: '35%',
            data: dataObj,
            disableClose: true
          });
          this.router.navigate(['app/job-advertisement']);
        }
      }, (error) => {
        this.btnshowLoader = false;
        console.log(error);
      });

    }
  }

}
