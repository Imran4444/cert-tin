import { Component, OnInit } from '@angular/core';
import { ViewJobComponent } from './view-job/view-job.component';
import { MdDialog } from '@angular/material';
import { Router } from '@angular/router';
import { ApiCalls } from '../services/services';

@Component({
  selector: 'app-job-advertisement',
  templateUrl: './job-advertisement.component.html',
  styleUrls: ['./job-advertisement.component.scss']
})
export class JobAdvertisementComponent implements OnInit {
  placeholdervalue: any;
  firstItem: any;
  searchResult: any;
  searchFields: any;
  currentPage: any;
  totalCount: any;
  lastItem: any;
  pageSize: any;
  sortingOrder: any;
  sortingField: any;
  searchValue: string;
  rowSelect: boolean;
  showLoader: boolean;
  paginationRequired: boolean;
  jobDetails: any;
  experienceSort: boolean;
  jobTypeSort: boolean;
  jobTitleSort: boolean;
  jobResponse: any;
  searchKey: any;
  cimOrgID: any;
  orgDetails: any;
  jobList: any;
  sortFieldValue: any;
  sortClickValue: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;

  constructor(
    public dialog: MdDialog,
    public router: Router,
    public apiCall: ApiCalls
  ) {
    this.searchResult = {};
    this.searchFields = {};
    this.currentPage = 1;
    this.firstItem = 1;
    this.totalCount = 0;
    this.lastItem = 10;
    this.sortingField = '';
    this.sortingOrder = '';
    this.sortFieldValue = '';
    this.searchKey = '';
    this.pageSize = 10;
    this.paginationRequired = true;

  }

  ngOnInit() {
    this.jobTitleSort = false;
    this.jobTypeSort = false;
    this.experienceSort = false;
    this.placeholdervalue = 'Search';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.orgDetails = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log('Org Details' + JSON.stringify(this.orgDetails));
    this.cimOrgID = this.orgDetails.organisation.cimOrganisationId;
    this.getList();
  }

  postJob() {
    this.router.navigate(['app/post-job']);
  }

  viewJob(job, indexValue, ev) {
    ev.stopPropagation();
    const dialogRef = this.dialog.open(ViewJobComponent, {
      width: '40%',
      height: '90%',
      data: job
    });
    dialogRef.afterClosed().subscribe((res) => {
      console.log(res);
    });
  }

  selectRow(job, indexValue, ev) {
    ev.stopPropagation();
    this.pagIndexVal = indexValue;
    console.log(job);
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.sortingField = '';
    this.sortingOrder = '';
    this.getList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getList();
    }
    console.log(this.firstItem);
    console.log(this.lastItem);
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      console.log(this.firstItem);
      console.log(this.lastItem);
      this.currentPage++;
      this.getList();
    }
    console.log(this.firstItem);
    console.log(this.lastItem);
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'jobTitle') {
      this.placeholdervalue = 'Job Title';
      this.searchKey = 'jobTitle';
    } else if (selectedKey === 'jobType') {
      this.placeholdervalue = 'Job Type';
      this.searchKey = 'jobType';
    } else {
      this.placeholdervalue = 'experience';
      this.placeholdervalue = 'Experience';
      this.searchKey = 'experience';
    }
  }

  getSearchResult() {
    console.log('Search Value' + this.searchValue);
    if (this.searchKey) {
      this.getList();
    }
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'job_title') {
      this.jobTitleSort = true;
      this.jobTypeSort = false;
      this.experienceSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'job_type') {
      this.jobTitleSort = false;
      this.jobTypeSort = true;
      this.experienceSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'experience') {
      this.jobTitleSort = false;
      this.jobTypeSort = false;
      this.experienceSort = true;
      this.SortClicking();
    }
    else {
      this.jobTitleSort = false;
      this.jobTypeSort = false;
      this.experienceSort = false;
    }
  }


  SortClicking() {
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getList();
  }


  getList() {
    this.showLoader = true;
    // const jobRequest = (this.cimOrgID,this.currentPage, this.pageSize, this.searchKey ,this.searchValue, this.sortingField, this.sortingOrder)
    // console.log(jobRequest);
    this.apiCall.doGetJobList(this.cimOrgID, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder).then((result) => {
        console.log(result);
        if (result) {
          this.showLoader = false;
          this.jobResponse = result;
          console.log('Job List Response' + JSON.stringify(this.jobResponse));
          this.jobList = this.jobResponse.listOfJobAdvertisements;
          this.totalCount = this.jobResponse.totalElements;
          this.totalPagesCount = this.jobResponse.totalPages;
        }
      }
      , (error) => {
        this.showLoader = false;
        console.log('' + JSON.stringify(error));
      });
  }

}
