import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-attendence-certificate',
  templateUrl: './attendence.component.html',
  styleUrls: ['./attendence.component.css']
})
export class AttendenceComponent implements OnInit {
  @Input() hero: any
  @Input() className: any
  public getPersonDetail: any;
  public getCertificateDetail: any;
  public footerInfo: string | any;
  public personResponse: any;
  public mm: any

  constructor() {
    this.mm = moment;
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
  }
  ngOnInit() {
    console.log(this.className)
  }
}
