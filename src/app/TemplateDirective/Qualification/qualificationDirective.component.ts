import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ApiCalls } from '../../services/services';

@Component({
  selector: 'app-qualification-directive',
  templateUrl: './qualificationDirective.component.html',
  styleUrls: ['./qualificationDirective.component.css']
})

export class QualificationDirectiveComponent implements OnInit {
  @Input() qualificationInfo: any
  @Input() borderTop: any;
  @Input() borderBtm: any;
  @Input() htmlContent: any;
  @Input() revoked: any
  public personResponse: any;
  public mm: any
  private organisationResponse: any;
  private orgId: any;
  private organizationDetails: any;
  public backgroundData: any;

  constructor(public apiCalls: ApiCalls) {
    this.mm = moment;
    console.log('hi this is raviraj');
  //  this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
    console.log(this.qualificationInfo);
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    this.orgId  = this.organisationResponse.organisation.cimOrganisationId;
    console.log(this.organisationResponse);
  }

  ngOnInit() {
    this.mm = moment;
    console.log(this.qualificationInfo);
/*    this.getOrganization( this.orgId );*/
    // this.getPersonDetails();
  }
/*  getPersonDetails() {
    this.apiCalls.doGetPerson(this.qualificationInfo.certificate.issuedPersonId).then((res) => {
      console.log(JSON.stringify(res));
      this.personResponse =  res;
    }, (err) => {
      console.log(err);
    });
  }*/
  getOrganization(orgId) {
    this.apiCalls.doGetOrganization(orgId).then((result) => {
      this.organizationDetails = result;
      console.log(this.organizationDetails);
      this.backgroundData = result;
    }, (error) => {
      console.log('org details error..' + JSON.stringify(error));
    });
  }
}
