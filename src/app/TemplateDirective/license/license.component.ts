import { Component, Input } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-license-directive',
  templateUrl: './license.component.html',
  styleUrls: ['./license.component.css']
})

export class LicenseComponent {
  @Input() hero: any
  @Input() className: any
  public getPersonDetail: any;
  public getCertificateDetail: any;
  public footerInfo: string | any;
  public personResponse: any;
  public mm: any

  constructor() {
    this.mm = moment;
    console.log('hi this is raviraj');
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
  }
  ngOnInit() {
    console.log(this.className)
  }
}
