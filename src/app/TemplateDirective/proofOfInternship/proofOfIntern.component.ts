import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-proofofIntern-dirctive',
  templateUrl: './proofOfIntern.component.html',
  styleUrls: ['./proofOfIntern.component.css']

})

export class ProofOfInternComponent implements OnInit{
  @Input() hero: any
  @Input() className: any
  public getPersonDetail: any;
  public getCertificateDetail: any;
  public footerInfo: string | any;
  public personResponse: any;
  public mm: any

  constructor() {
    this.mm = moment;
    console.log('hi this is raviraj');
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
  }

  ngOnInit() {
    console.log(this.className)
  }
}
