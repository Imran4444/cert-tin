import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { ApiCalls } from "../../services/services";

@Component({
  selector: 'app-work-template',
  templateUrl: './workAttestation.component.html',
  styleUrls: ['./workAttestation.compoennt.css']
})

export class WorkAttestationComponent implements OnInit {
  @Input() hero: any
  @Input() className: any
  @Input() htmlContent: any;
  @Input() revoked: boolean
  public getPersonDetail: any;
  public getCertificateDetail: any;
  public footerInfo: string | any;
  public personResponse: any;
  public mm: any;
  organisationResponse: any;
  orgId: any;
  organizationDetails: any;
  backgroundData: any;

  constructor(public apiCalls: ApiCalls) {
    this.mm = moment;
    /*this.personResponse = JSON.parse(localStorage.getItem('personResponse'));*/
    /*this.htmlContent1 = '<p>To Whom It May Concern:</p> <p>This is to certify that Sruthi Vegesna is part-time employee at vegesna.</p> <p>Position held: dfsdfsd</p> <p>Date started:10 Nov 2018</p> <p>Employment type:  part-time </p><p>Annual salary: fsdfsdf</p> <p>Employee number:34ret5</p>'*/
    /*this.htmlContent = '<p>To Whom It May Concern:</p><p>This letter is to verify that Sruthi Vegesna is employed at vegesna in the position of dfsdf.</p><p>Sruthi Vegesna started working in this capacity as a full-time employee on 01 Nov 2018 and is currently working in this position as per 10 Nov 2018. </p><p>Sruthi Vegesna has an annual salary of 23423 and his employee number is fsdfsdf34234.</p><p>If you require any additional information, please contact me at </p><p>+91 9951268699 or via email jayavegesna@gmail.com.</p>'*/
  }

  ngOnInit() {
    console.log(this.hero);
    console.log(this.className);
    console.log(this.htmlContent);
/*    console.log(this.organisationResponse);*/
    if (this.hero && this.hero.certificate  ) {
      if (this.hero.certificate.issuedPersonId) {
        this.getPersonDetails();
        this.getOrganization(this.hero.organisation.organisation.id);
      } else {
        this.getOrganization(this.orgId);
        this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
      }
    } else {
      this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
      this.orgId = this.organisationResponse.organisation.cimOrganisationId;
      console.log(this.hero)
      this.getOrganization(this.orgId);
      this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
    }

  }

  getPersonDetails() {
    this.apiCalls.doGetPerson(this.hero.certificate.issuedPersonId).then((res) => {
      this.personResponse = res;
      console.log(JSON.stringify(this.personResponse));
    }, (err) => {
      console.log(err);
    });
  }

  getOrganization(orgId) {
    this.apiCalls.doGetOrganization(orgId).then((result) => {
      this.organizationDetails = result;
      console.log(this.organizationDetails);
      this.backgroundData = result;
    }, (error) => {
      console.log('org details error..' + JSON.stringify(error));
    });
  }
}
