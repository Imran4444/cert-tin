import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-upgrade-status-popup',
  templateUrl: './upgrade-status-popup.component.html',
  styleUrls: ['./upgrade-status-popup.component.scss']
})
export class UpgradeStatusPopupComponent implements OnInit {

  constructor(public dialog: MdDialog,
     public router: Router,  
     public dialogRef: MdDialogRef<UpgradeStatusPopupComponent>) { }

  ngOnInit() {
  }
  
  cancel(){
    this.dialogRef.close();
    this.router.navigate(['/app/dashboard']);
  }

}
