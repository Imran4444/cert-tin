import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradeStatusPopupComponent } from './upgrade-status-popup.component';

describe('UpgradeStatusPopupComponent', () => {
  let component: UpgradeStatusPopupComponent;
  let fixture: ComponentFixture<UpgradeStatusPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradeStatusPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradeStatusPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
