import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycSuccessPopupComponent } from './kyc-success-popup.component';

describe('KycSuccessPopupComponent', () => {
  let component: KycSuccessPopupComponent;
  let fixture: ComponentFixture<KycSuccessPopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycSuccessPopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycSuccessPopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
