import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';import { UpgradeStatusPopupComponent } from '../upgrade-status-popup/upgrade-status-popup.component';

@Component({
  selector: 'app-kyc-success-popup',
  templateUrl: './kyc-success-popup.component.html',
  styleUrls: ['./kyc-success-popup.component.scss']
})
export class KycSuccessPopupComponent implements OnInit {
  redirectingUrl: any;
  constructor(public dialog: MdDialog,
              public router: Router,
              public dialogRef: MdDialogRef<KycSuccessPopupComponent>) {
    this.redirectingUrl = localStorage.getItem('redirectingUrl');
    console.log(this.redirectingUrl);
  }

 ngOnInit() {
 }
 upgradeSuccess() {
   if (this.redirectingUrl == 'email') {
     this.router.navigate(['/extra/accountLogin']);
   } else {
     this.router.navigate(['/app/dashboard']);
   }
   this.dialogRef.close();
 }

}
