import { Component, Inject, OnInit, } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef, MdDialog } from '@angular/material';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ApiCalls } from '../../services/services';
import { Router } from '@angular/router';
import { UpgradeProfessionalAccount } from '../../Modals/upgradeProAccModal';
import { UpgradeStatusPopupComponent } from '../upgrade-status-popup/upgrade-status-popup.component';
import { DEV_ROOT_PATH, UPGRADE_PRO_ACC_PATH } from '../../endpoint';

@Component({
  selector: 'app-upgrade-account',
  templateUrl: './upgradeAccount.component.html',
  styleUrls: ['./upgradeAccount.component.css']
})
export class UpgradeAccountComponent implements OnInit {
  public upgradeType: string;
  public sendEmail: FormGroup;
  public submitted: boolean;
  public selectedValue: any
  public userStatus: any;
  public orgDetails: any;
  public orgId: any;
  public adminResponse: any;
  public upgradeAcc: any;
  public emailValue: any;
  public email: any;
  public logDetails: any;
  public personName: any;
  public emailUpgradeResponse: any;
  public progressResponse: any;
  public upgradeAccountStatus: any;
  public showInprogress: boolean;
  emailProgress: any;
  public emailStatus: boolean;
  constructor(public formBuilder: FormBuilder, public apiCall: ApiCalls, public dialog: MdDialog, public router: Router, public dialogRef: MdDialogRef<UpgradeAccountComponent>, @Inject(MD_DIALOG_DATA) public data: any) {
    console.log('hi this is upgrade account model');

  }
  ngOnInit() {
    this.emailStatus = false;
    this.emailProgress = false;
    this.orgDetails = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    this.orgId = this.orgDetails.organisation.cimOrganisationId;
    this.logDetails = JSON.parse(localStorage.getItem('loginResponse'));
    this.showInprogress = this.data.showInprogress;
    this.emailProgress = this.data.email;
    console.log('Given email Id ' + this.emailProgress );
    console.log(this.showInprogress);
    this.personName = this.logDetails.personName;
    this.sendEmail = this.formBuilder.group({
      user: ['', Validators.required],
      email: [''],

    });

    this.sendEmail.get('user').valueChanges.subscribe(
      (user: string) => {
        console.log('====' + user);
        if (user === 'emailUser') {
          this.sendEmail.get('email').setValidators([Validators.required]);
        } else {
          this.sendEmail.get('email').clearValidators();
          this.submitted = false;
        }
        this.sendEmail.get('email').updateValueAndValidity();
      });

  }


  close() {
    this.dialogRef.close();
  }

  get f() {
    return this.sendEmail.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.sendEmail.invalid) {
      if (this.sendEmail.controls.user.value !== 'emailUser') {
        console.log(this.userStatus)
        //  this.submitted = false;

      }
      console.log(this.sendEmail.controls.email.value);
      return;
    } else {
      if (this.sendEmail.controls.user.value === 'loggedUser') {
        this.dialogRef.close();
        this.getOrgStatus();
        // this.router.navigate(['/app/professional-account']);
      } else {
        this.getEmailStatus();
      }
      this.submitted = false;
      console.log('selected Itek=' + JSON.stringify(this.sendEmail.value));
    }
  }

  getOrgStatus() {
    this.upgradeAcc = new UpgradeProfessionalAccount(UPGRADE_PRO_ACC_PATH,this.personName, null, this.orgId, this.logDetails.instanceLoginId);
    console.log('Upgrade Org Details' + JSON.stringify(this.upgradeAcc));
    if (this.orgId) {
      this.apiCall.doUpgradeProAccount(this.upgradeAcc).then(
        (result) => {
          this.adminResponse = result;
          console.log('Response' + JSON.stringify(this.adminResponse));
          localStorage.setItem('redirectingUrl', '');
          this.router.navigate(['/app/professional-account']);
        },
        (error) => {
          console.log('' + JSON.stringify(error));
        }
      );
    }
  }

  getEmailStatus() {
    this.emailValue = this.sendEmail.value;
    this.email = this.emailValue.email;
    this.upgradeAcc = new UpgradeProfessionalAccount(UPGRADE_PRO_ACC_PATH,this.personName, this.email, this.orgId, this.logDetails.instanceLoginId);
    console.log('Upgrade Email Details' + JSON.stringify(this.upgradeAcc));
    if (this.upgradeAcc) {
      this.apiCall.doUpgradeProAccount(this.upgradeAcc).then(
        (result) => {
          this.emailStatus = true;
          this.emailUpgradeResponse = result;
          console.log(' Upgrade Response' + JSON.stringify(this.emailUpgradeResponse));
          // let dialogRef = this.dialog.open(UpgradeStatusPopupComponent, {
          //   width: '600px',
          //   data: this.emailUpgradeResponse,
          // })
        },
        (error) => {
          console.log('' + JSON.stringify(error));
        }
      );
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}

