import {Component, Inject, OnInit} from '@angular/core';
import {MD_DIALOG_DATA, MdDialogRef} from '@angular/material';
import {PasswordModel} from '../../extra-pages/password/model/passwordModel';
import {ApiCalls} from '../../services/services';
import {Router} from '@angular/router';

@Component({
  selector: 'app-change-password',
  templateUrl: '../changePassword/changePassword.component.html'
})
export class ChangePasswordComponent implements OnInit {
  public changePasswordObj: PasswordModel;
  public loginResponse: any;
  public showLoader: any;
  public rememberPassword: any;
  passwordStrength: any;
  public barLabel = 'Password strength:';
  changePasswordError: any;
  passwordError: any;
  changePasswordResult: any;
  cpassword: any;
  constructor(public dialogRef: MdDialogRef<ChangePasswordComponent>,
              @Inject(MD_DIALOG_DATA) public data: any,
              public apiCall: ApiCalls,
              public router: Router) {
  }

  ngOnInit() {
    this.changePasswordObj = new PasswordModel('');
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
  }

  changePassword() {
    this.showLoader = true;
    const loginId = this.loginResponse.loginId;
    this.apiCall.doChangePassword(loginId, this.changePasswordObj.password).then((result) => {
      console.log('' + JSON.stringify(result));
      this.changePasswordResult = result;
      if (this.changePasswordResult.responseCode === '404') {
        this.changePasswordError = this.changePasswordResult.developerMessage;
        // this.changePasswordObj.password = '';
        // this.cpassword = '';
      } else {
        this.dialogRef.close();
        this.rememberPassword = localStorage.getItem('rememberPassword');
        if (this.rememberPassword) {
          localStorage.setItem('rememberPassword', this.changePasswordObj.password);
        }
        this.router.navigate(['/extra/accountLogin']);
      }

      this.showLoader = false;
    }, (error) => {
      console.log('Error ' + JSON.stringify(error));
      this.passwordError = error;
      this.changePasswordError = this.passwordError.developerMessage;
      // this.changePasswordObj.password = '';
      // this.cpassword = '';
      /* const resultData = this.validations.errorPopup('Invalid password format', error.responseMessage, '');
       const dialogRef = this.dialog.open(GlobalPopupComponent, {
       width: '450px',
       data: resultData,
       disableClose: true
       });*/
      this.showLoader = false;
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  passwordStatuss(ev) {
    this.passwordStrength = ev;
  }
}
