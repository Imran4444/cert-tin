import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-certificate-view',
  templateUrl: '../vewCertificate/certificateView.component.html',
  styleUrls: ['../vewCertificate/certificateView.component.css']
})


export class CertificateViewComponent {
  public loginResponse: any;
  constructor(public dialogRef: MdDialogRef<CertificateViewComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
    console.log(this.data);
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
  }

  close() {
    this.dialogRef.close();
  }
}
