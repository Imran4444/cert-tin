import { Component, OnInit } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-kyc-upgrade-popup',
  templateUrl: './kyc-upgrade-popup.component.html',
  styleUrls: ['./kyc-upgrade-popup.component.scss']
})
export class KycUpgradePopupComponent implements OnInit {

  constructor(
    public dialog: MdDialog,
    public dialogRef: MdDialogRef<KycUpgradePopupComponent>) { }

  ngOnInit() {
  }

  submit() {
    this.dialogRef.close();
  }
  close() {
    this.dialogRef.close();
  }

}
