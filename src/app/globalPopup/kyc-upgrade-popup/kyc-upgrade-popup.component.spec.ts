import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KycUpgradePopupComponent } from './kyc-upgrade-popup.component';

describe('KycUpgradePopupComponent', () => {
  let component: KycUpgradePopupComponent;
  let fixture: ComponentFixture<KycUpgradePopupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KycUpgradePopupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KycUpgradePopupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
