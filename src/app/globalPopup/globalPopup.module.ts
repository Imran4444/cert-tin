import { NgModule } from '@angular/core';
import { GlobalPopupComponent } from './globalPopup.component';
import { ChangePasswordComponent } from './changePassword/changePassword.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { CertificateViewComponent } from './vewCertificate/certificateView.component';
import { UpgradeAccountComponent } from './upgradeAccount/upgradeAccount.component';
import { MaterialModule } from '@angular/material';
import { UpgradeStatusPopupComponent } from './upgrade-status-popup/upgrade-status-popup.component';
import { KycSuccessPopupComponent } from './kyc-success-popup/kyc-success-popup.component';
import { KycUpgradePopupComponent } from './kyc-upgrade-popup/kyc-upgrade-popup.component';

@NgModule({
  imports: [FormsModule, BrowserModule, MaterialModule, ReactiveFormsModule],
  declarations: [GlobalPopupComponent, ChangePasswordComponent, CertificateViewComponent, UpgradeAccountComponent, UpgradeStatusPopupComponent, KycSuccessPopupComponent, KycUpgradePopupComponent],
  providers: [],
  entryComponents: [GlobalPopupComponent, KycUpgradePopupComponent, ChangePasswordComponent, CertificateViewComponent, UpgradeAccountComponent, UpgradeStatusPopupComponent, KycSuccessPopupComponent]
})

export class GlobalPopupModule {
  constructor() {
  }
}
