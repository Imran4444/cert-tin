import { Component, Inject } from '@angular/core';
import { MD_DIALOG_DATA, MdDialogRef } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-global-popup',
  templateUrl: '../globalPopup/globalPopup.component.html',
  styleUrls: ['../globalPopup/globalPopup.component.css']
})

export class GlobalPopupComponent {
  constructor(public router: Router, public dialogRef: MdDialogRef<GlobalPopupComponent>,
              @Inject(MD_DIALOG_DATA) public data: any) {
  }

  close(): void {
    this.dialogRef.close();
    this.router.navigate(this.data.route);
  }
}
