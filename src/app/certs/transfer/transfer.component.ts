import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../services/services';
import { Router } from '@angular/router';
import { LayoutService } from '../../layout/layout.service';
import {MySharedService} from "../../services/mySharedService ";
@Component({
  selector: 'transfer-certs-component',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.css']
})
export class TransferCertsComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  certsTransferred: any;
  certsTransferredList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  sortClickValue: any;
  sortFieldValue: any;
  requestSort: boolean;
  nameSort: boolean;
  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService, private layoutService: LayoutService ) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
  }
  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.getCertsTransferred();
  }

  selectRow(certsTransferred, indexValue, ev) {
    this.pagIndexVal = indexValue;
    console.log(certsTransferred);
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getCertsTransferred() {
    this.layoutService.updatePreloaderState('active');
   // this.showLoader = true;
    this.apiCall.getcertsTransferredData(this.organisationId, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder).then((result) => {
      console.log(result);
      this.layoutService.updatePreloaderState('hide');
    //  this.showLoader = false;
      this.certsTransferred = result;
      this.certsTransferredList = this.certsTransferred.certificates;
      this.totalPagesCount = this.certsTransferred.totalPages;
      this.totalpages = this.certsTransferred.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
    //  this.showLoader = false;
      if (error.responseCode === '506') {
        this.certsTransferredList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getCertsTransferred();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getCertsTransferred();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getCertsTransferred();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'requesterName') {
      this.placeholdervalue = 'Requester Name';
      this.searchKey = 'requesterName';
    } else if (selectedKey === 'requestType') {
      this.placeholdervalue = 'Requster Type';
      this.searchKey = 'type';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    console.log('searchValue...' + this.searchValue)
    if (this.searchKey) {
      this.getCertsTransferred();
    }
  }

  verify(certsTransferred) {
    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(certsTransferred));
    localStorage.setItem('CertificateType', certsTransferred.certificateType);
    this.router.navigate(['/app/certificate']);
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'request_name') {
      this.nameSort = true;
      this.SortClicking();
    } else if (this.sortingField === 'request_type') {
      this.nameSort = false;
      this.requestSort = true;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.requestSort = false;
      this.SortClicking();
    }
  }


  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getCertsTransferred();
  }
}
