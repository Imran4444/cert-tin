import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCalls } from '../../../services/services';
import { OrgKeyListModalRequest } from '../../../Modals/orgKeyListModel';
import { LayoutService } from '../../../layout/layout.service';
import {ValidationProvider} from "../../../providers/validationProvider";
import {GlobalPopupComponent} from "../../../globalPopup/globalPopup.component";
import {MdDialog} from "@angular/material";

@Component({
  selector: 'search-recipient',
  templateUrl: './searchRecipient.component.html',
  styleUrls: ['./searchRecipient.component.css']
})
export class SearchRecipientComponent implements OnInit {
  selectChoice: any;
  resultObj: any;
  searchResult: any = [];
  searchValue: any;
  orgResponse: any;
  orgSerachFields: any = {};
  paginationRequired: any;
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  totalpages: any;
  pagIndexVal: any;
  totalPagesCount: any;
  rowSelect: any;
  checkedIdx: any;
  orgResult: any;
  details: any;
  valueChoosed: any;
  IndvDetails: any;
  certDetails: any;
  private transferStatus: any;
  transferLoader: any;

  constructor(public router: Router, public apiCalls: ApiCalls, public layoutService: LayoutService,
  public validations : ValidationProvider, public dialog: MdDialog) {
    if (localStorage.getItem('chooseRecipientName')) {
      console.log(localStorage.getItem('chooseRecipientName'));
      this.valueChoosed = localStorage.getItem('chooseRecipientName');
      if (this.valueChoosed == 'transfer') {
        this.selectChoice = 'organisation';
      } else {
        this.selectChoice = 'individual';
      }
    } else {
      this.selectChoice = 'individual';
    }
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    if (localStorage.getItem('transferCertDetails')) {
      this.certDetails = JSON.parse(localStorage.getItem('transferCertDetails'));
      console.log(this.certDetails);
    } else {
    }
  }

  ngOnInit() {
    this.transferLoader = false;
    this.searchValue= '';
    this.getSrchList();
  }

  getSrchList() {
    console.log(this.searchValue);
    if (this.selectChoice == 'individual') {
      this.layoutService.updatePreloaderState('active');
      this.apiCalls.getIndividuals(this.searchValue, this.currentPage, this.pageSize).then((searchResult) => {
        this.layoutService.updatePreloaderState('hide');
        console.log(searchResult);
        this.orgResult = null;
        this.resultObj = searchResult;
        this.searchResult = this.resultObj.individuals;
        this.totalPagesCount = this.resultObj.totalPages;
        this.totalpages = this.resultObj.totalElements;
      }, (err) => {
        this.layoutService.updatePreloaderState('hide');
        console.log(err);
      });
    } else if (this.selectChoice == 'organisation') {
      this.getOrgNames(this.searchValue);
    }

  }

  selectRow(res, i, ev) {
    console.log(res);
    if (this.selectChoice === 'organisation') {
      this.details = res;
      this.IndvDetails = '';
    } else if (this.selectChoice === 'individual') {
      if (this.valueChoosed !== 'individual') {
        this.IndvDetails = res;
        this.details = '';
      } else {
        localStorage.setItem('recipientDetails', JSON.stringify(res));
        this.router.navigate(['/app/issue-newCertificate']);
      }
    }

  }

  verify() {

  }

  getOrgNames(searchFields) {
    this.layoutService.updatePreloaderState('active');
    this.apiCalls.getOrgList(this.searchValue).then((result) => {
        this.layoutService.updatePreloaderState('hide');
        console.log(result);
        this.resultObj = result;
        this.searchResult = null;
        this.orgResult = this.resultObj.organisations;
        console.log(this.resultObj);
        this.totalPagesCount = this.resultObj.totalPages;
        this.totalpages = this.resultObj.totalElements;
      }
      , (error) => {
        this.layoutService.updatePreloaderState('hide');
        console.log('' + JSON.stringify(error));
      });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getSrchList();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getSrchList();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getSrchList();
    }
  }

  changeType() {
    this.searchValue = '';
    this.getSrchList();
    this.details = '';
    this.IndvDetails = '';
  }

  transferCert() {
    this.transferLoader = true;
    const orgDetails = this.details;
    const reqData = {
      certificateId: this.certDetails.certificate.id,
      orgId: this.details.id,
      orgName: orgDetails.organisationName,
      orgEmail: ''
    };
    console.log(JSON.stringify(reqData));
    this.apiCalls.transferCertificate(JSON.stringify(reqData)).then((res) => {
      console.log(res);
      this.transferStatus = res;
      this.transferLoader = false;
      if (this.transferStatus.responseCode === '200') {
        const resultData = this.validations.errorPopup('Success', 'Certificate is transferred', ['/app/certs-issued']);
        const dialogRef1 = this.dialog.open(GlobalPopupComponent, {
          width: '300px',
          data: resultData,
          disableClose: true
        });
      }
    }, (err) => {
      this.transferLoader = false;
      console.log(err);
    });
  }
}
