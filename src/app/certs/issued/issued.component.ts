import { Component, OnInit } from '@angular/core';
import { ApiCalls} from '../../services/services';
import { Router} from '@angular/router';
import { LayoutService} from '../../layout/layout.service';
import {MySharedService} from "../../services/mySharedService ";
import {MdDialog} from "@angular/material";
import {ValidationProvider} from "../../providers/validationProvider";
import {GlobalPopupComponent} from "../../globalPopup/globalPopup.component";
@Component({
  selector : 'certs-issued-component',
  templateUrl: './issued.component.html',
  styleUrls: ['./issued.component.css']
})
export class CertsIssuedComponent implements OnInit {
  rowSelect: any;
  showLoader: any;
  loginResponse: any;
  organisationId: any;
  certsIssued: any;
  certsIssuedList: any[];
  currentPage: any;
  firstItem: any;
  lastItem: any;
  pageSize: any;
  pagIndexVal: any;
  checkedIdx: any;
  totalPagesCount: any;
  totalpages: any;
  searchValue: any;
  placeholdervalue: any;
  searchKey: any;
  sortingField: any;
  sortingOrder: any;
  inoutboxType: any;
  status: any;
  sortClickValue: any;
  sortFieldValue: any;
  requestSort: boolean;
  nameSort: boolean;
  selectedCertType: any;
  filterData: any;
  certNameSort: any;
  orgDetails: any;
  constructor(public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService, private layoutService: LayoutService,
              public validations: ValidationProvider, public dialog: MdDialog) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgId;
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.selectedCertType = 'ALL';
    this.filterData = 'ALL';
  }
  ngOnInit() {
    this.sortingField = '';
    this.sortingOrder = '';
    this.searchKey = '';
    this.searchValue = '';
    this.rowSelect = false;
    this.showLoader = false;
    this.placeholdervalue = 'Search';
    this.orgDetails = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log(this.orgDetails);
    this.getCertsIssued();
  }

  selectRow(certsIssued, indexValue, ev) {
    console.log(certsIssued);
    localStorage.setItem('CERTDETAILS', JSON.stringify(certsIssued));
    localStorage.setItem('certType', certsIssued.certificateType);
    this.pagIndexVal = indexValue;
    console.log(certsIssued);
    if (this.checkedIdx === indexValue) {
      this.checkedIdx = null;
      this.rowSelect = false;
    } else {
      this.checkedIdx = indexValue;
      this.rowSelect = true;
    }
  }

  getCertsIssued() {
    this.layoutService.updatePreloaderState('active');
    if (this.selectedCertType === 'ALL') {
      this.selectedCertType = '';
    }
//    this.showLoader = true;
    this.apiCall.getCertsIssuedData(this.organisationId, this.currentPage, this.pageSize, this.searchKey, this.searchValue, this.sortingField, this.sortingOrder, this.selectedCertType).then((result) => {
      console.log(result);
      this.layoutService.updatePreloaderState('hide');
  //    this.showLoader = false;
      this.certsIssued = result;
      this.certsIssuedList = this.certsIssued.certificates;
      console.log(this.certsIssuedList)
      this.totalPagesCount = this.certsIssued.totalPages;
      this.totalpages = this.certsIssued.totalElements;
    }, (error) => {
      console.log('Error' + JSON.stringify(error));
  //    this.showLoader = false;
      if (error.responseCode === '506') {
        this.certsIssuedList = [];
      }
    });
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    this.getCertsIssued();
  }

  prevPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      this.lastItem = this.lastItem - (+this.pageSize);
      this.getCertsIssued();
    }
  }

  nextPage() {
    if (this.pagIndexVal >= 0) {
      this.checkedIdx = null;
      this.rowSelect = false;
    }
    if (this.totalPagesCount > this.currentPage) {
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      this.getCertsIssued();
    }
  }

  selectedSearch(selectedKey) {
    this.searchValue = '';
    if (selectedKey === 'personName') {
      this.placeholdervalue = 'Recipient';
      this.searchKey = 'personName';
    } else if (selectedKey === 'certType') {
      this.placeholdervalue = 'Type';
      this.searchKey = 'certType';
    }  else if (selectedKey === 'certificateName') {
      this.placeholdervalue = 'Name';
      this.searchKey = 'certificateName';
    } else {
      this.placeholdervalue = 'Claim status';
      this.searchKey = 'claimStatus';
    }
  }

  getSearchResult() {
    console.log('searchValue...' + this.searchValue)
    if (this.searchKey) {
      this.getCertsIssued();
    }
  }

  verify(certsIssued) {
    // localStorage.setItem('IssuedCertDetails', JSON.stringify(certsIssued));

    localStorage.setItem('CERTIFICATEDETAILS', JSON.stringify(certsIssued));
    localStorage.setItem('CertificateType', certsIssued.certificateType);
    if (this.orgDetails.organisation.stripePlanName !== 'Trial') {
      this.router.navigate(['/app/certificate']);
    } else {
      const accountVerify = this.validations.errorPopup('Account Level', 'Your account has to be professional  to unlock this feature. To have a Professional account please complete your KYC by updating your personal and organisation details in My account.', '');
      const dialogRef = this.dialog.open(GlobalPopupComponent, {
        width: '450px',
        data: accountVerify,
        disableClose: true
      });
    }
  }

  sortingName(sortingType) {
    this.sortingField = sortingType;
    if (this.sortingField === 'personName') {
      this.nameSort = true;
      this.certNameSort = false;
      this.requestSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'certType') {
      this.nameSort = false;
      this.requestSort = true;
      this.certNameSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'certificateName') {
      this.nameSort = false;
      this.certNameSort = true;
      this.requestSort = false;
      this.SortClicking();
    } else {
      this.nameSort = false;
      this.requestSort = false;
      this.certNameSort = false;
      this.SortClicking();
    }
  }


  SortClicking() {
    this.checkedIdx = null;
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getCertsIssued();
  }
  getFilter(claimType) {
    this.pageSize = 10;
    this.totalpages = 0;
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = 10;
    this.totalpages = 0;
    this.sortClickValue = 0;
    this.selectedCertType = claimType;
    this.getCertsIssued();
  }

  issueCert(type) {
    if (type == 'cert') {
      localStorage.setItem('recipientDetails', null);
      localStorage.setItem('groupDetails', 'false');
      this.router.navigate(['app/issue-newCertificate']);
    } else {
      localStorage.setItem('recipientDetails', null);
      localStorage.setItem('groupDetails', 'true');
      this.router.navigate(['app/issue-newCertificate']);
    }
  }
}
