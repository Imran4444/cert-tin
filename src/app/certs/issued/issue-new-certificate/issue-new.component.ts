import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../../../services/services';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { auditService } from '../../../providers/audit.service';
import { LayoutService } from '../../../layout/layout.service';
import { TemplateServices } from '../../../services/templateServices';
import { ValidationProvider } from '../../../providers/validationProvider';
import { MdDialog } from '@angular/material';
import { Http } from '@angular/http';
import * as moment from 'moment';
import { CertModal } from '../../../Modals/certModal';
import { CertificateApply } from '../../../Modals/certificateapply';
import { AttendLetterModal } from '../../../Modals/attendlettermodal';
import { ProofInternModal } from '../../../Modals/proofinternmodal';
import { WorkAttestationModal } from '../../../Modals/workattestationmodal';
import { QualificationCertificateModal } from '../../../Modals/qualificationCertificatemodal';
import { LicenseCertificateModal } from '../../../Modals/licensecertificatemodal';
import { ReferenceLettermodal } from '../../../Modals/referencelettermodal';
import { FormControl, FormGroup } from '@angular/forms';
import { AWS_PHOTO_UPLOAD_URL } from '../../../endpoint';
import { IssueCertificateModal } from '../../../Modals/issuecertificatemodal';
import { GlobalPopupComponent } from '../../../globalPopup/globalPopup.component';
import { ProofOfEndrosment } from '../../../Modals/proofOfEndrosment';
import {PersonModal} from "../../../Modals/personModal";

@Component({
  selector: 'issue-new-cert-component',
  templateUrl: './issue-new.component.html',
  styleUrls: ['./issue-new.component.css']
})
export class IssueNewCertComponent implements OnInit {
  claimDetails: any;
  certType: any;
  getCertDetailsResponse: any;
  getCertDetail: any;
  getPersonDetail: any;
  commnets: any;
  endorseLevel: any;
  showLoader: any;
  endorseDeclineResult: string;
  inoutboxType: any;
  certificateID: any;
  certificateDetails: any;
  public mm: any;
  public template: any;
  public borderTop: string;
  public borderBtm: string;
  public currentHour: any;
  startDate: any;
  endDate: any;
  certificateIssueModel: any;
  qualicationModel: any;
  workattesstationModel: any;
  submitTry: any;
  issueForm: any;
  public personResponse: any;
  public footerInfo: any;
  public loginResponse: any;
  public personId: any;
  auditReq: any;
  public viewType: any;
  public certTypeList: any;
  public selected: any;
  public selectCertTypeResponse: any;
  public certTypeTemplate: any;
  public certificateTypes: any;
  public submitted: any;
  certificateapply: any;
  auditApplyReq: any;
  public fromDateName: any;
  public endDateName: any;
  public attendLetterModal: any;
  public proofinternModal: any;
  workAttestationModal: any;
  qualificationModal: any;
  licenseCertificateModal: any;
  referencelettermodalData: any;
  reqFormsData: any;
  applyCertificateResp: any;
  auditIssueCert: any;
  issuedCertResponse: any;
  public organizationName: any;
  public startDateInbox: any;
  public endDateInbox: any;
  public textViewInfo: any;
  public resultData: any;
  public file: any;
  public certImg: any;
  public showloader: boolean;
  public btnText: any;
  organisationResponse: any;
  public proofOfEndros: ProofOfEndrosment;
  public templatesSelected: string;
  public  endDateCheck: any;
  groupDetails: any;
  orgUploadUrl: any;
  errorFileFormat: any;
  showFileName: any;
  validationResult: any;
  uploadSuccess: any;
  errPop: any;
  fileURL: any;
  showLoaderIssue: any;
  validationStatus: any;
  constructor(public apiCall: ApiCalls,
              public router: Router,
              public datePipe: DatePipe,
              public auditHistory: auditService,
              public layoutService: LayoutService,
              public tempApi: TemplateServices,
              public validations: ValidationProvider,
              public dialog: MdDialog,
              public http: Http) {
    this.borderTop = 'assets/images/certBorders/border-top-1.png';
    this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
    this.fromDateName = 'Start Date';
    this.endDateName = 'End Date';
    /*this.btnText = 'Upload Template';*/
    this.viewType = 'text';
    this.mm = moment;
    this.reqFormsData = {};
    this.certificateIssueModel = new CertModal('', '', '', '', '', '', '', '', '', '', '', '');
    this.certificateapply = new CertificateApply('', '', '', '');
    this.attendLetterModal = new AttendLetterModal('', '', '', '', '', '', '', '', '', '');
    this.proofinternModal = new ProofInternModal('', '', '', '', '', '', '', '', '', '', '', '', '');
    this.workAttestationModal = new WorkAttestationModal('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    this.qualificationModal = new QualificationCertificateModal('', '', '', '', '', '', '', '', '');
    this.licenseCertificateModal = new LicenseCertificateModal('', '', '', '', '', '', '', '');
    this.referencelettermodalData = new ReferenceLettermodal('', '', '', '', '', '', '', '', '', '', '', '', '');
    this.proofOfEndros = new ProofOfEndrosment('', '', '', '', '', '', '', '', '', '');
    this.issueForm = new FormGroup({
      certName: new FormControl(''),
      courseName: new FormControl(''),
      splczation: new FormControl(''),
      strtDate: new FormControl(''),
      endate: new FormControl(''),
      descrptn: new FormControl(''),
      certificateTypeform: new FormControl(''),
      templateForm: new FormControl(''),
      eventName: new FormControl(''),
      roleName: new FormControl(''),
      currentlyWorkingForm: new FormControl(''),
      recomondForm: new FormControl(''),
      courseNameForm: new FormControl(''),
      knownYearsForm: new FormControl(''),
      certificationForm: new FormControl(''),
      locationForm: new FormControl(''),
      salaryeForm: new FormControl(''),
      empNumberForm: new FormControl(''),
      specializationForm: new FormControl(''),
      transcriptForm: new FormControl(''),
      levelForm: new FormControl(''),
      certificateNumberForm: new FormControl(''),
      positionForm: new FormControl(''),
      fullTimeForm: new FormControl(''),
      courseOfstudyForm: new FormControl(''),
      studentIdform: new FormControl('')
    });
    this.certificateapply.certType = 'defCertType';
    this.certificateapply.contentTemplateId = 'defConTemp';
    if (localStorage.getItem('recipientDetails')) {
      console.log('Stored recipient details');
      console.log(JSON.parse(localStorage.getItem('recipientDetails')));
      this.getPersonDetail = JSON.parse(localStorage.getItem('recipientDetails'));
    }
    console.log('details');
    console.log(this.getPersonDetail);
    this.groupDetails = localStorage.getItem('groupDetails');
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log(this.organisationResponse);
    this.organizationName = this.organisationResponse.organisation.organisationName;
    this.footerInfo = this.organisationResponse;
  }

  ngOnInit() {
    this.submitted = false;
    this.endDateCheck = false;
    this.getcertTypes();
    this.workAttestationModal.fulltime = false;
    this.proofinternModal.currentlyEmployed = false;
    this.personResponse = JSON.parse(localStorage.getItem('personResponse'));
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.templatesSelected = JSON.parse(localStorage.getItem('orgTemplate'));
    console.log(this.templatesSelected);
    this.personId = this.loginResponse.personId;
    this.currentHour = this.datePipe.transform(new Date(), 'HH:mm');
    if (localStorage.getItem('CERTDETAILS')) {
      this.certificateDetails = JSON.parse(localStorage.getItem('CERTDETAILS'));
      this.certificateID = this.certificateDetails.id;
    }
    this.certType = localStorage.getItem('certType');
    console.log('personResponse response...' + JSON.stringify(this.personResponse));
    /* console.log(JSON.stringify(this.certificateDetails));
     console.log(JSON.stringify(this.certificateID));*/
    this.getCertDetails();
    this.fileURL = ''; /*By Default we need to assign the value empty , bcoz for issuing individual we dont get file url*/
  }

  getcertTypes() {
    this.tempApi.getCertTypes().then((res) => {
      console.log('certTypeList' + JSON.stringify(res));
      this.certTypeList = res;
    });
  }

  selectedCertType(typeObj: any) {
    console.log(this.issueForm.value);
    this.certificateapply.contentTemplateId = 'defConTemp';
    console.log(typeObj);
    this.selected = typeObj;
    if (this.selected === 'TRANSCRIPT') {
      this.btnText = 'Upload Transcript';
    } else {
      this.btnText = 'Upload Template';
    }

    if (this.selected === 'LICENSE_CERTIFICATE') {
      this.fromDateName = 'Issued Date';
      this.endDateName = 'Expiry Date';
    } else {
      this.fromDateName = 'Start Date';
      this.endDateName = 'End Date';
    }
    this.tempApi.getCertContetntType(typeObj).then((result) => {
      this.selectCertTypeResponse = result;
      this.certTypeTemplate = this.selectCertTypeResponse.templates;
      console.log('select cert type' + JSON.stringify(result));
    });

  }

  setView(viewType: any) {
    this.viewType = viewType;
  }

  uploadPicture(event) {
    this.showloader = true;
    const formData: FormData = new FormData();
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];

    this.http.post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData).map((res) => res.json())
      .subscribe((data) => {
        this.btnText = files[0].name;
        // this.logoUrl = data.imageURL;
        this.certImg = data.imageURL;
        this.showloader = false;
        // alert('Image Uploaded Successfully');
      }, (err) => {
        this.showloader = false;
        alert('Oops !! Image Uploading Failed, Please Try Again');
      });
  }

  getCertDetails() {
    // doGetClaimDetails
    /* this.layoutService.updatePreloaderState('active');
     this.showLoader = true;
     this.certificateID = this.certificateDetails.id;
     this.apiCall.doGetClaimDetails(this.certificateID).then((result) => {
     this.showLoader = false;
     this.getCertDetailsResponse = result;
     this.getCertDetail = this.getCertDetailsResponse.calimRequest;
     this.footerInfo = this.getCertDetailsResponse.organisation;
     this.organizationName = this.getCertDetailsResponse.organisation.organisation.name;
     console.log('cert details resp:' + JSON.stringify(this.getCertDetailsResponse));
     this.certificateapply.certType = this.getCertDetail.requestType;
     this.selectedCertType(this.getCertDetail.requestType);
     this.template = this.getCertDetail.template;
     this.getPersonDetail = this.getCertDetailsResponse.person;
     if (this.template === 'WORKATTESTATION_DEFAULT' || this.template === 'QUALIFICATION_DEFAULT' || this.template === 'WORKATTESTATION_ORG' || this.template === 'QUALIFICATION_ORG') {
     this.borderTop = 'assets/images/certBorders/border-top-1.png';
     this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
     } else if (this.template === 'WORKATTESTATION_TEMP') {
     this.borderTop = 'assets/images/certBorders/top-border-2.png';
     this.borderBtm = 'assets/images/certBorders/bottom-border-2.png';
     } else if (this.template === 'QUALIFICATION_TEMP') {
     this.borderTop = 'assets/images/certBorders/top-border-3.png';
     this.borderBtm = 'assets/images/certBorders/bottom-border-3.png';
     } else {
     this.borderTop = 'assets/images/certBorders/border-top-1.png';
     this.borderBtm = 'assets/images/certBorders/border-bottom-1.png';
     }
     this.layoutService.updatePreloaderState('hide');
     }, (error) => {
     console.log(error);
     this.showLoader = false;
     });*/
  }

  createAuditLog() {
    this.auditHistory.userAudit('Certificate');
  }

  getEndroseDeclined(Status) {
    console.log(this.issueForm);
    this.submitTry = true;
    if (this.issueForm.valid) {
      console.log('' + Status);

      this.startDate = this.datePipe.transform(this.certificateIssueModel.startDate, 'yyyy-MM-dd') + ' ' + this.currentHour + ':00.527Z';
      this.endDate = this.datePipe.transform(this.certificateIssueModel.endDate, 'yyyy-MM-dd') + ' ' + this.currentHour + ':00.527Z';

      this.showLoader = true;
      if (Status === 'ISSUED') {
        // const endorseRequest = new CertModal(this.certificateID, Status, '', '1');
        this.auditReq = this.auditHistory.userAudit('CERTIFICATE ' + Status);
        const endorseRequest = new CertModal(this.auditReq, this.certificateID, this.certificateIssueModel.certificateName, Status, this.certificateIssueModel.courseName, this.certificateIssueModel.description, this.startDate, this.endDate, this.certificateIssueModel.specialization, 'Organisation', this.personId, this.certificateIssueModel.customTemplateUrl);
        console.log('endorseReq' + JSON.stringify(endorseRequest));
        console.log('auditReq' + JSON.stringify(this.auditReq));
        localStorage.setItem('issueObj', JSON.stringify(endorseRequest));
        localStorage.setItem('certDetails', JSON.stringify(this.getCertDetail));
        localStorage.setItem('INOUTBOX', 'CERTIFICATES');
        this.router.navigate(['/app/issue-endrose']);
      } else {
        localStorage.setItem('INOUTBOX', 'INBOX');
        this.router.navigate(['app/all-certificates']);
      }
    } else {
      console.log(this.issueForm.value);
    }
  }

  get f() {
    return this.issueForm.controls;
  }

  applyChanges() {
    this.submitted = true;
    if (this.groupDetails === 'true') {
      this.getPersonDetail = new PersonModal('Mr.' , 'Test', 'User', '', 'testuser@gmail.com', '+27', '4682222497');
    }
    if (this.issueForm.invalid || this.certificateapply.contentTemplateId === 'custom_template' || (this.groupDetails == 'false' && !this.getPersonDetail) || (this.groupDetails == 'true' && !this.fileURL)) {
      this.certificateIssueModel.customTemplateUrl = this.certImg;
      if (!this.getPersonDetail || !this.file) {
        if (this.groupDetails == 'false') {
          this.errPop = 'Please choose the recipient';
        } else {
          this.errPop = 'Please choose the file';
        }
        const popResultData = this.validations.errorPopup('Error', this.errPop , '');
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: popResultData
        });
      }
    } else {
      this.certificateIssueModel.customTemplateUrl = '';
      console.log(this.certificateIssueModel.startDate);
      if (this.certificateIssueModel.startDate) {
        this.startDateInbox = this.datePipe.transform(this.certificateIssueModel.startDate, 'yyyy-MM-dd');
      } else {
        this.startDateInbox = '';
      }
      if (this.certificateIssueModel.endDate) {
        this.endDateInbox = this.datePipe.transform(this.certificateIssueModel.endDate, 'yyyy-MM-dd');
      } else {
        this.endDateInbox = '';
      }
      if (this.certificateapply.certType === 'ATTENDANCE_LETTER') {
        this.reqFormsData = new AttendLetterModal(this.attendLetterModal.event, this.startDateInbox, this.endDateInbox, this.attendLetterModal.location, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.firstName, this.personResponse.person.lastName, this.fileURL);
      } else if (this.certificateapply.certType === 'PROOF_OF_INTERNSHIP') {
        this.reqFormsData = new ProofInternModal(this.proofinternModal.role, this.proofinternModal.currentlyEmployed, this.startDateInbox, this.endDateInbox, this.attendLetterModal.location, +this.proofinternModal.recommendationLevel, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.firstName, this.personResponse.person.lastName, 'M', this.fileURL);
      } else if (this.certificateapply.certType === 'WORK_ATTESTATION') {
        this.reqFormsData = new WorkAttestationModal(this.proofinternModal.role, this.proofinternModal.currentlyEmployed, this.startDateInbox, this.endDateInbox, this.workAttestationModal.salary, this.workAttestationModal.employeeNumber, 'M', this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.workAttestationModal.fulltime, this.personResponse.person.phones[0].countryCode, this.personResponse.person.phones[0].number, this.personResponse.person.emails[0].email, this.personResponse.person.firstName, this.personResponse.person.lastName, this.fileURL);
      } else if (this.certificateapply.certType === 'QUALIFICATION_CERTIFICATE') {
        this.reqFormsData = new QualificationCertificateModal(this.qualificationModal.courseName, this.qualificationModal.specialisation, this.startDateInbox, this.endDateInbox, this.qualificationModal.tranScript, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.fileURL);
      } else if (this.certificateapply.certType === 'LICENSE_CERTIFICATE') {
        this.reqFormsData = new LicenseCertificateModal(this.licenseCertificateModal.certification, this.startDateInbox, this.licenseCertificateModal.level, this.endDateInbox, this.licenseCertificateModal.certificationNumber, this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.fileURL);
      } else if (this.certificateapply.certType === 'REFERENCE_LETTER') {
        this.reqFormsData = new ReferenceLettermodal(this.referencelettermodalData.yearsKnown, this.referencelettermodalData.position, +this.proofinternModal.recommendationLevel, 'M', this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.organizationName, this.personResponse.person.phones[0].countryCode, this.personResponse.person.phones[0].number, this.personResponse.person.emails[0].email, this.personResponse.person.firstName, this.personResponse.person.lastName, this.fileURL);
      } else if (this.certificateapply.certType === 'PROOF_OF_ENROLLMENT') {
        this.reqFormsData = new ProofOfEndrosment(this.getPersonDetail.firstName, this.getPersonDetail.lastName, this.proofOfEndros.studentID, this.organizationName, this.workAttestationModal.fulltime, this.qualificationModal.courseName, this.qualificationModal.specialisation, this.startDateInbox, this.endDateInbox, this.fileURL);
      }
      console.log('req Forms Data' + JSON.stringify(this.reqFormsData));
      this.auditApplyReq = this.auditHistory.userAudit('Certificate Applynow');
      this.certificateapply = new CertificateApply(this.auditApplyReq, this.certificateapply.certType, this.certificateapply.contentTemplateId, this.reqFormsData);
      console.log('apply changes req...' + JSON.stringify(this.certificateapply));
      this.apiCall.doApplyCertificate(this.certificateapply).then((result) => {
        this.applyCertificateResp = result;
        this.textViewInfo = this.applyCertificateResp.content;

        console.log('applyCertificateResp...' + JSON.stringify(this.applyCertificateResp));
      }, (error) => {
        console.log('applyCertificateResp error...' + JSON.stringify(error));
      });
    }
  }

  issueCertificate() {
      if (this.certificateapply.contentTemplateId !== 'custom_template') {
        this.certificateIssueModel.customTemplateUrl = '';
      } else {
        this.applyCertificateResp = {};
        this.applyCertificateResp.htmlContent = '';
        this.applyCertificateResp.content = '';
      }
      if (this.certificateIssueModel.startDate) {
        const startDateInbox = this.datePipe.transform(this.certificateIssueModel.startDate, 'yyyy-MM-dd');
        this.startDateInbox = startDateInbox + ' ' + this.currentHour + ':00.527Z';
      } else {
        this.startDateInbox = '';
      }
      if (this.certificateIssueModel.endDate) {
        const endDateInbox = this.datePipe.transform(this.certificateIssueModel.endDate, 'yyyy-MM-dd');
        this.endDateInbox = endDateInbox + ' ' + this.currentHour + ':00.527Z';
      } else {
        this.endDateInbox = '';
      }
      this.auditIssueCert = this.auditHistory.userAudit('Issue certificate');
      const issuecertificateModal = new IssueCertificateModal([],
        this.auditIssueCert,
        this.certificateapply.certType,
        this.certificateapply.certType,
        '',
        this.qualificationModal.courseName,
        '',
        this.endDateInbox,
        this.getPersonDetail.cimPersonId,
        '',
        this.personId,
        this.footerInfo.organisation.cimOrganisationId,
        this.qualificationModal.specialisation,
        this.startDateInbox,
        this.endDateInbox,
        this.certificateIssueModel.customTemplateUrl,
        this.applyCertificateResp.htmlContent,
        this.applyCertificateResp.content,
        this.attendLetterModal.event,
        this.proofinternModal.role,
        this.licenseCertificateModal.certification,
        this.workAttestationModal.position,
        +this.referencelettermodalData.yearsKnown,
        this.certificateapply.contentTemplateId,
        this.fileURL);
      console.log('issued request....' + JSON.stringify(issuecertificateModal));
      this.showLoaderIssue =  true;
    if (this.groupDetails === 'true') {
      this.postValidation(issuecertificateModal);
    } else {
      this.apiCall.doIssueCertificate(issuecertificateModal).then((result) => {
        this.showLoaderIssue = false;
        this.issuedCertResponse = result;
        console.log('issuedCertResponse...' + JSON.stringify(this.issuedCertResponse));
        localStorage.setItem('verifyObj', JSON.stringify(this.issuedCertResponse));
        this.router.navigate(['app/verify']);
        /* this.resultData = this.validations.errorPopup('Success', this.issuedCertResponse.responseMessage, ['app/certs-issued']);
         const dialogRef = this.dialog.open(GlobalPopupComponent, {
         width: '450px',
         data: this.resultData,
         disableClose: true
         }); */
        console.log('issuedCertResponse...' + JSON.stringify(this.issuedCertResponse));
      }, (error) => {
        this.showLoaderIssue = false;
        this.resultData = this.validations.errorPopup('Fail', 'Issued failed, Please try again.', ['app/certs-issued']);
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: this.resultData,
          disableClose: true
        });
        console.log('issuedCertResponse error...' + JSON.stringify(error));
      });
    }
    }
  cancelCertificate() {
    this.router.navigate(['app/certs-issued']);
  }

  findRecipient() {
    localStorage.setItem('chooseRecipientName', 'individual');
    this.router.navigate(['app/search-recipient']);
  }
  currentlyWorkChange(checkValue) {
    console.log('check value...' + checkValue);
    if (checkValue == true) {
      this.endDateCheck = true;
      this.certificateIssueModel.endDate = '';
    } else {
      this.endDateCheck = false;
    }
  }
  uploadExcel(event) {
    this.showLoader = true;
    const formData: FormData = new FormData();
    const eventObj: MSInputMethodContext = event as MSInputMethodContext;
    const target: HTMLInputElement = eventObj.target as HTMLInputElement;
    const files: FileList = target.files;
    formData.append('file', files[0]);
    this.file = files[0];
    console.log(this.file);
    const type = this.file.name.split('.').pop();
    console.log(type);
    if (type == 'xls' || type == 'xlsx' || type == 'csv') {
      this.errorFileFormat = false;
      /*    this.http.post(AWS_PHOTO_UPLOAD_URL + files[0].name, formData).map((res) => res.json())
       .subscribe((data) => {
       this.showFileName = true;
       // this.orgUploadUrl = data.imageURL;
       this.orgUploadUrl = data.resizedImageUrl;
       this.showLoader = false;
       // alert('Image Uploaded Successfully');
       }, (err) => {
       this.showLoader = false;
       alert('Oops !! Image Uploading Failed, Please Try Again');
       });*/
      const fileuploaded = new FormData();
      fileuploaded.append('file', this.file);
      console.log(fileuploaded);
      this.showLoader = true;
      this.apiCall.uploadExcel(fileuploaded).then((res) => {
        console.log(res);
        this.showLoader = false;
        this.validationResult = res;
        if (this.validationResult.responseCode == '200') {
          this.showFileName = true;
          this.uploadSuccess = true;
          this.fileURL = this.validationResult.imageURL;
          console.log(this.fileURL);
        } else {
          this.uploadSuccess = false;
        }
      }, (err) => {
        this.showLoader = false;
        console.log(err);
      });
    } else {
      this.showLoader = false;
      this.errorFileFormat = true;
      this.showFileName = false;
    }
  }
  postValidation(fileuploaded) {
    console.log('Valid File');
    this.apiCall.validationFile(fileuploaded).then((validFile) => {
      this.validationStatus = validFile;
      console.log(this.validationStatus);
      if (this.validationStatus.responseCode == '200') {
        let msg = this.validationStatus.responseMessage;
        const popResultData = this.validations.errorPopup('Success', msg , ['/app/certs-issued']);
        const dialogRef = this.dialog.open(GlobalPopupComponent, {
          width: '450px',
          data: popResultData,
          disableClose: true
        });
      }
      this.showLoaderIssue = false;
    }, (err) => {
      this.showLoaderIssue = false;
      console.log(err);
    });
  }
}
