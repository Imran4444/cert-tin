import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ApiCalls} from "../../../services/services";
import {MdDialog} from "@angular/material";
import {GlobalPopupComponent} from "../../../globalPopup/globalPopup.component";
import {ValidationProvider} from "../../../providers/validationProvider";
@Component({
  selector : 'view-issued-component',
  templateUrl: './view-issued.component.html',
  styleUrls: ['./view-issued.component.css']
})
export class ViewIssuedCertsComponent implements OnInit {
  qrValue: any;
  certDetails: any;
  issuedDetails: any;
  revoked: any;
  resultData: any;
  certStatus: any;
  revokeLoader: any;
  transferLoader: any;
  constructor(public router: Router, public apiCall: ApiCalls, public mdDialog : MdDialog, public validations: ValidationProvider) {
    this.certDetails = JSON.parse(localStorage.getItem('IssuedCertDetails'));
  }
  ngOnInit() {
    this.qrValue = '0xc8384a8033485b24f0205fbf7e32eac106fdaeb7';
    this.getCertIdDetails();
    this.revokeLoader = true;
    this.transferLoader = false;
  }
  getCertIdDetails() {
    console.log(this.certDetails.id);
    this.apiCall.getIssuedCertDetails(this.certDetails.id).then((issuedDetails) => {
      console.log(JSON.stringify(issuedDetails));
      console.log(issuedDetails);
      this.issuedDetails = issuedDetails;
      this.certStatus = this.issuedDetails.certificate.isValid;
      console.log('certStatus ' + this.certStatus);
    });
  }
  transfer() {
    localStorage.setItem('chooseRecipientName', null);
    this.router.navigate(['app/search-recipient']);
  }
  revokeCert() {
    this.revokeLoader = true;
    const req = {
      certificateId: this.issuedDetails.certificate.id,
      cimOrgId: this.issuedDetails.organisation.organisation.id
    }
    this.apiCall.revokeCertificate(JSON.stringify(req)).then((rev) => {
      console.log(rev);
      this.revokeLoader = false;
      this.revoked = rev;
      this.resultData = this.validations.errorPopup('Revoked', 'Certificate is Revoked', ['/app/certs-issued']);
      if (this.revoked.certificate && !this.revoked.certificate.isValid) {
        const modal = this.mdDialog.open(GlobalPopupComponent, {data : this.resultData, width: '450px', disableClose: true});
        modal.afterClosed().subscribe((res) => {
          console.log(res);
        });
      }
    }, (err) => {
      this.revokeLoader = false;
      console.log(err);
    });
  }
}
