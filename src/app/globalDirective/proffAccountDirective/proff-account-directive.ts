import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Address } from 'ngx-google-places-autocomplete/objects/address';
import { GooglePlaceDirective } from 'ngx-google-places-autocomplete';
import { ComponentRestrictions } from 'ngx-google-places-autocomplete/objects/options/componentRestrictions';
import { ProffessionalAccountModel } from '../../Modals/proffAccountModel';
import { KycModel } from '../../Modals/kycAccountModel';
import { IndustryTypes } from '../../providers/industrytypes';
import { Countriescodes } from '../../providers/countriescodes';
import { ApiCalls } from '../../services/services';
import { GlobalPopupComponent } from '../../globalPopup/globalPopup.component';
import { APP_INSTANCE_ID, ORGANISATION_ID, PASSWORD_POLICY_ID } from '../../AppConstants';
import { SignUpRequest } from '../../Modals/signupRequestModal';
import { ValidationProvider } from '../../providers/validationProvider';
import { MdDialog } from '@angular/material';
import { AddressRequest } from '../../Modals/addressModal';
import { Emails } from '../../Modals/emailModal';
import { Phones } from '../../Modals/phonesModal';
import { DatePipe } from '@angular/common';
import { KlaviyoModal } from '../../Modals/klaviyoModal';
import { KlaviyoKycUpgradeModel } from "../../Modals/klaviyoKycUpgradeModel";
import { KycSuccessPopupComponent } from '../../globalPopup/kyc-success-popup/kyc-success-popup.component';
import { DEV_ROOT_PATH } from "../../endpoint";
import { UpgradeAccountComponent } from "../../globalPopup/upgradeAccount/upgradeAccount.component";

@Component({
  selector: 'app-proffAcc-directive',
  templateUrl: './proff-account-directive.html',
  styleUrls: ['./proff-account-directive.css']
})
export class ProffAccDirectiveComponent implements OnInit {
  @Input() orgData: any;
  @Input() personData: any;
  @Input() mailId: any;
  profAccountForm: FormGroup;
  profAccModel: any;
  countryDetails: any;
  submitted: any;
  validityStatus: any;
  siteCheck: any;
  industryNames: any;
  googlePlacesOptions: any;
  @ViewChild('placesRef')
  placesRef: GooglePlaceDirective;
  selectedObject: any;
  countryResponse: any;
  step1Status: any;
  step2Status: any;
  submittedTry: any;
  kycAccountForm: FormGroup;
  kycModel: any;

  /* orgData: any;
   personData: any;*/

  upgradeResult: any;
  kycStatus: any;
  org: any;
  person: any;
  loginResponseStorage: any;
  redirectingUrl: any;
  registeredStatus: any;
  profileUrl: any;
  emailsArray: any;
  addressArray: any;
  phonesArray: any;
  organisations: any;
  userRolesArray: any;
  appInstanceArray: any;
  userName: any;
  resultData: any;
  signupLoader: any;
  responseInfo: any;
  public klaviyodata: any
  public eventProperties: any;
  datereq: any;
  personId: any;
  maxDate: any;
  expiryDates: any;
  issuedCountrtyName: any;
  upgradeAccountStatus: any;
  progressResponse: any;
  orgId: any;

  constructor(public router: Router, public formBuilder: FormBuilder,
              public countriesCodes: Countriescodes, public apiCall: ApiCalls,
              public industryTypes: IndustryTypes, public validations: ValidationProvider,
              public dialog: MdDialog, public datePipe: DatePipe) {
    this.eventProperties = {}
    this.profAccModel = new ProffessionalAccountModel('', '', '', '', '', '', '', '', '', '');
    this.kycModel = new KycModel('', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
    this.klaviyodata = new KlaviyoModal('', '', '', '');
    this.countryDetails = this.countriesCodes.getCountries();
    this.industryNames = this.industryTypes.getIndustries();
    this.profAccModel.industry = 'defaultInd';
    this.profAccModel.countryCode = 'defaultCode';
    this.profAccModel.country = 'selectYourCountry';
    this.selectedObject = 'selectYourCountry';
    this.kycModel.nationality = 'defaultNationality';
    this.kycModel.idProofType = 'defaultIdtype';
    this.kycModel.issuedCountry = 'selectYourCountry';
    this.step1Status = true;
    this.step2Status = false;
    this.emailsArray = [];
    this.addressArray = [];
    this.phonesArray = [];
    this.organisations = [];
    this.userRolesArray = [];
    this.appInstanceArray = [];
    this.expiryDates = '';
  }

  ngOnInit() {
    console.log(this.orgData);
    this.orgId = this.orgData.cimOrganisationId;
    this.checkUpgradeStatus();
    this.maxDate = new Date();
    this.redirectingUrl = localStorage.getItem('redirectingUrl');
    this.registeredStatus = localStorage.getItem('registeredStatus');
    console.log('redirectingUrl ' + this.redirectingUrl, 'Reg status  ' + this.registeredStatus);
    this.profileUrl = 'https://flipaccess-resized.s3.amazonaws.com/resized-profile_pic.jpg';
    console.log(this.orgData);
    console.log(this.personData);
    /*this.getPersonDetails()*/
    // this.findMe();
    this.profAccountForm = this.formBuilder.group({
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      countrycode: ['', Validators.required],
      mobile: ['', Validators.required],
      email: ['', Validators.required],
      country: ['', Validators.required],
      org: ['', Validators.required],
      organisationWeb: ['', Validators.required],
      location: ['', Validators.required],
      industry: ['', Validators.required]
    });
    this.kycAccountForm = this.formBuilder.group({
      idType: ['', Validators.required],
      idNum: ['', Validators.required],
      national: ['', Validators.required],
      country: ['', Validators.required],
      regNum: ['', Validators.required],
      orgName: ['', Validators.required],
      vatNum: ['', Validators.required],
      regDate: ['', Validators.required],
      line1: ['', Validators.required],
      line2: [''],
      line3: [''],
      region: ['', Validators.required],
      pin: ['', Validators.required],
      expiry: [''],
      issue: ['']
    });
    this.getPersonDetails();
  }

  checkUpgradeStatus() {
    if (this.orgId) {
      this.apiCall.doGetOrgProcess(this.orgId).then(
        (result) => {
          console.log(result);
          this.progressResponse = result;
          this.upgradeAccountStatus = this.progressResponse.organisation.customFields.upgradeAcountStatus;
          console.log(' Progress Response' + JSON.stringify(this.progressResponse));
          if (this.upgradeAccountStatus === 'submitted') {
            this.openPopup(true);
          } else if ((this.upgradeAccountStatus === 'inProgress')) {

          } else {
          }
        },
        (error) => {
          console.log('' + JSON.stringify(error));
        }
      );
    }
  }

  openPopup(progress) {
    console.log(progress);
    const resultData = this.validations.errorPopup('In Progress', 'Upgrade initiation is in progress.', ['app/dashboard']);
    const dialogRef = this.dialog.open(GlobalPopupComponent, {
      width: '300px',
      data: resultData,
      disableClose: true
    });
  }

  getPersonDetails() {
    /* this.person = JSON.parse(localStorage.getItem('personResponse'));
     this.personData = this.person.person;*/
    console.log(this.orgData);
    console.log(this.personData);
    if (this.personData) {
      this.registeredStatus = 'true';
      localStorage.setItem('registeredStatus', 'true');
      this.profAccModel.firstName = this.personData.firstName;
      this.profAccModel.lastName = this.personData.lastName;
      this.profAccModel.countryCode = this.personData.phones[0].countryCode;
      this.profAccModel.mobileNumber = this.personData.phones[0].number;
      this.countryDetails.forEach((countryObj) => {
        if (countryObj.name === this.personData.address[0].country) {
          this.selectedObject = countryObj.code;
          this.profAccModel.country = this.personData.address[0].country;
          console.log('Country Code' + this.selectedObject);
        } else {
        }
      });
    }
    if (this.mailId) {
      this.profAccModel.emailId = this.mailId;
    } else {
      this.profAccModel.emailId = this.personData.emails[0].email;
    }

    if (this.orgData) {
      this.profAccModel.organisation = this.orgData.organisationName;
      this.profAccModel.organisationWebsite = this.orgData.organisationWebsite;
      this.profAccModel.location = this.orgData.location;
      this.profAccModel.industry = this.orgData.industry;
    }
    console.log(this.profAccModel.country);
  }

  get f() {
    return this.profAccountForm.controls;
  }

  get g() {
    return this.kycAccountForm.controls;
  }

  checkValidSite() {
    if (this.profAccModel.organisationWebsite) {
      this.apiCall
        .getValidStatus(this.profAccModel.organisationWebsite)
        .then((result) => {
          console.log(result);
          this.siteCheck = result;
          this.validityStatus = this.siteCheck.status;
        });
    }
  }

  handleAddressChange(address: Address) {
    this.profAccModel.location = address.formatted_address;
    console.log('change address' + JSON.stringify(this.profAccModel.location));
  }

  goToKycForm() {
    this.submitted = true;
    if (this.profAccountForm.valid) {
      if (this.registeredStatus == 'true') {
        this.step2Status = true;
        this.step1Status = false;
      } else {
        this.signupCall();
        this.step2Status = true;
        this.step1Status = false;
      }
    } else {
    }

  }

  signupCall() {
    this.signupLoader = true;
    this.userName = this.profAccModel.emailId;
    const address = new AddressRequest(
      this.profAccModel.location,
      '',
      '',
      '',
      this.profAccModel.country,
      '',
      true,
      '',
      'Physical Address'
    );
    this.addressArray.push(address);
    /*Organisation Model*/
    this.organisations.push(ORGANISATION_ID);
    /*User Roles*/
    const userRole = 'CERTTIN_ADMIN';
    this.userRolesArray.push(userRole);
    /*Custom Fields Model*/
    const customFields = {};
    /*Emails Modal*/
    const emails = new Emails(true, this.profAccModel.emailId, 'private', false);
    this.emailsArray.push(emails);

    /*Phones Modal*/
    const phones = new Phones(
      true,
      this.profAccModel.countryCode,
      this.profAccModel.mobileNumber,
      'private',
      false
    );
    this.phonesArray.push(phones);
    const signupRequest = new SignUpRequest(
      true,
      this.profileUrl,
      this.profAccModel.firstName,
      this.profAccModel.lastName,
      this.addressArray,
      this.emailsArray,
      this.phonesArray,
      APP_INSTANCE_ID,
      true,
      this.profAccModel.organisation,
      this.profAccModel.industry,
      this.profAccModel.organisationWebsite,
      ORGANISATION_ID,
      PASSWORD_POLICY_ID,
      this.userRolesArray,
      this.userName,
      customFields,
      'web',
      ''
    );
    console.log(signupRequest)
    this.apiCall.kycDoSignup(signupRequest).then(
      (result) => {
        this.responseInfo = result;
        /*    const resultData = this.validations.registarion(
              this.profAccModel.emailId,
              this.profAccModel.mobileNumber,
              ['/extra/login']
            );*/
        /*     const dialogRef = this.dialog.open(GlobalPopupComponent, {
               width: '450px',
               data: resultData,
               disableClose: true
             });*/
        this.signupLoader = false;
        console.log('' + JSON.stringify(result));

      }, (error) => {
        this.signupLoader = false;
        console.log('' + JSON.stringify(error));
      }
    );
  }

  goBack() {
    this.step2Status = false;
    this.step1Status = true;
  }

  // dateConsole(){
  //   this.datereq = this.datePipe.transform(this.kycModel.regDate, 'yyyy-MM-dd');
  //   console.log(this.datereq  + ' 08:42:34.745Z');
  // }

  upgrade() {
    this.submittedTry = true;
    if (this.personData) {
      this.personId = this.personData.id;
    } else {
      this.personId = this.responseInfo.user.cimPersonId;
    }
    if (this.kycModel.expiryDate) {
      this.expiryDates = (this.datePipe.transform(this.kycModel.expiryDate, 'yyyy-MM-dd') + ' 08:42:34.745Z');
    } else {

    }
    if (this.kycAccountForm.valid) {
      this.countryDetails.forEach((countryObj) => {
        if (countryObj.code === this.kycModel.issuedCountry) {
          this.issuedCountrtyName = countryObj.name;
          console.log('issued Countrty Name ' + this.issuedCountrtyName);
        } else {
        }
      });
      const request = {
        orgKyv: {
          businessStartDate: (this.datePipe.transform(this.kycModel.regDate, 'yyyy-MM-dd') + ' 08:42:34.745Z'),
          organisationId: this.orgData.cimOrganisationId,
          registrationNumber: this.kycModel.registrationNumber,
          taxNumber: this.kycModel.vatNumber,
          organisationName: this.profAccModel.organisation,
          organisationWebsite: this.profAccModel.organisationWebsite,
          address: {
            city: this.kycModel.region,
            country: this.kycModel.issuedCountry,
            line1: this.kycModel.line1,
            line2: this.kycModel.line2,
            line3: this.kycModel.line3,
            postCode: this.kycModel.postCode,
            region: this.kycModel.region
          },
        },
        personId: this.personId,
        personKyc: {
          idNumber: this.kycModel.idNumber,
          idProofType: this.kycModel.idProofType,
          nationality: this.kycModel.nationality,
          expiryDate: this.expiryDates,
          issuedDate: (this.datePipe.transform(this.kycModel.issuedDate, 'yyyy-MM-dd') + ' 08:42:34.745Z')
        }
      };
      console.log(JSON.stringify(request));
      this.apiCall.upgradeKyc(request).then((kycUpgrade) => {
        console.log(kycUpgrade);
        this.klaviyoInfo(request);
        this.saveKyc(request);
        this.createZohoLead(request);
        this.upgradeResult = kycUpgrade;
        if (this.upgradeResult.responseMessage == 'Success') {
          this.kycStatus = this.upgradeResult.responseMessage;
          console.log(this.kycStatus);
          localStorage.setItem('kycStatus', this.kycStatus);
          const dialogRef = this.dialog.open(KycSuccessPopupComponent, {
            width: '540px',
            disableClose: true
          });
          /*   if (this.redirectingUrl == 'email') {
               this.router.navigate(['/extra/accountLogin']);
             } else {
               this.router.navigate(['/app/dashboard']);
             }*/
        } else {

        }
      }, (err) => {
        console.log(err);
        // this.saveKyc(request);
      });
    } else {

    }
  }

  createZohoLead(request) {
    console.log(request)
    const leadXml = "<Leads><row no='1'><FL val='Lead Owner'>" + request.firstname + "</FL><FL val='First Name'>" + request.firstname + "</FL><FL val='Project Name'>Certtin</FL><FL val='NDA'>No</FL><FL val='Company'>" + request.organisation + " </FL><FL val='Last Name'> " + request.lastName + "</FL><FL val='Email'>" + request.emailId + "</FL><FL val='City'>" + request.location + "</FL><FL val='Description'>" + JSON.stringify(request) + "</FL></row></Leads>"
    this.apiCall.createLead(leadXml).then((result) => {
      console.log('lead created');
    })
  }

  saveKyc(request) {
    this.apiCall.saveKycAndKyv(JSON.stringify(request)).then((saveDetails) => {
      console.log('Save Details ' + JSON.stringify(saveDetails));
    }, (err) => {
      console.log(err);
    });
  }

  klaviyoInfo(details: any) {
    console.log(details);
    const emailID = {
      email: 'support@fliptin.com'
    };
    this.eventProperties = new KlaviyoKycUpgradeModel(this.kycModel.region, this.kycModel.vatNumber, this.kycModel.line1, this.kycModel.line2,
      this.kycModel.line3, this.datePipe.transform(this.kycModel.regDate, 'dd-MM-yyyy'), this.kycModel.idNumber, this.kycModel.postCode, this.kycModel.registrationNumber, this.kycModel.idProofType,
      this.kycModel.nationality, this.issuedCountrtyName, this.profAccModel.emailId, this.profAccModel.mobileNumber, this.profAccModel.countryCode,
      this.profAccModel.firstName, this.profAccModel.country, this.profAccModel.industry, this.profAccModel.location, this.profAccModel.organisationWebsite,
      this.profAccModel.lastName, this.profAccModel.organisation, 'web', this.orgData.cimOrganisationId, this.personId, DEV_ROOT_PATH, this.datePipe.transform(this.kycModel.expiryDate, 'dd-MM-yyyy'),
      this.datePipe.transform(this.kycModel.issuedDate, 'dd-MM-yyyy'));
    /*this.eventProperties.personInfo = this.profAccModel;
    this.eventProperties.orgInfo = this.kycModel;*/
    console.log(JSON.stringify(this.eventProperties));
    this.klaviyodata = new KlaviyoModal('Juv2YU', 'KYC Update', emailID, this.eventProperties)
    this.klaviyodata = (JSON.stringify(this.klaviyodata));
    this.klaviyodata = btoa(this.klaviyodata);
    console.log(this.klaviyodata);
    this.apiCall.klaviyoProfile(this.klaviyodata).then((result) => {
      console.log(result);
    }, (error) => {
      console.log('' + JSON.stringify(error));
    });

  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  selectCountry(event: any) {

    console.log(this.selectedObject);
    this.countryDetails.forEach((countryObj) => {
      if (countryObj.code === this.selectedObject) {
        this.placesRef.options.componentRestrictions = new ComponentRestrictions(
          {
            country: this.selectedObject
          }
        );
        this.placesRef.reset();
        this.profAccModel.country = countryObj.name;
        this.profAccModel.countryCode = countryObj.dial_code;
        console.log(this.profAccModel.country);
      }
    });
  }

  /* findMe() {
     if (navigator.geolocation) {
       navigator.geolocation.getCurrentPosition((position) => {
         console.log(position);
         this.apiCall.getPersonsLocation(position).then((res) => {
           this.countryResponse = res;
           console.log(this.countryResponse);
           this.profAccModel.country = this.countryResponse.long_name;
           for (let i = 0; i < this.countryDetails.length; i++) {
             if (this.countryDetails[i].name === this.profAccModel.country) {
               this.profAccModel.countryCode = this.countryDetails[i].dial_code;
           //    this.profAccModel.country = this.countryDetails[i].code;
               console.log(this.profAccModel.country);
             }
           }
           this.selectedObject = this.countryResponse.short_name;
           console.log(this.selectedObject);
           console.log(this.countryResponse.short_name);
           this.placesRef.options.componentRestrictions = new ComponentRestrictions(
             {
               country: this.countryResponse.short_name
             }
           );
           this.placesRef.reset();
         });
         console.log('asasas..' + this.profAccModel.country);
       });
     } else {
       alert('Geolocation is not supported by this browser.');
     }
     console.log(this.profAccModel.country);
   }*/
  cancelBtn() {
    this.router.navigate(['/app/dashboard']);
  }
}
