import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../services/services';
import { GetListModalRequest } from '../Modals/getListModal';
import * as moment from "moment";

@Component({
  selector: 'app-audit-logs',
  templateUrl: './audit-logs.component.html',
  styleUrls: ['./audit-logs.component.scss']
})
export class AuditLogsComponent implements OnInit {
  selectedCheckbox: any;
  pageSize: any;
  searchValue: any;
  placeholdervalue: any;
  showLoader: boolean;
  public auditList: any;
  auditResponse: any;
  currentPage: any;
  paginationRequired: boolean;
  sortingOrder: any;
  sortingField: any;
  totalCount: any;
  size: any;
  orgName: any;
  firstItem: number;
  lastItem: number;
  searchKey: any;
  selectedKey: any;
  searchResult: any = {};
  totalPages: any;
  loginResponse: any;
  organisationId: any;
  sortClickValue: any;
  sortFieldValue: any;
  userNameSort: boolean;
  clientIpSort: boolean;
  browserTypeSort: boolean;
  requestTimeSort: boolean;
  verbSort: boolean;
  mm: any;
  application_name: any;
  orgname: any;
  audit_fields: any;

  constructor(public apiCall: ApiCalls) {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.organisationId = this.loginResponse.orgName;
    this.searchResult = {};
    this.currentPage = 1;
    this.firstItem = 1;
    this.totalCount = 0;
    this.lastItem = 10;
    this.sortingField = 'request_time';
    this.sortingOrder = 'DESC';
    this.pageSize = 10;
    this.mm = moment;
    this.paginationRequired = true;
  }

  ngOnInit() {
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.orgname = this.loginResponse.orgName;
    console.log('Org Name' + this.organisationId)
    this.placeholdervalue = 'Search';
    this.searchValue = '';
    this.selectedCheckbox = false;
    // this.searchResult['orgname'] = this.organisationId;
    this.showLoader = false;
    this.paginationRequired = true;
    this.sortingField = 'request_time';
    this.sortingOrder = 'DESC';
    const application_name = 'CertTin';
    const http_method = 'POST';
    this.getAuditLogs(this.currentPage, { application_name, http_method, 'audit_fields.orgname': this.orgname }, this.pageSize);
  }

  getSearchResult() {
    this.currentPage = 1;
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    const application_name = 'CertTin';
    const http_method = 'POST';
    /* if (this.searchValue) {
      audit_fields.orgname
    this.searchResult[this.searchKey] = this.searchValue;
     this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
    } else {
      this.searchResult = {};
      this.searchResult['orgname'] = this.organisationId;
    this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
    } */

    if (this.searchKey && this.searchValue) {
      console.log('Search Value' + this.searchValue);

      if (this.searchKey == 'audit_fields.web.browser') {
        this.searchResult = { 'audit_fields.web.browser': this.searchValue }
        this.getAuditLogs(this.currentPage, { application_name, http_method, 'audit_fields.orgname': this.orgname,'audit_fields.web.browser': this.searchValue }, this.pageSize);
      } else if (this.searchKey == 'audit_fields.clientIp') {
        this.searchResult = { 'audit_fields.clientIp': this.searchValue }
        this.getAuditLogs(this.currentPage, { application_name, http_method, 'audit_fields.orgname': this.orgname,'audit_fields.clientIp': this.searchValue }, this.pageSize);
      } else if (this.searchKey == 'audit_fields.userName') {
        this.searchResult = { 'audit_fields.userName': this.searchValue }
        this.getAuditLogs(this.currentPage, { application_name, http_method, 'audit_fields.orgname': this.orgname,'audit_fields.userName': this.searchValue }, this.pageSize);
      } else if (this.searchKey == 'service_name') {
        this.searchResult = { 'service_name': this.searchValue }
        this.getAuditLogs(this.currentPage, { application_name, http_method, 'audit_fields.orgname': this.orgname,'service_name': this.searchValue }, this.pageSize);
        /* } else if (this.searchKey == 'requestTime') {
          this.searchResult = { 'requestTime': this.searchValue }
          this.getAuditLogs(this.currentPage, { 'requestTime': this.searchValue }, this.pageSize); */
      }
    }
    else {
      this.searchResult = { application_name, http_method, 'audit_fields.orgname': this.orgname }
      this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
    }
  }

  selectedSearch(selectedKey) {
    this.placeholdervalue = selectedKey;
    if (selectedKey === 'Browser Type') {
      this.searchKey = 'audit_fields.web.browser';
    } else if (selectedKey === 'Client Ip') {
      this.searchKey = 'audit_fields.clientIp';
    } else if (selectedKey === 'User Name') {
      this.searchKey = 'audit_fields.userName';
      /*   } else if (selectedKey === 'Request Time') {
          this.searchKey = 'requestTime'; */
    } else if (selectedKey === 'Action') {
      this.searchKey = 'service_name';
    }
    console.log(this.searchKey);
  }

  recordsPerPage() {
    this.currentPage = 1;
    console.log(this.pageSize);
    this.firstItem = 1;
    this.lastItem = +this.pageSize;
    console.log(this.pageSize);
    const application_name = 'CertTin';
    const http_method = 'POST';
    this.searchResult = { application_name, http_method , 'audit_fields.orgname': this.orgname }
    this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
  }

  sortingName(sortingType, sortingOrder) {
    this.sortingField = sortingType;
    if (this.sortingField === 'audit_fields.userName') {
      this.userNameSort = true;
      this.clientIpSort = false;
      this.browserTypeSort = false;
      this.requestTimeSort = false;
      this.verbSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'audit_fields.clientIp') {
      this.userNameSort = false;
      this.clientIpSort = true;
      this.browserTypeSort = false;
      this.requestTimeSort = false;
      this.verbSort = false;
      this.SortClicking();
    } else if (this.sortingField === 'audit_fields.web.browser') {
      this.userNameSort = false;
      this.clientIpSort = false;
      this.browserTypeSort = true;
      this.requestTimeSort = false;
      this.verbSort = false;
      this.SortClicking();
    }
    else if (this.sortingField === 'requestTime') {
      this.userNameSort = false;
      this.clientIpSort = false;
      this.browserTypeSort = false;
      this.requestTimeSort = true;
      this.verbSort = false;
      this.SortClicking();
    }
    else if (this.sortingField === 'service_name') {
      this.userNameSort = false;
      this.clientIpSort = false;
      this.browserTypeSort = false;
      this.requestTimeSort = false;
      this.verbSort = true;
      this.SortClicking();
    }
    else {
      this.userNameSort = false;
      this.clientIpSort = false;
      this.browserTypeSort = false;
      this.requestTimeSort = false;
      this.verbSort = false;
    }
  }


  SortClicking() {
    if (this.sortFieldValue === this.sortingField) {
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    } else {
      this.sortClickValue = 0;
      this.sortClickValue++;
      if (this.sortClickValue === 1) {
        this.sortingOrder = 'ASC';
      } else if (this.sortClickValue === 2) {
        this.sortingOrder = 'DESC';
      } else if (this.sortClickValue === 3) {
        this.sortingOrder = '';
        this.sortClickValue = 0;
      }
    }
    this.sortFieldValue = this.sortingField;
    this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
  }

  prevPage() {
    if (this.firstItem !== 1) {
      this.currentPage--;
      this.firstItem = this.firstItem - (+this.pageSize);
      if (this.firstItem < 0) {
        this.firstItem = 1;
      }
      this.lastItem = this.lastItem - (+this.pageSize);
      const application_name = 'CertTin';
      const http_method = 'POST';
      this.sortingField = 'null';
      this.sortingOrder = 'ASC';
      console.log(this.currentPage);
      console.log(this.pageSize);
      this.searchResult = { application_name, http_method , 'audit_fields.orgname': this.orgname }
      this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
    }
  }

  nextPage() {
    console.log(this.currentPage);
    console.log(this.pageSize);
      if (this.auditResponse.totalPages > this.currentPage){
      this.firstItem = this.firstItem + (+this.pageSize);
      this.lastItem = this.lastItem + (+this.pageSize);
      this.currentPage++;
      console.log(this.currentPage);
      console.log(this.pageSize);
      const application_name = 'CertTin';
      const http_method = 'POST';
      this.sortingField = 'null';
      this.sortingOrder = 'ASC';
      this.searchResult = { application_name, http_method , 'audit_fields.orgname': this.orgname}
      this.getAuditLogs(this.currentPage, this.searchResult, this.pageSize);
      }
  }

  getAuditLogs(currentPage, searchFields, pageSize) {
    this.showLoader = true;
    console.log('Page Size' + pageSize);
    console.log('Search Fields' + JSON.stringify(searchFields));
    console.log(currentPage);
    let auditPageRequest = new GetListModalRequest(currentPage, this.paginationRequired, searchFields, pageSize, this.sortingField, this.sortingOrder);
    console.log('Audit Requestt' + JSON.stringify(auditPageRequest));
    this.apiCall.doGetAuditLog(auditPageRequest).then((result) => {
      this.auditResponse = result;
      this.showLoader = false;
      console.log(this.auditResponse);
      this.auditList = this.auditResponse.listOfOrganisation;
      this.totalCount = this.auditResponse.totalElements;

    }, (error) => {
      this.showLoader = false;
      console.log('' + JSON.stringify(error));
    });
  }
}
