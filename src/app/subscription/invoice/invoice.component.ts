import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../services/services';
import {tick} from "@angular/core/testing";
import * as moment from 'moment';
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'app-invoice',
  templateUrl: './invoice.component.html',
  styleUrls: ['./invoice.component.scss']
})
export class InvoiceComponent implements OnInit {
  viewDetails: boolean;
  showLoader: any;
  organisationResponse: any;
  invoicesResponse: any;
  public invoicesList: any;
  public invoiceDetails: any;
  public address: any;
  public customerId: any;
  public mm: any;

  constructor(public apiCall: ApiCalls, public layoutService: LayoutService) {
    this.mm = moment;
  }

  ngOnInit() {
    this.viewDetails = false;
    this.showLoader = false;
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log('' + this.organisationResponse);
    if (this.organisationResponse) {
      this.customerId = this.organisationResponse.organisation.stripeCustomerId;
      console.log('Customer ID: ' + this.customerId);
      this.getAllInvoices(this.customerId);
    }
  }

  goToDetails(invoice: any) {
    this.viewDetails = true;
    console.log('' + JSON.stringify(invoice));
    this.invoiceDetails = invoice;
    // this.address = this.invoiceDetails.address.line1 + ', ' + this.invoiceDetails.address.line2 + ', ' + this.invoiceDetails.address.city + ', ' + this.invoiceDetails.address.country + ', ' + this.invoiceDetails.address.state + ', ' + this.invoiceDetails.address.postal_code + '.';
    if (this.invoiceDetails.address.line1) {
      this.address = this.invoiceDetails.address.line1;
    } else if (this.invoiceDetails.address.line2) {
      this.address = this.address + ', ' + this.invoiceDetails.address.line2;
    } else if (this.invoiceDetails.address.city) {
      this.address = this.address + ', ' + this.invoiceDetails.address.city;
    } else if (this.invoiceDetails.address.country) {
      this.address = this.address + ', ' + this.invoiceDetails.address.country;
    } else if (this.invoiceDetails.address.state) {
      this.address = this.address + ', ' + this.invoiceDetails.address.state;
    } else if (this.invoiceDetails.address.postal_code) {
      this.address = this.address + ', ' + this.invoiceDetails.address.postal_code + '.';
    }
  }

  goBack() {
    this.viewDetails = false;
  }

  getAllInvoices(Id) {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCall.doGetAllInvoices(Id).then((result) => {
      console.log('' + JSON.stringify(result));
      this.invoicesResponse = result;
      this.invoicesList = this.invoicesResponse.invoices;
      this.showLoader = false;
      this.layoutService.updatePreloaderState('hide');
    }, (error) => {
      console.log('' + JSON.stringify(error));
      this.showLoader = false;
    });
  }
}
