import { Component, OnInit } from '@angular/core';
import { ApiCalls } from '../services/services';
import { UpgradeSubscription } from '../Modals/upgradeSubscriptionModal';
import { Countriescodes } from '../providers/countriescodes';
import { SubscriptionAddress } from '../Modals/subscriptionAddressModal';
import { Router } from '@angular/router';
import { ShipAddressModal } from './../Modals/shipAddressModal';
import { BillAddressModal } from './../Modals/billAddressModal';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import { AuditModal } from '../Modals/auditModal';
import 'moment-timezone';
import { DatePipe } from '@angular/common';
import { LoginService } from '../extra-pages/login/loginService';
import { auditService } from '../providers/audit.service';
import { MySharedService } from "../services/mySharedService ";

@Component({
  selector: 'app-subscription',
  templateUrl: '../subscription/subscription.component.html',
  styleUrls: ['../subscription/subscription.component.css']
})

export class SubscriptionComponent implements OnInit {

  planId: any;
  selectPlan: boolean;
  planDetails: boolean;
  reviewOrder: boolean;
  paymentDetails: boolean;
  viewList: boolean;
  selectedPlan: any;
  selectedPrice: number;
  totalselectedPrice: any;
  planForm: boolean;
  planDetailForm: boolean;
  reviewForm: boolean;
  billingForm: boolean;
  shipingForm: boolean;
  planEdit: boolean;
  planDuration: any;
  loginResponse: any;
  organizationResponse: any;
  organisationId: any;
  customerId: any;
  subscriptionId: any;
  addTypeBill: any;
  companyNameBill: any;
  phoneNumberBill: any;
  shipAddressModel: any;
  billAddressModel: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  public countryDetails: any;
  public planName: any;
  public cityBill: any;
  public countryBill: any;
  public streetAddressBill: any;
  public selectStateBill: any;
  public postalCodeBill: any;
  addressResult: any;
  showLoader: boolean;
  organisationResponse: any;
  organisationRes: any;
  subscriptionForm: FormGroup;
  submitted: true;
  submitCheck: any;
  auditReq : any;
  constructor(public apiCall: ApiCalls,
    public countriesCodes: Countriescodes,
    public router: Router,
    public datePipe: DatePipe,
    public loginservice: LoginService,
    public auditHistory: auditService,
    public shareService: MySharedService,
    public formBuilder : FormBuilder) {
    this.shipAddressModel = new ShipAddressModal('', '', '', '', '', '', '', '');
    this.billAddressModel = new BillAddressModal('', '', '', '', '', '', '', '');
    this.auditModal = new AuditModal('','', '', '', '', '', '', '', '');

  }

  ngOnInit() {
    this.subscriptionForm = this.formBuilder.group({
      companyName: ['', Validators.required],
      phnNumber: ['', Validators.required],
      country: ['', Validators.required],
      strtAdrs: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      postCode: ['', Validators.required]
    })
    this.countryDetails = this.countriesCodes.getCountries();
    this.gotoList();
    this.planForm = true;
    this.planDetailForm = false;
    this.reviewForm = false;
    this.billingForm = false;
    this.shipingForm = false;
    this.selectPlan = true;
    this.viewList = false;
    this.reviewOrder = false;
    this.planEdit = false;
    this.planName = 'Trial';
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    if (this.loginResponse) {
      this.organisationId = this.loginResponse.orgId;
      //  this.getOrganization(this.loginResponse.orgId);
      this.getOrganisationDetails(this.organisationId);
    }
    this.showLoader = false;
  }

  gotoList() {
    this.viewList = true;
  }
  get f() {
    return this.subscriptionForm.controls;
  }
  addAddress() {

    const billAddress = new BillAddressModal(this.billAddressModel.addTypeBill, this.billAddressModel.companyNameBill,
      this.billAddressModel.phoneNumberBill, this.billAddressModel.countryBill, this.billAddressModel.streetAddressBill, this.billAddressModel.cityBill, this.billAddressModel.selectStateBill, this.billAddressModel.postalCodeBill);
    console.log(billAddress);
    localStorage.setItem('billAddress', JSON.stringify(billAddress));

    let shipAddress = new ShipAddressModal(this.shipAddressModel.addTypeShip, this.shipAddressModel.companyNameShip, this.shipAddressModel.phoneNumberShip,
      this.shipAddressModel.countryShip, this.shipAddressModel.streetAddressShip, this.shipAddressModel.cityShip, this.shipAddressModel.selectStateShip, this.shipAddressModel.postalCodeShip);
    console.log(shipAddress);
  }

  changeShipAdd() {
    this.shipAddressModel = new ShipAddressModal('', '', '', '', '', '', '', '');
    console.log(this.shipAddressModel);
    this.shipingForm = false;
    this.addressResult = JSON.parse(localStorage.getItem('billAddress'));
    console.log(this.addressResult);
    let shipAddress = this.addressResult;
    console.log(shipAddress);
  }

  goToPlanForm() {
    this.viewList = false;
    this.planForm = true;
    this.planDetailForm = false;
    this.reviewForm = false;
    this.billingForm = false;
  }

  goToPlanDetailForm() {
    this.planForm = false;
    this.planDetailForm = true;
    this.reviewForm = false;
    this.billingForm = false;
  }

  goToReviewForm() {
    this.planForm = false;
    this.planDetailForm = false;
    this.reviewForm = true;
    this.billingForm = false;
  }

  goToBillingForm() {
    this.reviewOrder = true;
    this.planForm = false;
    this.planDetailForm = false;
    this.reviewForm = false;
    this.billingForm = true;
  }

  goToPlanDetails(plandetail: any, price: any) {
    this.planForm = false;
    this.planDetailForm = true;
    this.selectedPlan = true;
    if (plandetail === 'PREMIUM') {
      this.selectedPlan = 'Premium';
      this.selectedPrice = 35;
      this.planDetails = true;

    } else if (plandetail === 'ENTERPRISE') {
      this.selectedPlan = 'Enterprise';
      this.selectedPrice = 100
      this.planDetails = true;

    } else {
      this.planDetails = false;
    }
    this.totalselectedPrice = '';
    this.planDuration = '';
  }

  goToReview(duration: any) {
    this.submitCheck = true;
    if (this.planDuration) {
      this.planEdit = true;
      this.planForm = false;
      this.planDetailForm = false;
      this.reviewForm = true;
      this.billingForm = false;
    }
  }

  goToShippingForm(form: NgForm) {
    this.shipAddressModel = new ShipAddressModal('', '', '', '', '', '', '', '');
    console.log(this.shipAddressModel);
    this.shipingForm = true;
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  createAuditLog() {
    this.auditHistory.userAudit('Subscribe');
  }

  subscription(event: any, amounts: any) {
    this.showLoader = true;
    this.submitted = true;
    if (this.subscriptionForm.valid) {
      this.createAuditLog();
      this.showLoader = true;
      const amountValue = amounts * 100;
      if (event === 'Premium') {
        this.planId = 'plan_DIfDCtQVbBqJhs';
      } else if (event === 'Enterprise') {
        this.planId = 'plan_DIfEfXCZZSvXQS';
      }
      let self = this;
      const handler = (<any>window).StripeCheckout.configure({
        key: 'pk_test_RCW99ITDG5g8QL3ehAuSiyOb',
        locale: 'auto',
        token: function (token: any) {
          // You can access the token ID with `token.id`.
          // Get the token ID to your server-side code for use.
          console.log('Token ' + JSON.stringify(token));

          if (token) {
            // alert(self.planId);
            this.auditReq = this.auditHistory.userAudit('Upgrade Subcription');
            const addressData = new SubscriptionAddress(self.billAddressModel.cityBill, self.billAddressModel.selectCountryBill, self.billAddressModel.companyNameBill, self.billAddressModel.streetAddressBill, self.billAddressModel.postCodeBill, self.billAddressModel.selectStateBill);
            const upgradeRequest = new UpgradeSubscription(this.auditReq, addressData, self.customerId, self.organisationId, self.planId, token.id, self.subscriptionId, event);
            self.apiCall.doUpgradeSubscription(upgradeRequest).then((result) => {
              console.log('' + JSON.stringify(result));
              self.router.navigate(['/app/mySubscriptions']);
              self.getOrganisationDetails(self.organisationId);
              self.showLoader = false;
              this.showLoader = false;
            }, (error) => {
              console.log('' + JSON.stringify(error));
              this.showLoader = false;
            });
          }
        }
      });

      handler.open({
        name: 'Cert Tin',
        amount: amountValue,
        currency: 'usd',
        image: '/assets/images/logo/stripe.png',
        email: this.loginResponse.username
      });
    }
  }
  selectSubscription(planDuration) {
    if (planDuration === 'Monthly') {
      this.totalselectedPrice = this.selectedPrice;
    } else {
      this.totalselectedPrice = this.selectedPrice * 12;
    }
  }

  /* getOrganization(orgId) {
   this.apiCall.doGetOrganization(orgId).then((result) => {
   this.organizationResponse = result;
   if (this.organizationResponse.organisation.customFields) {
   this.customerId = this.organizationResponse.organisation.customFields.customerId;
   this.subscriptionId = this.organizationResponse.organisation.customFields.subscriptionId;
   this.planName = this.organizationResponse.organisation.customFields.planName;
   }
   console.log('org details..' + JSON.stringify(this.organizationResponse));
   }, (error) => {
   console.log('org details error..' + JSON.stringify(error));
   });
   }*/

  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then((result) => {
      this.organisationResponse = result;
      localStorage.setItem('STRIPE_ORGDETAILS', JSON.stringify(this.organisationResponse));
      this.organisationRes = this.organisationResponse.organisation;
      if (this.organisationRes) {
        this.customerId = this.organisationRes.stripeCustomerId;
        this.subscriptionId = this.organisationRes.stripeSubscriptionId;
        this.planName = this.organisationRes.stripePlanName;
        console.log('' + this.customerId + '' + this.subscriptionId);
        this.shareService.setOrganisationDetails(this.organisationResponse);
      }
    }, (error) => {
      console.log('' + JSON.stringify(error));
    });
  }
}
