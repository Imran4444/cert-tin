import { NgModule } from '@angular/core';
import { SubscriptionComponent } from './subscription.component';
import { MaterialModule, MdNativeDateModule } from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MySubscriptionsComponent } from './my-subscriptions/my-subscriptions.component';
import { InvoiceComponent } from './invoice/invoice.component';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    MaterialModule,
    FormsModule,
    RouterModule,
    MdNativeDateModule,
    CommonModule,
    ReactiveFormsModule
  ],
  providers: [],
  declarations: [SubscriptionComponent, MySubscriptionsComponent, InvoiceComponent],
  entryComponents: []
})

export class SubscriptionModule {
  constructor() {
  }
}
