import {Component, OnInit} from '@angular/core';
import {ApiCalls} from '../../services/services';
import * as moment from 'moment';
import {DatePipe} from '@angular/common';
import {GET_STRIPE_URL} from '../../endpoint';
import { LayoutService } from '../../layout/layout.service';

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.scss']
})
export class MySubscriptionsComponent implements OnInit {
  viewDetails: boolean;
  showLoader: boolean;
  organisationResponse: any;
  subscriptionsResponse: any;
  subscriptionsList: any;
  customerId: any;
  subscriptionDetails: any;
  subscriptionDetailsResponse: any;
  public invoicesList: any;
  public customerEmail: any;
  public addressDetails: any;
  public upcomingEndDate: any;
  public mm: any;

  constructor(public apiCall: ApiCalls,
              public datePipe: DatePipe, public layoutService: LayoutService) {
    this.mm = moment;
  }

  ngOnInit() {
    this.viewDetails = false;
    this.showLoader = false;
    this.organisationResponse = localStorage.getItem('STRIPE_ORGDETAILS');
    if (this.organisationResponse) {
      this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
      console.log('' + this.organisationResponse);
      if (this.organisationResponse) {
        this.customerId = this.organisationResponse.organisation.stripeCustomerId;
        console.log('Customer ID: ' + this.customerId);
        this.getAllSubscriptions(this.customerId);
      }
    }
  }

  goToDetails(subscription, subscriptionId) {
    this.viewDetails = true;
    this.subscriptionDetails = subscription;
    console.log('' + this.subscriptionDetails);
    this.getSubscriptionsDetails(this.customerId, subscriptionId);
  }

  goBack() {
    this.viewDetails = false;
  }

  getAllSubscriptions(Id) {
    this.layoutService.updatePreloaderState('active');
    this.showLoader = true;
    this.apiCall.doGetAllSubscriptions(Id).then((result) => {
      console.log('' + JSON.stringify(result));
      this.subscriptionsResponse = result;
      this.customerEmail = this.subscriptionsResponse.customerEmail;
      this.subscriptionsList = this.subscriptionsResponse.subscriptions;
      this.showLoader = false;
      this.layoutService.updatePreloaderState('hide');
    }, (error) => {
      console.log('' + JSON.stringify(error));
      this.showLoader = false;
    });
  }

  getSubscriptionsDetails(customerId, subscriptionId) {
    this.apiCall.doGetSubscriptionById(customerId, subscriptionId).then((result) => {
      console.log('' + JSON.stringify(JSON.stringify(result)));
      this.subscriptionDetailsResponse = result;
      this.invoicesList = this.subscriptionDetailsResponse.invoices;
      this.addressDetails = this.invoicesList[0].address;

      this.upcomingEndDate = this.subscriptionDetailsResponse.upcomingInvoice.period_end;
      const dateUpComing = this.datePipe.transform(this.upcomingEndDate, 'yyyy-MM-dd');
      const changeMonth = moment(dateUpComing).add(1, 'month').toString();
      this.upcomingEndDate = this.datePipe.transform(changeMonth, 'yyyy-MM-dd');
      // this.loginExpDate = moment(this.loginExpDate).utc().format();
    }, (error) => {
      console.log('' + JSON.stringify(error));
    });
  }
}
