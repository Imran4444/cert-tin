import { Component } from '@angular/core';
import {LOADING_GIF} from '../providers/constants';
@Component({
  selector: 'loader-component',
  styles: [],
  templateUrl: './loader.component.html',
})
export class LoaderComponent {
  loadingGif: any;
  keyProgress: any;
  typeProgress: any;
  constructor() {
    this.loadingGif = LOADING_GIF;
    this.keyProgress = 10;
    this.typeProgress = 'indeterminate';
  }
}
