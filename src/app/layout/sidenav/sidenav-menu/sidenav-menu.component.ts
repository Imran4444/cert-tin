import {Component, Input, OnInit} from '@angular/core';
import {LeftNavigationItemList} from '../../../providers/leftNavigationItemList';
import {Router} from '@angular/router';
import {MySharedService} from '../../../services/mySharedService ';
import {ApiCalls} from "../../../services/services";
import {ValidationProvider} from "../../../providers/validationProvider";
import {GlobalPopupComponent} from "../../../globalPopup/globalPopup.component";
import {MdDialog} from "@angular/material";

@Component({
  selector: 'my-app-sidenav-menu',
  styleUrls: ['../../../../assets/css/styles.css'],
  templateUrl: './sidenav-menu.component.html'
})

export class AppSidenavMenuComponent implements OnInit {
  @Input() menuCollapse: any;
  @Input() tCollapse: any;
  private orgdetails: any;
  private itemList: any;
  private loggedAs: any;
  private transactionUrl: string;
  public status: any;
  loginResponse: any;
  organisationId: any;
  claimCountResponse: any;
  claimCountObject: any;
  certificateCountObject: any;
  inboxCount: any;
  outboxCount: any;
  count: any;
  claimCount: any;
  certCount: any;
  sharedCount: any;
  outcount: any;
  outclaimCount: any;
  outcertCount: any;
  public organisationResponse: any;
  public inboxCountData: any;
  public sentBoxCountData: any;
public certBoxCountData: any;
  roleType: any;
  constructor(public navItemList: LeftNavigationItemList,
              public route: Router,
              public apiCalls: ApiCalls,
              public shareService: MySharedService,
              public validations: ValidationProvider,
              public dialog: MdDialog) {
    this.claimCountObject = {};
    this.certificateCountObject = {};
    this.shareService.orgInfo.subscribe((res) => {
      this.organisationResponse = res;
    });
  }

  ngOnInit() {
    /*    this.itemList = this.navItemList.getListOfnavigationItems(this.orgdetails.authorities);*/
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.roleType = this.loginResponse.roles[0];
    this.organisationId = this.loginResponse.orgId;
    this.getInboxCount('INBOX');
    this.getOutboxCount('OUTBOX');
    this.getCertsCount();
  }

  logout() {
    localStorage.removeItem('organizer');
    this.route.navigate(['/extra/login']);

  }

  selectCliamBox(key: any) {
    localStorage.setItem('INOUTBOX', key);
    // this.shareService.setInboxOutbox(key);
    if (key === 'INBOX') {
      this.status = 'OPEN';
    } else if (key === 'OUTBOX') {
      this.status = 'CLOSED';
    }
    if (this.status) {
      this.apiCalls.doGetClaimTypeCount(this.organisationId, this.status).then((result) => {
        this.claimCountResponse = result;
        this.claimCountObject = this.claimCountResponse.claimsTypeCount;
        this.certificateCountObject = this.claimCountResponse.certTypeCount;
      }, (error) => {
        console.log('' + JSON.stringify(error));
      });
    }
  }

  getInboxCount(key) {
    /*   this.route.navigate(['app/all-claims']);*/
    localStorage.setItem('INOUTBOX', key);
    /*
     this.apiCalls.doGetClaimTypeCount(this.organisationId, 'OPEN').then((result) => {
     this.count = result;
     //   console.log('Inbox' + JSON.stringify( this.count));
     this.claimCount = this.count.claimsTypeCount;
     this.certCount = this.count.certTypeCount;
     this.sharedCount = this.count.certTypeCount.shared;
     }, (error) => {
     console.log('' + JSON.stringify(error));
     });
     */

    this.apiCalls.doInboxTabCount(this.organisationId, '/inboxCount').then((result) => {
      this.inboxCountData = result;
    }, (error) => {
      console.log('in box count error..' + JSON.stringify(error));
    });

  }

  getOutboxCount(key) {
    localStorage.setItem('INOUTBOX', key);
    /*
     this.apiCalls.doGetClaimTypeCount(this.organisationId, 'CLOSED').then((result) => {
     // console.log('Outbox' + JSON.stringify(result));
     this.outcount = result;
     //  console.log('Outbox' + JSON.stringify( this.outcount));
     this.outclaimCount = this.outcount.claimsTypeCount;
     this.outcertCount = this.outcount.certTypeCount;
     }, (error) => {
     console.log('' + JSON.stringify(error));
     });
     */
    this.apiCalls.doInboxTabCount(this.organisationId, '/sentCount').then((result) => {
      this.sentBoxCountData = result;
    }, (error) => {
      console.log('sentcount error..' + JSON.stringify(error));
    });

  }

  getCertsCount(){
    this.apiCalls.doInboxTabCount(this.organisationId, '/certificateCount').then((result) => {
      this.certBoxCountData = result;
    }, (error) => {
      console.log('certs error..' + JSON.stringify(error));
    });

  }

  certificateMenuClicking() {
    if (this.organisationResponse.organisation.accountLevel === 1) {
      const accountVerify = this.validations.errorPopup('Account Level', 'Your account has to be professional  to unlock this feature. To have a Professional account please complete your KYC by updating your personal and organisation details in My account.', '');
      const dialogRef = this.dialog.open(GlobalPopupComponent, {
        width: '450px',
        data: accountVerify,
        disableClose: true
      });
    } else {
      this.route.navigate(['/app/certs-issued']);
    /*  if (key === 'INBOX') {
        this.route.navigate(['/app/all-certificates']);
      } else {
        this.route.navigate(['/app/all-outbox-certificates']);
      }*/
    }
  }
}
