import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import { APPCONFIG } from '../../config';

@Component({
  selector: 'my-app-sidenav',
  styles: [],
  templateUrl: './sidenav.component.html'
})

export class AppSidenavComponent implements OnInit {
  @Input() valueToggle: any;
  AppConfig;
  toggleCollapse: boolean;

  ngOnInit() {
    this.AppConfig = APPCONFIG;
    this.toggleCollapse = false;
    this.valueToggle=0;
  }
  toggleCollapsedNav() {
    if(this.valueToggle==0){
      this.valueToggle=1;
    }else if(this.valueToggle==1){
      this.valueToggle=0;
      this.toggleCollapse=false;
    }
    this.AppConfig.navCollapsed = !this.AppConfig.navCollapsed;
  }

 

  hoverIn() {
    this.toggleCollapse = false;
  }

  hoverOut() {
   if(this.valueToggle==1){
   this.toggleCollapse=true;
   }
    
  }
}


