import { NgModule } from '@angular/core';
import { LayoutRoutingModule } from './layout-routing.module';
import { LeftNavigationItemList } from '../providers/leftNavigationItemList';
import { AccountInfoModule } from '../accountInformation/accountInfo.module';
import { InboxModule } from '../Inbox/inbox.module';
import { CvsDocsModule } from '../CvsDocs/Cvs.Docs.module';
import { SubscriptionModule } from '../subscription/subscription.module';
import { MysubscriptionModule } from '../mysubscription/mysubscription.module';
import { LayoutService } from './layout.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EditorModule } from '../editor/editor.module';
import { ViewJobComponent } from '../job-advertisement/view-job/view-job.component';

@NgModule({
  imports: [
    LayoutRoutingModule,
    AccountInfoModule,
    InboxModule,
    EditorModule,
    CvsDocsModule,
    SubscriptionModule,
    MysubscriptionModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [],
  providers: [LeftNavigationItemList, LayoutService],
  entryComponents: [ViewJobComponent]
})

export class LayoutModule {
}
