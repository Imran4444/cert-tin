import { Component, Input, OnInit, ViewChild, ElementRef } from '@angular/core';
import { APPCONFIG } from '../../config';
import { Router } from '@angular/router';
import { MdDialog } from '@angular/material';
import { ChangePasswordComponent } from '../../globalPopup/changePassword/changePassword.component';
import { ApiCalls } from '../../services/services';
import { MySharedService } from '../../services/mySharedService ';
import { AuditModal } from '../../Modals/auditModal';
import { LoginService } from '../../../app/extra-pages/login/loginService';
import 'moment-timezone';
import { DatePipe } from "@angular/common";
import { auditService } from '../../providers/audit.service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'my-app-header',
  styleUrls: ['./header.component.scss'],
  templateUrl: './header.component.html'
})

export class AppHeaderComponent implements OnInit {
  @Input('profileImg') profileData: string;

  public AppConfig: any;
  public selectedTab: string;
  private loginDetails: any;
  private loggedAs: any;
  public loginResponse: any;
  public personDetails: any;
  public auditModal: AuditModal;
  getBrowser: any;
  localeIP: any;
  localIP: any;
  browser: any;
  timeZone: any;
  verb: any;
  sourseIP: any;
  appName: any;
  location: any;
  requestTime: any;
  auditLog: any;
  loginResult: any;
  organisationResponse: any;
  organisationRes: any;
  accountLevel: any;
  accountType: any;

  constructor(public route: Router,
              public dialog: MdDialog,
              public apiCalls: ApiCalls,
              public datePipe: DatePipe,
              public shareService: MySharedService,
              public loginservice: LoginService,
              public auditHistory: auditService,
              public cookieService: CookieService) {
    //  this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
    this.loginResponse = JSON.parse(localStorage.getItem('personResponse'));
    // this.auditModal = new AuditModal('', '', '', '', '', '', '', '', '', '', '');
    this.shareService.dataChange.subscribe((res) => {
      this.loginResponse = res;
    });
    /*  this.accountType = 'Trial';
      this.accountLevel = 1;*/
    this.shareService.accountHeader.subscribe((res) => {
      this.organisationResponse = res;
      this.organisationRes = this.organisationResponse.organisation;
      if (this.organisationRes) {
        this.accountType = this.organisationRes.stripePlanName;
        this.accountLevel = this.organisationRes.accountLevel;
      }
    });
  }

  ngOnInit() {
    this.AppConfig = APPCONFIG;
    // this.getOrganisationDetails(this.loginResponse.orgId);
    /*  this.personDetails = JSON.parse(localStorage.getItem('personResponse'));
     this.getPersonDetails(this.loginResponse.personId);*/
    // console.log('person details heaer..' + JSON.stringify(this.personDetails));
    this.organisationResponse = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));


    if (this.organisationRes) {
      this.organisationRes = this.organisationResponse.organisation;
      this.accountType = this.organisationRes.stripePlanName;
      this.accountLevel = this.organisationRes.accountLevel;
    }
  }

  changePassword() {
    const dialogRef = this.dialog.open(ChangePasswordComponent, {
      width: '400px',
      data: 'Work'
    });
  }

  createAuditLog() {
    this.auditHistory.userAudit('Logout');
  }

  logout() {
    this.createAuditLog();
    this.cookieService.delete('user');
    localStorage.removeItem('linkedInUser');
    localStorage.removeItem('organizer');
    localStorage.removeItem('loginResponse');
    this.route.navigate(['/extra/accountLogin']);
  }

  selecttab(tab) {
    this.selectedTab = tab;
  }

  close() {
    this.selectedTab = '';
  }

  myAcc() {

    this.route.navigate(['/app/account']);
  }

  mySubs() {
    this.route.navigate(['/app/subscription']);
  }

  /*
   getPersonDetails(personId) {
   this.apiCalls.doGetPerson(personId).then((result) => {
   this.personDetails = result;
   console.log('get person details..' + JSON.stringify(this.personDetails))
   localStorage.setItem('personResponse', JSON.stringify(this.personDetails));
   }, (error) => {
   console.log('get person error..' + JSON.stringify(error));
   });
   }
   */
}
