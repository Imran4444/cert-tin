import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { UpgradeAccountComponent } from '../globalPopup/upgradeAccount/upgradeAccount.component';
import {ApiCalls} from "../services/services";
import {Router} from "@angular/router";
import {MySharedService} from "../services/mySharedService ";


@Component({
  selector: 'my-app-layout',
  templateUrl: './layout.component.html',
})
export class LayoutComponent implements OnInit{
  public imgUrl: string = 'http://d2btkkuyusotz5.cloudfront.net/soulstice.co.za/Open-Platform-01b.png'
  public orgDetails: any;
  showInprogress: any;
  upgradeAccountStatus: any;
  orgId: any;
  progressResponse: any;
  loginResponse: any;
  constructor(public dialog: MdDialog,
              public apiCall: ApiCalls,
              public router: Router,
              public shareService: MySharedService) {
  }
ngOnInit() {
 /*   this.orgDetails = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    console.log(this.orgDetails);
    if (this.orgDetails.organisation) {
      this.orgId = this.orgDetails.organisation.cimOrganisationId;
    } else {
      this.orgId = this.orgDetails.cimOrganisationId;
    }*/
   this.loginResponse = JSON.parse(localStorage.getItem('loginResponse'));
   this.getOrganisationDetails(this.loginResponse.orgId);
}
  getOrganisationDetails(orgId) {
    this.apiCall.doGetOrganisationDetails(orgId).then((result) => {
      this.orgDetails = result;
      localStorage.setItem('STRIPE_ORGDETAILS', JSON.stringify(this.orgDetails));
      this.shareService.setOrganisationDetails(this.orgDetails);
      this.orgId = this.orgDetails.organisation.cimOrganisationId;
      // if (this.organisationResponse) {
      //   this.shareService.setOrganisationDetails(this.organisationResponse);
      // }
    }, (error) => {
      console.log('' + JSON.stringify(error));
    });
  }
  getUpgrade() {
    this.getOrgProgress();
  }
  getOrgProgress() {
    if (this.orgId) {
      this.apiCall.doGetOrgProcess(this.orgId).then(
        (result) => {
          this.progressResponse = result;
          this.upgradeAccountStatus = this.progressResponse.organisation.customFields.upgradeAcountStatus;
          console.log(' Progress Response' + JSON.stringify(this.progressResponse));
          if (this.upgradeAccountStatus === 'inProgress' && !this.progressResponse.organisation.customFields.kycUpgradeEmail) {
            this.router.navigate(['/app/professional-account']);
          } else if (this.upgradeAccountStatus === 'submitted') {
            this.showInprogress = true;
            this.openPopup(this.showInprogress);
          } else if ( (this.upgradeAccountStatus === 'inProgress' && this.progressResponse.organisation.customFields.kycUpgradeEmail)) {
            this.showInprogress = true;
            this.openPopup(this.showInprogress);
          } else {
            this.showInprogress = null;
            this.openPopup(this.showInprogress);
          }
        },
        (error) => {
          console.log('' + JSON.stringify(error));
        }
      );
    }
  }
  openPopup(progress) {
    console.log(progress);
    const dialogRef = this.dialog.open(UpgradeAccountComponent, {
      width: '600px',
      data: {showInprogress: progress, email : true}
    });
  }
}
