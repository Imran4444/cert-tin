import {RouterModule, Routes} from '@angular/router';
import {LayoutComponent} from './layout.component';
import {DashboardComponent} from '../dashboard/dashboard.component';
import {AccountInfoComponent} from '../accountInformation/accountInfo.component';
import {WorkclaimComponent} from '../Inbox/work/workclaim.component';
import {QualificationComponent} from '../Inbox/qualification/qualification.component';
import {CvsDocsComponent} from '../CvsDocs/CvsDocs.component';
import {SubscriptionComponent} from '../subscription/subscription.component';
import {MySubscriptionsComponent} from '../subscription/my-subscriptions/my-subscriptions.component';
import {InvoiceComponent} from '../subscription/invoice/invoice.component';
import {MysubscriptionComponent} from '../mysubscription/mysubscription.component';
import {AdminSettingsComponent} from '../admin-settings/admin-settings.component';
import {AddUserComponent} from '../usermanagement/add-user/add-user.component';
import {UserListComponent} from '../usermanagement/user-list/user-list.component';
import {AuditLogsComponent} from '../audit-logs/audit-logs.component';
import {TransactionsComponent} from '../transactions/transactions.component';
import {IndividualComponent} from '../individual/individual.component';
import {ClaimsComponent} from '../Inbox/claims/claims.component';
import {PersonalComponent} from '../Inbox/claims/personal/personal.component';
import {WorkClaimComponent} from '../Inbox/claims/work-claim/work-claim.component';
import {SkillClaimComponent} from '../Inbox/claims/skill-claim/skill-claim.component';
import {CourseClaimComponent} from '../Inbox/claims/course-claim/course-claim.component';
import {ClaimDetailsComponent} from '../individual/claimdetails/claimdetails.component';
import {ViewComponent} from '../Inbox/claims/view/view.component';
import {AllClaimComponent} from '../Inbox/claims/all-claim/allclaim.component';
import {AddressDetailsComponent} from "../myAddress/myAddress.component";
import {CertviewComponent} from '../Inbox/claims/certView/certview.component';
import {WorkAttestationsComponent} from '../Inbox/work-attestations/work-attestations.component';
import {TemplatesComponent} from '../templates/templates.component';
import {AllCertificatesComponent} from "../Inbox/claims/all-certificates/allcertificates.component";
import {CertificateOutboxViewComponent} from "../Inbox/claims/certificate-outbox-view/certificate-outbox-view.component";
import {CertInboxViewComponent} from '../Inbox/cert-inbox-view/cert-inbox-view.component';
import {VerifyProcessComponent} from '../Inbox/verifyprocess/verifyProcess.component';
import {IssueEndrosedComponent} from '../Inbox/IssueEndrosedProcess/issueEndrosed.component';
import {VerificationsComponent} from '../Inbox/verifications/verifications.component';
import {VerifiedComponent} from '../Inbox/verified/verified.component';
import {VerifyCertificateComponent} from '../Inbox/verify-certificate-view/verifycert.component';
import {ChangePasswordComponent} from '../changePassword/changePassword.component';
import {ProfessionalAccountComponent} from "../professional-account/professional-account.component";
import { EditorComponent } from '../editor/editor.component';
import {CertsIssuedComponent} from "../certs/issued/issued.component";
import {ViewIssuedCertsComponent} from "../certs/issued/viewIssuedCerts/view-issued.component";
import {TransferCertsComponent} from "../certs/transfer/transfer.component";
import {IssueNewCertComponent} from "../certs/issued/issue-new-certificate/issue-new.component";
import {SearchRecipientComponent} from "../certs/issued/search-recipient/searchRecipient.component";
import { JobAdvertisementComponent } from '../job-advertisement/job-advertisement.component';
import { PostJobComponent } from '../job-advertisement/post-job/post-job.component';

const routes: Routes = [
  {
    path: 'app',
    component: LayoutComponent,
    children: [
      {path: '', redirectTo: '/extra/accountLogin', pathMatch: 'full'},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'account', component: AccountInfoComponent},
      {path: 'work-attestations', component: WorkAttestationsComponent},
      {path: 'outbox-work-attestations', component: WorkAttestationsComponent},
      {path: 'qualification', component: QualificationComponent},
      {path: 'outbox-qualification', component: QualificationComponent},
      {path: 'cvdocs', component: CvsDocsComponent},
      {path: 'subscription', component: SubscriptionComponent},
      {path: 'mySubscriptions', component: MySubscriptionsComponent},
      {path: 'invoice', component: InvoiceComponent},
      {path: 'mysubscriptions', component: MysubscriptionComponent},
      {path: 'adminSettings', component: AdminSettingsComponent},
      {path: 'add-user', component: AddUserComponent},
      {path: 'user-list', component: UserListComponent},
      {path: 'audit-logs', component: AuditLogsComponent},
      {path: 'transactions', component: TransactionsComponent},
      {path: 'individual', component: IndividualComponent},
      {path: 'claims', component: ClaimsComponent},
      {path: 'personal-claims', component: PersonalComponent},
      {path: 'personal-outbox-claims', component: PersonalComponent},
      {path: 'work-claims', component: WorkClaimComponent},
      {path: 'work-outbox-claims', component: WorkClaimComponent},
      {path: 'skill-claims', component: SkillClaimComponent},
      {path: 'skill-outbox-claims', component: SkillClaimComponent},
      {path: 'course-claims', component: CourseClaimComponent},
      {path: 'course-outbox-claims', component: CourseClaimComponent},
      {path: 'all-claims', component: AllClaimComponent},
      {path: 'all-certificates', component: AllCertificatesComponent},
      {path: 'all-outbox-claims', component: AllClaimComponent},
      {path: 'all-outbox-certificates', component: AllCertificatesComponent},
      {path: 'individualClaimDetails', component: ClaimDetailsComponent},
      {path: 'view', component: ViewComponent},
      {path: 'cert-inbox-view', component: CertInboxViewComponent},
      {path: 'claim-certificate', component: CertviewComponent},
      {path: 'certificate', component: CertificateOutboxViewComponent},
      {path: 'address', component: AddressDetailsComponent},
      {path: 'templates', component: TemplatesComponent},
      {path: 'verify', component: VerifyProcessComponent},
      {path: 'issue-endrose', component: IssueEndrosedComponent},
      {path: 'verifications', component: VerificationsComponent},
      {path: 'verified', component: VerifiedComponent},
      {path: 'verifyCertificate', component: VerifyCertificateComponent},
      {path: 'change-password', component: ChangePasswordComponent},
      {path : 'professional-account', component: ProfessionalAccountComponent},
      {path: 'editor', component: EditorComponent},
      {path: 'certs-issued', component : CertsIssuedComponent},
      {path: 'view-issuedCerts', component: ViewIssuedCertsComponent},
      {path: 'certs-transfer', component: TransferCertsComponent},
      {path : 'issue-newCertificate', component: IssueNewCertComponent},
      {path: 'search-recipient', component: SearchRecipientComponent},
      {path: 'job-advertisement', component: JobAdvertisementComponent},
      {path: 'post-job', component: PostJobComponent}
    ]
  }
];

export const LayoutRoutingModule = RouterModule.forChild(routes);
