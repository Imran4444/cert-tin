import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiCalls } from "../services/services";

declare var google: any;

@Component({
  selector: 'app-professional-account',
  templateUrl: './professional-account.component.html',
  styleUrls: ['./professional-account.component.scss']
})
export class ProfessionalAccountComponent implements OnInit {
  public personDetails: any;
  public orgDetails: any;

  org: any;
  person: any;
  loginResponseStorage: any;
  constructor(public router: Router, public apiCalls: ApiCalls) {
  }

  ngOnInit() {
    this.loginResponseStorage = JSON.parse(localStorage.getItem('loginResponse'));
    this.person = JSON.parse(localStorage.getItem('personResponse'));
    this.personDetails = this.person.person;
    this.org = JSON.parse(localStorage.getItem('STRIPE_ORGDETAILS'));
    this.orgDetails = this.org.organisation;
  }
}
